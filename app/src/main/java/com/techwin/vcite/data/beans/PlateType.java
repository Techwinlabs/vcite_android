package com.techwin.vcite.data.beans;

import com.google.gson.annotations.SerializedName;

public class PlateType {

    @SerializedName("pltType")
    private String pltType;
    private String abbreviation;
    public boolean checked;

    public String getPltType() {
        return pltType;
    }

    public void setPltType(String pltType) {
        this.pltType = pltType;
        this.abbreviation=pltType;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        if(abbreviation!=null)
        this.abbreviation = abbreviation;
    }


}
