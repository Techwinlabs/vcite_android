package com.techwin.vcite.data.repo;

import com.techwin.vcite.data.beans.CampusBean;
import com.techwin.vcite.data.beans.PlateType;
import com.techwin.vcite.data.beans.SearchBean;
import com.techwin.vcite.data.beans.TemplateBean;
import com.techwin.vcite.data.beans.UserBean;
import com.techwin.vcite.data.beans.UserPermission;
import com.techwin.vcite.data.beans.VehType;
import com.techwin.vcite.data.beans.VehicleColorType;
import com.techwin.vcite.data.beans.VelTypeLnn;
import com.techwin.vcite.data.beans.ViolationType;
import com.techwin.vcite.data.beans.base.ApiResponse;
import com.techwin.vcite.data.beans.base.UploadBean;
import com.techwin.vcite.data.local.entity.CitationBean;
import com.techwin.vcite.data.remote.helper.Resource;

import java.util.List;
import java.util.Map;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;

public interface BaseRepo {
    Single<ApiResponse<SearchBean>> getSearch(String url, Map<String, String> map);

    Single<ApiResponse<String>> password(Map<String, String> map);

    Flowable<Resource<UploadBean>> uploadFile(String filepath);

    Flowable<Resource<UploadBean>> uploadFile(byte[] filepath);

    Single<ApiResponse<List<PlateType>>> getPlateType();

    Single<ApiResponse<VelTypeLnn>> getPlateForLynn();

    Single<ApiResponse<VelTypeLnn>> vehMake();

    Single<ApiResponse<VehicleColorType>> getVehicleColor();

    Single<ApiResponse<ViolationType>> getViolation(Map<String, String> map);

    Single<ApiResponse<VehType>> vehType();

    Observable<ApiResponse<CampusBean>> campusType(Map<String,String> map);

    Single<ApiResponse<String>> addCitation(String map, CitationBean citationBean);

    Single<ApiResponse<String>> editCitation(String map, CitationBean citationBean);

    Observable<ApiResponse<TemplateBean>> citationTemplate(Map<String, String> map, String type);

    Single<ApiResponse<List<UserPermission>>> getUserPermission(Map<String, String> map);

    Single<ApiResponse<UserBean>> login(Map<String, String> map);
}
