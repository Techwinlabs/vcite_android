package com.techwin.vcite.data.local;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.techwin.vcite.data.beans.CampusBean;
import com.techwin.vcite.data.beans.CitTempBean;
import com.techwin.vcite.data.beans.DefaultBean;
import com.techwin.vcite.data.beans.PlateType;
import com.techwin.vcite.data.beans.UserBean;
import com.techwin.vcite.data.beans.UserPermission;
import com.techwin.vcite.data.beans.VehType;
import com.techwin.vcite.data.beans.VehicleColorType;
import com.techwin.vcite.data.beans.VelTypeLnn;
import com.techwin.vcite.util.Constants;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

public class SharedPrefImpl implements SharedPref {
    private SharedPreferences sharedPreferences;

    public SharedPrefImpl(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

/*
    @Override
    public boolean putUserBean(@NonNull UserBean userBean) {
        return sharedPreferences.edit().putString("user", new Gson().toJson(userBean)).commit();
    }

    @Nullable
    @Override
    public UserBean getUserBean() {
        try {
            String s = sharedPreferences.getString("user", null);
            return new Gson().fromJson(s, UserBean.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }*/

    @Override
    public CampusBean getCampus() {
        try {
            String s = sharedPreferences.getString("CampusBean", null);
            return new Gson().fromJson(s, CampusBean.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean setCampus(CampusBean userBean) {
        return sharedPreferences.edit().putString("CampusBean", new Gson().toJson(userBean)).commit();
    }

    @Override
    public List<CitTempBean> getTemplate(String type) {
        try {
            String tag = type.equalsIgnoreCase(Constants.CITATION_TYPE_PARKING) ? "parking" : "enfor";
            String s = sharedPreferences.getString(tag, null);
            Type typeToken = new TypeToken<List<CitTempBean>>() {
            }.getType();
            return new Gson().fromJson(s, typeToken);
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @Override
    public List<CitTempBean> getTemplate() {
        List<CitTempBean> tempBeans = new ArrayList<>();
        try {
            String s = sharedPreferences.getString("parking", null);
            Type typeToken = new TypeToken<List<CitTempBean>>() {
            }.getType();
            List<CitTempBean> beans = new Gson().fromJson(s, typeToken);
            if (beans != null) {
                tempBeans.addAll(beans);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            String s = sharedPreferences.getString("enfor", null);
            Type typeToken = new TypeToken<List<CitTempBean>>() {
            }.getType();
            List<CitTempBean> beans = new Gson().fromJson(s, typeToken);
            if (beans != null) {
                tempBeans.addAll(beans);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tempBeans;
    }

    @Override
    public boolean setTemplate(List<CitTempBean> userBean, String type) {
        String tag = type.equalsIgnoreCase(Constants.CITATION_TYPE_PARKING) ? "parking" : "enfor";
        return sharedPreferences.edit().putString(tag, new Gson().toJson(userBean)).commit();
    }

    @Nullable
    @Override
    public List<UserPermission> getUserPermission() {
        try {
            String s = sharedPreferences.getString("perms", null);
            Type typeToken = new TypeToken<List<UserPermission>>() {
            }.getType();
            return new Gson().fromJson(s, typeToken);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean setUserPermission(List<UserPermission> userBean) {
        return sharedPreferences.edit().putString("perms", new Gson().toJson(userBean)).commit();

    }

    @Override
    public List<VehType.VehtTypDTO> getVehType() {
        try {
            String s = sharedPreferences.getString("v_type", null);
            if (s == null)
                return new ArrayList<>();
            Type typeToken = new TypeToken<List<VehType.VehtTypDTO>>() {
            }.getType();
            return new Gson().fromJson(s, typeToken);
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @Override
    public boolean setVehType(List<VehType.VehtTypDTO> userBean) {
        return sharedPreferences.edit().putString("v_type", new Gson().toJson(userBean)).commit();
    }

    @Override
    public UserBean getUser() {
        try {
            String s = sharedPreferences.getString("user", null);
            return new Gson().fromJson(s, UserBean.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void removeUser() {
        sharedPreferences.edit().remove("databases").remove("template").remove("user").apply();
    }

    @Override
    public boolean putUser(UserBean userBean) {
        return sharedPreferences.edit().putString("user", new Gson().toJson(userBean)).commit();
    }

    @Override
    public DefaultBean getDefault() {
        try {
            String s = sharedPreferences.getString("databases", null);
            if (s == null)
                return new DefaultBean();
            return new Gson().fromJson(s, DefaultBean.class);
        } catch (Exception e) {
            e.printStackTrace();
            return new DefaultBean();
        }
    }

    @Override
    public boolean putDefault(DefaultBean defaultBean) {
        if (defaultBean == null)
            return sharedPreferences.edit().remove("databases").commit();
        return sharedPreferences.edit().putString("databases", new Gson().toJson(defaultBean)).commit();
    }

    @Override
    public boolean putPlateTypeList(List<PlateType> plateTypes) {
        return sharedPreferences.edit().putString("plateTypes", new Gson().toJson(plateTypes)).commit();

    }

    @Nullable
    @Override
    public List<PlateType> getPlateTypeList() {
        try {
            String s = sharedPreferences.getString("plateTypes", null);
            Type typeToken = new TypeToken<List<PlateType>>() {
            }.getType();
            return new Gson().fromJson(s, typeToken);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean putColorType(VehicleColorType vehicleColorType) {

        try {
            List<VehicleColorType.VehtTypBean> temp = new ArrayList<>();
            List<VehicleColorType.VehtTypBean> temp2 = new ArrayList<>();

            for (VehicleColorType.VehtTypBean type : vehicleColorType.vehtTyp) {
                if (type.Name.equalsIgnoreCase("white") || type.Name.equalsIgnoreCase("black"))
                    temp.add(type);
                else
                    temp2.add(type);
            }

            temp.addAll(temp2);
            vehicleColorType.vehtTyp = temp;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sharedPreferences.edit().putString("vehicleColorType", new Gson().toJson(vehicleColorType)).commit();

    }

    @Override
    public VehicleColorType getColorType() {
        try {
            String s = sharedPreferences.getString("vehicleColorType", null);
            return new Gson().fromJson(s, VehicleColorType.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean putVehMake(List<VelTypeLnn.VehtAbbrBean> bean) {
        return sharedPreferences.edit().putString("makeTypes", new Gson().toJson(bean)).commit();
    }


    @Override
    public List<VelTypeLnn.VehtAbbrBean> getVehMake() {
        try {
            String s = sharedPreferences.getString("makeTypes", null);
            Type typeToken = new TypeToken<List<VelTypeLnn.VehtAbbrBean>>() {
            }.getType();
            return new Gson().fromJson(s, typeToken);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void put(String key, int value) {
        sharedPreferences.edit().putInt(key, value).apply();
    }

    @Override
    public int get(String key, int defaultValue) {
        return sharedPreferences.getInt(key, defaultValue);
    }

    @Override
    public void put(String key, float value) {
        sharedPreferences.edit().putFloat(key, value).apply();
    }

    @Override
    public float get(String key, float defaultValue) {
        return sharedPreferences.getFloat(key, defaultValue);
    }

    @Override
    public void put(String key, boolean value) {
        sharedPreferences.edit().putBoolean(key, value).apply();
    }

    @Override
    public boolean get(String key, boolean defaultValue) {
        return sharedPreferences.getBoolean(key, defaultValue);
    }

    @Override
    public void put(String key, long value) {
        sharedPreferences.edit().putLong(key, value).apply();
    }

    @Override
    public long get(String key, long defaultValue) {
        return sharedPreferences.getLong(key, defaultValue);
    }

    @Override
    public void put(String key, String value) {
        sharedPreferences.edit().putString(key, value).apply();
    }

    @Override
    public String get(String key, String defaultValue) {
        return sharedPreferences.getString(key, defaultValue);
    }

    @Override
    public void delete(String key) {
        sharedPreferences.edit().remove(key).apply();
    }

    @Override
    public void deleteAll() {
        sharedPreferences.edit().clear().apply();
    }
}
