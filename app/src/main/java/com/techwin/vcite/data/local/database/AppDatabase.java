package com.techwin.vcite.data.local.database;


import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.techwin.vcite.data.local.dao.CitationDao;
import com.techwin.vcite.data.local.dao.MediaBeanDao;
import com.techwin.vcite.data.local.dao.TicketBeanDao;
import com.techwin.vcite.data.local.dao.TransactionDao;
import com.techwin.vcite.data.local.dao.ViolationDao;
import com.techwin.vcite.data.local.database.convertor.TypeConvertor;
import com.techwin.vcite.data.local.entity.CitationBean;
import com.techwin.vcite.data.local.entity.MediaBean;
import com.techwin.vcite.data.local.entity.TicketBean;
import com.techwin.vcite.data.local.entity.ViolationBean;

/*
 * REFERENCE:
 * http://zdominguez.com/2017/05/23/arch-components-1.html
 */
@Database(entities = {CitationBean.class, MediaBean.class, ViolationBean.class, TicketBean.class},
        version = 21,
        exportSchema = false
)
@TypeConverters({
        TypeConvertor.class
})
public abstract class AppDatabase extends RoomDatabase {
    public abstract CitationDao citationDao();

    public abstract MediaBeanDao mediaDao();

    public abstract ViolationDao violationDao();

    public abstract TransactionDao transactionDao();

    public abstract TicketBeanDao ticketBeanDao();


}
