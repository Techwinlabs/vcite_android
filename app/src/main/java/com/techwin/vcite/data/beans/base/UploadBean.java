package com.techwin.vcite.data.beans.base;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class UploadBean implements Parcelable {

    private long bytesWritten;
    private long contentLength;
    private boolean isUploaded;
    private String path;

    public String getPath() {
        return path;
    }

    public UploadBean() {
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isUploaded() {
        return isUploaded;
    }

    public void setUploaded(boolean uploaded) {
        isUploaded = uploaded;
    }


    public UploadBean(long bytesWritten, long contentLength) {
        this.bytesWritten = bytesWritten;
        this.contentLength = contentLength;
    }

    public long getBytesWritten() {
        return bytesWritten;
    }

    public long getContentLength() {
        return contentLength;
    }

    public int getUploadPercent() {
        Log.e("bytes", "==" + bytesWritten + "==" + contentLength);
        return (int) (100 * ((1.0 * bytesWritten) / contentLength));
    }

    public void setBytesWritten(long bytesWritten) {
        this.bytesWritten = bytesWritten;
    }

    public void setContentLength(long contentLength) {
        this.contentLength = contentLength;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.bytesWritten);
        dest.writeLong(this.contentLength);
        dest.writeByte(this.isUploaded ? (byte) 1 : (byte) 0);
        dest.writeString(this.path);
    }

    protected UploadBean(Parcel in) {
        this.bytesWritten = in.readLong();
        this.contentLength = in.readLong();
        this.isUploaded = in.readByte() != 0;
        this.path = in.readString();
    }

    public static final Creator<UploadBean> CREATOR = new Creator<UploadBean>() {
        @Override
        public UploadBean createFromParcel(Parcel source) {
            return new UploadBean(source);
        }

        @Override
        public UploadBean[] newArray(int size) {
            return new UploadBean[size];
        }
    };
}
