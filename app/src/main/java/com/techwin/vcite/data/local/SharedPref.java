package com.techwin.vcite.data.local;


import com.techwin.vcite.data.beans.CampusBean;
import com.techwin.vcite.data.beans.CitTempBean;
import com.techwin.vcite.data.beans.DefaultBean;
import com.techwin.vcite.data.beans.PlateType;
import com.techwin.vcite.data.beans.UserBean;
import com.techwin.vcite.data.beans.UserPermission;
import com.techwin.vcite.data.beans.VehType;
import com.techwin.vcite.data.beans.VehicleColorType;
import com.techwin.vcite.data.beans.VelTypeLnn;

import java.util.List;

import javax.annotation.Nullable;

public interface SharedPref {
    CampusBean getCampus();

    boolean setCampus(CampusBean userBean);

    List<CitTempBean> getTemplate(String type);

    List<CitTempBean> getTemplate();

    boolean setTemplate(List<CitTempBean> userBean, String type);

    @Nullable
    List<UserPermission> getUserPermission();

    boolean setUserPermission(List<UserPermission> userBean);

    List<VehType.VehtTypDTO> getVehType();

    boolean setVehType(List<VehType.VehtTypDTO> userBean);

    UserBean getUser();

    void removeUser();

    boolean putUser(UserBean userBean);

    DefaultBean getDefault();

    boolean putDefault(DefaultBean defaultBean);


    boolean putPlateTypeList(List<PlateType> plateTypes);

    @Nullable
    List<PlateType> getPlateTypeList();


    boolean putColorType(VehicleColorType vehicleColorType);

    @Nullable
    VehicleColorType getColorType();

    boolean putVehMake(List<VelTypeLnn.VehtAbbrBean> bean);

    @Nullable
    List<VelTypeLnn.VehtAbbrBean> getVehMake();

    void put(String key, int value);

    /**
     * Get integer
     *
     * @param key
     * @param defaultValue
     * @return
     */
    int get(String key, int defaultValue);

    /**
     * Put float
     *
     * @param key
     * @param value
     */
    void put(String key, float value);

    /**
     * Get float
     *
     * @param key
     * @param defaultValue
     * @return
     */
    float get(String key, float defaultValue);

    /**
     * Put boolean
     *
     * @param key
     * @param value
     */
    void put(String key, boolean value);

    /**
     * Get boolean
     *
     * @param key
     * @param defaultValue
     * @return
     */
    boolean get(String key, boolean defaultValue);

    /**
     * Put long
     *
     * @param key
     * @param value
     */
    void put(String key, long value);


    /**
     * Get long
     *
     * @param key
     * @param defaultValue
     * @return
     */
    long get(String key, long defaultValue);

    /**
     * Put string
     *
     * @param key
     * @param value
     */
    void put(String key, String value);

    /**
     * Get string
     *
     * @param key
     * @param defaultValue
     * @return
     */
    String get(String key, String defaultValue);

    /**
     * Delete data specified by key
     *
     * @param key
     */
    void delete(String key);

    /**
     * Delete all data from shared preferences
     */
    void deleteAll();


}
