package com.techwin.vcite.data.local.dao;


import androidx.room.Dao;
import androidx.room.Query;

import com.techwin.vcite.data.local.entity.MediaBean;

import java.util.List;

@Dao
public abstract class MediaBeanDao extends BaseDao<MediaBean> {
    @Query("DELETE FROM " + MediaBean.TABLE_NAME + " WHERE cit_no =:citationNo AND org_no=:orgNo")
    public abstract int deleteAll(String citationNo, String orgNo);
    @Query("DELETE FROM " + MediaBean.TABLE_NAME + " WHERE cit_no =:citationNo AND org_no=:orgNo AND pic_type=:picType")
    public abstract int delete(String citationNo, String orgNo,int picType);

    @Query("DELETE FROM " + MediaBean.TABLE_NAME+" WHERE org_no=:orgNo")
    public abstract int deleteAll(String orgNo);

    @Query("SELECT * FROM " + MediaBean.TABLE_NAME + " WHERE cit_no =:citationNo AND org_no=:orgNo")
    public abstract List<MediaBean> getList(String citationNo, String orgNo);
}
