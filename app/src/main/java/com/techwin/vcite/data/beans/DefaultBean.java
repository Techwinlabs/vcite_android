package com.techwin.vcite.data.beans;

import com.techwin.vcite.util.Constants;

public class DefaultBean {
    public String plateType;
    public String vehState;
    public String vehColor;
    public String vehPlateColor;
    public String vehMake;
    public boolean addressLookUp;
    public boolean parcelId;
    public String customerId;
    public String serverUrl= Constants.BASEURL;
    public boolean geoLocation;
    public boolean notification;
    public boolean scoffMessage;
    public boolean vChalk;
}
