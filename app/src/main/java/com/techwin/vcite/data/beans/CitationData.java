package com.techwin.vcite.data.beans;

import android.os.Parcel;
import android.os.Parcelable;

public class CitationData implements Parcelable {
    public String citationNo;
    public String orgNo;
    public  String citationType;
    public boolean uploaded;


    public CitationData() {
    }

    @Override
    public String toString() {
        return "CitationData{" +
                "citationNo='" + citationNo + '\'' +
                ", orgNo='" + orgNo + '\'' +
                '}';
    }

    public CitationData(String citationNo, String orgNo) {
        this.citationNo = citationNo;
        this.orgNo = orgNo;
    }

    public CitationData(String citationNo, String orgNo,String type,boolean uploaded) {
        this.citationNo = citationNo;
        this.orgNo = orgNo;
        this.citationType=type;
        this.uploaded=uploaded;
    }

    public String getCitationType() {
        return citationType;
    }

    public void setCitationType(String citationType) {
        this.citationType = citationType;
    }

    public String getCitationNo() {
        return citationNo;
    }

    public boolean isUploaded() {
        return uploaded;
    }

    public void setUploaded(boolean uploaded) {
        this.uploaded = uploaded;
    }

    public void setCitationNo(String citationNo) {
        this.citationNo = citationNo;
    }

    public String getOrgNo() {
        return orgNo;
    }

    public void setOrgNo(String orgNo) {
        this.orgNo = orgNo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.citationNo);
        dest.writeString(this.orgNo);
        dest.writeInt(this.uploaded ? 1 : 0);
        dest.writeString(this.citationType);
    }

    protected CitationData(Parcel in) {
        this.citationNo = in.readString();
        this.orgNo = in.readString();
        this.uploaded = in.readInt() == 1;
        this.citationType = in.readString();
    }

    public static final Creator<CitationData> CREATOR = new Creator<CitationData>() {
        @Override
        public CitationData createFromParcel(Parcel source) {
            return new CitationData(source);
        }

        @Override
        public CitationData[] newArray(int size) {
            return new CitationData[size];
        }
    };
}
