package com.techwin.vcite.data.beans;

import com.google.gson.annotations.SerializedName;

public class UserPermission {

    /**
     * permID : DeleteAllDocument
     * shortDesc : Delete All Citations
     */

    @SerializedName("permID")
    private String permID;
    @SerializedName("shortDesc")
    private String shortDesc;

    public String getPermID() {
        return permID;
    }

    public void setPermID(String permID) {
        this.permID = permID;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }
}
