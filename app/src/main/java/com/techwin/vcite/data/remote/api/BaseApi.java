package com.techwin.vcite.data.remote.api;

import com.techwin.vcite.data.beans.CampusBean;
import com.techwin.vcite.data.beans.PlateType;
import com.techwin.vcite.data.beans.SearchBean;
import com.techwin.vcite.data.beans.TemplateBean;
import com.techwin.vcite.data.beans.UserBean;
import com.techwin.vcite.data.beans.UserPermission;
import com.techwin.vcite.data.beans.VehType;
import com.techwin.vcite.data.beans.VehicleColorType;
import com.techwin.vcite.data.beans.VelTypeLnn;
import com.techwin.vcite.data.beans.ViolationType;
import com.techwin.vcite.data.beans.base.ApiResponse;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

public interface BaseApi {


    @Multipart
    @POST("api/upload")
    Single<ApiResponse<String>> uploadfile(@Part MultipartBody.Part part);

    @GET("api/citation/PlateTypes")
    Single<ApiResponse<List<PlateType>>> getPlateType();

    @GET("api/citation/vehAbbrNPlateTypForLynn")
    Single<ApiResponse<VelTypeLnn>> vehAbbrNPlateTypForLynn();

    @GET("api/citation/vehMake")
    Single<ApiResponse<VelTypeLnn>> vehMake();

    @GET("api/citation/vehColor")
    Single<ApiResponse<VehicleColorType>> getVehicleColor();

    @GET("api/citation/ViolationType")
    Single<ApiResponse<ViolationType>> getViolation(@QueryMap Map<String, String> map);

    @POST("api/citation/AddCodeCitation1")
    Single<ApiResponse<String>> addCodeEnforcementCitation(@Body RequestBody map);
    /* @POST("api/citation/AddCodeCitation")
    Single<ApiResponse<String>> addCodeEnforcementCitation(@Body RequestBody map);*/

    @POST("api/citation/AddCitation1")
    Single<ApiResponse<String>> addParkingCitation(@Body RequestBody map);

    @POST("api/citation/EditCitation")
    Single<ApiResponse<String>> EditCitation(@Body RequestBody map);


    @GET("api/citation/vehType")
    Single<ApiResponse<VehType>> vehType();

    @GET("api/citation/campus")
    Observable<ApiResponse<CampusBean>> campus(@QueryMap Map<String,String> map);

    @GET("/api/citation/{url}")
    Single<ApiResponse<SearchBean>> getSearch(@Path("url") String url, @QueryMap Map<String, String> map);

    @POST("api/citation/checkAuthentication1")
    Single<ApiResponse<UserBean>> login(@QueryMap Map<String, String> map);

    @POST("api/citation/resetPassword")
    Single<ApiResponse<String>> password(@QueryMap Map<String, String> map);


    @GET("api/citation/getUserPermission")
    Single<ApiResponse<List<UserPermission>>> getUserPermission(@QueryMap Map<String, String> map);

    @GET("api/citation/citationTemplate")
    Observable<ApiResponse<TemplateBean>> citationTemplate(@QueryMap Map<String, String> map);
}
