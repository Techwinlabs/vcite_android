package com.techwin.vcite.data.repo;

import com.techwin.vcite.data.beans.CampusBean;
import com.techwin.vcite.data.beans.PlateType;
import com.techwin.vcite.data.beans.SearchBean;
import com.techwin.vcite.data.beans.TemplateBean;
import com.techwin.vcite.data.beans.UserBean;
import com.techwin.vcite.data.beans.UserPermission;
import com.techwin.vcite.data.beans.VehType;
import com.techwin.vcite.data.beans.VehicleColorType;
import com.techwin.vcite.data.beans.VelTypeLnn;
import com.techwin.vcite.data.beans.ViolationType;
import com.techwin.vcite.data.beans.base.ApiResponse;
import com.techwin.vcite.data.beans.base.UploadBean;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.local.dao.TransactionDao;
import com.techwin.vcite.data.local.entity.CitationBean;
import com.techwin.vcite.data.remote.api.BaseApi;
import com.techwin.vcite.data.remote.helper.ApiUtils;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.util.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class BaseRepoImpl implements BaseRepo {
    private final BaseApi baseApi;
    private final SharedPref sharedPref;
    private final TransactionDao transactionDao;


    public BaseRepoImpl(BaseApi baseApi, SharedPref sharedPref, TransactionDao transactionDao) {
        this.baseApi = baseApi;
        this.sharedPref = sharedPref;
        this.transactionDao = transactionDao;
    }


    @Override
    public Single<ApiResponse<SearchBean>> getSearch(String url, Map<String, String> map) {
        return baseApi.getSearch(url, map);
    }

    @Override
    public Single<ApiResponse<String>> password(Map<String, String> map) {
        return baseApi.password(map);
    }

    @Override
    public Flowable<Resource<UploadBean>> uploadFile(final String filePath) {
        return Flowable.create(emitter -> {
           /* try {
                ApiResponse<String> response = baseApi.uploadfile(ApiUtils.createMultipartBody("media", filePath, emitter)).blockingGet();
                if (response.getStatus() == HttpURLConnection.HTTP_OK) {
                    UploadBean uploadBean = new UploadBean();
                    uploadBean.setUploaded(true);
                    uploadBean.setPath(response.getData());
                    emitter.onNext(Resource.success(uploadBean, response.getMessage()));
                } else {
                    emitter.onNext(Resource.error(null, response.getMessage()));
                }
                emitter.onComplete();
            } catch (Exception e) {
                emitter.tryOnError(e);
            }*/
        }, BackpressureStrategy.LATEST);

    }

    @Override
    public Flowable<Resource<UploadBean>> uploadFile(byte[] filepath) {
        return Flowable.create(emitter -> {
            try {
                ApiResponse<String> response = baseApi.uploadfile(ApiUtils.createMultipartBody("media", filepath, emitter)).blockingGet();
              /*  if (response.getStatus() == HttpURLConnection.HTTP_OK) {
                    UploadBean uploadBean = new UploadBean();
                    uploadBean.setUploaded(true);
                    uploadBean.setPath(response.getData());
                    emitter.onNext(Resource.success(uploadBean, response.getMessage()));
                } else {
                    emitter.onNext(Resource.error(null, response.getMessage()));
                }*/
                emitter.onComplete();
            } catch (Exception e) {
                emitter.tryOnError(e);
            }
        }, BackpressureStrategy.LATEST);

    }

    @Override
    public Single<ApiResponse<List<PlateType>>> getPlateType() {
        return baseApi.getPlateType().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).doOnSuccess(new Consumer<ApiResponse<List<PlateType>>>() {
                    @Override
                    public void accept(ApiResponse<List<PlateType>> listApiResponse) throws Exception {
                        if (listApiResponse.getData() != null)
                            sharedPref.putPlateTypeList(listApiResponse.getData());
                    }
                });
    }

    @Override
    public Single<ApiResponse<VelTypeLnn>> getPlateForLynn() {
        return baseApi.vehAbbrNPlateTypForLynn().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).doOnSuccess(new Consumer<ApiResponse<VelTypeLnn>>() {
                    @Override
                    public void accept(ApiResponse<VelTypeLnn> listApiResponse) throws Exception {
                        if (listApiResponse.getData() != null) {
                            List<PlateType> plateTypes = new ArrayList<>();
                            if (listApiResponse.getData().pltTyp != null) {
                                plateTypes.addAll(listApiResponse.getData().pltTyp);
                            }
                            sharedPref.putPlateTypeList(plateTypes);
                        }
                    }
                });
    }

    @Override
    public Single<ApiResponse<VelTypeLnn>> vehMake() {
        return baseApi.vehMake().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).doOnSuccess(new Consumer<ApiResponse<VelTypeLnn>>() {
                    @Override
                    public void accept(ApiResponse<VelTypeLnn> listApiResponse) throws Exception {
                        if (listApiResponse.getData() != null&&listApiResponse.getData().vehtAbbr!=null) {
                            sharedPref.putVehMake(listApiResponse.getData().vehtAbbr);
                        }
                    }
                });
    }


    @Override
    public Single<ApiResponse<VehicleColorType>> getVehicleColor() {
        return baseApi.getVehicleColor().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnSuccess(new Consumer<ApiResponse<VehicleColorType>>() {
                    @Override
                    public void accept(ApiResponse<VehicleColorType> vehicleColorTypeApiResponse) throws Exception {
                        if (vehicleColorTypeApiResponse.getData() != null) {

                            sharedPref.putColorType(vehicleColorTypeApiResponse.getData());
                        }
                    }
                });
    }


    @Override
    public Single<ApiResponse<ViolationType>> getViolation(Map<String, String> map) {
        return baseApi.getViolation(map)
                .doOnSuccess(new Consumer<ApiResponse<ViolationType>>() {
                    @Override
                    public void accept(ApiResponse<ViolationType> response) throws Exception {
                        if (response.getData() != null && response.getData().voilTyp != null)
                            transactionDao.saveViolationTrans(response.getData().voilTyp);
                    }
                });
    }

    @Override
    public Single<ApiResponse<VehType>> vehType() {
        return baseApi.vehType()
                .doOnSuccess(new Consumer<ApiResponse<VehType>>() {
                    @Override
                    public void accept(ApiResponse<VehType> response) throws Exception {
                        if (response.getData() != null && response.getData().getVehtTyp() != null)
                            sharedPref.setVehType(response.getData().getVehtTyp());
                    }
                });
    }

    @Override
    public Observable<ApiResponse<CampusBean>> campusType(Map<String,String> map) {
        return baseApi.campus(map);
    }

    @Override
    public Single<ApiResponse<String>> addCitation(String map, CitationBean citationBean) {
        if (citationBean.getCitationType().equalsIgnoreCase(Constants.CITATION_TYPE_PARKING)) {
            return baseApi.addParkingCitation(RequestBody.create(MediaType.parse("application/json"), map))
                    .doOnSuccess(new Consumer<ApiResponse<String>>() {
                        @Override
                        public void accept(ApiResponse<String> response) throws Exception {
                            if (response.getData() != null && response.getData().equalsIgnoreCase("Success"))
                                citationBean.setUploaded(true);
                            transactionDao.saveCitationTrans(citationBean);
                        }
                    });
        } else if (citationBean.getCitationType().equalsIgnoreCase(Constants.CITATION_TYPE_CODE_ENFORCE)) {
            return baseApi.addCodeEnforcementCitation(RequestBody.create(MediaType.parse("application/json"), map))
                    .doOnSuccess(new Consumer<ApiResponse<String>>() {
                        @Override
                        public void accept(ApiResponse<String> response) throws Exception {
                            if (response.getData() != null && response.getData().equalsIgnoreCase("Success"))
                                citationBean.setUploaded(true);
                            transactionDao.saveCitationTrans(citationBean);
                        }
                    });
        } else {
            throw new RuntimeException("Not correct citation type from backend" + citationBean.getCitationType());
        }
    }

    @Override
    public Single<ApiResponse<String>> editCitation(String map, CitationBean citationBean) {
        return baseApi.EditCitation(RequestBody.create(MediaType.parse("application/json"), map))
                .doOnSuccess(new Consumer<ApiResponse<String>>() {
                    @Override
                    public void accept(ApiResponse<String> response) throws Exception {
                     /*   if (response.getData() != null && response.getData().equalsIgnoreCase("Success"))
                            citationBean.setUploaded(true);
                        transactionDao.saveCitationTrans(citationBean);*/
                    }
                });
    }

    @Override
    public Observable<ApiResponse<TemplateBean>> citationTemplate(Map<String, String> map, String type) {
        return baseApi.citationTemplate(map);/*(new Consumer<ApiResponse<TemplateBean>>() {
            @Override
            public void accept(ApiResponse<TemplateBean> templateBeanApiResponse) throws Exception {
                if (templateBeanApiResponse.getData() != null) {
                    sharedPref.setTemplate(templateBeanApiResponse.getData().getCitTempList(), type);
                }
            }
        });*/
    }

    @Override
    public Single<ApiResponse<List<UserPermission>>> getUserPermission(Map<String, String> map) {
        return baseApi.getUserPermission(map).doOnSuccess(new Consumer<ApiResponse<List<UserPermission>>>() {
            @Override
            public void accept(ApiResponse<List<UserPermission>> templateBeanApiResponse) throws Exception {
                if (templateBeanApiResponse.getData() != null) {
                    sharedPref.setUserPermission(templateBeanApiResponse.getData());
                }
            }
        });
    }

    @Override
    public Single<ApiResponse<UserBean>> login(Map<String, String> map) {
        return baseApi.login(map).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).doOnSuccess(new Consumer<ApiResponse<UserBean>>() {
                    @Override
                    public void accept(ApiResponse<UserBean> userBeanApiResponse) throws Exception {
                        if (userBeanApiResponse.getData() != null) {
                            sharedPref.putUser(userBeanApiResponse.getData());
                        }
                    }
                });
    }
}
