package com.techwin.vcite.data.local.dao;


import androidx.room.Dao;
import androidx.room.Query;

import com.techwin.vcite.data.local.entity.MediaBean;
import com.techwin.vcite.data.local.entity.ViolationBean;

import java.util.List;

@Dao
public abstract class ViolationDao extends BaseDao<ViolationBean> {
    @Query("DELETE FROM " + ViolationBean.TABLE_NAME)
    public abstract int deleteAll();

    @Query("SELECT * FROM " + ViolationBean.TABLE_NAME )
    public abstract List<ViolationBean> getList();
}
