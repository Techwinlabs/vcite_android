package com.techwin.vcite.data.beans;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VehicleColorType {

    @SerializedName("vehtTyp")
    public List<VehtTypBean> vehtTyp;

    public static class VehtTypBean implements Parcelable {
        @SerializedName("VehColorKey")
        public int VehColorKey;
        @SerializedName("Name")
        public String Name;
        @SerializedName("Abbreviation")
        public String Abbreviation;

        public boolean checked;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.VehColorKey);
            dest.writeString(this.Name);
            dest.writeString(this.Abbreviation);
        }

        public VehtTypBean() {
        }

        protected VehtTypBean(Parcel in) {
            this.VehColorKey = in.readInt();
            this.Name = in.readString();
            this.Abbreviation = in.readString();
        }

        public static final Parcelable.Creator<VehtTypBean> CREATOR = new Parcelable.Creator<VehtTypBean>() {
            @Override
            public VehtTypBean createFromParcel(Parcel source) {
                return new VehtTypBean(source);
            }

            @Override
            public VehtTypBean[] newArray(int size) {
                return new VehtTypBean[size];
            }
        };
    }
}
