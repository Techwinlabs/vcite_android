package com.techwin.vcite.data.beans;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchBean {


    @SerializedName(value = "streetNum",alternate = {"streetName","streetUnit"})
    public List<String> streetNum;

    @SerializedName("streetParcel")
    public String streetParcel;
}
