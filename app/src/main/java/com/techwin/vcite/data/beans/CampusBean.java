package com.techwin.vcite.data.beans;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CampusBean {

    @SerializedName("campusTyp")
    public List<CampusTyp> campusTyp;

    public static class CampusTyp {
        /**
         * campusKey : 1
         * Name : Career & Technology Center
         * Abbreviation : CTC
         */

        @SerializedName("campusKey")
        private int campusKey;
        @SerializedName("Name")
        private String Name;
        @SerializedName("Abbreviation")
        private String Abbreviation;
        public boolean checked;

        public int getCampusKey() {
            return campusKey;
        }

        public void setCampusKey(int campusKey) {
            this.campusKey = campusKey;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getAbbreviation() {
            if (Abbreviation == null)
                return Name;
            return Abbreviation;
        }

        public void setAbbreviation(String abbreviation) {
            Abbreviation = abbreviation;
        }
    }
}
