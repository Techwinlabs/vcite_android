package com.techwin.vcite.data.beans.base;

public class SimpleApiResponse {
    protected Object ContentEncoding;
    protected Object ContentType;
    protected int JsonRequestBehavior;
    protected Object MaxJsonLength;
    protected Object RecursionLimit;

    public Object getContentEncoding() {
        return ContentEncoding;
    }

    public Object getContentType() {
        return ContentType;
    }

    public int getJsonRequestBehavior() {
        return JsonRequestBehavior;
    }

    public Object getMaxJsonLength() {
        return MaxJsonLength;
    }

    public Object getRecursionLimit() {
        return RecursionLimit;
    }
}
