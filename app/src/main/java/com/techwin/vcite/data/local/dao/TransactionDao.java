package com.techwin.vcite.data.local.dao;


import androidx.room.Dao;
import androidx.room.Transaction;

import com.techwin.vcite.data.local.database.AppDatabase;
import com.techwin.vcite.data.local.entity.CitationBean;
import com.techwin.vcite.data.local.entity.MediaBean;
import com.techwin.vcite.data.local.entity.TicketBean;
import com.techwin.vcite.data.local.entity.ViolationBean;

import java.util.Date;
import java.util.List;

@Dao
public abstract class TransactionDao {
    private final AppDatabase appDatabase;

    public TransactionDao(AppDatabase appDatabase) {
        this.appDatabase = appDatabase;
    }

    @Transaction
    public void clearCitation(String orgNo) {
        appDatabase.citationDao().deleteAll(orgNo);
        appDatabase.mediaDao().deleteAll(orgNo);

    }

    @Transaction
    public void clearVolation() {
        appDatabase.violationDao().deleteAll();
    }

    @Transaction
    public void clear7dayCitation() {
        Date now = new Date();
        int DAYS = 7;
        Date last = new Date(now.getTime() - (DAYS * 24 * 60 * 60 * 1000));
        appDatabase.citationDao().deleteByDate(true, last);
    }

    @Transaction
    public void clearCitation(CitationBean citationBean) {
        appDatabase.citationDao().delete(citationBean);
        appDatabase.mediaDao().deleteAll(citationBean.getCitationNo(), citationBean.getOrgNo());

    }

 /*   @Transaction
    public void saveContacts(List<ContactBean> contactBeans) {
        if (contactBeans != null && contactBeans.size() > 0)
            appDatabase.contactDao().insertAll(contactBeans);
    }*/


    @Transaction
    public long saveCitationTrans(CitationBean citationData) {
        return appDatabase.citationDao().insert(citationData);
    }

    @Transaction
    public long saveTicketTrans(TicketBean ticketBean) {
        return appDatabase.ticketBeanDao().insert(ticketBean);
    }

    @Transaction
    public long updateTrans(CitationBean citationData) {
        return appDatabase.citationDao().update(citationData);
    }

    @Transaction
    public long[] saveImagesTrans(List<MediaBean> mediaBean) {
        return appDatabase.mediaDao().insertAll(mediaBean);
    }

    @Transaction
    public long saveMedia(MediaBean mediaBean) {
        appDatabase.mediaDao().delete(mediaBean.getCitationNo(), mediaBean.getOrgNo(), mediaBean.getPicType());
        return appDatabase.mediaDao().insert(mediaBean);
    }

    @Transaction
    public long[] saveViolationTrans(List<ViolationBean> mediaBean) {
        return appDatabase.violationDao().insertAll(mediaBean);
    }

   /* @Transaction
    public long addToCart(MenuBean menuBean) {
        if (menuBean == null)
            return -1;

        MenuBean s = appDatabase.menuDao().load(menuBean.getRestaurantId(), menuBean.getMenuId());
        if (s != null) {
            int qty = s.getQuantity();
            qty++;
            s.setQuantity(qty);
           return appDatabase.menuDao().insert(s);
        } else {
            menuBean.setQuantity(1);
           return appDatabase.menuDao().insert(menuBean);
        }
    }
*/

  /*  @Transaction
    public long updateCart(MenuBean menuBean) {
        if (menuBean == null)
            return -1;
        return appDatabase.menuDao().insert(menuBean);
    }*/

 /*   @Transaction
    public int deleteCartItem(MenuBean menuBean) {
        if (menuBean == null)
            return -1;
        return appDatabase.menuDao().delete(menuBean);
    }*/
/*




    @Transaction
    public void saveInspectionOnly(InspectionResponse inspectionResponse) {
        if (inspectionResponse == null)
            return;

        Inspection inspection = appDatabase.inspectionDao().load(
                inspectionResponse.getId());

        if (inspection != null) {
            appDatabase.inspectionDao().update(inspectionResponse);
        } else {
            appDatabase.inspectionDao().insert(inspectionResponse);
        }
    }

    @Transaction
    public void saveConcern(long stepAutoId, ConcernResponse concernResponse) {
        saveConcernResponse(stepAutoId, concernResponse);
    }

    @Transaction
    public void saveConcernList(long stepAutoId, List<ConcernResponse> concernResponseList) {
        if (concernResponseList == null)
            return;

        ConcernResponse concernResponse = concernResponseList.get(concernResponseList.size() - 1);
        Concern concern = appDatabase.concernDao().load(
                concernResponse.getPivot().getId(),
                concernResponse.getPivot().getInspectionId(),
                concernResponse.getPivot().getSuperGroupId(),
                concernResponse.getPivot().getStepGroupId(),
                concernResponse.getPivot().getStepId(),
                concernResponse.getPivot().getConcernId());

        if (concern == null) {
            saveConcernResponse(stepAutoId, concernResponse);
        }


     *//*   for (ConcernResponse concernResponse : concernResponseList) {
            Concern concern = appDatabase.concernDao().load(
                    concernResponse.getPivot().getId(),
                    concernResponse.getPivot().getInspectionId(),
                    concernResponse.getPivot().getSuperGroupId(),
                    concernResponse.getPivot().getStepGroupId(),
                    concernResponse.getPivot().getStepId(),
                    concernResponse.getPivot().getConcernId());

            if (concern == null) {
                saveConcernResponse(stepAutoId, concernResponse);
            }
        }*//*
    }

    @Transaction
    public void saveStepList(long stepGroupAutoId, List<StepResponse> stepList) {
        if (stepList == null)
            return;

        for (StepResponse step : stepList) {
            Step step1 = appDatabase.stepDao().load(
                    step.getPivot().getId(),
                    step.getPivot().getInspectionId(),
                    step.getPivot().getSuperGroupId(),
                    step.getPivot().getStepGroupId(),
                    step.getPivot().getStepId());

            if (step1 == null) {
                saveStepResponse(stepGroupAutoId, step);
            }
        }
    }

    @Transaction
    public void saveRecommendation(long concernAutoId, RecommendationResponse recommendationResponse) {
        saveRecommendationResponse(concernAutoId, recommendationResponse);
    }

    @Transaction
    public void saveRecommendationList(long concernAutoId, List<RecommendationResponse> recommendationResponseList) {
        if (recommendationResponseList == null)
            return;

        RecommendationResponse recommendationResponse = recommendationResponseList.get(recommendationResponseList.size() - 1);
        Recommendation recommendation = appDatabase.recommendationDao().load(
                recommendationResponse.getPivot().getId(),
                recommendationResponse.getPivot().getInspectionId(),
                recommendationResponse.getPivot().getSuperGroupId(),
                recommendationResponse.getPivot().getStepGroupId(),
                recommendationResponse.getPivot().getStepId(),
                recommendationResponse.getPivot().getConcernId(),
                recommendationResponse.getPivot().getRecommendationId());

        if (recommendation == null) {
            saveRecommendationResponse(concernAutoId, recommendationResponse);
        }

        *//*for (RecommendationResponse recommendationResponse : recommendationResponseList) {
            Recommendation recommendation = appDatabase.recommendationDao().load(
                    recommendationResponse.getPivot().getId(),
                    recommendationResponse.getPivot().getInspectionId(),
                    recommendationResponse.getPivot().getSuperGroupId(),
                    recommendationResponse.getPivot().getStepGroupId(),
                    recommendationResponse.getPivot().getStepId(),
                    recommendationResponse.getPivot().getConcernId(),
                    recommendationResponse.getPivot().getRecommendationId());
            if (recommendation == null) {
                saveRecommendationResponse(concernAutoId, recommendationResponse);
            }
        }*//*
    }

    @Transaction
    public void savePart(long recommendationAutoId, PartResponse partResponse) {
        savePartResponse(recommendationAutoId, partResponse);
    }

    @Transaction
    public void savePartList(long recommendationAutoId, List<PartResponse> partResponseList) {
        if (partResponseList == null)
            return;

        for (PartResponse partResponse : partResponseList) {
            Part part = appDatabase.partDao().load(
                    partResponse.getPivot().getId(),
                    partResponse.getPivot().getInspectionId(),
                    partResponse.getPivot().getSuperGroupId(),
                    partResponse.getPivot().getStepGroupId(),
                    partResponse.getPivot().getStepId(),
                    partResponse.getPivot().getConcernId(),
                    partResponse.getPivot().getRecommendationId(),
                    partResponse.getPivot().getPartId());

            if (part == null) {
                savePartResponse(recommendationAutoId, partResponse);
            }
        }
    }

    @Transaction
    public SuperGroup selectSuperGroup(long superGroupAutoId) {
        SuperGroup newSuperGroup = appDatabase.superGroupDao().load(superGroupAutoId);
        if (newSuperGroup != null) {
            newSuperGroup.getPivot().setSkipped(false);
            newSuperGroup.getPivot().setSelected(true);
            newSuperGroup.getPivot().setCompleted(true);
            appDatabase.superGroupDao().update(newSuperGroup);
        }

        return newSuperGroup;
    }

    @Transaction
    public InputGroup selectInputGroup(long inputGroupAutoId) {
        InputGroup newInputGroup = appDatabase.inputGroupDao().load(inputGroupAutoId);
        if (newInputGroup != null) {
            newInputGroup.getPivot().setSkipped(false);
            newInputGroup.getPivot().setSelected(true);
            appDatabase.inputGroupDao().update(newInputGroup);
        }

        return newInputGroup;
    }

    @Transaction
    public StepGroup selectStepGroup(long stepGroupAutoId) {
        StepGroup newStepGroup = appDatabase.stepGroupDao().load(stepGroupAutoId);
        if (newStepGroup == null)
            return null;

        //update super group
        SuperGroup superGroup = appDatabase.superGroupDao().load(newStepGroup.getSuperGroupAutoId());
        if (!superGroup.getPivot().isCompleted()) {
            superGroup.getPivot().setCompleted(true);
            appDatabase.superGroupDao().update(superGroup);
        }

        if (newStepGroup.getPivot().isSelected())
            return newStepGroup;

        newStepGroup.getPivot().setSkipped(false);
        newStepGroup.getPivot().setSelected(true);
        appDatabase.stepGroupDao().update(newStepGroup);

        return newStepGroup;
    }

    @Transaction
    public Step selectStep(long stepAutoId) {
        Step newStep = appDatabase.stepDao().load(stepAutoId);
        if (newStep == null)
            return null;

//        if (newStep.getPivot().isSelected())
//            return newStep;

        StepGroup stepGroup = appDatabase.stepGroupDao().load(newStep.getStepGroupAutoId());
        if (stepGroup != null) {

            boolean isDefault = false;
            if (ConcernCategory.GOOD.equalsIgnoreCase(newStep.getName())
                    || ConcernCategory.NA.equalsIgnoreCase(newStep.getName())) {

                isDefault = true;
                // updating all the step selected false
                for (Step step : appDatabase.stepDao().loadAllByStepGroup(newStep.getStepGroupAutoId())) {
                    if (step.getPivot().isSelected()) {
                        //updating the concern selected to false
                        for (Concern concern : appDatabase.concernDao().loadAllByStep(step.getAutoId())) {
                            if (concern.getPivot().isSelected()) {
                                //updating the recommendation selected to false
                                for (Recommendation recommendation : appDatabase.recommendationDao().loadAllByConcern(concern.getAutoId())) {
                                    recommendation.getPivot().setSelected(false);
                                    recommendation.getPivot().setCompleted(false);
                                    appDatabase.recommendationDao().update(recommendation);
                                }
                            }
                            concern.getPivot().setCompleted(false);
                            concern.getPivot().setSelected(false);
                            concern.getPivot().setTag(null);
                            concern.getPivot().setHasRecommendation(false);
                            appDatabase.concernDao().update(concern);

                        }
                        step.getPivot().setHasConcern(false);
                        step.getPivot().setSelected(false);
//                        step.getPivot().setCompleted(false);
                        appDatabase.stepDao().update(step);
                    }
                    step.getPivot().setSelected(false);
                    appDatabase.stepDao().update(step);
                }
            } else {
                for (Step step : appDatabase.stepDao().loadAllByStepGroup(newStep.getStepGroupAutoId())) {
                    if (step.getName().equalsIgnoreCase(ConcernCategory.GOOD) || step.getName().equalsIgnoreCase(ConcernCategory.NA)) {
                        step.getPivot().setSelected(false);
                        appDatabase.stepDao().update(step);
                    }
                }
            }

            //update the concern & recommendation regarding the selected step good
            if (ConcernCategory.GOOD.equalsIgnoreCase(newStep.getName())) {
                //updating all the concern & recommendation to good / NA as per step selection
                for (Step step : appDatabase.stepDao().loadAllByStepGroup(newStep.getStepGroupAutoId())) {
                    //updating the concern to good
                    for (Concern concern : appDatabase.concernDao().loadAllByStep(step.getAutoId())) {
                        //updating the recommendation to good
                        for (Recommendation recommendation : appDatabase.recommendationDao().loadAllByConcern(concern.getAutoId())) {
                            if (recommendation.getName().equalsIgnoreCase(ConcernCategory.GOOD)) {
                                recommendation.getPivot().setSelected(true);
                                recommendation.getPivot().setCompleted(true);
                                appDatabase.recommendationDao().update(recommendation);
                            } else {
                                recommendation.getPivot().setSelected(false);
                                recommendation.getPivot().setCompleted(false);
                                appDatabase.recommendationDao().update(recommendation);
                            }
                        }
                        if (concern.getName().equalsIgnoreCase(ConcernCategory.GOOD)) {
                            concern.getPivot().setCompleted(true);
                            concern.getPivot().setSelected(true);
                            concern.getPivot().setTag(ConcernCategory.GOOD);
                            concern.getPivot().setHasRecommendation(false);
                            appDatabase.concernDao().update(concern);
                        } else {
                            concern.getPivot().setCompleted(false);
                            concern.getPivot().setSelected(false);
                            concern.getPivot().setTag(ConcernCategory.GOOD);
                            concern.getPivot().setHasRecommendation(false);
                            appDatabase.concernDao().update(concern);
                        }

                    }
                    step.getPivot().setSelected(false);
//                    if(!step.getName().equalsIgnoreCase(ConcernCategory.GOOD)&&!step.getName().equalsIgnoreCase(ConcernCategory.NA))
//                        step.getPivot().setCompleted(true);
                    appDatabase.stepDao().update(step);
                }
            }
            //update the concern & recommendation regarding the selected step NA
            if (ConcernCategory.NA.equalsIgnoreCase(newStep.getName())) {
                //updating all the concern & recommendation to good / NA as per step selection
                for (Step step : appDatabase.stepDao().loadAllByStepGroup(newStep.getStepGroupAutoId())) {
                    //updating the concern to good
                    for (Concern concern : appDatabase.concernDao().loadAllByStep(step.getAutoId())) {
                        //updating the recommendation to good
                        for (Recommendation recommendation : appDatabase.recommendationDao().loadAllByConcern(concern.getAutoId())) {
                            if (recommendation.getName().equalsIgnoreCase(ConcernCategory.NA)) {
                                recommendation.getPivot().setSelected(true);
                                recommendation.getPivot().setCompleted(true);
                                appDatabase.recommendationDao().update(recommendation);
                            } else {
                                recommendation.getPivot().setSelected(false);
                                recommendation.getPivot().setCompleted(false);
                                appDatabase.recommendationDao().update(recommendation);
                            }
                        }
                        if (concern.getName().equalsIgnoreCase(ConcernCategory.NA)) {
                            concern.getPivot().setCompleted(true);
                            concern.getPivot().setSelected(true);
                            concern.getPivot().setTag(ConcernCategory.GOOD);
                            concern.getPivot().setHasRecommendation(false);
                            appDatabase.concernDao().update(concern);
                        } else {
                            concern.getPivot().setCompleted(false);
                            concern.getPivot().setSelected(false);
                            concern.getPivot().setTag(ConcernCategory.GOOD);
                            concern.getPivot().setHasRecommendation(false);
                            appDatabase.concernDao().update(concern);
                        }

                    }
                    step.getPivot().setSelected(false);
//                    if(!step.getName().equalsIgnoreCase(ConcernCategory.GOOD)&&!step.getName().equalsIgnoreCase(ConcernCategory.NA))
//                        step.getPivot().setCompleted(true);
                    appDatabase.stepDao().update(step);
                }
            }
            stepGroup.getPivot().setHasSteps(!isDefault);
            stepGroup.getPivot().setCompleted(true);
            appDatabase.stepGroupDao().update(stepGroup);
        }
        newStep.getPivot().setSkipped(false);
        newStep.getPivot().setSelected(true);
        appDatabase.stepDao().update(newStep);

        return newStep;
    }


    @Transaction
    public Step unselectStep(long stepAutoId) {
        Step newStep = appDatabase.stepDao().load(stepAutoId);
        if (newStep == null)
            return null;


        for (Concern concern : appDatabase.concernDao().loadAllByStep(newStep.getAutoId())) {
            //updating the recommendation selected to false
            for (Recommendation recommendation : appDatabase.recommendationDao().loadAllByConcern(concern.getAutoId())) {
                for (Part part : appDatabase.partDao().loadAllByRecommendation(recommendation.getAutoId())) {
                    appDatabase.partDao().delete(part.getAutoId());
                }
                appDatabase.recommendationDao().delete(recommendation.getAutoId());
            }
            appDatabase.concernDao().delete(concern.getAutoId());

        }
        newStep.getPivot().setSelected(false);
        newStep.getPivot().setCompleted(false);
        newStep.getPivot().setHasConcern(false);
        //updating the step to false
        appDatabase.stepDao().update(newStep);
        return newStep;
    }

    @Transaction
    public Concern selectConcern(long concernAutoId) {
        Concern newConcern = appDatabase.concernDao().load(concernAutoId);
        if (newConcern == null)
            return null;

//        if (newConcern.getPivot().isSelected())
//            return newConcern;

        for (Concern concern : appDatabase.concernDao().loadAllByStep(newConcern.getStepAutoId())) {
            for (Recommendation recommendation : appDatabase.recommendationDao().loadAllByConcern(concern.getAutoId())) {
                // update part under recommendation to selected=false
                appDatabase.partDao().updateAllByRecommendation(
                        recommendation.getAutoId(),
                        false);
            }

            // update recommendation under concern to selected=false
            *//*appDatabase.recommendationDao().updateAllByConcern(
                    concern.getAutoId(),
                    false);*//*
        }

        // update concern siblings to selected=false
        *//*appDatabase.concernDao().updateAllByStep(
                newConcern.getStepAutoId(),
                false);*//*

        Step step = appDatabase.stepDao().load(newConcern.getStepAutoId());
        if (step != null) {
            if (!step.getPivot().isCompleted()) {
                // TODO increase step completed
                Inspection inspection = appDatabase.inspectionDao().load(step.getPivot().getInspectionId());
                int newTotalCompletedSteps = inspection.getTotalCompletedSteps() + 1;
                inspection.setTotalCompletedSteps(newTotalCompletedSteps);
                appDatabase.inspectionDao().update(inspection);
            }

            boolean isDefault = false;
            if (ConcernCategory.GOOD.equalsIgnoreCase(newConcern.getName())
                    || ConcernCategory.NA.equalsIgnoreCase(newConcern.getName())) {
                isDefault = true;
            }
            step.getPivot().setHasConcern(!isDefault);
            step.getPivot().setCompleted(true);
            appDatabase.stepDao().update(step);
        }

        newConcern.getPivot().setSkipped(false);
        newConcern.getPivot().setSelected(true);
        appDatabase.concernDao().update(newConcern);

        return newConcern;
    }

    @Transaction
    public Concern selectConcern(long concernAutoId, String tag) {
        Concern newConcern = appDatabase.concernDao().load(concernAutoId);
        if (newConcern == null)
            return null;

//        if (newConcern.getPivot().isSelected())
//            return newConcern;

        for (Concern concern : appDatabase.concernDao().loadAllByStep(newConcern.getStepAutoId())) {
            for (Recommendation recommendation : appDatabase.recommendationDao().loadAllByConcern(concern.getAutoId())) {
                // update part under recommendation to selected=false
                appDatabase.partDao().updateAllByRecommendation(
                        recommendation.getAutoId(),
                        false);
            }

            // update recommendation under concern to selected=false
//            appDatabase.recommendationDao().updateAllByConcern(
//                    concern.getAutoId(),
//                    false);
        }

        // update concern siblings to selected=false
        appDatabase.concernDao().updateAllByStep(
                newConcern.getStepAutoId(),
                false);

        Step step = appDatabase.stepDao().load(newConcern.getStepAutoId());
        if (step != null) {
            if (!step.getPivot().isCompleted()) {
                // TODO increase step completed
                Inspection inspection = appDatabase.inspectionDao().load(step.getPivot().getInspectionId());
                int newTotalCompletedSteps = inspection.getTotalCompletedSteps() + 1;
                inspection.setTotalCompletedSteps(newTotalCompletedSteps);
                appDatabase.inspectionDao().update(inspection);
            }

            boolean isDefault = false;
            if (ConcernCategory.GOOD.equalsIgnoreCase(newConcern.getName())) {
                isDefault = true;
                // updating all the step selected false
                for (Concern concern : appDatabase.concernDao().loadAllByStep(newConcern.getStepAutoId())) {
                    //update all the recommendation under previous selected concern to be false (selected or completed)
                    for (Recommendation recommendation : appDatabase.recommendationDao().loadAllByConcern(concern.getAutoId())) {
                        recommendation.getPivot().setSelected(false);
                        recommendation.getPivot().setCompleted(false);
                        appDatabase.recommendationDao().update(recommendation);
                    }

                    concern.getPivot().setSelected(false);
                    concern.getPivot().setCompleted(false);
                    concern.getPivot().setTag(null);
                    appDatabase.concernDao().update(concern);
                }
            } else if (ConcernCategory.NA.equalsIgnoreCase(newConcern.getName())) {
                isDefault = true;
                // updating all the step selected false
                for (Concern concern : appDatabase.concernDao().loadAllByStep(newConcern.getStepAutoId())) {
                    //update all the recommendation under previous selected concern to be false (selected or completed)
                    for (Recommendation recommendation : appDatabase.recommendationDao().loadAllByConcern(concern.getAutoId())) {
                        recommendation.getPivot().setSelected(false);
                        recommendation.getPivot().setCompleted(false);
                        appDatabase.recommendationDao().update(recommendation);
                    }

                    concern.getPivot().setCompleted(false);
                    concern.getPivot().setSelected(false);
                    concern.getPivot().setTag(null);
                    appDatabase.concernDao().update(concern);
                }
            } else {
                for (Concern concern : appDatabase.concernDao().loadAllByStep(newConcern.getStepAutoId())) {
                    if (concern.getName().equalsIgnoreCase(ConcernCategory.GOOD) || concern.getName().equalsIgnoreCase(ConcernCategory.NA)) {
                        concern.getPivot().setSelected(false);
                        concern.getPivot().setCompleted(false);
                        appDatabase.concernDao().update(concern);
                    }

                }
            }

            step.getPivot().setHasConcern(!isDefault);
            step.getPivot().setCompleted(true);
            appDatabase.stepDao().update(step);
        }

        newConcern.getPivot().setSkipped(false);
        newConcern.getPivot().setSelected(true);
        newConcern.getPivot().setCompleted(true);
        newConcern.getPivot().setTag(tag);
        appDatabase.concernDao().update(newConcern);

        return newConcern;
    }

    @Transaction
    public Concern unselectConcern(long concernAutoId, String tag) {
        Concern newConcern = appDatabase.concernDao().load(concernAutoId);
        if (newConcern == null)
            return null;

        for (Recommendation recommendation : appDatabase.recommendationDao().loadAllByConcern(newConcern.getAutoId())) {
            for (Part part : appDatabase.partDao().loadAllByRecommendation(recommendation.getAutoId())) {
                appDatabase.partDao().delete(part.getAutoId());
            }
            appDatabase.recommendationDao().delete(recommendation.getAutoId());
        }
        newConcern.getPivot().setSkipped(false);
        newConcern.getPivot().setSelected(false);
        newConcern.getPivot().setCompleted(false);
        newConcern.getPivot().setTag(null);
        appDatabase.concernDao().update(newConcern);

        return newConcern;
    }

    @Transaction
    public Recommendation selectRecommendation(long recommendationAutoId) {
        Recommendation newRecommendation = appDatabase.recommendationDao().load(recommendationAutoId);
        if (newRecommendation == null)
            return null;

//        if (newRecommendation.getPivot().isSelected())
//            return newRecommendation;

        for (Recommendation recommendation : appDatabase.recommendationDao().loadAllByConcern(newRecommendation.getConcernAutoId())) {
            // update part under recommendation to selected=false
            appDatabase.partDao().updateAllByRecommendation(
                    recommendation.getAutoId(),
                    false);
        }

        // update recommendation siblings to selected=false
        appDatabase.recommendationDao().updateAllByConcern(
                newRecommendation.getConcernAutoId(),
                false);

        // updating all the recommendation selected false
        for (Recommendation recommendation : appDatabase.recommendationDao().loadAllByConcern(newRecommendation.getConcernAutoId())) {
            recommendation.getPivot().setSelected(false);
            recommendation.getPivot().setCompleted(false);
            appDatabase.recommendationDao().update(recommendation);
        }

        Concern concern = appDatabase.concernDao().load(newRecommendation.getConcernAutoId());
        if (concern != null) {
           *//* if (!concern.getPivot().isCompleted()) {
                // TODO increase step completed
                Inspection inspection = appDatabase.inspectionDao().load(concern.getPivot().getInspectionId());
                int newTotalCompletedSteps = inspection.getTotalCompletedSteps() + 1;
                inspection.setTotalCompletedSteps(newTotalCompletedSteps);
                appDatabase.inspectionDao().update(inspection);
            }*//*

            boolean isDefault = false;
            if (ConcernCategory.GOOD.equalsIgnoreCase(newRecommendation.getName())
                    || ConcernCategory.NA.equalsIgnoreCase(newRecommendation.getName())) {
                isDefault = true;
                for (Recommendation recommendation : appDatabase.recommendationDao().loadAllByConcern(newRecommendation.getConcernAutoId())) {
                    recommendation.getPivot().setClicked(false);
                }
            }
            if (ConcernCategory.GOOD.equalsIgnoreCase(newRecommendation.getName())
                    || ConcernCategory.NA.equalsIgnoreCase(newRecommendation.getName())) {
                concern.getPivot().setTag("GOOD");
            } else {
                if (concern.getPivot().getTag().equalsIgnoreCase("GOOD")) {
                    concern.getPivot().setTag("GOOD");
                }
            }
            concern.getPivot().setHasRecommendation(!isDefault);
            concern.getPivot().setCompleted(true);
            appDatabase.concernDao().update(concern);
        }

        newRecommendation.getPivot().setSkipped(false);
        newRecommendation.getPivot().setSelected(true);
        newRecommendation.getPivot().setCompleted(true);
        newRecommendation.getPivot().setClicked(true);
        appDatabase.recommendationDao().update(newRecommendation);

        return newRecommendation;
    }


    @Transaction
    public Recommendation unselectRecommendation(long recommendationAutoId) {
        Recommendation newRecommendation = appDatabase.recommendationDao().load(recommendationAutoId);
        if (newRecommendation == null)
            return null;

        for (Part part : appDatabase.partDao().loadAllByRecommendation(newRecommendation.getAutoId())) {
            appDatabase.partDao().delete(part.getAutoId());
        }
        newRecommendation.getPivot().setSkipped(false);
        newRecommendation.getPivot().setSelected(false);
        newRecommendation.getPivot().setCompleted(false);
        newRecommendation.getPivot().setClicked(false);
        appDatabase.recommendationDao().update(newRecommendation);

        return newRecommendation;
    }

    @Transaction
    public Part selectPart(long partAutoId) {
        Part newPart = appDatabase.partDao().load(partAutoId);
        if (newPart == null)
            return null;

        if (newPart.getPivot().isSelected())
            return newPart;

        appDatabase.partDao().updateAllByRecommendation(
                newPart.getRecommendationAutoId(),
                false);

        newPart.getPivot().setSkipped(false);
        newPart.getPivot().setSelected(true);
        appDatabase.partDao().update(newPart);

        return newPart;
    }

    private void saveInspectionResponse(InspectionResponse inspectionResponse) {
        if (inspectionResponse == null)
            return;

        Inspection inspection = appDatabase.inspectionDao().load(
                inspectionResponse.getId());

        if (inspection != null) {
            int totalCompletedSteps = inspection.getTotalCompletedSteps();
            int totalSteps = inspection.getTotalSteps();
            *//**
     * TODO save in new table
     *//*

            appDatabase.inspectionDao().update(inspectionResponse);
        } else {
            appDatabase.inspectionDao().insert(inspectionResponse);
        }

        saveSuperGroupResponseList(
                inspectionResponse.getId(),
                inspectionResponse.getSuperGroupResponseList());
    }

    *//**
     * Save super group from response list
     *
     * @param inspectionId
     * @param superGroupResponseList
     *//*
    private void saveSuperGroupResponseList(long inspectionId, List<SuperGroupResponse> superGroupResponseList) {
        if (superGroupResponseList == null)
            return;

        for (SuperGroupResponse superGroupResponse : superGroupResponseList) {
            saveSuperGroupResponse(inspectionId, superGroupResponse);
        }
    }

    *//**
     * Save super group from response
     *
     * @param inspectionId
     * @param superGroupResponse
     *//*
    private void saveSuperGroupResponse(long inspectionId, SuperGroupResponse superGroupResponse) {
        if (superGroupResponse == null)
            return;

        // set foreign key (inspection id)
        superGroupResponse.setInspectionId(inspectionId);
        // save super group
        long superGroupAutoId;

        SuperGroup superGroup = appDatabase.superGroupDao().load(
                superGroupResponse.getPivot().getId(),
                superGroupResponse.getPivot().getInspectionId(),
                superGroupResponse.getPivot().getSuperGroupId());

        if (superGroup != null) {
            superGroupAutoId = superGroup.getAutoId();
            superGroupResponse.setAutoId(superGroupAutoId);
            appDatabase.superGroupDao().update(superGroupResponse);
        } else {
            superGroupAutoId = appDatabase.superGroupDao().insert(superGroupResponse);
        }

        if (superGroupAutoId != App.INVALID_ID) {
            // save input group list
            saveInputGroupResponseList(superGroupAutoId, superGroupResponse.getInputGroupResponseList());
            // save step group list
            saveStepGroupResponseList(superGroupAutoId, superGroupResponse.getStepGroupResponseList());
        }
    }

    *//**
     * Save step group from response list
     *
     * @param superGroupAutoId
     * @param stepGroupResponseList
     *//*
    private void saveStepGroupResponseList(long superGroupAutoId, List<StepGroupResponse> stepGroupResponseList) {
        if (stepGroupResponseList == null)
            return;

        for (StepGroupResponse stepGroupResponse : stepGroupResponseList) {
            saveStepGroupResponse(superGroupAutoId, stepGroupResponse);
        }
    }

    *//**
     * Save step group from response
     *
     * @param superGroupAutoId
     * @param stepGroupResponse
     *//*
    private void saveStepGroupResponse(long superGroupAutoId, StepGroupResponse stepGroupResponse) {
        if (stepGroupResponse == null)
            return;

        // set foreign key (super group auto id)
        stepGroupResponse.setSuperGroupAutoId(superGroupAutoId);
        // save step group
        long stepGroupAutoId;

        StepGroup stepGroup = appDatabase.stepGroupDao().load(
                stepGroupResponse.getPivot().getId(),
                stepGroupResponse.getPivot().getInspectionId(),
                stepGroupResponse.getPivot().getSuperGroupId(),
                stepGroupResponse.getPivot().getStepGroupId());

        if (stepGroup != null) {
            stepGroupAutoId = stepGroup.getAutoId();
            stepGroupResponse.setAutoId(stepGroupAutoId);
            appDatabase.stepGroupDao().update(stepGroupResponse);
        } else {
            stepGroupAutoId = appDatabase.stepGroupDao().insert(stepGroupResponse);
        }

        if (stepGroupAutoId != App.INVALID_ID) {
            // save condition list
            saveConditionResponseList(
                    stepGroupResponse.getPivot().getInspectionId(),
                    InspectionField.STEP_GROUP,
                    stepGroupAutoId,
                    stepGroupResponse.getConditionResponseList());
            // save step list

            // TODO: hide this
//            saveStepResponseList(stepGroupAutoId, stepGroupResponse.getStepResponseList());
        }
    }

    *//**
     * Save step from response list
     *
     * @param stepGroupAutoId
     * @param stepResponseList
     *//*
    private void saveStepResponseList(long stepGroupAutoId, List<StepResponse> stepResponseList) {
        if (stepResponseList == null)
            return;

        for (StepResponse stepResponse : stepResponseList) {
            saveStepResponse(stepGroupAutoId, stepResponse);
        }
    }

    *//**
     * Save step from response
     *
     * @param stepGroupAutoId
     * @param stepResponse
     *//*
    private void saveStepResponse(long stepGroupAutoId, StepResponse stepResponse) {
        if (stepResponse == null)
            return;

        // set foreign key (step group auto id)
        stepResponse.setStepGroupAutoId(stepGroupAutoId);
        // save step
        long stepAutoId;


        Step step = appDatabase.stepDao().load(
                stepResponse.getPivot().getId(),
                stepResponse.getPivot().getInspectionId(),
                stepResponse.getPivot().getSuperGroupId(),
                stepResponse.getPivot().getStepGroupId(),
                stepResponse.getPivot().getStepId());

        if (step != null) {
            stepAutoId = step.getAutoId();
            stepResponse.setAutoId(stepAutoId);
            appDatabase.stepDao().update(stepResponse);
        } else {
            stepAutoId = appDatabase.stepDao().insert(stepResponse);
        }

        if (stepAutoId != App.INVALID_ID) {
            // save concern list
            saveConcernResponseList(stepAutoId, stepResponse.getConcernResponseList());
        }
    }

    *//**
     * Save concern from response list
     *
     * @param stepAutoId
     * @param concernResponseList
     *//*
    private void saveConcernResponseList(long stepAutoId, List<ConcernResponse> concernResponseList) {
        if (concernResponseList == null)
            return;

        for (ConcernResponse concernResponse : concernResponseList) {
            saveConcernResponse(stepAutoId, concernResponse);
        }
    }

    *//**
     * Save concern from response
     *
     * @param stepAutoId
     * @param concernResponse
     *//*
    private void saveConcernResponse(long stepAutoId, ConcernResponse concernResponse) {
        if (concernResponse == null)
            return;

        // set foreign key (step auto id)
        concernResponse.setStepAutoId(stepAutoId);
        // save concern
        long concernAutoId;

        Concern concern = appDatabase.concernDao().load(
                concernResponse.getPivot().getId(),
                concernResponse.getPivot().getInspectionId(),
                concernResponse.getPivot().getSuperGroupId(),
                concernResponse.getPivot().getStepGroupId(),
                concernResponse.getPivot().getStepId(),
                concernResponse.getPivot().getConcernId());

        if (concern != null) {
            concernAutoId = concern.getAutoId();
            concernResponse.setAutoId(concernAutoId);
            appDatabase.concernDao().update(concernResponse);
        } else {
            concernAutoId = appDatabase.concernDao().insert(concernResponse);
        }

        if (concernAutoId != App.INVALID_ID) {
            // save recommendation list
            saveRecommendationResponseList(concernAutoId, concernResponse.getRecommendationResponseList());
        }
    }

    *//**
     * Save recommendation from response list
     *
     * @param concernAutoId
     * @param recommendationResponseList
     *//*
    private void saveRecommendationResponseList(long concernAutoId, List<RecommendationResponse> recommendationResponseList) {
        if (recommendationResponseList == null)
            return;

        for (RecommendationResponse recommendationResponse : recommendationResponseList) {
            saveRecommendationResponse(concernAutoId, recommendationResponse);
        }
    }

    *//**
     * Save recommendation from response
     *
     * @param concernAutoId
     * @param recommendationResponse
     *//*
    private void saveRecommendationResponse(long concernAutoId, RecommendationResponse recommendationResponse) {
        if (recommendationResponse == null)
            return;

        // set foreign key (concern auto id)
        recommendationResponse.setConcernAutoId(concernAutoId);
        // save recommendation
        long recommendationAutoId;

        Recommendation recommendation = appDatabase.recommendationDao().load(
                recommendationResponse.getPivot().getId(),
                recommendationResponse.getPivot().getInspectionId(),
                recommendationResponse.getPivot().getSuperGroupId(),
                recommendationResponse.getPivot().getStepGroupId(),
                recommendationResponse.getPivot().getStepId(),
                recommendationResponse.getPivot().getConcernId(),
                recommendationResponse.getPivot().getRecommendationId());

        if (recommendation != null) {
            recommendationAutoId = recommendation.getAutoId();
            recommendationResponse.setAutoId(recommendationAutoId);
            appDatabase.recommendationDao().update(recommendationResponse);
        } else {
            recommendationAutoId = appDatabase.recommendationDao().insert(recommendationResponse);
        }

        if (recommendationAutoId != App.INVALID_ID) {
            // save part list
            savePartResponseList(recommendationAutoId, recommendationResponse.getPartResponseList());
        }
    }

    *//**
     * Save part from response list
     *
     * @param recommendationAutoId
     * @param partResponseList
     *//*
    private void savePartResponseList(long recommendationAutoId, List<PartResponse> partResponseList) {
        if (partResponseList == null)
            return;

        for (PartResponse partResponse : partResponseList) {
            savePartResponse(recommendationAutoId, partResponse);
        }
    }

    *//**
     * Save part from response
     *
     * @param recommendationAutoId
     * @param partResponse
     *//*
    private void savePartResponse(long recommendationAutoId, PartResponse partResponse) {
        if (partResponse == null)
            return;

        // set foreign key (recommendation auto id)
        partResponse.setRecommendationAutoId(recommendationAutoId);
        // save part
        long partAutoId;

        Part part = appDatabase.partDao().load(
                partResponse.getPivot().getId(),
                partResponse.getPivot().getInspectionId(),
                partResponse.getPivot().getSuperGroupId(),
                partResponse.getPivot().getStepGroupId(),
                partResponse.getPivot().getStepId(),
                partResponse.getPivot().getConcernId(),
                partResponse.getPivot().getRecommendationId(),
                partResponse.getPivot().getPartId());

        if (part != null) {
            partAutoId = part.getAutoId();
            partResponse.setAutoId(partAutoId);
            appDatabase.partDao().update(partResponse);
        } else {
            partAutoId = appDatabase.partDao().insert(partResponse);
        }
    }

    *//**
     * Save condition from response list
     *
     * @param inspectionId
     * @param targetType
     * @param targetId
     * @param conditionResponseList
     *//*
    private void saveConditionResponseList(long inspectionId, String targetType, long targetId,
                                           List<ConditionResponse> conditionResponseList) {
        if (conditionResponseList == null)
            return;

        for (ConditionResponse conditionResponse : conditionResponseList) {
            saveConditionResponse(inspectionId, targetType, targetId, conditionResponse);
        }
    }

    *//**
     * Save condition from response
     *
     * @param inspectionId
     * @param targetType
     * @param targetId
     * @param conditionResponse
     *//*
    private void saveConditionResponse(long inspectionId, String targetType, long targetId,
                                       ConditionResponse conditionResponse) {
        if (conditionResponse == null)
            return;

        // set condition data
        Condition condition = new Condition();
        condition.setInspectionId(inspectionId);
        condition.setTargetType(targetType);
        condition.setTargetId(targetId);
        // save condition
        long conditionId = appDatabase.conditionDao().insert(condition);

        if (conditionId != App.INVALID_ID) {
            // save parameter list
            saveParameterResponseList(conditionId, conditionResponse.getParameterResponseList());
        }
    }

    *//**
     * Save parameter from response list
     *
     * @param conditionId
     * @param parameterResponseList
     *//*
    private void saveParameterResponseList(long conditionId, List<ParameterResponse> parameterResponseList) {
        if (parameterResponseList == null)
            return;

        for (ParameterResponse parameterResponse : parameterResponseList) {
            saveParameterResponse(conditionId, parameterResponse);
        }
    }

    *//**
     * Save parameter from response
     *
     * @param conditionId
     * @param parameterResponse
     *//*
    private void saveParameterResponse(long conditionId, ParameterResponse parameterResponse) {
        if (parameterResponse == null)
            return;

        // set foreign key (condition id)
        parameterResponse.setConditionId(conditionId);
        // save parameter
        appDatabase.parameterDao().insert(parameterResponse);
    }

    *//**
     * Save input group from response list
     *
     * @param superGroupAutoId
     * @param inputGroupResponseList
     *//*
    private void saveInputGroupResponseList(long superGroupAutoId, List<InputGroupResponse> inputGroupResponseList) {
        if (inputGroupResponseList == null)
            return;

        for (InputGroupResponse inputGroupResponse : inputGroupResponseList) {
            saveInputGroupResponse(superGroupAutoId, inputGroupResponse);
        }
    }

    *//**
     * Save input group from response
     *
     * @param superGroupAutoId
     * @param inputGroupResponse
     *//*
    private void saveInputGroupResponse(long superGroupAutoId, InputGroupResponse inputGroupResponse) {
        if (inputGroupResponse == null)
            return;

        // set foreign key (super group auto id)
        inputGroupResponse.setSuperGroupAutoId(superGroupAutoId);
        // save input group
        long inputGroupAutoId;

        InputGroup inputGroup = appDatabase.inputGroupDao().load(
                inputGroupResponse.getPivot().getId(),
                inputGroupResponse.getPivot().getInspectionId(),
                inputGroupResponse.getPivot().getSuperGroupId(),
                inputGroupResponse.getPivot().getInputGroupId());

        if (inputGroup != null) {
            inputGroupAutoId = inputGroup.getAutoId();
            inputGroupResponse.setAutoId(inputGroupAutoId);
            appDatabase.inputGroupDao().update(inputGroupResponse);
        } else {
            inputGroupAutoId = appDatabase.inputGroupDao().insert(inputGroupResponse);
        }

        if (inputGroupAutoId != App.INVALID_ID) {
            // save choice list
            saveChoiceResponseList(inputGroupAutoId, inputGroupResponse.getPivot().getChoices());
        }
    }

    *//**
     * Save choice from response list
     *
     * @param inputGroupAutoId
     * @param choices
     *//*
    private void saveChoiceResponseList(long inputGroupAutoId, String choices) {

        try {
            Gson gson = new Gson();
            Type listType = new TypeToken<List<ChoiceResponse>>() {
            }.getType();
            List<ChoiceResponse> choiceResponseList = gson.fromJson(choices, listType);

            if (choiceResponseList == null)
                return;

            for (ChoiceResponse choiceResponse : choiceResponseList) {
                saveChoiceResponse(inputGroupAutoId, choiceResponse);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    *//**
     * Save choice from response
     *
     * @param inputGroupId
     * @param choiceResponse
     *//*
    private void saveChoiceResponse(long inputGroupId, ChoiceResponse choiceResponse) {
        if (choiceResponse == null)
            return;

        // set foreign key (input group id)
        choiceResponse.setInputGroupAutoId(inputGroupId);
        // save choice
        appDatabase.choiceDao().insert(choiceResponse);
    }

    @Transaction
    public void saveMyStepList(long stepGroupAutoId, List<Step> stepList) {
        if (stepList == null)
            return;

        for (Step step : stepList) {
            // establish foreign key
            step.setStepGroupAutoId(stepGroupAutoId);

            // find step in db
            Step dbStep = appDatabase.stepDao().load(
                    step.getPivot().getId(),
                    step.getPivot().getInspectionId(),
                    step.getPivot().getSuperGroupId(),
                    step.getPivot().getStepGroupId(),
                    step.getPivot().getStepId());

            if (dbStep != null) {
                // step exist, update step
                step.setAutoId(dbStep.getAutoId());
                appDatabase.stepDao().update(step);
            } else {
                // step does not exist, insert step
                appDatabase.stepDao().insert(step);
            }
        }
    }

    @Transaction
    public void saveMyConcernList(long stepAutoId, List<Concern> concernList) {
        if (concernList == null)
            return;

        for (Concern concern : concernList) {
            // establish foreign key
            concern.setStepAutoId(stepAutoId);

            // find concern in db
            Concern dbConcern = appDatabase.concernDao().load(
                    concern.getPivot().getId(),
                    concern.getPivot().getInspectionId(),
                    concern.getPivot().getSuperGroupId(),
                    concern.getPivot().getStepGroupId(),
                    concern.getPivot().getStepId(),
                    concern.getPivot().getConcernId());

            if (dbConcern != null) {
                // concern exist, update concern
                concern.setAutoId(dbConcern.getAutoId());
                appDatabase.concernDao().update(concern);
            } else {
                // concern does not exist, insert concern
                appDatabase.concernDao().insert(concern);
            }
        }
    }

    @Transaction
    public void saveMyRecommendationList(long concernAutoId, List<Recommendation> recommendationList) {
        if (recommendationList == null)
            return;

        for (Recommendation recommendation : recommendationList) {
            // establish foreign key
            recommendation.setConcernAutoId(concernAutoId);

            // find recommendation in db
            Recommendation dbRecommendation = appDatabase.recommendationDao().load(
                    recommendation.getPivot().getId(),
                    recommendation.getPivot().getInspectionId(),
                    recommendation.getPivot().getSuperGroupId(),
                    recommendation.getPivot().getStepGroupId(),
                    recommendation.getPivot().getStepId(),
                    recommendation.getPivot().getConcernId(),
                    recommendation.getPivot().getRecommendationId());

            if (dbRecommendation != null) {
                // recommendation exist, update recommendation
                recommendation.setAutoId(dbRecommendation.getAutoId());
                appDatabase.recommendationDao().update(recommendation);
            } else {
                // recommendation does not exist, insert recommendation
                appDatabase.recommendationDao().insert(recommendation);
            }
        }
    }

    @Transaction
    public void saveMyPartList(long recommendationAutoId, List<Part> partList) {
        if (partList == null)
            return;

        for (Part part : partList) {
            // establish foreign key
            part.setRecommendationAutoId(recommendationAutoId);

            // find part in db
            Part dbPart = appDatabase.partDao().load(
                    part.getPivot().getId(),
                    part.getPivot().getInspectionId(),
                    part.getPivot().getSuperGroupId(),
                    part.getPivot().getStepGroupId(),
                    part.getPivot().getStepId(),
                    part.getPivot().getConcernId(),
                    part.getPivot().getRecommendationId(),
                    part.getPivot().getPartId());

            if (dbPart != null) {
                // part exist, update part
                part.setAutoId(dbPart.getAutoId());
                appDatabase.partDao().update(part);
            } else {
                // part does not exist, insert part
                appDatabase.partDao().insert(part);
            }
        }
    }*/
}
