package com.techwin.vcite.data.beans;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TemplateBean {

    @SerializedName("citTempList")
    private List<CitTempBean> citTempList;

    public List<CitTempBean> getCitTempList() {
        return citTempList;
    }

    public void setCitTempList(List<CitTempBean> citTempList) {
        this.citTempList = citTempList;
    }

}
