package com.techwin.vcite.data.local.dao;


import androidx.room.Dao;
import androidx.room.Query;

import com.techwin.vcite.data.local.entity.TicketBean;

import java.util.List;

@Dao
public abstract class TicketBeanDao extends BaseDao<TicketBean> {
    @Query("DELETE FROM " + TicketBean.TABLE_NAME + " WHERE cit_no =:citationNo AND org_no=:orgNo")
    public abstract int deleteAll(String citationNo, String orgNo);

    @Query("DELETE FROM " + TicketBean.TABLE_NAME)
    public abstract int deleteAll();

    @Query("SELECT * FROM " + TicketBean.TABLE_NAME + " WHERE cit_no =:citationNo AND org_no=:orgNo")
    public abstract List<TicketBean> getList(String citationNo, String orgNo);
}
