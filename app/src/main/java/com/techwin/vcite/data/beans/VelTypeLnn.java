package com.techwin.vcite.data.beans;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VelTypeLnn {

    @SerializedName("vehtAbbr")
    public List<VehtAbbrBean> vehtAbbr;
    @SerializedName("pltTyp")
    public List<PlateType> pltTyp;

    public static class VehtAbbrBean {
        @SerializedName("VehKey")
        public int vehKey;
        @SerializedName("Name_Abbr")
        public String nameAbbr;
        @SerializedName("Name")
        public String name;
        @SerializedName("Abbreviation")
        public String abbreviation;
        public boolean checked;
    }


}
