package com.techwin.vcite.data.remote.retrofit;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static RetrofitClient retrofitClient = new RetrofitClient();
    private Map<String, Retrofit> retrofitMap;
    private final int REQUEST_TIMEOUT = 120;

    public static RetrofitClient getInstance() {
        return retrofitClient;
    }

    private RetrofitClient() {
        retrofitMap = new HashMap<>();
    }

    public Retrofit getClient(String baseUrl) {
        if (retrofitMap.containsKey(baseUrl)) {
            return retrofitMap.get(baseUrl);
        } else {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(initOkHttp())
                    .build();
            retrofitMap.put(baseUrl, retrofit);
            return retrofit;
        }
    }

    private OkHttpClient initOkHttp() {
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient().newBuilder()
                .connectTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS);

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClientBuilder.addInterceptor(logging);  // <-
        // add logging as last interceptor
        httpClientBuilder.addInterceptor(logging);  // <-- this is the important line!
        return httpClientBuilder.build();
    }
}
