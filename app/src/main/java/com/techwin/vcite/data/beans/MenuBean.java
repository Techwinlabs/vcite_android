package com.techwin.vcite.data.beans;


import androidx.annotation.DrawableRes;

public class MenuBean {
    public String title;
    @DrawableRes
    public int icon;
    public String id;

    public MenuBean(@DrawableRes int icon, String title, String id) {
        this.title = title;
        this.icon = icon;
        this.id = id;
    }

}
