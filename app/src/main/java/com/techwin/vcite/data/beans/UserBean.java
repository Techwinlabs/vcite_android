package com.techwin.vcite.data.beans;

import com.google.gson.annotations.SerializedName;

public class UserBean {

    /**
     * UserKey : 1011
     * DocKey : 1301
     * UserID : LynnTest
     * CustKey : 39
     * CustId : Lynn
     * DNSName : lynnma.vciteplus.com
     */

    @SerializedName("UserKey")
    public int UserKey;
    @SerializedName("DocKey")
    public int DocKey;
    @SerializedName("UserID")
    public String UserID;
    @SerializedName("CustKey")
    public int CustKey;
    @SerializedName("CustId")
    public String CustId;
    @SerializedName("DNSName")
    public String DNSName;
    public String ClientLogo;
    public String OfficerID;

}
