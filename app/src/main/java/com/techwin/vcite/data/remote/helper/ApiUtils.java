package com.techwin.vcite.data.remote.helper;

import androidx.annotation.NonNull;

import com.techwin.vcite.data.beans.base.UploadBean;
import com.techwin.vcite.data.remote.retrofit.CountingRequestBody;

import java.io.File;

import io.reactivex.FlowableEmitter;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ApiUtils {


    public static MultipartBody.Part createMultipartBody(@NonNull String key, @NonNull String filePath, FlowableEmitter<Resource<UploadBean>> emitter) {
        File file = new File(filePath);
        return MultipartBody.Part.createFormData(key, file.getName(), createCountingRequestBody(file, emitter));
    }


    public static MultipartBody.Part createMultipartBody(@NonNull String key, byte[] bytes, FlowableEmitter<Resource<UploadBean>> emitter) {
        return MultipartBody.Part.createFormData(key,"file", createCountingRequestBody(bytes, emitter));
    }

    private static RequestBody createCountingRequestBody(File file, final FlowableEmitter<Resource<UploadBean>> emitter) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);

        return new CountingRequestBody(requestBody, (bytesWritten, contentLength) ->
                emitter.onNext(Resource.loading(new UploadBean(bytesWritten, contentLength))));


    }

    private static RequestBody createCountingRequestBody(byte[] bytes, final FlowableEmitter<Resource<UploadBean>> emitter) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), bytes);

        return new CountingRequestBody(requestBody, (bytesWritten, contentLength) ->
                emitter.onNext(Resource.loading(new UploadBean(bytesWritten, contentLength))));


    }

}
