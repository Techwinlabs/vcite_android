package com.techwin.vcite.data.local.entity;

import static com.techwin.vcite.data.local.entity.CitationBean.TABLE_NAME;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;

import java.util.ArrayList;
import java.util.Date;

@Entity(tableName = TABLE_NAME, primaryKeys = {"cit_no", "org_no"})
public class CitationBean implements Parcelable {
    public final static String TABLE_NAME = "Citation";

    @NonNull
    @ColumnInfo(name = "cit_no")
    private String citationNo;
    @NonNull
    @ColumnInfo(name = "org_no")
    private String orgNo;

    @ColumnInfo(name = "plate_type")
    private String plateType;
    @ColumnInfo(name = "veh_type")
    private String vehType;
    @ColumnInfo(name = "veh_model")
    private String vehModel;
    @ColumnInfo(name = "clerk_id")
    private String clerkId;
    @ColumnInfo(name = "division_id")
    private String divisionId;
    @ColumnInfo(name = "officer_id")
    private String officerId;

    @ColumnInfo(name = "licence_no")
    private String licenceNo;
    @ColumnInfo(name = "issuance")
    private String issuance;
    @ColumnInfo(name = "expiry_date")
    private long expiryDate;
    private String vin;
    @ColumnInfo(name = "citation_type")
    private String citationType;
    private boolean uploaded;
    private String color;
    private String make;
    private String plateColor;
    private Date date;
    private Date createdOn;
    private Date pdfSentDate;
    private boolean pdfSent;
    private boolean pdfCreated;
    @ColumnInfo(name = "street_no")
    private String streetNo;
    @ColumnInfo(name = "street_name")
    private String streetName;
    @ColumnInfo(name = "street_zip")
    private String streetZip;
    @ColumnInfo(name = "estab")
    private String estab;
    @ColumnInfo(name = "unit")
    private String unit;
    @ColumnInfo(name = "remark")
    private String remarks;
    @ColumnInfo(name = "parcel")
    private String parcelid;
    @ColumnInfo(name = "meterno")
    private String meterno;

    @ColumnInfo(name = "cust4")
    private String cust4;

    @ColumnInfo(name = "cust5")
    private String cust5;


    private String state;
    private int dockey;
    private int statuskey;
    private int inituserkey;
    private String serialnumber;
    private String location;
    private ArrayList<ViolationBean> violation;
    private String comment;

    private boolean isvoid;
    private boolean warning;

    public String getClerkId() {
        return clerkId;
    }

    public void setClerkId(String clerkId) {
        this.clerkId = clerkId;
    }

    public String getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(String divisionId) {
        this.divisionId = divisionId;
    }

    public String getOfficerId() {
        return officerId;
    }

    public void setOfficerId(String officerId) {
        this.officerId = officerId;
    }

    private double latitude;
    private double Longitude;

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getPdfSentDate() {
        return pdfSentDate;
    }

    public void setPdfSentDate(Date pdfSentDate) {
        this.pdfSentDate = pdfSentDate;
    }

    public boolean isPdfSent() {
        return pdfSent;
    }

    public boolean isPdfCreated() {
        return pdfCreated;
    }

    public void setPdfCreated(boolean pdfCreated) {
        this.pdfCreated = pdfCreated;
    }

    public void setPdfSent(boolean pdfSent) {
        this.pdfSent = pdfSent;
    }

    public String getCitationType() {
        return citationType;
    }

    public void setCitationType(String citationType) {
        this.citationType = citationType;
    }

    public String getVehType() {
        return vehType;
    }

    public void setVehType(String vehType) {
        this.vehType = vehType;
    }

    public String getVehModel() {
        return vehModel;
    }

    public void setVehModel(String vehModel) {
        this.vehModel = vehModel;
    }

    public boolean isUploaded() {
        return uploaded;
    }

    public String getCust4() {
        return cust4;
    }

    public void setCust4(String cust4) {
        this.cust4 = cust4;
    }

    public String getCust5() {
        return cust5;
    }

    public void setCust5(String cust5) {
        this.cust5 = cust5;
    }

    public void setUploaded(boolean uploaded) {
        this.uploaded = uploaded;
    }

    public String getPlateColor() {
        return plateColor;
    }

    public void setPlateColor(String plateColor) {
        this.plateColor = plateColor;
    }

    public String getPlateType() {
        return plateType;
    }

    public void setPlateType(String plateType) {
        this.plateType = plateType;
    }

    public String getLicenceNo() {
        return licenceNo;
    }

    public void setLicenceNo(String licenceNo) {
        this.licenceNo = licenceNo;
    }

    public String getIssuance() {
        return issuance;
    }

    public void setIssuance(String issuance) {
        this.issuance = issuance;
    }

    public long getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(long expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }


    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


    public CitationBean() {
    }

    public String getParcelid() {
        return parcelid;
    }

    public void setParcelid(String parcelid) {
        this.parcelid = parcelid;
    }

    public String getMeterno() {
        return meterno;
    }

    public void setMeterno(String meterno) {
        this.meterno = meterno;
    }

    public String getCitationNo() {
        return citationNo;
    }

    public void setCitationNo(String citationNo) {
        this.citationNo = citationNo;
    }

    public String getOrgNo() {
        return orgNo;
    }

    public void setOrgNo(String orgNo) {
        this.orgNo = orgNo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getDockey() {
        return dockey;
    }

    public void setDockey(int dockey) {
        this.dockey = dockey;
    }

    public int getStatuskey() {
        return statuskey;
    }

    public void setStatuskey(int statuskey) {
        this.statuskey = statuskey;
    }

    public int getInituserkey() {
        return inituserkey;
    }

    public void setInituserkey(int inituserkey) {
        this.inituserkey = inituserkey;
    }

    public String getSerialnumber() {
        return serialnumber;
    }

    public void setSerialnumber(String serialnumber) {
        this.serialnumber = serialnumber;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public ArrayList<ViolationBean> getViolation() {
        return violation;
    }


    public void setViolation(ArrayList<ViolationBean> violation) {
        this.violation = violation;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isIsvoid() {
        return isvoid;
    }

    public void setIsvoid(boolean isvoid) {
        this.isvoid = isvoid;
    }

    public boolean isWarning() {
        return warning;
    }

    public void setWarning(boolean warning) {
        this.warning = warning;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double longitude) {
        Longitude = longitude;
    }

    public String getStreetNo() {
        return streetNo;
    }

    public void setStreetNo(String streetNo) {
        this.streetNo = streetNo;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getStreetZip() {
        return streetZip;
    }

    public void setStreetZip(String streetZip) {
        this.streetZip = streetZip;
    }

    public String getEstab() {
        return estab;
    }

    public void setEstab(String estab) {
        this.estab = estab;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.citationNo);
        dest.writeString(this.orgNo);
        dest.writeString(this.plateType);
        dest.writeString(this.vehType);
        dest.writeString(this.vehModel);
        dest.writeString(this.clerkId);
        dest.writeString(this.divisionId);
        dest.writeString(this.officerId);
        dest.writeString(this.licenceNo);
        dest.writeString(this.issuance);
        dest.writeLong(this.expiryDate);
        dest.writeString(this.vin);
        dest.writeString(this.citationType);
        dest.writeByte(this.uploaded ? (byte) 1 : (byte) 0);
        dest.writeString(this.color);
        dest.writeString(this.make);
        dest.writeString(this.plateColor);
        dest.writeLong(this.date != null ? this.date.getTime() : -1);
        dest.writeLong(this.createdOn != null ? this.createdOn.getTime() : -1);
        dest.writeLong(this.pdfSentDate != null ? this.pdfSentDate.getTime() : -1);
        dest.writeByte(this.pdfSent ? (byte) 1 : (byte) 0);
        dest.writeByte(this.pdfCreated ? (byte) 1 : (byte) 0);
        dest.writeString(this.streetNo);
        dest.writeString(this.streetName);
        dest.writeString(this.streetZip);
        dest.writeString(this.estab);
        dest.writeString(this.unit);
        dest.writeString(this.remarks);
        dest.writeString(this.parcelid);
        dest.writeString(this.meterno);
        dest.writeString(this.cust4);
        dest.writeString(this.cust5);
        dest.writeString(this.state);
        dest.writeInt(this.dockey);
        dest.writeInt(this.statuskey);
        dest.writeInt(this.inituserkey);
        dest.writeString(this.serialnumber);
        dest.writeString(this.location);
        dest.writeTypedList(this.violation);
        dest.writeString(this.comment);
        dest.writeByte(this.isvoid ? (byte) 1 : (byte) 0);
        dest.writeByte(this.warning ? (byte) 1 : (byte) 0);
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.Longitude);
    }

    public void readFromParcel(Parcel source) {
        this.citationNo = source.readString();
        this.orgNo = source.readString();
        this.plateType = source.readString();
        this.vehType = source.readString();
        this.vehModel = source.readString();
        this.clerkId = source.readString();
        this.divisionId = source.readString();
        this.officerId = source.readString();
        this.licenceNo = source.readString();
        this.issuance = source.readString();
        this.expiryDate = source.readLong();
        this.vin = source.readString();
        this.citationType = source.readString();
        this.uploaded = source.readByte() != 0;
        this.color = source.readString();
        this.make = source.readString();
        this.plateColor = source.readString();
        long tmpDate = source.readLong();
        this.date = tmpDate == -1 ? null : new Date(tmpDate);
        long tmpCreatedOn = source.readLong();
        this.createdOn = tmpCreatedOn == -1 ? null : new Date(tmpCreatedOn);
        long tmpPdfSentDate = source.readLong();
        this.pdfSentDate = tmpPdfSentDate == -1 ? null : new Date(tmpPdfSentDate);
        this.pdfSent = source.readByte() != 0;
        this.pdfCreated = source.readByte() != 0;
        this.streetNo = source.readString();
        this.streetName = source.readString();
        this.streetZip = source.readString();
        this.estab = source.readString();
        this.unit = source.readString();
        this.remarks = source.readString();
        this.parcelid = source.readString();
        this.meterno = source.readString();
        this.cust4 = source.readString();
        this.cust5 = source.readString();
        this.state = source.readString();
        this.dockey = source.readInt();
        this.statuskey = source.readInt();
        this.inituserkey = source.readInt();
        this.serialnumber = source.readString();
        this.location = source.readString();
        this.violation = source.createTypedArrayList(ViolationBean.CREATOR);
        this.comment = source.readString();
        this.isvoid = source.readByte() != 0;
        this.warning = source.readByte() != 0;
        this.latitude = source.readDouble();
        this.Longitude = source.readDouble();
    }

    protected CitationBean(Parcel in) {
        this.citationNo = in.readString();
        this.orgNo = in.readString();
        this.plateType = in.readString();
        this.vehType = in.readString();
        this.vehModel = in.readString();
        this.clerkId = in.readString();
        this.divisionId = in.readString();
        this.officerId = in.readString();
        this.licenceNo = in.readString();
        this.issuance = in.readString();
        this.expiryDate = in.readLong();
        this.vin = in.readString();
        this.citationType = in.readString();
        this.uploaded = in.readByte() != 0;
        this.color = in.readString();
        this.make = in.readString();
        this.plateColor = in.readString();
        long tmpDate = in.readLong();
        this.date = tmpDate == -1 ? null : new Date(tmpDate);
        long tmpCreatedOn = in.readLong();
        this.createdOn = tmpCreatedOn == -1 ? null : new Date(tmpCreatedOn);
        long tmpPdfSentDate = in.readLong();
        this.pdfSentDate = tmpPdfSentDate == -1 ? null : new Date(tmpPdfSentDate);
        this.pdfSent = in.readByte() != 0;
        this.pdfCreated = in.readByte() != 0;
        this.streetNo = in.readString();
        this.streetName = in.readString();
        this.streetZip = in.readString();
        this.estab = in.readString();
        this.unit = in.readString();
        this.remarks = in.readString();
        this.parcelid = in.readString();
        this.meterno = in.readString();
        this.cust4 = in.readString();
        this.cust5 = in.readString();
        this.state = in.readString();
        this.dockey = in.readInt();
        this.statuskey = in.readInt();
        this.inituserkey = in.readInt();
        this.serialnumber = in.readString();
        this.location = in.readString();
        this.violation = in.createTypedArrayList(ViolationBean.CREATOR);
        this.comment = in.readString();
        this.isvoid = in.readByte() != 0;
        this.warning = in.readByte() != 0;
        this.latitude = in.readDouble();
        this.Longitude = in.readDouble();
    }

    public static final Parcelable.Creator<CitationBean> CREATOR = new Parcelable.Creator<CitationBean>() {
        @Override
        public CitationBean createFromParcel(Parcel source) {
            return new CitationBean(source);
        }

        @Override
        public CitationBean[] newArray(int size) {
            return new CitationBean[size];
        }
    };
}
