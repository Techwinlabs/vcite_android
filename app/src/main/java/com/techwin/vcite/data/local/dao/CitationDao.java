package com.techwin.vcite.data.local.dao;


import androidx.room.Dao;
import androidx.room.Query;

import com.techwin.vcite.data.local.entity.CitationBean;

import java.util.Date;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Maybe;

@Dao
public abstract class CitationDao extends BaseDao<CitationBean> {
    @Query("DELETE FROM " + CitationBean.TABLE_NAME + " WHERE org_no=:orgNo ")
    public abstract int deleteAll(String orgNo);

    @Query("DELETE FROM " + CitationBean.TABLE_NAME + " WHERE uploaded = :uploaded AND createdOn<:createdon")
    abstract void deleteByDate(boolean uploaded, Date createdon);

    @Query("SELECT * FROM " + CitationBean.TABLE_NAME + " WHERE uploaded=:uploaded  AND org_no=:orgNo ORDER BY date DESC")
    public abstract List<CitationBean> loadAllCitation(boolean uploaded, String orgNo);

    @Query("SELECT * FROM " + CitationBean.TABLE_NAME + " WHERE org_no=:orgNo ORDER BY date DESC")
    public abstract Flowable<List<CitationBean>> loadAllCitationFlowable(String orgNo);

    @Query("SELECT * FROM " + CitationBean.TABLE_NAME + " WHERE cit_no =:citationNo AND org_no=:orgNo AND citation_type=:citation_type ")
    public abstract CitationBean loadCitation(String citationNo, String orgNo, String citation_type);

    @Query("SELECT * FROM " + CitationBean.TABLE_NAME + " WHERE cit_no =:citationNo AND org_no=:orgNo")
    public abstract Maybe<CitationBean> getCitation(String citationNo, String orgNo);


}
