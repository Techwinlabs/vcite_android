package com.techwin.vcite.data.beans;

import com.google.gson.annotations.SerializedName;

public class CitTempBean {
    /**
     * custkey : 39
     * MyProperty : 0
     * citiationType : Parking
     * fieldID : VehLicense
     * fieldName : Vehicle License
     * includeField : true
     * fieldType : Text
     * mandatory : true
     * fieldValue :
     * SequenceNo : 3
     */

    @SerializedName("custkey")
    private int custkey;
    @SerializedName("MyProperty")
    private int MyProperty;
    @SerializedName("citiationType")
    private String citiationType;
    @SerializedName("fieldID")
    private String fieldID;
    @SerializedName("fieldName")
    private String fieldName;
    @SerializedName("includeField")
    private boolean includeField;
    @SerializedName("fieldType")
    private String fieldType;
    @SerializedName("mandatory")
    private boolean mandatory;

    @SerializedName("fieldValue")
    private String fieldValue;
    @SerializedName("SequenceNo")
    private int SequenceNo;

    public int getCustkey() {
        return custkey;
    }

    public void setCustkey(int custkey) {
        this.custkey = custkey;
    }

    public int getMyProperty() {
        return MyProperty;
    }

    public void setMyProperty(int MyProperty) {
        this.MyProperty = MyProperty;
    }

    public String getCitiationType() {
        return citiationType;
    }

    public void setCitiationType(String citiationType) {
        this.citiationType = citiationType;
    }

    public String getFieldID() {
        return fieldID;
    }

    public void setFieldID(String fieldID) {
        this.fieldID = fieldID;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public boolean isIncludeField() {
        return includeField;
    }

    public void setIncludeField(boolean includeField) {
        this.includeField = includeField;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public int getSequenceNo() {
        return SequenceNo;
    }

    public void setSequenceNo(int SequenceNo) {
        this.SequenceNo = SequenceNo;
    }

    @Override
    public String toString() {
        return "CitTempBean{" +
                "custkey=" + custkey +
                ", MyProperty=" + MyProperty +
                ", citiationType='" + citiationType + '\'' +
                ", fieldID='" + fieldID + '\'' +
                ", fieldName='" + fieldName + '\'' +
                ", includeField=" + includeField +
                ", fieldType='" + fieldType + '\'' +
                ", mandatory=" + mandatory +
                ", fieldValue='" + fieldValue + '\'' +
                ", SequenceNo=" + SequenceNo +
                '}';
    }
}
