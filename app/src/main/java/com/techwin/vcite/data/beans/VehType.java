package com.techwin.vcite.data.beans;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VehType {

    @SerializedName("vehtTyp")
    private List<VehtTypDTO> vehtTyp;

    public List<VehtTypDTO> getVehtTyp() {
        return vehtTyp;
    }

    public void setVehtTyp(List<VehtTypDTO> vehtTyp) {
        this.vehtTyp = vehtTyp;
    }

    public static class VehtTypDTO {
        /**
         * VehTypeKey : 3
         * Name : 4 Door
         * Abbreviation : 4DR
         */

        @SerializedName("VehTypeKey")
        private int VehTypeKey;
        @SerializedName("Name")
        private String Name;
        @SerializedName("Abbreviation")
        private String Abbreviation;
        public boolean checked;

        public int getVehTypeKey() {
            return VehTypeKey;
        }

        public void setVehTypeKey(int VehTypeKey) {
            this.VehTypeKey = VehTypeKey;
        }

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public String getAbbreviation() {
            return Abbreviation;
        }

        public void setAbbreviation(String Abbreviation) {
            this.Abbreviation = Abbreviation;
        }
    }
}

