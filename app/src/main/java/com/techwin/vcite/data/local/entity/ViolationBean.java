package com.techwin.vcite.data.local.entity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import static com.techwin.vcite.data.local.entity.ViolationBean.TABLE_NAME;

@Entity(tableName = TABLE_NAME)
public class ViolationBean implements Parcelable {


    /**
     * id : 21609
     * description : Meter Violations
     * violation_id : A01
     */
    public final static String TABLE_NAME = "Violation";
    @PrimaryKey
    @SerializedName("id")
    private long id;
    @ColumnInfo(name = "description")
    @SerializedName("description")
    private String description;
    @ColumnInfo(name = "violation_id")
    @SerializedName("violation_id")
    private String violationId;

    @Ignore
    private boolean checked;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getViolationId() {
        return violationId;
    }

    public void setViolationId(String violationId) {
        this.violationId = violationId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.description);
        dest.writeString(this.violationId);
    }

    public ViolationBean() {
    }

    protected ViolationBean(Parcel in) {
        this.id = in.readLong();
        this.description = in.readString();
        this.violationId = in.readString();
    }

    public static final Parcelable.Creator<ViolationBean> CREATOR = new Parcelable.Creator<ViolationBean>() {
        @Override
        public ViolationBean createFromParcel(Parcel source) {
            return new ViolationBean(source);
        }

        @Override
        public ViolationBean[] newArray(int size) {
            return new ViolationBean[size];
        }
    };
}
