package com.techwin.vcite.data.local.entity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import static com.techwin.vcite.data.local.entity.MediaBean.TABLE_NAME;

import com.techwin.vcite.util.Constants;

@Entity(tableName = TABLE_NAME)
public class MediaBean implements Parcelable {
    public final static String TABLE_NAME = "MediaBean";

    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "cit_no")
    private String citationNo;

    @ColumnInfo(name = "org_no")
    private String orgNo;

    private String image;
    @ColumnInfo(name = "pic_type")
    private int picType= Constants.PIC_TYPE_NORMAL;

    @Ignore
    private Integer itemtype;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCitationNo() {
        return citationNo;
    }

    public void setCitationNo(String citationNo) {
        this.citationNo = citationNo;
    }

    public String getOrgNo() {
        return orgNo;
    }

    public void setOrgNo(String orgNo) {
        this.orgNo = orgNo;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getItemtype() {
        return itemtype;
    }

    public void setItemtype(Integer itemtype) {
        this.itemtype = itemtype;
    }


    public int getPicType() {
        return picType;
    }

    public void setPicType(int picType) {
        this.picType = picType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.citationNo);
        dest.writeString(this.orgNo);
        dest.writeString(this.image);
        dest.writeInt(this.picType);
        dest.writeValue(this.itemtype);
    }

    public void readFromParcel(Parcel source) {
        this.id = source.readLong();
        this.citationNo = source.readString();
        this.orgNo = source.readString();
        this.image = source.readString();
        this.picType = source.readInt();
        this.itemtype = (Integer) source.readValue(Integer.class.getClassLoader());
    }

    public MediaBean() {
    }

    protected MediaBean(Parcel in) {
        this.id = in.readLong();
        this.citationNo = in.readString();
        this.orgNo = in.readString();
        this.image = in.readString();
        this.picType = in.readInt();
        this.itemtype = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<MediaBean> CREATOR = new Creator<MediaBean>() {
        @Override
        public MediaBean createFromParcel(Parcel source) {
            return new MediaBean(source);
        }

        @Override
        public MediaBean[] newArray(int size) {
            return new MediaBean[size];
        }
    };
}
