package com.techwin.vcite.data.local.database.convertor;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.techwin.vcite.data.local.entity.ViolationBean;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TypeConvertor {


    @TypeConverter
    public static List<String> stringToList(String json) {
        try {
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<String>>() {
            }.getType();
            return gson.fromJson(json, type);
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @TypeConverter
    public static String ListToString(List<String> list) {
        if (list == null)
            list = new ArrayList<>();
        Gson gson = new Gson();
        return gson.toJson(list);
    }

    @TypeConverter
    public static ArrayList<ViolationBean> stringToViolation(String json) {
        try {
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<ViolationBean>>() {
            }.getType();
            return gson.fromJson(json, type);
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @TypeConverter
    public static String ViolationToString(ArrayList<ViolationBean> list) {
        if (list == null)
            list = new ArrayList<>();
        Gson gson = new Gson();
        return gson.toJson(list);
    }


    @TypeConverter
    public static long dateToString(Date date) {
        if (date == null)
            return 0;
        return date.getTime();
    }

    @TypeConverter
    public static Date dateToString(long date) {
        if (date == 0)
            return null;
        return new Date(date);
    }
}
