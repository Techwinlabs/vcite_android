package com.techwin.vcite.data.local.entity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;

import java.util.Date;

import static com.techwin.vcite.data.local.entity.TicketBean.TABLE_NAME;


@Entity(tableName = TABLE_NAME, primaryKeys = {"cit_no", "org_no"})
public class TicketBean implements Parcelable {
    public final static String TABLE_NAME = "TicketBean";

    @NonNull
    @ColumnInfo(name = "cit_no")
    private String citationNo;
    @NonNull
    @ColumnInfo(name = "org_no")
    private String orgNo;
    @ColumnInfo(name = "type")
    private String ticketType;
    private Date createdOn;

    public TicketBean() {
    }

    @NonNull
    public String getCitationNo() {
        return citationNo;
    }

    public void setCitationNo(@NonNull String citationNo) {
        this.citationNo = citationNo;
    }

    @NonNull
    public String getOrgNo() {
        return orgNo;
    }

    public void setOrgNo(@NonNull String orgNo) {
        this.orgNo = orgNo;
    }

    public String getTicketType() {
        return ticketType;
    }

    public void setTicketType(String ticketType) {
        this.ticketType = ticketType;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.citationNo);
        dest.writeString(this.orgNo);
        dest.writeString(this.ticketType);
        dest.writeLong(this.createdOn != null ? this.createdOn.getTime() : -1);

    }

    protected TicketBean(Parcel in) {
        this.citationNo = in.readString();
        this.orgNo = in.readString();
        this.ticketType = in.readString();
        long tmpDate = in.readLong();
        this.createdOn = tmpDate == -1 ? null : new Date(tmpDate);
    }

    public static final Creator<TicketBean> CREATOR = new Creator<TicketBean>() {
        @Override
        public TicketBean createFromParcel(Parcel source) {
            return new TicketBean(source);
        }

        @Override
        public TicketBean[] newArray(int size) {
            return new TicketBean[size];
        }
    };
}
