package com.techwin.vcite.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.provider.Settings;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.textfield.TextInputLayout;
import com.techwin.vcite.data.beans.CitTempBean;
import com.techwin.vcite.data.local.entity.CitationBean;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Arvind Poonia on 11/26/2018.
 */
public class AppUtils {

    public static void setTypefaceToInputLayout(TextInputLayout inputLayout, Typeface typeFace) {
        try {
            inputLayout.getEditText().setTypeface(typeFace);

            // Retrieve the CollapsingTextHelper Field
            final Field collapsingTextHelperField = inputLayout.getClass().getDeclaredField("mCollapsingTextHelper");
            collapsingTextHelperField.setAccessible(true);

            // Retrieve an instance of CollapsingTextHelper and its TextPaint
            final Object collapsingTextHelper = collapsingTextHelperField.get(inputLayout);
            final Field tpf = collapsingTextHelper.getClass().getDeclaredField("mTextPaint");
            tpf.setAccessible(true);

            // Apply your Typeface to the CollapsingTextHelper TextPaint
            ((TextPaint) tpf.get(collapsingTextHelper)).setTypeface(typeFace);
        } catch (Exception ignored) {
            // Nothing to do
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public static String getTimeFromUtc(long l) {
        if (l == 0)
            return "";
        try {
            return new SimpleDateFormat("hh:mm a", Locale.getDefault()).format(new Date(l));
        } catch (Exception e) {
            return "";
        }
    }


    public static String getTimeStamp(Date l) {
        if (l == null)
            return "";
        try {
            Calendar current = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

            Calendar date = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            date.setTime(l);

            if (current.get(Calendar.DAY_OF_YEAR) == date.get(Calendar.DAY_OF_YEAR))
                return new SimpleDateFormat("hh:mm a", Locale.getDefault()).format(date.getTime());
            else
                return new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.getDefault()).format(date.getTime());

        } catch (Exception e) {
            return "";
        }
    }

    public static String getTimeWithFormatFromUtc(long l, String format) {
        if (l == 0)
            return "";
        try {
            return new SimpleDateFormat(format, Locale.getDefault()).format(new Date(l));
        } catch (Exception e) {
            return "";
        }
    }

    public static String getExpiryDate(Date date) {
        if (date == null)
            return "";
        try {
            return new SimpleDateFormat("MMM,yyyy", Locale.US).format(date);
        } catch (Exception e) {
            return "";
        }
    }

    public static String getExpiryDate(@Nullable Long date) {
        if (date != null && date > 0) {
            try {
                return new SimpleDateFormat("MMM,yyyy", Locale.US).format(date);
            } catch (Exception e) {
                return "";
            }
        }
        return "";
    }


    public static String getCurrencyFormat(double amount) {
        try {
            // NumberFormat us = NumberFormat.getCurrencyInstance(Locale.US);
            return String.valueOf(amount);
        } catch (Exception e) {
            return "Error";
        }

    }


    public static String getHeaderAgodate(long s) {
        Calendar now = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        Calendar time = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        time.setTimeInMillis(s);
        StringBuilder builder = new StringBuilder();
        if (now.get(Calendar.DATE) == time.get(Calendar.DATE)) {
            builder.append("Today");
        } else if (now.get(Calendar.DATE) - time.get(Calendar.DATE) == 1) {
            builder.append("Yesterday");
        } else
            builder.append(new SimpleDateFormat("dd MMM yyyy", Locale.getDefault()).format(new Date(s)));
        return builder.toString();
    }


    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);

            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.showSoftInput(activity.getCurrentFocus(), InputMethodManager.SHOW_IMPLICIT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static synchronized int convertDpToPixel(float dp) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return Math.round(px);
    }

    public static synchronized int convertPxTodp(int px) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return Math.round(dp);
    }

    @SuppressLint("HardwareIds")
    public static String getAndroidId(@NonNull Context context) {
        try {
            return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        } catch (Exception e) {
            return "";
        }

    }


    public static Bitmap createBitmapFromView(@NonNull Activity context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }


    public static String requestStatus(Integer s) {
        if (s == null)
            return "";
        switch (s) {
            case 1:
                return "Accepted";
            case 2:
                return "Rejected";
            default:
                return "Pending";
        }
    }

    /*public static String getImageUrl(String url) {
            if (url == null)
                return "";
            return Constants.MEDIA_URL + url;
        }*/

    public static String imagetoBase64(String filePath) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Bitmap bitmap = BitmapFactory.decodeFile(filePath);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
        byte[] bytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(bytes, Base64.NO_WRAP);
    }

    public static String imagetoBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
        byte[] bytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(bytes, Base64.NO_WRAP);
    }

    public static Bitmap base64ToImage(String encodedImage) {
        try {
            byte[] decodedString = Base64.decode(encodedImage, Base64.NO_WRAP);
            return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String convertArrayToString(List<String> strings) {
        if (strings != null && strings.size() > 0) {
            StringBuilder stringBuilder = new StringBuilder(" [");
            for (int i = 0; i < strings.size(); i++) {
                stringBuilder.append(strings.get(i));
                if (i < strings.size() - 1)
                    stringBuilder.append(", ");
            }
            stringBuilder.append("]");
            return stringBuilder.toString();
        }
        return null;
    }

    public static List<String> missingEnforcement(CitationBean citationBean, List<CitTempBean> templateBeans) {
        List<String> missingField = new ArrayList<>();
        if (citationBean == null)
            return missingField;
        if (citationBean.getViolation().size() == 0) {
            missingField.add("Violation");
        }
        return missingField;
    }

    public static List<String> missing(CitationBean citationBean, List<CitTempBean> templateBeans) {
        List<String> missingField = new ArrayList<>();
        if (citationBean == null)
            return missingField;
        boolean addLiorvin = true;
        for (CitTempBean bean : templateBeans) {
            if (bean.isIncludeField()) {
                final String id = bean.getFieldID().replace("\t\t\r\n", "");
                if (bean.isMandatory()) {
                    if (Constants.fieldID_plateType.equalsIgnoreCase(id)) {
                        if (TextUtils.isEmpty(citationBean.getPlateType()))
                            missingField.add("Plate Type");
                    } else if (Constants.fieldID_platecolor.equalsIgnoreCase(id)) {
                        if (TextUtils.isEmpty(citationBean.getPlateColor()))
                            missingField.add("Plate Color");
                    }/* else if (Constants.fieldID_VehLicense.equalsIgnoreCase(id)) {
                        if (TextUtils.isEmpty(citationBean.getLicenceNo()))
                            missingField.add("License");
                    }*/ else if (Constants.fieldID_VehState.equalsIgnoreCase(id)) {
                        if (TextUtils.isEmpty(citationBean.getState()))
                            missingField.add("State");

                    } else if (Constants.fieldID_ExpDate.equalsIgnoreCase(id)) {
                        if (citationBean.getExpiryDate() == 0)
                            missingField.add("Exp date");

                    }/* else if (Constants.fieldID_VIN.equalsIgnoreCase(id)) {
                        if (TextUtils.isEmpty(citationBean.getVin()))
                            missingField.add("Vin No");
                    } */ else if (Constants.fieldID_VehColor.equalsIgnoreCase(id)) {
                        if (TextUtils.isEmpty(citationBean.getColor()))
                            missingField.add("Color");
                    } else if (Constants.fieldID_VehMake.equalsIgnoreCase(id)) {
                        if (TextUtils.isEmpty(citationBean.getMake()))
                            missingField.add("Make");
                    } else if (Constants.fieldID_VehModel.equalsIgnoreCase(id)) {
                        if (TextUtils.isEmpty(citationBean.getVehModel()))
                            missingField.add("Model");
                    } else if (Constants.fieldID_ClerkID.equalsIgnoreCase(id)) {
                        if (TextUtils.isEmpty(citationBean.getClerkId()))
                            missingField.add("Clerk Id");
                    } else if (Constants.fieldID_OfficerID.equalsIgnoreCase(id)) {
                        if (TextUtils.isEmpty(citationBean.getOfficerId()))
                            missingField.add("Officer Id");
                    } else if (Constants.fieldID_Division.equalsIgnoreCase(id)) {
                        if (TextUtils.isEmpty(citationBean.getDivisionId()))
                            missingField.add("Division");
                    } else if (Constants.fieldID_VehType.equalsIgnoreCase(id)) {
                        if (TextUtils.isEmpty(citationBean.getVehType()))
                            missingField.add("Veh Type");
                    }/* else if (Constants.fieldID_RequireOCR.equalsIgnoreCase(id)) {

                        } else if (Constants.fieldID_RequireOCRVin.equalsIgnoreCase(id)) {

                        }*/ else if (Constants.fieldID_CustField4.equalsIgnoreCase(id)) {
                        if (TextUtils.isEmpty(citationBean.getCust4()))
                            missingField.add("Cust 4");
                    } else if (Constants.fieldID_CustField5.equalsIgnoreCase(id)) {
                        if (TextUtils.isEmpty(citationBean.getCust5()))
                            missingField.add("Cust 5");
                    }


                    if (citationBean.getCitationType().equalsIgnoreCase(Constants.CITATION_TYPE_CODE_ENFORCE)) {

                        /*-------------------------------------*/

                        if (Constants.fieldID_StreetNumber.equals(id)) {
                            if (TextUtils.isEmpty(citationBean.getStreetNo()))
                                missingField.add("Street no");
                        } else if (Constants.fieldID_ZipCode.equals(id)) {
                            if (TextUtils.isEmpty(citationBean.getStreetZip()))
                                missingField.add("Zip");
                        } else if (Constants.fieldID_StreetName.equals(id)) {
                            if (TextUtils.isEmpty(citationBean.getStreetName()))
                                missingField.add("Street");
                        } else if (Constants.fieldID_Establishment.equals(id)) {
                            if (TextUtils.isEmpty(citationBean.getEstab()))
                                missingField.add("Estb");
                        } else if (Constants.fieldID_Unit.equals(id)) {
                            if (TextUtils.isEmpty(citationBean.getUnit()))
                                missingField.add("Unit");
                        } else if (Constants.fieldID_Remarks.equals(id)) {
                            if (TextUtils.isEmpty(citationBean.getRemarks()))
                                missingField.add("Remark");
                        } else if (Constants.fieldID_Parcel.equals(id)) {
                            if (TextUtils.isEmpty(citationBean.getParcelid()))
                                missingField.add("Parcel");
                        }
                    }
                }
                if (Constants.fieldID_VehLicense.equalsIgnoreCase(id)) {
                    if (!TextUtils.isEmpty(citationBean.getLicenceNo()))
                        addLiorvin = false;
                }
                if (Constants.fieldID_VIN.equalsIgnoreCase(id)) {
                    if (!TextUtils.isEmpty(citationBean.getVin()))
                        addLiorvin = false;
                }
            }
        }
        if (addLiorvin) {
            missingField.add("Licence or Vin no");
        }
        return missingField;
    }


    public static String toBase64(String text) {

        try {
            return Base64.encodeToString(text.getBytes("UTF-8"), Base64.DEFAULT);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }

    }


    public static boolean is24hrspassed(Date time) {
       /* Date now = new Date();
        Date hrs24agotime = new Date(now.getTime() - (24 * 60 * 60 * 1000));
        return hrs24agotime.getTime() > time.getTime();*/
        return false;
    }

    public static boolean compareText(String type, String s) {
        if (type == null)
            return false;
        if (s == null)
            return true;
        return type.toLowerCase().contains(s.toLowerCase());

    }

    public static String convertPathToBitmap(String filePath, Activity context) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Bitmap bitmap = BitmapFactory.decodeFile(filePath);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
        return storeImage(bitmap, context);
    }

    public static String storeImage(Bitmap image, Activity context) {
        File pictureFile = getOutputMediaFile(context);
        if (pictureFile == null) {
            Log.d("TAG_RK",
                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return "";
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d("TAG_RK", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("TAG_RK", "Error accessing file: " + e.getMessage());
        }
        return pictureFile.getAbsolutePath();
    }

    private static File getOutputMediaFile(Activity context) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(context.getCacheDir()
                + "/Vcite");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new Date());
        File mediaFile;
        String mImageName = "MI_" + timeStamp + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

}
