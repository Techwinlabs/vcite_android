package com.techwin.vcite.util.dump;

import android.content.Context;
import android.graphics.Bitmap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

public class PdfUtils {
    private Context context;
    @Nullable
    private Callback callback;

    private PdfUtils(Context context) {
        this.context = context;
    }

    public void setCallback(@Nullable Callback callback) {
        this.callback = callback;
    }

    public static PdfUtils newInstance(Context context) {
        return new PdfUtils(context);
    }

    public void makePdf(@NonNull List<Bitmap> list) {
        try {
            Document document = new Document();
            String path = getFilePath();
            PdfWriter.getInstance(document, new FileOutputStream(path));
            document.open();
            for (Bitmap bitmap : list) {
                try {
                    document.add(addImage(bitmap,document));
                } catch (DocumentException e) {
                    if (callback != null)
                        callback.onBitmapError(e.getMessage());
                }
            }
            document.close();
            if (callback != null)
                callback.onSuccess(path);
        } catch (FileNotFoundException | DocumentException e) {
            if (callback != null)
                callback.onError(e.getMessage());
        }
    }

    public String makePdfAsyc(@NonNull List<Bitmap> list) throws Exception {
        Document document = new Document();
        String path = getFilePath();
        PdfWriter.getInstance(document, new FileOutputStream(path));
        document.open();
        for (Bitmap bitmap : list) {
            document.add(addImage(bitmap, document));
        }
        document.close();
        return path;
    }

    private String getFilePath() {
        String filename = "vcite" + System.currentTimeMillis() + ".pdf";
      /*
        File dest = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), filename);
        return dest.getAbsolutePath();*/
        File mediaStorageDir = new File(context.getExternalFilesDir(""), "vcite");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            mediaStorageDir.mkdirs();
        }

        return mediaStorageDir.getAbsolutePath() + "/" + filename;
    }

    @Nullable
    private Image addImage(Bitmap b, Document document) {
        try {
            Bitmap bmp = Bitmap.createScaledBitmap(b, (int) (b.getWidth() * 0.5), (int) (b.getHeight() * 0.5), false);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            Image myImg = Image.getInstance(stream.toByteArray());
            myImg.scaleToFit(PageSize.A4);
//            float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
//                    - document.rightMargin() - 5) / myImg.getWidth()) * 100;
//
//            myImg.scalePercent(scaler);
            bmp.recycle();
            return myImg;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public interface Callback {
        void onSuccess(String filepath);

        void onError(String message);

        void onBitmapError(String message);
    }
}
