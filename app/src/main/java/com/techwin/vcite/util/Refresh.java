package com.techwin.vcite.util;

public class Refresh {

    private String message;
    private String to;
    private String from;

    public Refresh(String message, String to, String from) {
        this.message = message;
        this.to = to;
        this.from = from;
    }

    public String message() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String to() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String from() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

}
