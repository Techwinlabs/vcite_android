package com.techwin.vcite.util;

/**
 * Created by shank_000 on 10/3/2018.
 */

public interface Constants {

    //String BASEURL = "https://vmobile.vciteplus.com/";
    String BASEURL = "http://vcitemobile.com/";  // Production url

    //  String BASEURL = "http://216.83.136.38:8080/api/citation/";  // developement url

    String FIELD_TYPE_TEXT = "Text";
    String FIELD_TYPE_DATE = "Time/Date";
    String FIELD_TYPE_Calender = "Calendar";
    String FIELD_TYPE_DROP = "DropDown";

    String fieldID_VehLicense = "VehLicense";
    String fieldID_VehState = "VehState";
    String fieldID_vehInfoBox = "vehInfoBox";
    String fieldID_RequireOCR = "RequireOCR";
    String fieldID_VIN = "VIN";
    String fieldID_SaveOCR = "SaveOCR";
    String fieldID_RequireOCRVin = "RequireOCRVin";
    String fieldID_IssueDate = "IssueDate";
    String fieldID_SaveOCRVin = "SaveOCRVin";
    String fieldID_VehColor = "VehColor";
    String fieldID_VehType = "VehType";
    String fieldID_VehMake = "VehMake";
    String fieldID_VehModel = "VehModel";
    String fieldID_ExpDate = "ExpDate";
    String fieldID_ClerkID = "ClerkID";
    String fieldID_plateType = "CustField1";
    String fieldID_platecolor = "CustField2";
    String fieldID_parcel_id = "CustField3";
    String fieldID_Void = "Void";
    String fieldID_Warning = "Warning";

    String fieldID_Violation = "Violation";
    String fieldID_Location = "Location";

    String fieldID_Establishment = "Establishment";
    String fieldID_StreetNumber = "StreetNumber";
    String fieldID_StreetName = "StreetName";
    String fieldID_ZipCode = "ZipCode";
    String fieldID_Unit = "Unit";
    String fieldID_Remarks = "Remarks";
    String fieldID_Parcel = "Parcel";
    String fieldID_MeterNo = "MeterNo";
    String fieldID_Division = "Division";

    String fieldID_violation = "violation";
    String fieldID_OfficerRemarks = "OfficerRemarks";
    String fieldID_codeWarning = "codeWarning";
    String fieldID_codeMeter = "codeMeter";
    String fieldID_codeLimitPhoto = "codeLimitPhoto";
    String fieldID_codePhotoSize = "codePhotoSize";
    String fieldID_codePropertyInfo = "codePropertyInfo";
    String fieldID_codeViolationBox = "codeViolationBox";

    String fieldID_OfficerID = "OfficerID";
    String fieldID_Latitude = "Latitude";
    String fieldID_Longitude = "Longitude";
    String fieldID_CustField4 = "CustField4";
    String fieldID_CustField5 = "CustField5";

    /*permissions*/
    String PERM_AddDocument = "AddDocument";
    String PERM_DeleteAllDocument = "DeleteAllDocument";
    String PERM_DeleteUserDocument = "DeleteUserDocument";
    String PERM_EditAllDocument = "EditAllDocument";
    String PERM_EditUserDocument = "EditUserDocument";
    String PERM_ResetPassword = "ResetPassword";

    String CITATION_TYPE_PARKING = "parking";
    String CITATION_TYPE_CODE_ENFORCE = "Code enforcement";
    int LYNNMA_CUSTKEY = 39;
    String DEVELOPER_EMAIL = "support@velosum.com";

    int PIC_TYPE_PLATE = 1;
    int PIC_TYPE_TICKET = 3;
    int PIC_TYPE_VIN = 2;
    int PIC_TYPE_NORMAL = 0;

}
