package com.techwin.vcite.util.databinding;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.techwin.vcite.R;
import com.techwin.vcite.data.local.entity.CitationBean;
import com.techwin.vcite.util.AppUtils;

public class ImageViewBindingUtils {

    @BindingAdapter("loadImagePath")
    public static void loadImagePath(ImageView imageView, String filePath) {
        try {
            Glide.with(imageView.getContext()).load(filePath).into(imageView);
        } catch (Exception exception) {
            Toast.makeText(imageView.getContext(), exception.getMessage() + "", Toast.LENGTH_SHORT).show();
        }

    }

    @BindingAdapter("setImageStatusImage")
    public static void setImageStatusImage(ImageView imageView, CitationBean citationBean) {
        imageView.setImageResource(R.drawable.ic_about);
        imageView.setColorFilter(ContextCompat.getColor(imageView.getContext(), R.color.red));
        if (citationBean.isPdfCreated() || citationBean.isPdfSent()) {
            imageView.setImageResource(R.drawable.ic_pdf);
            imageView.setColorFilter(ContextCompat.getColor(imageView.getContext(), R.color.orange));
        }
        if (citationBean.isUploaded()) {
            imageView.setImageResource(R.drawable.ic_about);
            imageView.setColorFilter(ContextCompat.getColor(imageView.getContext(), R.color.green));
        }
    }

    @BindingAdapter(value = {"image_url", "resize", "placeholder"}, requireAll = false)
    public static void setImageUrl(final ImageView imageView, String image_url, boolean resize, Drawable placeholder) {
        RequestBuilder<Drawable> requests = Glide.with(imageView.getContext()).load(image_url);
        if (resize)
            requests.override(120, 100);
        if (placeholder != null)
            requests.placeholder(placeholder);
        requests.centerCrop();
        requests.into(imageView);
    }

    @BindingAdapter(value = {"image_base64", "resize", "placeholder"}, requireAll = false)
    public static void setImageBase64(final ImageView imageView, String image_url, boolean resize, Drawable placeholder) {
        if (image_url != null) {
            Bitmap bitmap = AppUtils.base64ToImage(image_url);
            RequestBuilder<Drawable> requests = Glide.with(imageView.getContext()).load(bitmap);
            if (resize)
                requests.override(120, 100);
            if (placeholder != null)
                requests.placeholder(placeholder);
            requests.centerCrop();
            requests.into(imageView);
        }
    }

    @BindingAdapter(value = {"image_base64_noncrop"}, requireAll = false)
    public static void setImageBase64n(final ImageView imageView, String image_url) {
        Bitmap bitmap = AppUtils.base64ToImage(image_url);
        RequestBuilder<Drawable> requests = Glide.with(imageView.getContext()).load(bitmap);
        requests.into(imageView);
    }

    @BindingAdapter(value = {"rectangle", "view_width", "view_height", "placeholder"}, requireAll = false)
    public static void rectangle(final ImageView imageView, String image_url, Integer view_width, Integer view_height, Drawable placeholder) {
        RequestBuilder<Drawable> options = Glide.with(imageView.getContext()).load(image_url);
        options.centerCrop();
        if (view_width != null && view_height != null)
            options.override(view_width, view_height);
        if (placeholder != null)
            options.placeholder(placeholder);
        options.into(imageView);
    }


    @BindingAdapter(value = {"square", "placeholder"}, requireAll = false)
    public static void setSqure(final ImageView imageView, String image_url, Drawable placeholder) {
        RequestBuilder<Drawable> requests = Glide.with(imageView.getContext()).load(image_url);
        requests.override(120);
        if (placeholder != null)
            requests.placeholder(placeholder);
        requests.centerCrop();
        requests.into(imageView);
    }

    @BindingAdapter(value = {"view_image"})
    public static void setImageUrl(final ImageView imageView, String image_url) {
        RequestBuilder<Drawable> requests = Glide.with(imageView.getContext()).load(image_url).fitCenter();
        requests.into(imageView);
    }




/*
    @BindingAdapter(value = {"profile_url"}, requireAll = false)
    public static void setProfileUrl(final ImageView imageView, String image_url) {
          RequestBuilder<Drawable> requests= Glide.with(imageView.getContext()).load("" + image_url);
        requests.placeholder(R.drawable.ic_dummy_profile);
        requests.circleCrop();
        requests.override(150);
        requests.into(imageView);
    }*/


    @BindingAdapter(value = {"simpleResourse"})
    public static void setStepGroupIcon(final ImageView imageView, int simpleResourse) {

        if (simpleResourse != -1) {
            imageView.setImageResource(simpleResourse);

        }
    }




  /*  @BindingAdapter(value = {"request_status", "is_read"})
    public static void setMeetIcon(final ImageView imageView, Integer status, Integer read) {

        if (status == null || read == null) {
            imageView.setVisibility(View.GONE);
            return;
        }
        imageView.setVisibility(View.VISIBLE);
        if (status == 1) {
            imageView.setImageResource(R.drawable.ic_two_check);
            imageView.setColorFilter(ContextCompat.getColor(imageView.getContext(), R.color.green), PorterDuff.Mode.SRC_IN);
        } else if (read == 1) {
            imageView.setImageResource(R.drawable.ic_two_check);
            imageView.setColorFilter(ContextCompat.getColor(imageView.getContext(), R.color.dim_black), PorterDuff.Mode.SRC_IN);
        } else {
            imageView.setImageResource(R.drawable.ic_one_check);
            imageView.setColorFilter(ContextCompat.getColor(imageView.getContext(), R.color.dim_black), PorterDuff.Mode.SRC_IN);
        }

    }
*/

    @BindingAdapter(value = {"chat_thumbnil", "corner"}, requireAll = false)
    public static void setThumnilUrl(final ImageView imageView, String image_url, String message) {
        RequestBuilder<Drawable> requests = Glide.with(imageView.getContext()).load(image_url);
        requests.centerCrop();
     /*   if (message != null && message.length() > 0)
            requests.transform(new RoundedCornersTransformation(AppUtils.convertDpToPixel(20), AppUtils.convertDpToPixel(5), RoundedCornersTransformation.CornerType.TOP));
        else
            requests.transform(new RoundedCornersTransformation(AppUtils.convertDpToPixel(20), AppUtils.convertDpToPixel(5)));*/
        requests.into(imageView);
    }


    @BindingAdapter(value = {"image_local_path"}, requireAll = false)
    public static void setImageLocalPath(final ImageView imageView, String image_url) {
        RequestBuilder<Drawable> requests = Glide.with(imageView.getContext()).load(image_url);
        requests.override(120, 100);
        requests.centerCrop();
        requests.into(imageView);
    }


}
