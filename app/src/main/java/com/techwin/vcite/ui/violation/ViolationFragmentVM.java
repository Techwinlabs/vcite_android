package com.techwin.vcite.ui.violation;


import android.util.Log;

import androidx.databinding.ObservableField;

import com.techwin.vcite.data.beans.CitationData;
import com.techwin.vcite.data.local.dao.CitationDao;
import com.techwin.vcite.data.local.dao.TransactionDao;
import com.techwin.vcite.data.local.entity.CitationBean;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.di.base.viewmodel.BaseViewModel;
import com.techwin.vcite.util.event.SingleLiveEvent;
import com.techwin.vcite.util.event.SingleRequestEvent;

import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;


public class ViolationFragmentVM extends BaseViewModel {

    final SingleRequestEvent<CitationBean> obrData = new SingleRequestEvent<>();
    private final TransactionDao transactionDao;
    private final CitationDao citationDao;
    final SingleRequestEvent<Void> obrSave = new SingleRequestEvent<>();
    public final ObservableField<String> field_location = new ObservableField<>();
    public final ObservableField<String> field_meterno = new ObservableField<>();
    public final ObservableField<String> field_parcel = new ObservableField<>();
    public final ObservableField<String> field_voilation = new ObservableField<>();

    @Inject
    public ViolationFragmentVM(TransactionDao transactionDao, CitationDao citationDao) {
        this.transactionDao = transactionDao;

        this.citationDao = citationDao;
    }


    void saveCitation(CitationBean citationBean) {
        compositeDisposable.add(Observable.fromCallable(() -> transactionDao.updateTrans(citationBean)).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(aLong -> {
                    Log.e(TAG, "insert" + aLong);
                    obrSave.setValue(Resource.success(null, "Saved"));
                }, throwable -> {
                    obrSave.setValue(Resource.success(null, "Error :" + throwable.getMessage()));
                    Log.e(TAG, "error" + throwable.getMessage());
                }));
    }

    void getCitation(CitationData citationData) {
        obrData.setValue(Resource.loading(null));
        compositeDisposable.add(Observable.fromCallable(() -> citationDao.loadCitation(citationData.citationNo, citationData.orgNo, citationData.citationType))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(citationBean ->
                                obrData.setValue(Resource.success(citationBean, "done")),
                        e -> obrData.setValue(Resource.error(null, e.getMessage()))));


    }
}
