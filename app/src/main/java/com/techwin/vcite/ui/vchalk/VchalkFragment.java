package com.techwin.vcite.ui.vchalk;

import android.content.Context;

import androidx.annotation.Nullable;

import com.techwin.vcite.R;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.databinding.FragmentAboutBinding;
import com.techwin.vcite.databinding.FragmentVchalkBinding;
import com.techwin.vcite.di.base.view.AppFragment;
import com.techwin.vcite.ui.about.AboutFragmentVM;
import com.techwin.vcite.ui.main.MainActivity;

import javax.inject.Inject;

public class VchalkFragment extends AppFragment<FragmentVchalkBinding, VchalkFragmentVM> {
    public static final String TAG = "VchalkFragment";
    @Nullable
    private MainActivity mainActivity;
    @Inject
    SharedPref sharedPref;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mainActivity = (MainActivity) context;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected BindingFragment<VchalkFragmentVM> getBindingFragment() {
        return new BindingFragment<>(R.layout.fragment_vchalk, VchalkFragmentVM.class);
    }

    public static VchalkFragment newInstance() {
        return new VchalkFragment();
    }

    @Override
    protected void subscribeToEvents(final VchalkFragmentVM vm) {
        binding.tvInfor.fromAsset("vchalk_info.pdf").show();

    }


}
