package com.techwin.vcite.ui.main;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupMenu;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.arvind.android.permissions.PermissionHandler;
import com.arvind.android.permissions.Permissions;
import com.permissionx.guolindev.PermissionX;
import com.permissionx.guolindev.callback.ExplainReasonCallback;
import com.permissionx.guolindev.callback.RequestCallback;
import com.permissionx.guolindev.request.ExplainScope;
import com.techwin.vcite.BR;
import com.techwin.vcite.R;
import com.techwin.vcite.data.beans.CitationData;
import com.techwin.vcite.data.beans.MenuBean;
import com.techwin.vcite.data.beans.UserBean;
import com.techwin.vcite.data.beans.UserPermission;
import com.techwin.vcite.data.beans.VehType;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.local.entity.CitationBean;
import com.techwin.vcite.data.local.entity.MediaBean;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.data.remote.helper.Status;
import com.techwin.vcite.databinding.ActivityMainBinding;
import com.techwin.vcite.databinding.HolderMenuBinding;
import com.techwin.vcite.di.base.adapter.SimpleRecyclerViewAdapter;
import com.techwin.vcite.di.base.view.AppActivity;
import com.techwin.vcite.di.base.view.BaseAlertDialog;
import com.techwin.vcite.ui.about.AboutFragment;
import com.techwin.vcite.ui.enforment.EnforceFragment;
import com.techwin.vcite.ui.home.CitationsFragment;
import com.techwin.vcite.ui.imagePicker.CustomImagePickerActivity;
import com.techwin.vcite.ui.main2.vehcile.Vehicle2Fragment;
import com.techwin.vcite.ui.password.PasswordFragment;
import com.techwin.vcite.ui.photo.PhotoFragment;
import com.techwin.vcite.ui.refrence.RefrenceFragment;
import com.techwin.vcite.ui.review.ReviewFragment;
import com.techwin.vcite.ui.scan.ScanActivity;
import com.techwin.vcite.ui.setting.SettingFragment;
import com.techwin.vcite.ui.splash.SplashActivity;
import com.techwin.vcite.ui.vchalk.VchalkFragment;
import com.techwin.vcite.ui.violation.ViolationFragment;
import com.techwin.vcite.util.AppUtils;
import com.techwin.vcite.util.Constants;
import com.techwin.vcite.util.dump.PdfUtils;
import com.techwin.vcite.util.event.SingleRequestEvent;
import com.techwin.vcite.util.misc.BackStackManager;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppActivity<ActivityMainBinding, MainActivityVM> implements BackStackManager.StackListioner {

    private static final int SCAN_CODE = 101;
    private static final String SYNC = "sync";
    private static final String LOGIN_TAG = "login";
    public BackStackManager backStackManager;
    public CitationData citationData;
    @Inject
    SharedPref sharedPref;

    @Override
    protected BindingActivity<MainActivityVM> getBindingActivity() {
        return new BindingActivity<>(R.layout.activity_main, MainActivityVM.class);
    }

    public static Intent newIntent(Context c) {
        Intent intent = new Intent(c, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    private void closeDrawer() {
        if (binding.mainDl.isDrawerOpen(GravityCompat.START)) {
            binding.mainDl.closeDrawer(GravityCompat.START);
        }
    }

    private void openDrawer() {
        binding.mainDl.openDrawer(GravityCompat.START);

    }

    @Override
    protected void subscribeToEvents(final MainActivityVM vm) {
        Permissions.check(this, Manifest.permission.READ_EXTERNAL_STORAGE, null, new PermissionHandler() {
            @Override
            public void onGranted() {
            }
        });
        binding.ivUser.setImageBitmap(AppUtils.base64ToImage(sharedPref.getUser().ClientLogo));
        backStackManager = BackStackManager.getInstance(this, "main");
        backStackManager.clear();
        backStackManager.setStackListioner(this);
        vm.onClick.observe(this, v -> {
            switch (v.getId()) {
             /*   case R.id.iv_add:
                    Intent intent = CreateActivity.newIntent(MainActivity.this);
                    startNewActivity(intent);
                    break;*/
                case R.id.iv_menu:
                    openDrawer();

                    break;
            }
        });
        vm.menu_bean.observe(this, menuBean -> {
            if (menuBean != null) {
                changeFragment(menuBean.id);
            }
        });
        vm.obrList.observe(this, (SingleRequestEvent.RequestObserver<List<CitationBean>>) resource -> {
            switch (resource.status) {
                case LOADING:
                    showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    addCitationList(resource.data);
                    break;
                case ERROR:
                    dismissProgressDialog();
                    break;
            }
        });
        vm.obrSubmit.observe(this, (SingleRequestEvent.RequestObserver<String>) resource -> {
            switch (resource.status) {
                case LOADING:
                    showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    if (resource.data != null) {
                        if (resource.data.equalsIgnoreCase("Success")) {
                            viewModel.success.setValue(resource.data);
                            citationData = null;
                            changeFragment(CitationsFragment.TAG);
                        } else {
                            citationData = null;
                            changeFragment(CitationsFragment.TAG);
                            viewModel.error.setValue(resource.data);
                        }
                    }
                    break;
                case ERROR:
                    dismissProgressDialog();
                    viewModel.error.setValue(resource.message);
                    break;
            }
        });
        vm.bottombar.observe(this, menuItem -> {
            if (menuItem != null && fragmentStateAdapter != null) {
                int itemId = menuItem.getItemId();
                if (itemId == R.id.action_item1) {
                    if (0 < fragmentStateAdapter.getItemCount())
                        binding.pager.setCurrentItem(0, false);
                } else if (itemId == R.id.action_item2) {
                    if (1 < fragmentStateAdapter.getItemCount())
                        binding.pager.setCurrentItem(1, false);
                } else if (itemId == R.id.action_item3) {
                    if (2 < fragmentStateAdapter.getItemCount())
                        binding.pager.setCurrentItem(2, false);
                } else if (itemId == R.id.action_item4) {
                    if (3 < fragmentStateAdapter.getItemCount())
                        binding.pager.setCurrentItem(3, false);
                }
            }
        });
        vm.obrLogout.observe(this, (Observer<Resource<Void>>) voidResource -> {
            sharedPref.removeUser();
            Intent intent = SplashActivity.newIntent(MainActivity.this);
            startNewActivity(intent);
            finishAffinity();
        });
        vm.obrVehType.observe(this, (SingleRequestEvent.RequestObserver<VehType>) resource -> {
            switch (resource.status) {
                case LOADING:
                    //   showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    if (resource.data != null) {
                        Log.e("Veh", resource.data.toString());
                    }
                    //   vm.success.setValue(resource.message);
                    break;
                case ERROR:
                    dismissProgressDialog();
                    //   vm.error.setValue(resource.message);
                    break;
            }
        });

        vm.obrSave.observe(this, (SingleRequestEvent.RequestObserver<Void>) resource -> {
            switch (resource.status) {
                case SUCCESS:
                    //  viewModel.success.setValue("Pdf sent");
                    break;
                case ERROR:
                    viewModel.error.setValue(resource.message);
                    break;
            }
        });

        setDrawerMenu();
        CitationData temp = getIntent().getParcelableExtra("lookup");
        if (temp == null)
            changeFragment(CitationsFragment.TAG);
        else openFromLookup(temp);
        if (sharedPref.getVehType().size() == 0) vm.getVehType();
    }

    private FragmentStateAdapter fragmentStateAdapter;

    private void setViewPager() {
        List<Fragment> fragments = new ArrayList<>();
        Fragment fragment = getFirstTab();
        if (fragment != null)
            fragments.add(fragment);
        fragments.add(ViolationFragment.newInstance());
        fragments.add(PhotoFragment.newInstance());
        fragments.add(ReviewFragment.newInstance());
        fragmentStateAdapter = new FragmentStateAdapter(this) {
            @NonNull
            @Override
            public Fragment createFragment(int position) {
                return fragments.get(position);
            }

            @Override
            public int getItemCount() {
                return fragments.size();
            }
        };
        binding.pager.setAdapter(fragmentStateAdapter);
        binding.pager.setOffscreenPageLimit(fragments.size());
        binding.pager.setUserInputEnabled(false);
        binding.bvOne.setSelectedItemId(R.id.action_item1);
        binding.bvOne.setSelected(true);
    }


    public Fragment getFirstTab() {
        updateBottomNavigation();
        if (citationData == null)
            return null;
        if (citationData.getCitationType().equalsIgnoreCase(Constants.CITATION_TYPE_PARKING))
            return Vehicle2Fragment.newInstance();
        else
            return EnforceFragment.newInstance();
    }

    public void openFirstTab() {
        if (citationData == null)
            return;
       /* if (citationData.getCitationType().equalsIgnoreCase(Constants.CITATION_TYPE_PARKING))
            changeFragment(Vehicle2Fragment.TAG);
        else
            changeFragment(EnforceFragment.TAG);*/
        setViewPager();
        binding.setShowpager(true);
    }

    public void changeFragment(String tag) {
        updateBottomNavigation();
        switch (tag) {
            case RefrenceFragment.TAG:
                binding.tvTitle.setText("Reference");
                backStackManager.pushFragment(R.id.container, RefrenceFragment.TAG, true);
                binding.setShowpager(false);
                break;
            case CitationsFragment.TAG:
                binding.tvTitle.setText("Citations");
                backStackManager.pushFragment(R.id.container, CitationsFragment.TAG, true);
                binding.setShowpager(false);
                break;
            case SettingFragment.TAG:
                binding.tvTitle.setText("Settings");
                backStackManager.pushFragment(R.id.container, SettingFragment.TAG, true);
                binding.setShowpager(false);
                break;
            case AboutFragment.TAG:
                binding.tvTitle.setText("About Us");
                backStackManager.pushFragment(R.id.container, AboutFragment.TAG, true);
                binding.setShowpager(false);
                break;
            case VchalkFragment.TAG:
                binding.tvTitle.setText("vChalk");
                backStackManager.pushFragment(R.id.container, VchalkFragment.TAG, true);
                binding.setShowpager(false);
                break;
            case PasswordFragment.TAG:
                if (hasPermission(Constants.PERM_AddDocument, "Permission Error", "You have not permission to change password")) {
                    binding.tvTitle.setText("Change Password");
                    backStackManager.pushFragment(R.id.container, PasswordFragment.TAG, true);
                    binding.setShowpager(false);
                }
                break;
            case SYNC:
                syncData();
                break;
            case LOGIN_TAG:
                new AlertDialog.Builder(this)
                        .setTitle("Logout")
                        .setMessage("Are you sure want to logout?")
                        .setNegativeButton("No", (dialog, which) -> dialog.dismiss())
                        .setPositiveButton("Yes", (dialog, which) -> {
                            dialog.dismiss();
                            viewModel.logout();
                        })
                        .show();
                break;
        }
    }

    private void updateBottomNavigation() {
        if (citationData != null) {
            if (citationData.getCitationType().equalsIgnoreCase(Constants.CITATION_TYPE_PARKING)) {
                MenuItem menuItem = binding.bvOne.getMenu().getItem(0);
                menuItem.setTitle("Vehicle");
                menuItem.setIcon(R.drawable.ic_action_car);
            } else {
                MenuItem menuItem = binding.bvOne.getMenu().getItem(0);
                menuItem.setTitle("Property");
                menuItem.setIcon(R.drawable.ic_action_apart);
            }
        }

    }

    @NonNull
    @Override
    public Fragment getFragment(@NonNull String tag) {
        switch (tag) {
            case SettingFragment.TAG:
                return SettingFragment.newInstance();
            case RefrenceFragment.TAG:
                return RefrenceFragment.newInstance();
            case CitationsFragment.TAG:
                return CitationsFragment.newInstance();
            case AboutFragment.TAG:
                return AboutFragment.newInstance();
            case VchalkFragment.TAG:
                return VchalkFragment.newInstance();
            case PasswordFragment.TAG:
                return PasswordFragment.getInstance();
            default:
                throw new NullPointerException(tag + " not implemented");
        }
    }

    @Override
    public void onFragmentChange(@NonNull String tag, @NonNull Fragment fragment, boolean isSubFragment) {
        switch (tag) {
            case RefrenceFragment.TAG:
                RefrenceFragment.newInstance();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (binding.getShowpager())
            binding.setShowpager(false);
        else if (backStackManager.getCurrentTab() != null && !backStackManager.getCurrentTab().equals(CitationsFragment.TAG)) {
            changeFragment(CitationsFragment.TAG);
        } else
            super.onBackPressed();
    }

    private void setDrawerMenu() {
        binding.rvDrawer.setLayoutManager(new LinearLayoutManager(this));
        SimpleRecyclerViewAdapter<MenuBean, HolderMenuBinding> adapter = new SimpleRecyclerViewAdapter<>(R.layout.holder_menu, BR.bean, (v, menuBean) -> {
            closeDrawer();
            viewModel.setMenuItem(menuBean);
        });
        binding.rvDrawer.setAdapter(adapter);
        List<MenuBean> menuBeanList = new ArrayList<>();
        menuBeanList.add(new MenuBean(R.drawable.ic_qr_code, "Citations", CitationsFragment.TAG));
        menuBeanList.add(new MenuBean(R.drawable.ic_edit, "vChalk", VchalkFragment.TAG));
        menuBeanList.add(new MenuBean(R.drawable.ic_documents, "Reference", RefrenceFragment.TAG));
        menuBeanList.add(new MenuBean(R.drawable.ic_about, "About", AboutFragment.TAG));
        menuBeanList.add(new MenuBean(R.drawable.ic_action_lock, "Change Password", PasswordFragment.TAG));
        menuBeanList.add(new MenuBean(R.drawable.ic_login, "Logout", LOGIN_TAG));
        //  menuBeanList.add(new MenuBean(R.drawable.ic_sync, "Sync", SYNC));
        adapter.setList(menuBeanList);
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadData();
    }

    public void loadData() {
        viewModel.apiPlateType();
        viewModel.apiVehicleColor();
        viewModel.apiVehMake();
        loadPermissions();
    }

    private void loadPermissions() {
        UserBean userBean = sharedPref.getUser();
        if (userBean != null)
            viewModel.getPermissions(String.valueOf(userBean.UserKey));
    }

    public void openScanner() {
        Intent intent = ScanActivity.newIntent(this);
        startActivityForResult(intent, SCAN_CODE);
        animateActivity();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case SCAN_CODE: {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    citationData = data.getParcelableExtra("data");
                    viewModel.success.setValue(citationData.toString());
                    openFirstTab();
                }
            }
            break;
        }
    }

    public void openFromLookup(CitationData citationData) {
        this.citationData = citationData;
        if (citationData == null)
            return;
        changeFragment(CitationsFragment.TAG);
        openFirstTab();
//        if (citationData.getCitationType().equalsIgnoreCase(Constants.CITATION_TYPE_PARKING))
//            changeFragment(Vehicle2Fragment.TAG);
//        else
//            changeFragment(EnforceFragment.TAG);
    }

    public void addCitation(CitationBean citationBean) {
        if (hasAddDocumentPermission()) {
            List<String> miss = AppUtils.missing(citationBean, sharedPref.getTemplate());
            if (miss.size() == 0)
                viewModel.apiSubmitCitation(citationBean);
            else
                viewModel.info.setValue("Missing :" + AppUtils.convertArrayToString(miss));
        }

    }

    private boolean hasAddDocumentPermission() {
        return hasPermission(Constants.PERM_AddDocument, "Permission Error", "You have not permission to add document");
    }

    private boolean hasPermission(String permId, String title, String message) {
        List<UserPermission> userPermissions = sharedPref.getUserPermission();
        if (userPermissions == null) {
            loadPermissions();
            viewModel.info.setValue("Loading Permissions");
            return false;
        }
        for (UserPermission permission : userPermissions) {
            if (permission.getPermID().equalsIgnoreCase(permId)) {
                return true;
            }
        }
        BaseAlertDialog alertDialog = new BaseAlertDialog(MainActivity.this);
        alertDialog.setLabels(title, message, "Ok", null);
        alertDialog.setListioner(new BaseAlertDialog.ClickListioner() {
            @Override
            public void onOkClick() {
                alertDialog.dismiss();
            }

            @Override
            public void onCancelClick() {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
        alertDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        return false;
    }

    public void syncData() {
        if (hasAddDocumentPermission()) {
            citationData = null;
            changeFragment(CitationsFragment.TAG);
            viewModel.syncList();
        }
    }


    public void addCitationList(List<CitationBean> citationBeans) {
        if (citationBeans != null) {
            for (CitationBean citationBean : citationBeans) {
                if (citationBean.getCitationType().equalsIgnoreCase(Constants.CITATION_TYPE_PARKING)) {
                    List<String> miss = AppUtils.missing(citationBean, sharedPref.getTemplate(citationBean.getCitationType()));
                    if (miss.size() == 0)
                        viewModel.apiSubmitCitation(citationBean);
                    else viewModel.info.setValue(miss + "");
                } else if (citationBean.getCitationType().equalsIgnoreCase(Constants.CITATION_TYPE_CODE_ENFORCE)) {
                    List<String> miss = AppUtils.missingEnforcement(citationBean, sharedPref.getTemplate(citationBean.getCitationType()));
                    if (miss.size() == 0)
                        viewModel.apiSubmitCitation(citationBean);
                    else viewModel.info.setValue(miss + "");
                }

            }
        }
    }

    @Nullable
    private CitationBean citationBean;

    private final ActivityResultLauncher<Intent> launcher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
        @Override
        public void onActivityResult(ActivityResult result) {
            if (citationBean != null) {
                citationBean.setPdfSent(true);
                citationBean.setPdfSentDate(Calendar.getInstance().getTime());
                viewModel.saveCitation(citationBean);
            }

        }
    });
    @Nullable
    private PopupMenu popupMenu;

    public void startBitmapCapture(CitationBean citationBean, View view) {
        popupMenu = new PopupMenu(new ContextThemeWrapper(this, R.style.popupstyle), view);
        popupMenu.getMenuInflater().inflate(R.menu.menu_pdf, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(menuItem -> {
            int itemId = menuItem.getItemId();
            if (itemId == R.id.action_1) {
                capturePdf(citationBean, true);
                return true;
            } else if (itemId == R.id.action_2) {
                capturePdf(citationBean, false);
                return true;
            }
            return false;
        });
        popupMenu.show();
    }

    private void capturePdf(CitationBean citationBean, boolean byEmail) {
        this.citationBean = citationBean;
        PermissionX.init(this)
                .permissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .onExplainRequestReason((scope, deniedList) -> scope.showRequestReasonDialog(deniedList, "Pdf creator is based on these permissions", "OK", "Cancel"))
                .request((allGranted, grantedList, deniedList) -> {
                    if (fragmentStateAdapter != null) {
                        if (citationBean.getCitationType().equalsIgnoreCase(Constants.CITATION_TYPE_PARKING)) {
                            List<String> missing = AppUtils.missing(citationBean, sharedPref.getTemplate(Constants.CITATION_TYPE_PARKING));
                            if (missing.size() > 0) {
                                StringBuilder builder = new StringBuilder();
                                for (String s : missing) {
                                    builder.append(s);
                                    builder.append(",");
                                }
                                viewModel.info.setValue("Missing :" + builder);
                                return;
                            }
                        } else if (citationBean.getCitationType().equalsIgnoreCase(Constants.CITATION_TYPE_CODE_ENFORCE)) {
                            List<String> missing = AppUtils.missingEnforcement(citationBean, sharedPref.getTemplate(Constants.CITATION_TYPE_PARKING));
                            if (missing.size() > 0) {
                                StringBuilder builder = new StringBuilder();
                                for (String s : missing) {
                                    builder.append(s);
                                    builder.append(",");
                                }
                                viewModel.info.setValue("Missing :" + builder);
                                return;
                            }
                        }

                        showProgressDialog(R.string.plz_wait);
                        List<Bitmap> list = new ArrayList<>();
                        List<MediaBean> photos = new ArrayList<>();
                        for (int i = 0; i < fragmentStateAdapter.getItemCount(); i++) {
                            Fragment fragment = fragmentStateAdapter.createFragment(i);
                            if (fragment instanceof Vehicle2Fragment) {
                                Vehicle2Fragment f1 = (Vehicle2Fragment) fragment;
                                list.add(f1.savePdfBitmap());
                            } else if (fragment instanceof EnforceFragment) {
                                EnforceFragment f1 = (EnforceFragment) fragment;
                                list.add(f1.savePdfBitmap());
                            } else if (fragment instanceof ViolationFragment) {
                                ViolationFragment f1 = (ViolationFragment) fragment;
                                list.add(f1.savePdfBitmap());
                            } else if (fragment instanceof ReviewFragment) {
                                ReviewFragment f1 = (ReviewFragment) fragment;
                                list.add(f1.savePdfBitmap());
                            } else if (fragment instanceof PhotoFragment) {
                                PhotoFragment f1 = (PhotoFragment) fragment;
                                photos.addAll(f1.getMedias());
                            }
                        }

                       /* PdfUtils pdfUtils = PdfUtils.newInstance(MainActivity.this);
                        pdfUtils.setCallback(new PdfUtils.Callback() {
                            @Override
                            public void onSuccess(String filepath) {
                                ArrayList<Uri> uriList = new ArrayList<>();
                                uriList.add(FileProvider.getUriForFile(MainActivity.this, getPackageName() + ".provider", new File(filepath)));
                                for (MediaBean mediaBean : photos) {
                                    try {
                                        Bitmap bitmap = AppUtils.base64ToImage(mediaBean.getImage());
                                        if (bitmap == null)
                                            continue;
                                        String filename = "media" + System.currentTimeMillis() + ".png";
                                        File mediaStorageDir = new File(getExternalFilesDir(""), "vcite");
                                        if (!mediaStorageDir.exists()) {
                                            mediaStorageDir.mkdirs();
                                        }
                                        File file = new File(mediaStorageDir.getAbsolutePath() + "/" + filename);
                                        FileOutputStream fOut = new FileOutputStream(file);
                                        bitmap.compress(Bitmap.CompressFormat.PNG, 80, fOut);
                                        fOut.flush();
                                        fOut.close();
                                        uriList.add(FileProvider.getUriForFile(MainActivity.this, getPackageName() + ".provider", file));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                                try {
                                    Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
                                    emailIntent.setType("message/rfc822");
                                    String to[] = {Constants.DEVELOPER_EMAIL};
                                    emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
                                    emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uriList);
                                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, getEmailSubject(citationBean));
                                    launcher.launch(emailIntent);

                                    citationBean.setPdfCreated(true);
                                    viewModel.saveCitation(citationBean);
                                } catch (ActivityNotFoundException e) {
                                    viewModel.error.setValue(e.getMessage());
                                }
                                dismissProgressDialog();
                            }

                            @Override
                            public void onError(String message) {
                                viewModel.error.setValue(message);
                                dismissProgressDialog();
                            }

                            @Override
                            public void onBitmapError(String message) {
                                viewModel.error.setValue(message);
                                dismissProgressDialog();
                            }
                        });
                        pdfUtils.makePdf(list);*/

                        showProgressDialog(R.string.plz_wait);
                        viewModel.compositeDisposable.add(Observable.fromCallable(() -> {
                            ArrayList<Uri> uriList = new ArrayList<>();
                            PdfUtils pdfUtils = PdfUtils.newInstance(MainActivity.this);
                            // String filepath = pdfUtils.makePdfAsyc(list);
                            // uriList.add(FileProvider.getUriForFile(MainActivity.this, getPackageName() + ".provider", new File(filepath)));
                            for (MediaBean mediaBean : photos) {
                                try {
                                    Bitmap bitmap = AppUtils.base64ToImage(mediaBean.getImage());
                                    if (bitmap == null)
                                        continue;
                                    String filename = "media" + System.currentTimeMillis() + ".png";
                                    File mediaStorageDir = new File(getExternalFilesDir(""), "vcite");
                                    if (!mediaStorageDir.exists()) {
                                        mediaStorageDir.mkdirs();
                                    }
                                    File file = new File(mediaStorageDir.getAbsolutePath() + "/" + filename);
                                    FileOutputStream fOut = new FileOutputStream(file);
                                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                                    list.add(bitmap);
                                    fOut.flush();
                                    fOut.close();
                                    uriList.add(FileProvider.getUriForFile(MainActivity.this, getPackageName() + ".provider", file));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            String filepath = pdfUtils.makePdfAsyc(list);
                            uriList.add(FileProvider.getUriForFile(MainActivity.this, getPackageName() + ".provider", new File(filepath)));

                            if (!byEmail) {
                                for (Bitmap bitmap : list) {
                                    try {
                                        if (bitmap == null)
                                            continue;
                                        String filename = "media" + System.currentTimeMillis() + ".png";
                                        File mediaStorageDir = new File(getExternalFilesDir(""), "vcite");
                                        if (!mediaStorageDir.exists()) {
                                            mediaStorageDir.mkdirs();
                                        }
                                        File file = new File(mediaStorageDir.getAbsolutePath() + "/" + filename);
                                        FileOutputStream fOut = new FileOutputStream(file);
                                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                                        fOut.flush();
                                        fOut.close();
                                        uriList.add(FileProvider.getUriForFile(MainActivity.this, getPackageName() + ".provider", file));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                            return uriList;
                        }).subscribeOn(Schedulers.computation()).observeOn(AndroidSchedulers.mainThread())
                                .subscribe(uriList -> {
                                    dismissProgressDialog();
                                    try {
                                        Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
                                        emailIntent.setType("image/*");
                                        if (byEmail) {
                                            //  emailIntent.setType("message/rfc822");
                                            String[] to = {Constants.DEVELOPER_EMAIL};
                                            emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
                                            emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uriList);
                                            emailIntent.putExtra(Intent.EXTRA_SUBJECT, getEmailSubject(citationBean));
                                        } else {
                                            emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uriList);
                                        }
                                        launcher.launch(emailIntent);
                                        citationBean.setPdfCreated(true);
                                        citationBean.setPdfSent(true);
                                        citationBean.setPdfSentDate(Calendar.getInstance().getTime());
                                        viewModel.saveCitation(citationBean);
                                    } catch (
                                            ActivityNotFoundException e) {
                                        viewModel.error.setValue(e.getMessage());
                                    }
                                }, throwable -> {
                                    dismissProgressDialog();
                                    viewModel.error.setValue(throwable.getMessage());
                                }));


                    }

                });

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (popupMenu != null) {
            popupMenu.dismiss();
        }
    }

    private String getEmailSubject(CitationBean bean) {
        UserBean userBean = sharedPref.getUser();
        return "Please manually enter this citation " + userBean.CustKey +
                "-" +
                userBean.UserID +
                "-" +
                bean.getCitationNo() +
                "-" +
                new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.US).format(bean.getCreatedOn()) +
                "-" +
                AppUtils.getAndroidId(this);

    }

    @Override
    protected void onDestroy() {
        launcher.unregister();
        super.onDestroy();
    }

}

