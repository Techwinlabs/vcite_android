package com.techwin.vcite.ui.plate;


import android.content.Context;

import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.remote.helper.NetworkErrorHandler;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.di.base.viewmodel.BaseViewModel;
import com.techwin.vcite.util.event.SingleRequestEvent;
import com.techwin.vcite.util.location.LiveLocationDetecter;

import org.openalpr.OpenALPR;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class PlateActivityVM extends BaseViewModel {


    private final SharedPref sharedPref;
    public final LiveLocationDetecter liveLocationDetecter;
    private final NetworkErrorHandler networkErrorHandler;
    final SingleRequestEvent<String> obrResult = new SingleRequestEvent<>();

    @Inject
    public PlateActivityVM(SharedPref sharedPref, LiveLocationDetecter liveLocationDetecter, NetworkErrorHandler networkErrorHandler) {
        this.sharedPref = sharedPref;
        this.liveLocationDetecter = liveLocationDetecter;
        this.networkErrorHandler = networkErrorHandler;
    }


    void getResult(Context context, String dir, String country, String region, String path, String openAlprConfFile, int candidates) {
        compositeDisposable.add(Observable.fromCallable(() -> OpenALPR.Factory.create(context, dir)
                .recognizeWithCountryRegionNConfig(country, region, path, openAlprConfFile, candidates)).subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> obrResult.setValue(Resource.success(s, "done")), throwable -> obrResult.setValue(Resource.error(null, throwable.getMessage()))));
    }
}
