package com.techwin.vcite.ui.selector.vehcolor;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.techwin.vcite.BR;
import com.techwin.vcite.R;
import com.techwin.vcite.data.beans.VehicleColorType;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.data.remote.helper.Status;
import com.techwin.vcite.databinding.ActivityVehColorBinding;
import com.techwin.vcite.databinding.ActivityVehTypeBinding;
import com.techwin.vcite.databinding.HolderColorSelectorBinding;
import com.techwin.vcite.di.base.adapter.SimpleRecyclerViewAdapter;
import com.techwin.vcite.di.base.view.AppActivity;
import com.techwin.vcite.util.AppUtils;
import com.techwin.vcite.util.decoration.ListVerticalItemDecoration;
import com.techwin.vcite.util.dump.InputUtils;
import com.techwin.vcite.util.event.SingleRequestEvent;

import java.util.ArrayList;
import java.util.List;

public class VehColorActivity extends AppActivity<ActivityVehColorBinding, VehColorActivityVM> {
    @Nullable
    private String selected;
    private SimpleRecyclerViewAdapter<VehicleColorType.VehtTypBean, HolderColorSelectorBinding> adapter;
    private final List<VehicleColorType.VehtTypBean> dataList = new ArrayList<>();

    @Override
    protected BindingActivity<VehColorActivityVM> getBindingActivity() {
        return new BindingActivity<>(R.layout.activity_veh_color, VehColorActivityVM.class);
    }

    public static Intent newIntent(Context activity, @Nullable String selected) {
        Intent intent = new Intent(activity, VehColorActivity.class);
        intent.putExtra("selected", selected);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    @Override
    protected void subscribeToEvents(final VehColorActivityVM vm) {
        selected = getIntent().getStringExtra("selected");
        binding.toolbar.tvTitle.setText("Vehicle Type");
        adapter = new SimpleRecyclerViewAdapter<>(R.layout.holder_color_selector, BR.bean);
        adapter.setCallback((v, bean) -> {
            selected = bean.Abbreviation;
            for (int i = 0; i < adapter.getItemCount(); i++) {
                adapter.getList().get(i).checked = adapter.getList().get(i).VehColorKey == bean.VehColorKey;
            }
            adapter.notifyDataSetChanged();
        });
        binding.rvOne.setLayoutManager(new LinearLayoutManager(this));
        binding.rvOne.addItemDecoration(new ListVerticalItemDecoration(this, R.dimen.dp_5));
        binding.rvOne.setAdapter(adapter);

        vm.onClick.observe(this, new Observer<View>() {
            @Override
            public void onChanged(@Nullable View view) {
                int i = view != null ? view.getId() : 0;
                if (i == R.id.tv_save) {
                    if (adapter.getItemCount() == 0) {
                        selected = binding.etSearch.getText().toString();
                    }
                    Intent intent = new Intent();
                    intent.putExtra("data", selected);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                    animateCloseSheet();
                } else if (i == R.id.iv_back) {
                    finish(true);
                    animateCloseSheet();
                }
            }
        });

        vm.obrData.observe(this, (SingleRequestEvent.RequestObserver<List<VehicleColorType.VehtTypBean>>) resource -> {
            switch (resource.status) {
                case LOADING:
                    //   showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    dataList.clear();
                    if (resource.data != null) {
                        dataList.addAll(resource.data);
                    }
                    updateAdapter();
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    break;
            }
        });
        vm.singleLiveEvent.observe(this, s -> {
            if (InputUtils.isEmpty(s)) {
                adapter.setList(dataList);
            } else {
                adapter.clearList();
                for (VehicleColorType.VehtTypBean s1 : dataList) {
                    if (AppUtils.compareText(s1.Name,s)) {
                        adapter.addData(s1);
                    }
                }
            }
        });
        vm.setConsumer(binding.etSearch);
        vm.getList();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        animateCloseSheet();
    }

    private void updateAdapter() {
        if (selected != null) {
            for (int i = 0; i < dataList.size(); i++) {
                if (dataList.get(i).Abbreviation.equals(selected)) {
                    dataList.get(i).checked = true;
                    break;
                }
            }

        }
        adapter.setList(dataList);
    }

/*

    public static String getPlateType(String text) {
        if (text == null)
            return null;
        switch (text) {
            case "PAN":
                return "Normal Passenger";
            case "PAR":
                return "Reserved Passenger";
            case "PAS":
                return "Special Passenger";
            case "PAV":
                return "Vanity Passenger";
            case "MCN":
                return "Normal Motorcycle";
            case "MCF":
                return "Reserved Motorcycle";
            case "MCV":
                return "Vanity Motorcycle";
            case "AHN":
                return "Normal Motorized Home";
            case "TAR":
                return "Reserved Motorized Home";
            case "TAN":
                return "Normal Taxi";
           *//* case "TAR":
                return "Reserved Taxi";*//*
            case "BUN":
                return "Normal Bus";
            case "BUR":
                return "Reserved Bus";
            case "SBN":
                return "Normal School Bus";
            case "SBR":
                return "Reserved School Bus";
            case "SPN":
                return "Normal School Pupil";
            case "COM":
                return "Normal Commercial";
            case "COR":
                return "Reserved Commercial";
            case "TRN":
                return "Normal Trailer";
            case "TRR":
                return "Reserved Trailer";
            case "AMN":
                return "Normal Ambulance";
            case "AMR":
                return "Reserved Ambulance";
            case "ATN":
                return "Authority Vehicle MBRA - WRA - ETC";
            case "AXN":
                return "Authority Vehicle MBTA - MOTORCYCLE - ETC";
            case "MVN":
                return "Normal Municiple Vehicle";
            case "MXR":
                return "Municipal Motorcycle";
            case "STN":
                return "State Vehicle - RMV - MDC - NATIONAL GUARD - ETC";
            case "SXN":
                return "State Vehicle - RMV - MDC - NATIONAL GUARD - ETC";
            case "TA":
                return "Taxi";
            case "TR":
                return "Trailer";
            case "SM":
                return "Semi-Trailer";
            case "SP":
                return "Pupils";
            case "VP":
                return "Van Pool";
            case "CA":
                return "Camper";
            case "MTB":
                return "T";
            case "BU":
                return "Bus";
            case "SB":
                return "School Bus";
            case "ST":
                return "State";
            case "VT":
                return "Veteran  - Flag";
         *//*   case "VT":
                return "Veteran - Pearl Harbor";*//*
            case "PH":
                return "Veteran - Purple Heart";
            case "BB":
                return "Basketball Hall of Fame";
            case "RT":
                return "Right Whale";
            case "RW":
                return "Right Whale";
            case "CI":
                return "Cape of Island";
            case "OS":
                return "Olympic Spirit";
            case "TC":
                return "Invest in Children";
            case "FW":
                return "Preserve the Trust";
            case "BV":
                return "Preserve the Trust";
            case "XPOW":
                return "Ex POW";
            case "HP":
                return "Handicapped Parking";
            case "TV":
                return "Disabled Veteran";
            case "NG":
                return "National Guard";
            default:
                return null;
        }
    }

    public static class PlateColor {
        public String id;
        public String type;
        public boolean checked;

        public PlateColor(String id, String type) {
            this.id = id;
            this.type = type;
        }

    }*/

}

