package com.techwin.vcite.ui.parcel;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.techwin.vcite.BR;
import com.techwin.vcite.R;
import com.techwin.vcite.data.beans.CitationData;
import com.techwin.vcite.data.beans.SearchBean;
import com.techwin.vcite.data.local.entity.CitationBean;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.data.remote.helper.Status;
import com.techwin.vcite.databinding.ActivityStreetnoBinding;
import com.techwin.vcite.databinding.HolderSearchItemBinding;
import com.techwin.vcite.di.base.adapter.SimpleRecyclerViewAdapter;
import com.techwin.vcite.di.base.view.AppActivity;
import com.techwin.vcite.util.dump.InputUtils;
import com.techwin.vcite.util.event.SingleRequestEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.techwin.vcite.ui.parcel.SearchNameActivity.getAddressLine;

public class SearchNoActivity extends AppActivity<ActivityStreetnoBinding, SearchNoActivityVM> {

    private CitationBean citationBean;
    private String streetNo;
    private SimpleRecyclerViewAdapter<String, HolderSearchItemBinding> adapter;
    private ArrayList<String> list = new ArrayList<>();

    @Override
    protected BindingActivity<SearchNoActivityVM> getBindingActivity() {
        return new BindingActivity<>(R.layout.activity_streetno, SearchNoActivityVM.class);
    }

    public static Intent newIntent(Context activity, CitationData citationData) {
        Intent intent = new Intent(activity, SearchNoActivity.class);
        intent.putExtra("bean", citationData);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }


    @SuppressLint("NonConstantResourceId")
    @Override
    protected void subscribeToEvents(final SearchNoActivityVM vm) {
        binding.toolbar.tvTitle.setText(R.string.prperty_lookup);
        adapter = new SimpleRecyclerViewAdapter<>(R.layout.holder_search_item, BR.bean, (v, s) -> {
            streetNo = s;
            binding.tvLine3.setText(getAddressLine(streetNo, citationBean.getStreetName(), citationBean.getUnit()));

        });
        binding.rvOne.setLayoutManager(new LinearLayoutManager(this));
        binding.rvOne.setAdapter(adapter);
        binding.etDns.setHint("Search Street No");
        vm.onClick.observe(this, view -> {
            switch (view != null ? view.getId() : 0) {
                case R.id.iv_back: {
                    finish();
                }
                break;
                case R.id.btn_submit: {
                    if (InputUtils.isEmpty(streetNo))
                        vm.info.setValue("Select Street no");
                    else
                        saveToLocal();
                }
                break;
            }
        });
        vm.obrData.observe(this, (SingleRequestEvent.RequestObserver<CitationBean>) resource -> {
            switch (resource.status) {
                case LOADING:
                    showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    if (resource.data != null) {
                        citationBean = resource.data;
                        streetNo = citationBean.getStreetNo();
                        binding.tvLine3.setText(getAddressLine(streetNo, citationBean.getStreetName(), citationBean.getUnit()));
                        Map<String, String> map = new HashMap<>();
                        map.put("street", citationBean.getStreetName());
                        vm.search("getStreetNumber", map);
                        showProgressDialog(R.string.plz_wait);
                    }
                    dismissProgressDialog();
                    break;
                case ERROR:
                    dismissProgressDialog();
                    break;
            }
        });
        vm.obrSearch.observe(this, new SingleRequestEvent.RequestObserver<SearchBean>() {
            @Override
            public void onRequestReceived(@NonNull Resource<SearchBean> resource) {
                switch (resource.status) {
                    case LOADING:
                        binding.pbOne.setVisibility(View.VISIBLE);
                        break;
                    case SUCCESS:
                        binding.pbOne.setVisibility(View.GONE);
                        if (resource.data != null) {
                            list.clear();
                            if (resource.data.streetNum != null)
                                list.addAll(resource.data.streetNum);
                            adapter.setList(list);
                        }
                        dismissProgressDialog();
                        break;
                    case WARN:
                        binding.pbOne.setVisibility(View.GONE);
                        vm.warn.setValue(resource.message);
                        dismissProgressDialog();
                        break;
                    case ERROR:
                        dismissProgressDialog();
                        binding.pbOne.setVisibility(View.GONE);
                        vm.error.setValue(resource.message);
                        break;
                }
            }
        });
        vm.obrSave.observe(this, (SingleRequestEvent.RequestObserver<Void>) resource -> {
            switch (resource.status) {
                case SUCCESS:
                    viewModel.success.setValue(resource.message);
                    showUnitUi();
                    break;
                case ERROR:
                    viewModel.error.setValue(resource.message);
                    break;
            }
        });
        vm.singleLiveEvent.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if(InputUtils.isEmpty(s)){
                    adapter.setList(list);
                }else {
                adapter.clearList();
                for (String s1 : list) {
                    if (s1 != null && s1.contains(s)) {
                        adapter.addData(s1);
                    }}
                }
            }
        });
        vm.setConsumer(binding.etDns);
    }

    private void showUnitUi() {
        CitationData citationData = getIntent().getParcelableExtra("bean");
        if (citationData != null) {
            Intent intent = SearchUnitActivity.newIntent(this, citationData);
            startNewActivity(intent);
        }
    }


    public void saveToLocal() {
        if (!citationBean.getStreetNo().equals(InputUtils.getTrimString(streetNo))) {
            citationBean.setUnit("");
            citationBean.setParcelid("");
        }
        citationBean.setStreetNo(InputUtils.getTrimString(streetNo));
        viewModel.saveCitation(citationBean);
    }

    @Override
    protected void onStart() {
        super.onStart();
        CitationData citationData = getIntent().getParcelableExtra("bean");
        if (citationData != null) {
            viewModel.getCitation(citationData);
        }
    }
}

