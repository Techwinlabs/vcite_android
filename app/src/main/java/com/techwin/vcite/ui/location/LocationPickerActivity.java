package com.techwin.vcite.ui.location;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.PopupMenu;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.arvind.android.permissions.PermissionHandler;
import com.arvind.android.permissions.Permissions;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.techwin.vcite.R;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.databinding.ActivityLocationPickerBinding;
import com.techwin.vcite.di.base.view.AppActivity;
import com.techwin.vcite.util.location.LiveLocationDetecter;
import com.techwin.vcite.util.location.LocUtils;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;


public class LocationPickerActivity extends AppActivity<ActivityLocationPickerBinding, LocationPickerActivityVM> {
    public static final String TAG = LocationPickerActivity.class.getSimpleName();
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 8675;
    private static final float DEFAULT_ZOOM = 17f;
    public static final String EXTRA_LATLNG = "extralatlng";
    private final String KEY_MAP_STYLES = "mapstyle";
    @Nullable
    private GoogleMap googleMap;
    @Inject
    LiveLocationDetecter locationDetector;
    @Inject
    SharedPref sharedPref;
    @Nullable
    private static Location currentLocation;
    private boolean showCurrentLocation = true;
    private Handler handler = new Handler();
    private boolean moved = false;
    @Nullable
    private String countryCode;

    @Override
    protected BindingActivity<LocationPickerActivityVM> getBindingActivity() {
        return new BindingActivity<>(R.layout.activity_location_picker, LocationPickerActivityVM.class);
    }

    public static Intent newIntent(Context activity) {
        Intent intent = new Intent(activity, LocationPickerActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!Places.isInitialized()) {
            Places.initialize(this, getString(R.string.place_key));
        }
    }

    @Override
    protected void subscribeToEvents(LocationPickerActivityVM vm) {
        binding.toolbar.tvTitle.setText("Select Location");
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.frg_map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(googleMap -> {
                this.googleMap = googleMap;
              /*  try {
                    googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(LocationPickerActivity.this, R.raw.map_style));
                } catch (Resources.NotFoundException e) {
                    Log.e(TAG, "Can't find style. Error: ", e);
                }*/
                googleMap.setMinZoomPreference(5);
                setMapListener();
                setMyLocation();
                setStyle(sharedPref.get(KEY_MAP_STYLES, GoogleMap.MAP_TYPE_NORMAL), false);
                setCurrentLocation(locationDetector.getLastLocation());
            });
        }
        vm.onClick.observe(this, view -> {
            switch (view != null ? view.getId() : 0) {
                case R.id.iv_back:
                    setResult(Activity.RESULT_CANCELED);
                    finish(true);
                    break;
                case R.id.iv_search:
                    showFilter();
                    break;
                case R.id.iv_gps:
                    getCurrentLocation();
                    break;
                case R.id.iv_layer:
                    showLayer(view);
                    break;
                case R.id.btn_done:
                    LatLng latLng = binding.getLatlong();
                    if (latLng != null) {
                        Intent intent = new Intent();
                        intent.putExtra(EXTRA_LATLNG, latLng);
                        setResult(Activity.RESULT_OK, intent);
                        finish(true);
                    } else {
                        vm.error.setValue("Location error");
                    }
                    break;
            }
        });

        locationDetector.observe(this, locCallback -> {
            if (locCallback == null)
                return;
            switch (locCallback.getType()) {
                case ERROR:
                    viewModel.error.setValue(locCallback.getMessage());
                    break;
                case FOUND:
                    setCurrentLocation(locCallback.getLocation());
                    break;
                case OPEN_GPS:
                    try {
                        locCallback.getApiException().startResolutionForResult(LocationPickerActivity.this, LiveLocationDetecter.REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException e) {
                        e.printStackTrace();
                    }

                    break;

            }
        });

        getCurrentLocation();
    }

    private void setCurrentLocation(@Nullable Location location) {
        if (location == null)
            return;
        if (showCurrentLocation) {
            binding.setGpshint("Accurate to " + Math.round(location.getAccuracy()) + " meter");
            if (currentLocation != null && currentLocation.distanceTo(location) > 0)
                currentLocation = location;
            else if (currentLocation == null)
                currentLocation = location;
            Address address = LocUtils.getAddressObject(LocationPickerActivity.this, currentLocation);
            if (address != null) {
                countryCode = address.getCountryCode();
                Log.e("address", address.toString());
                binding.setAddress(address.getAddressLine(0));
                binding.setLatlong(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()));
                setMapLocation(binding.getLatlong(), DEFAULT_ZOOM);
                setLoading(false);
            }

        }

    }

    private void showFilter() {
        startActivityForResult(getPlaceIntent(), PLACE_AUTOCOMPLETE_REQUEST_CODE);
        animateActivity();

    }

    private Intent getPlaceIntent() {
        List<Place.Field> fields = Arrays.asList(Place.Field.ID,
                Place.Field.NAME,
                Place.Field.PHONE_NUMBER,
                Place.Field.TYPES,
                Place.Field.OPENING_HOURS,
                Place.Field.ADDRESS,
                Place.Field.LAT_LNG);
        Autocomplete.IntentBuilder builder = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields);
        if (countryCode != null)
            builder.setCountry(countryCode);
        return builder.build(this);
    }


    private void getCurrentLocation() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(0);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(0);
        Permissions.check(this, Manifest.permission.ACCESS_FINE_LOCATION, null, new PermissionHandler() {
            @Override
            public void onGranted() {
                setMyLocation();
                showCurrentLocation = true;
                locationDetector.startLocationUpdates(mLocationRequest);
                if (currentLocation != null)
                    setCurrentLocation(currentLocation);
                else
                    setLoading(true);
            }
        });
    }

    private void setLoading(boolean b) {
        binding.setLoading(b);
        if (b) binding.svOne.startShimmerAnimation();
        else binding.svOne.stopShimmerAnimation();
    }


    private void setMapListener() {
        if (googleMap != null) {
            googleMap.setOnCameraIdleListener(onCameraIdleListener);
            googleMap.setOnCameraMoveStartedListener(onCameraMoveStartedListener);
        }
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            moved = false;
            LatLng latLng = googleMap.getCameraPosition().target;
            try {
                Address address = LocUtils.getAddressObject(LocationPickerActivity.this, latLng);
                if (address != null) {
                    setLoading(false);
                    Log.e("address", address.toString());
                    binding.setAddress(address.getAddressLine(0));
                    binding.setLatlong(latLng);
                    binding.setGpshint(null);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            setLoading(false);
        }
    };
    private GoogleMap.OnCameraIdleListener onCameraIdleListener = () -> {
        if (moved) {
            showCurrentLocation = false;
            handler.postDelayed(runnable, 1000);
        }
    };

    private GoogleMap.OnCameraMoveStartedListener onCameraMoveStartedListener = i -> {
        if (i == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
            if (!moved)
                setLoading(true);
            moved = true;
            handler.removeCallbacks(runnable);
        }

    };


    private void setMyLocation() {
        if (googleMap != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            googleMap.setMyLocationEnabled(true);
            googleMap.getUiSettings().setMyLocationButtonEnabled(false);

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case LiveLocationDetecter.REQUEST_CHECK_SETTINGS:
                locationDetector.onActivityResult(requestCode, resultCode);
                break;
            case PLACE_AUTOCOMPLETE_REQUEST_CODE: {
                if (resultCode == RESULT_OK) {
                    if (data == null)
                        return;
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    if (place.getLatLng() != null) {
                        try {
                            showCurrentLocation = false;
                            Address address = LocUtils.getAddressObject(LocationPickerActivity.this, place.getLatLng());
                            if (address != null) {
                                setLoading(false);
                                Log.e("address", address.toString());
                                binding.setAddress(address.getAddressLine(0));
                                binding.setLatlong(place.getLatLng());
                                binding.setGpshint(null);
                                setMapLocation(place.getLatLng(), DEFAULT_ZOOM);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            viewModel.error.setValue(e.getMessage());
                        }
                    }
                } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                    if (data == null)
                        return;
                    Status status = Autocomplete.getStatusFromIntent(data);
                    viewModel.error.setValue(status.getStatusMessage());
                }
            }
            break;
        }
    }

    private void setMapLocation(@Nullable LatLng latLng, float zoom) {
        if (googleMap != null && latLng != null) {
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
        }
    }

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_CANCELED);
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        locationDetector.removeLocationUpdates();
        super.onDestroy();
    }

    public void showLayer(View view) {
        PopupMenu popupMenu = new PopupMenu(new ContextThemeWrapper(this, R.style.popupstyle), view);
        popupMenu.getMenuInflater().inflate(R.menu.map_style, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.action_normal:
                    setStyle(GoogleMap.MAP_TYPE_NORMAL, true);
                    return true;
                case R.id.action_satlite:
                    setStyle(GoogleMap.MAP_TYPE_SATELLITE, true);
                    return true;
                case R.id.action_hybrid:
                    setStyle(GoogleMap.MAP_TYPE_HYBRID, true);
                    return true;
                case R.id.action_terion:
                    setStyle(GoogleMap.MAP_TYPE_TERRAIN, true);
                    return true;
                default:
                    return false;
            }
        });
        popupMenu.show();
    }

    public void setStyle(int s, boolean save) {
        if (googleMap != null)
            googleMap.setMapType(s);
        if (save)
            sharedPref.put(KEY_MAP_STYLES, s);
    }

}
