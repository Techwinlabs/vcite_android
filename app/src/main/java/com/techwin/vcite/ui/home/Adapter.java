package com.techwin.vcite.ui.home;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.techwin.vcite.BR;
import com.techwin.vcite.R;
import com.techwin.vcite.data.local.entity.CitationBean;
import com.techwin.vcite.databinding.HolderCitationBinding;
import com.techwin.vcite.di.base.adapter.BaseRecyclerViewAdapter;

public class Adapter extends BaseRecyclerViewAdapter<CitationBean, Adapter.Holder> {
    private Listioner listioner;

    public Adapter(Listioner listioner) {
        this.listioner = listioner;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        HolderCitationBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.holder_citation, viewGroup, false);
        binding.setVariable(BR.callback, listioner);
        return new Holder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        holder.binding.setVariable(BR.bean, dataList.get(i));
        holder.binding.setVariable(BR.missing,listioner.missingElements(dataList.get(i)));
        holder.binding.executePendingBindings();
    }

    public static class Holder extends RecyclerView.ViewHolder {
        HolderCitationBinding binding;

        public Holder(HolderCitationBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
