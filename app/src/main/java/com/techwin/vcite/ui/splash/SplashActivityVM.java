package com.techwin.vcite.ui.splash;


import com.techwin.vcite.data.beans.CampusBean;
import com.techwin.vcite.data.beans.CitTempBean;
import com.techwin.vcite.data.beans.TemplateBean;
import com.techwin.vcite.data.beans.base.ApiResponse;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.remote.helper.NetworkErrorHandler;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.data.repo.BaseRepo;
import com.techwin.vcite.di.base.viewmodel.BaseViewModel;
import com.techwin.vcite.util.Constants;
import com.techwin.vcite.util.event.SingleRequestEvent;
import com.techwin.vcite.util.location.LiveLocationDetecter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function3;
import io.reactivex.schedulers.Schedulers;

public class SplashActivityVM extends BaseViewModel {


    private final SharedPref sharedPref;
    public final LiveLocationDetecter liveLocationDetecter;
    private final NetworkErrorHandler networkErrorHandler;
    final SingleRequestEvent<Boolean> obrData = new SingleRequestEvent<>();
    private final BaseRepo baseRepo;

    @Inject
    public SplashActivityVM(SharedPref sharedPref, LiveLocationDetecter liveLocationDetecter, NetworkErrorHandler networkErrorHandler, BaseRepo baseRepo) {

        this.sharedPref = sharedPref;
        this.liveLocationDetecter = liveLocationDetecter;
        this.networkErrorHandler = networkErrorHandler;
        this.baseRepo = baseRepo;
    }

    /*new B new BiFunction<ApiResponse<TemplateBean>, ApiResponse<TemplateBean>,ApiResponse<CampusBean>, Boolean>() {
                @NonNull
                @Override
                public Boolean apply(@NonNull ApiResponse<TemplateBean> t1, @NonNull ApiResponse<TemplateBean> t2,@NonNull ApiResponse<CampusBean> t3) throws Exception {
                    if (t1.getData() != null) {
                        sharedPref.setTemplate(t1.getData().getCitTempList(), Constants.CITATION_TYPE_PARKING);
                    }
                    if (t2.getData() != null) {
                        sharedPref.setTemplate(t2.getData().getCitTempList(), Constants.CITATION_TYPE_CODE_ENFORCE);
                    }
                    return true;
                }
            }*/
    void getTemplate(int id) {
        obrData.setValue(Resource.loading(null));
        Observable<ApiResponse<TemplateBean>> temp1 = getobserver(id, Constants.CITATION_TYPE_PARKING);
        Observable<ApiResponse<TemplateBean>> temp2 = getobserver(id, Constants.CITATION_TYPE_CODE_ENFORCE);
        Observable<ApiResponse<CampusBean>> temp3 = getCampusObserver(id);
        compositeDisposable.add(Observable.zip(temp1, temp2, temp3, new Function3<ApiResponse<TemplateBean>, ApiResponse<TemplateBean>, ApiResponse<CampusBean>, Boolean>() {
            @NonNull
            @Override
            public Boolean apply(@NonNull ApiResponse<TemplateBean> t1, @NonNull ApiResponse<TemplateBean> t2, @NonNull ApiResponse<CampusBean> t3) throws Exception {
                if (t1.getData() != null) {
                    List<CitTempBean> temp1 = new ArrayList<>();
                    for (CitTempBean bean : t1.getData().getCitTempList()) {
                        if (bean.getCitiationType().equalsIgnoreCase(Constants.CITATION_TYPE_PARKING)) {
                            temp1.add(bean);
                        }
                    }
                    sharedPref.setTemplate(temp1, Constants.CITATION_TYPE_PARKING);
                }
                if (t2.getData() != null) {
                    List<CitTempBean> temp1 = new ArrayList<>();
                    for (CitTempBean bean : t1.getData().getCitTempList()) {
                        if (bean.getCitiationType().equalsIgnoreCase(Constants.CITATION_TYPE_CODE_ENFORCE)) {
                            temp1.add(bean);
                        }
                    }
                    sharedPref.setTemplate(temp1, Constants.CITATION_TYPE_CODE_ENFORCE);
                }
                if (t3.getData() != null) {
                    sharedPref.setCampus(t3.getData());
                }
                return true;
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Boolean>() {
            @Override
            public void accept(Boolean aBoolean) throws Exception {
                obrData.setValue(Resource.success(true, "done"));
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                obrData.setValue(Resource.error(true, networkErrorHandler.getErrMsg(throwable)));
            }
        }));

    }

    private Observable<ApiResponse<TemplateBean>> getobserver(int id, String type) {
        Map<String, String> map = new HashMap<>();
        map.put("custkey", String.valueOf(id));
        map.put("citationType", type);
        return baseRepo.citationTemplate(map, type);

    }

    private Observable<ApiResponse<CampusBean>> getCampusObserver(int id) {
        Map<String, String> map = new HashMap<>();
        map.put("custkey", String.valueOf(id));
        map.put("fieldID", "custfield4");
        return baseRepo.campusType(map);

    }
}
