package com.techwin.vcite.ui.photo;

import static android.app.Activity.RESULT_OK;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;

import com.arvind.android.permissions.PermissionHandler;
import com.arvind.android.permissions.Permissions;
import com.github.drjacky.imagepicker.ImagePicker;
import com.github.drjacky.imagepicker.util.FileUriUtils;
import com.kroegerama.imgpicker.BottomSheetImagePicker;
import com.kroegerama.imgpicker.ButtonType;
import com.techwin.vcite.BR;
import com.techwin.vcite.R;
import com.techwin.vcite.data.beans.CitationData;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.local.entity.CitationBean;
import com.techwin.vcite.data.local.entity.MediaBean;
import com.techwin.vcite.databinding.DialogImagePickerBinding;
import com.techwin.vcite.databinding.FragmentPhotosBinding;
import com.techwin.vcite.databinding.HolderPhotoBinding;
import com.techwin.vcite.di.base.adapter.SimpleRecyclerViewAdapter;
import com.techwin.vcite.di.base.view.AppFragment;
import com.techwin.vcite.di.base.view.BaseCustomDialog;
import com.techwin.vcite.ui.imagePicker.CustomImagePickerActivity;
import com.techwin.vcite.ui.main.MainActivity;
import com.techwin.vcite.ui.multiImagePicker.MultipleImagePickerActivity;
import com.techwin.vcite.ui.photo.detail.ImageDetailActivity;
import com.techwin.vcite.util.AppUtils;
import com.techwin.vcite.util.Constants;
import com.techwin.vcite.util.event.SingleRequestEvent;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class PhotoFragment extends AppFragment<FragmentPhotosBinding, PhotoFragmentVM> implements BottomSheetImagePicker.OnImagesSelectedListener {
    public static final String TAG = "PhotoFragment";
    private static final int CODE_TICKET = 101;
    private static final int CODE_IMAGE = 102;
    @Nullable
    private MainActivity mainActivity;
    @Inject
    SharedPref sharedPref;
    private SimpleRecyclerViewAdapter<MediaBean, HolderPhotoBinding> adapter;
    @Nullable
    private CitationData citationData;
    private List<MediaBean> medias = new ArrayList<>();
    @Nullable
    private CitationBean ciatationBean;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mainActivity = (MainActivity) context;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected BindingFragment<PhotoFragmentVM> getBindingFragment() {
        return new BindingFragment<>(R.layout.fragment_photos, PhotoFragmentVM.class);
    }

    public static PhotoFragment newInstance() {
        return new PhotoFragment();
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    protected void subscribeToEvents(final PhotoFragmentVM vm) {
        vm.onClick.observe(this, view -> {
            switch (view.getId()) {
                case R.id.iv_save:
                    // saveToLocal();
                    if (mainActivity != null && ciatationBean != null) {
                        mainActivity.startBitmapCapture(ciatationBean, view);
                    }
                    break;
            }
        });
        adapter = new SimpleRecyclerViewAdapter<>(R.layout.holder_photo, BR.bean);
        adapter.setCallback(new SimpleRecyclerViewAdapter.SimpleCallback<MediaBean>() {
            @Override
            public void onItemClick(View v, MediaBean imageBean) {
                switch (v.getId()) {
                    case R.id.iv_add: {
                        Permissions.check(requireActivity(), Manifest.permission.READ_EXTERNAL_STORAGE, null, new PermissionHandler() {
                            @Override
                            public void onGranted() {
                                showPickerDialog();
                            }
                        });
                        /*ImagePicker.Companion.with(getActivity())
                                .maxResultSize(1080, 1080, true)    //Final image resolution will be less than 1080 x 1080(Optional)
                                .createIntentFromDialog((new Function1<Intent, Unit>() {
                                    @Override
                                    public Unit invoke(Intent intent) {
                                        launcherImage.launch(intent);
                                        return Unit.INSTANCE;
                                    }
                                }));*/



                        /*Intent intent = ImagePicker.with(getActivity())
                                .maxResultSize(1080, 1080, true)
                                .cameraOnly()    //User can only capture image using Camera
                                .createIntent();
                        launcherImage.launch(intent);*/

                        /*Intent intent = ImagePicker.with(getActivity())
                                .maxResultSize(1080, 1080, true)
                                .galleryOnly()    //User can only select image from Gallery
                                .createIntent();    //Default Request Code is ImagePicker.REQUEST_CODE
                        launcherImage.launch(intent);*/

                    }
                    break;

                    case R.id.tv_ticket:
                        Permissions.check(requireActivity(), Manifest.permission.READ_EXTERNAL_STORAGE, null, new PermissionHandler() {
                            @Override
                            public void onGranted() {
                                Intent intent = CustomImagePickerActivity.newIntent(requireActivity());
                                launcherTicket.launch(intent);
                            }
                        });
                        /*if (Build.VERSION.SDK_INT == Build.VERSION_CODES.Q) {
                            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            launcherTicketOne.launch(cameraIntent);
                        } else {*/

//                        Intent intent = ImagePicker.Companion.with(getActivity())
//                                .crop()
//                                .cropFreeStyle()
//                                .cameraOnly()//Crop image(Optional), Check Customization for more option
//                                .maxResultSize(1080, 1080, true)    //Final image resolution will be less than 1080 x 1080(Optional)
//                                .createIntent();
//                        launcherTicket.launch(intent);
                        /*}*/
                        break;
                }
            }

            @Override
            public void onItemClick(View view, int pos) {
                if (view.getId() == R.id.iv_delete) {
                    new AlertDialog.Builder(requireActivity())
                            .setTitle("Photo delete")
                            .setMessage("Are you sure want to delete this photo?")
                            .setNegativeButton("No", (dialog, which) -> dialog.dismiss())
                            .setPositiveButton("Yes", (dialog, which) -> {
                                dialog.dismiss();
                                if (adapter.getList().get(pos).getId() > 0) {
                                    vm.deleteMediaBeans(adapter.getList().get(pos), citationData);
                                }
                            })
                            .show();
                } else if (view.getId() == R.id.iv_eye) {
                    Intent intent = ImageDetailActivity.newIntent(baseContext, pos, citationData);
                    startNewActivity(intent);
                }
            }
        });

        binding.rvOne.setLayoutManager(new GridLayoutManager(baseContext, 2));
        binding.rvOne.setAdapter(adapter);
        vm.obrData.observe(this, (SingleRequestEvent.RequestObserver<List<MediaBean>>) resource -> {
            switch (resource.status) {
                case LOADING:
                    // showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    medias.clear();
                    if (resource.data != null) {
                        medias.addAll(resource.data);
                    }
                    setDataInAdapter();
                    break;
                case ERROR:
                    dismissProgressDialog();
                    medias.clear();
                    setDataInAdapter();
                    break;
            }
        });

        vm.obrSave.observe(this, (SingleRequestEvent.RequestObserver<Void>) resource -> {
            switch (resource.status) {
                case SUCCESS:
                    vm.getMediaBeans(citationData);
                    //     viewModel.success.setValue(resource.message);
                    break;
                case ERROR:
                    viewModel.error.setValue(resource.message);
                    break;
            }
        });
        vm.obrCitationData.observe(this, (SingleRequestEvent.RequestObserver<CitationBean>) resource -> {
            switch (resource.status) {
                case LOADING:
                    //showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    ciatationBean = resource.data;
                    break;
                case ERROR:
                    break;
            }
        });
        loadData();
    }

    private BaseCustomDialog<DialogImagePickerBinding> dialog;

    @SuppressLint("NonConstantResourceId")
    private void showPickerDialog() {
        dialog = new BaseCustomDialog<>(baseContext, R.layout.dialog_image_picker, view -> {
            switch (view.getId()) {
                case R.id.tvCamera:
                    dialog.dismiss();
                    Intent intent = MultipleImagePickerActivity.newIntent(requireActivity());
                    multipleImageCapture.launch(intent);
//                    if (Build.VERSION.SDK_INT == Build.VERSION_CODES.Q) {
//                        // only for android version 10
//                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                        launcherCamera.launch(cameraIntent);
//                    } else {
//                    Intent intent = ImagePicker.with(requireActivity())
//                            .maxResultSize(1080, 1080, true)
//                            .cameraOnly()    //User can only capture image using Camera
//                            .createIntent();
//                    launcherImage.launch(intent);
//                    }
                    break;
                case R.id.tvGallery:
                    dialog.dismiss();
                    final int maxNumPhotosAndVideos = 10;
                    Intent intent2 = new Intent();
                    intent2.setType("image/*");
                    intent2.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    intent2.setAction(Intent.ACTION_GET_CONTENT);
                    launcherImage.launch(intent2);
//                    Intent intent1 = ImagePicker.with(requireActivity())
//                            .maxResultSize(1080, 1080, true)
//                            .galleryOnly()    //User can only select image from Gallery
//                            .createIntent();    //Default Request Code is ImagePicker.REQUEST_CODE
//                    launcherImage.launch(intent1);
                    break;
                case R.id.tvCancel:
                    dialog.dismiss();
                    break;
            }
        });

        dialog.getBinding().tvTitle.setText("Choose");
        dialog.show();
    }

    private final ActivityResultLauncher<Intent> multipleImageCapture =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), (ActivityResult result) -> {
                if (result.getResultCode() == RESULT_OK && result.getData() != null) {
                    if (citationData != null) {
                        ArrayList<String> list = result.getData().getStringArrayListExtra("IMAGES_DATA");
                        saveImageList(list);
                    }
                }
            });

    @Override
    public void onResume() {
        super.onResume();
        loadData();
    }

    private void loadData() {
        if (mainActivity != null) {
            citationData = mainActivity.citationData;
            viewModel.getMediaBeans(citationData);
            viewModel.getCitation(citationData);
            if (mainActivity.citationData != null) {
                binding.tvTitle.setText(String.format("Citations #%s", mainActivity.citationData.getCitationNo()));
            }
        }
    }

    public List<MediaBean> getMedias() {
        return medias;
    }

    private void setDataInAdapter() {
        List<MediaBean> beans = new ArrayList<>();
        MediaBean mediaBean = new MediaBean();
        mediaBean.setItemtype(1);
        beans.add(mediaBean);
        boolean hasTicket = false;
        if (medias != null) {
            for (MediaBean s : medias) {
                if (s.getPicType() == Constants.PIC_TYPE_TICKET) {
                    beans.add(1, s);
                    hasTicket = true;
                } else beans.add(s);
            }
        }
        if (!hasTicket) {
            MediaBean mediaBean1 = new MediaBean();
            mediaBean1.setItemtype(2);
            beans.add(1, mediaBean1);
        }
        adapter.setList(beans);
    }
/*
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case CODE_IMAGE: {
                Log.e(TAG, "Result");
                if (resultCode == RESULT_OK) {
                    String imageFile = ImagePicker.Companion.getFilePath(data);
                    if (citationData != null) {
                        String path = AppUtils.imagetoBase64(imageFile);
                        MediaBean mediaBean = new MediaBean();
                        mediaBean.setCitationNo(citationData.citationNo);
                        mediaBean.setOrgNo(citationData.orgNo);
                        mediaBean.setImage(path);
                        List<MediaBean> bean = new ArrayList<>();
                        bean.add(mediaBean);
                        viewModel.saveMediaBeans(bean);
                    }
                } else if (resultCode == ImagePicker.RESULT_ERROR) {
                    viewModel.error.setValue(ImagePicker.Companion.getError(data));
                }
            }
            break;
            case CODE_TICKET: {
                Log.e(TAG, "Result");
                if (resultCode == RESULT_OK) {
                    String imageFile = ImagePicker.Companion.getFilePath(data);
                    if (citationData != null) {
                        String path = AppUtils.imagetoBase64(imageFile);
                        MediaBean mediaBean = new MediaBean();
                        mediaBean.setCitationNo(citationData.citationNo);
                        mediaBean.setOrgNo(citationData.orgNo);
                        mediaBean.setImage(path);
                        mediaBean.setTicket(true);
                        List<MediaBean> bean = new ArrayList<>();
                        bean.add(mediaBean);
                        viewModel.saveMediaBeans(bean);
                    }
                } else if (resultCode == ImagePicker.RESULT_ERROR) {
                    viewModel.error.setValue(ImagePicker.Companion.getError(data));
                }
            }
        }
    }*/

    private final ActivityResultLauncher<Intent> launcherCamera =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), (ActivityResult result) -> {
                if (result.getResultCode() == RESULT_OK && result.getData() != null) {
                    //String file = FileUriUtils.INSTANCE.getRealPath(baseContext, getImageUri(requireActivity(), (Bitmap) result.getData().getExtras().get("data")));
                    if (citationData != null) {
                        String path = AppUtils.imagetoBase64((Bitmap) result.getData().getExtras().get("data"));
                        MediaBean mediaBean = new MediaBean();
                        mediaBean.setCitationNo(citationData.citationNo);
                        mediaBean.setOrgNo(citationData.orgNo);
                        mediaBean.setImage(path);
                        List<MediaBean> bean = new ArrayList<>();
                        bean.add(mediaBean);
                        viewModel.saveMediaBeans(bean);
                    }
                } else if (result.getResultCode() == ImagePicker.RESULT_ERROR) {
                    viewModel.error.setValue(ImagePicker.Companion.getError(result.getData()));
                }

            });

    private final ActivityResultLauncher<Intent> launcherImage =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), (ActivityResult result) -> {
                if (result.getResultCode() == RESULT_OK
                        && null != result.getData()) {
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    if (result.getData().getClipData() != null) {
                        ClipData mClipData = result.getData().getClipData();
                        ArrayList<Bitmap> list = new ArrayList<>();
                        for (int i = 0; i < mClipData.getItemCount(); i++) {
                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();
                            try {
                                Bitmap bitmap = MediaStore.Images.Media.getBitmap(requireActivity().getContentResolver(), uri);
                                list.add(bitmap);
                            } catch (IOException e) {
                                e.printStackTrace();
                                Toast.makeText(mainActivity, e.getMessage() + "", Toast.LENGTH_SHORT).show();
                            }
                            // Get the cursor
                            Cursor cursor = requireActivity().getContentResolver().query(uri, filePathColumn, null, null, null);
                            // Move to first row
                            cursor.moveToFirst();
                            cursor.close();

                        }
                        saveBitmapList(list);

                    } else if (result.getData().getData() != null) {
                        ArrayList<Bitmap> list = new ArrayList<>();
                        Bitmap bitmap;
                        try {
                            bitmap = MediaStore.Images.Media.getBitmap(requireActivity().getContentResolver(), result.getData().getData());
                            list.add(bitmap);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        saveBitmapList(list);
                    }
                }
              /*  if (result.getResultCode() == RESULT_OK && result.getData() != null) {
                    String file = FileUriUtils.INSTANCE.getRealPath(baseContext, result.getData().getData());
                    if (citationData != null) {
                        String path = AppUtils.imagetoBase64(file);
                        //showProgressDialog(R.string.plz_wait);
                        //String path = AppUtils.convertPathToBitmap(file, getActivity());
                        //dismissProgressDialog();
                        MediaBean mediaBean = new MediaBean();
                        mediaBean.setCitationNo(citationData.citationNo);
                        mediaBean.setOrgNo(citationData.orgNo);
                        mediaBean.setImage(path);
                        List<MediaBean> bean = new ArrayList<>();
                        bean.add(mediaBean);
                        viewModel.saveMediaBeans(bean);
                    }
                } else if (result.getResultCode() == ImagePicker.RESULT_ERROR) {
                    viewModel.error.setValue(ImagePicker.Companion.getError(result.getData()));
                }*/

            });

    private void saveBitmapList(List<Bitmap> list) {
        if (list != null && citationData != null) {
            for (int i = 0; i < list.size(); i++) {
                String path = AppUtils.imagetoBase64(list.get(i));
                MediaBean mediaBean = new MediaBean();
                mediaBean.setCitationNo(citationData.citationNo);
                mediaBean.setOrgNo(citationData.orgNo);
                mediaBean.setImage(path);
                List<MediaBean> bean = new ArrayList<>();
                bean.add(mediaBean);
                viewModel.saveMediaBeans(bean);
            }
        }
    }

    private void saveImageList(List<String> list) {
        if (list != null && citationData != null) {
            for (int i = 0; i < list.size(); i++) {
                String path = AppUtils.imagetoBase64(list.get(i));
                MediaBean mediaBean = new MediaBean();
                mediaBean.setCitationNo(citationData.citationNo);
                mediaBean.setOrgNo(citationData.orgNo);
                mediaBean.setImage(path);
                List<MediaBean> bean = new ArrayList<>();
                bean.add(mediaBean);
                viewModel.saveMediaBeans(bean);
            }
        }
    }

    private final ActivityResultLauncher<Intent> launcherTicket =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), (ActivityResult result) -> {

                if (result.getResultCode() == RESULT_OK && result.getData() != null) {
                    try {
                        String imageFile = result.getData().getSerializableExtra("image_arr").toString();
                        if (citationData != null) {
                            String path = AppUtils.imagetoBase64(imageFile);
                            //showProgressDialog(R.string.plz_wait);
                            //String path = AppUtils.convertPathToBitmap(imageFile, getActivity());
                            //dismissProgressDialog();
                            MediaBean mediaBean = new MediaBean();
                            mediaBean.setCitationNo(citationData.citationNo);
                            mediaBean.setOrgNo(citationData.orgNo);
                            mediaBean.setImage(path);
                            mediaBean.setPicType(Constants.PIC_TYPE_TICKET);
                            List<MediaBean> bean = new ArrayList<>();
                            bean.add(mediaBean);
                            viewModel.saveMediaBeans(bean);

                        }
                    } catch (Exception e) {

                    }
                }
//
//                if (result.getResultCode() == RESULT_OK) {
//                    String imageFile = ImagePicker.Companion.getFilePath(result.getData());
//                    if (citationData != null) {
//                        String path = AppUtils.imagetoBase64(imageFile);
//                        //showProgressDialog(R.string.plz_wait);
//                        //String path = AppUtils.convertPathToBitmap(imageFile, getActivity());
//                        //dismissProgressDialog();
//                        MediaBean mediaBean = new MediaBean();
//                        mediaBean.setCitationNo(citationData.citationNo);
//                        mediaBean.setOrgNo(citationData.orgNo);
//                        mediaBean.setImage(path);
//                        mediaBean.setPicType(Constants.PIC_TYPE_TICKET);
//                        List<MediaBean> bean = new ArrayList<>();
//                        bean.add(mediaBean);
//                        viewModel.saveMediaBeans(bean);
//
//                    }
//                } else if (result.getResultCode() == ImagePicker.RESULT_ERROR) {
//                    viewModel.error.setValue(ImagePicker.Companion.getError(result.getData()));
//                }

            });

    private final ActivityResultLauncher<Intent> launcherTicketOne =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), (ActivityResult result) -> {
                if (result.getResultCode() == RESULT_OK) {
                    //String imageFile = ImagePicker.Companion.getFilePath(result.getData());
                    if (citationData != null) {
                        String path = AppUtils.imagetoBase64((Bitmap) result.getData().getExtras().get("data"));
                        MediaBean mediaBean = new MediaBean();
                        mediaBean.setCitationNo(citationData.citationNo);
                        mediaBean.setOrgNo(citationData.orgNo);
                        mediaBean.setImage(path);
                        mediaBean.setPicType(Constants.PIC_TYPE_TICKET);
                        List<MediaBean> bean = new ArrayList<>();
                        bean.add(mediaBean);
                        viewModel.saveMediaBeans(bean);

                    }
                } else if (result.getResultCode() == ImagePicker.RESULT_ERROR) {
                    viewModel.error.setValue(ImagePicker.Companion.getError(result.getData()));
                }

            });


    @Override
    public void onImagesSelected(@NonNull List<? extends Uri> uris, @Nullable String
            s) {
        for (Uri uri : uris) {

        }
    }
}
