package com.techwin.vcite.ui.selector.state;


import android.widget.EditText;

import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.remote.helper.NetworkErrorHandler;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.data.repo.BaseRepo;
import com.techwin.vcite.di.base.viewmodel.BaseViewModel;
import com.techwin.vcite.util.event.SingleLiveEvent;
import com.techwin.vcite.util.event.SingleRequestEvent;
import com.techwin.vcite.util.misc.RxSearchObservable;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;

public class StateActivityVM extends BaseViewModel {
    private final SharedPref sharedPref;
    private final NetworkErrorHandler networkErrorHandler;
    final SingleRequestEvent<List<StateActivity.DummyBean>> obrData = new SingleRequestEvent<>();
    private final BaseRepo baseRepo;
    final SingleLiveEvent<String> singleLiveEvent = new SingleLiveEvent<>();

    @Inject
    public StateActivityVM(SharedPref sharedPref, NetworkErrorHandler networkErrorHandler, BaseRepo baseRepo) {
        this.sharedPref = sharedPref;
        this.networkErrorHandler = networkErrorHandler;
        this.baseRepo = baseRepo;
    }

    void setConsumer(EditText editText) {
        compositeDisposable.add(RxSearchObservable.from(editText).
                debounce(RxSearchObservable.DEFAULT_WAIT, TimeUnit.MILLISECONDS)
                .filter(s -> true).distinctUntilChanged()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(singleLiveEvent::setValue));
    }

    void getList() {
        obrData.setValue(Resource.success(StateActivity.getSatesList(), "from local"));
      /*  List<VelTypeLnn.VehtAbbrBean> s = sharedPref.getVehMake();
        if (s != null) {
            obrData.setValue(Resource.success(s, "from local"));
        } else {
            baseRepo.vehMake().subscribe(new SingleObserver<ApiResponse<VelTypeLnn>>() {
                @Override
                public void onSubscribe(Disposable d) {
                    compositeDisposable.add(d);
                    obrData.setValue(Resource.loading(null));
                }

                @Override
                public void onSuccess(ApiResponse<VelTypeLnn> listApiResponse) {
                    if (listApiResponse.getData() != null)
                        obrData.setValue(Resource.success(listApiResponse.getData().vehtAbbr, "Done"));
                }

                @Override
                public void onError(Throwable e) {
                    obrData.setValue(Resource.error(null, networkErrorHandler.getErrMsg(e)));
                }
            });
        }*/
    }

}

