package com.techwin.vcite.ui.scan.text;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.Image;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.arvind.android.permissions.PermissionHandler;
import com.arvind.android.permissions.Permissions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.mlkit.vision.camera.CameraSourceConfig;
import com.google.mlkit.vision.camera.CameraXSource;
import com.google.mlkit.vision.camera.DetectionTaskCallback;
import com.google.mlkit.vision.common.Detector;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.text.Text;
import com.google.mlkit.vision.text.TextRecognition;
import com.google.mlkit.vision.text.TextRecognizer;
import com.google.mlkit.vision.text.TextRecognizerOptions;
import com.techwin.vcite.BR;
import com.techwin.vcite.R;
import com.techwin.vcite.databinding.ActivityTextScanBinding;
import com.techwin.vcite.databinding.HolderVinNoBinding;
import com.techwin.vcite.di.base.adapter.SimpleRecyclerViewAdapter;
import com.techwin.vcite.di.base.view.AppActivity;
import com.techwin.vcite.util.decoration.ListVerticalItemDecoration;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.List;

public class TextScanActivity extends AppActivity<ActivityTextScanBinding, TextScanActivityVM> {


    private static final String TAG = "TextScanActivity";
    private static final int requestPermissionID = 567;
    private SimpleRecyclerViewAdapter<Row, HolderVinNoBinding> adapter;

    @Override
    protected BindingActivity<TextScanActivityVM> getBindingActivity() {
        return new BindingActivity<>(R.layout.activity_text_scan, TextScanActivityVM.class);
    }

    public static Intent newIntent(Context activity) {
        Intent intent = new Intent(activity, TextScanActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }


    @Override
    protected void subscribeToEvents(final TextScanActivityVM vm) {
        binding.toolbar.tvTitle.setText("Vin Scan");
        vm.onClick.observe(this, new Observer<View>() {
            @Override
            public void onChanged(@Nullable View view) {
                switch (view != null ? view.getId() : 0) {
                    case R.id.iv_scan:

                        break;
                    case R.id.ll_continue:

                        break;
                    case R.id.iv_back:
                        finish(true);
                        break;

                }
            }
        });

        adapter = new SimpleRecyclerViewAdapter<>(R.layout.holder_vin_no, BR.bean);
        adapter.setCallback((v, resultsBean) -> {

            Intent intent = new Intent();
            intent.putExtra("data", resultsBean.text);
            // intent.putExtra("image", resultsBean.bitmap);
            setResult(Activity.RESULT_OK, intent);
            finish(true);
        });
        binding.rvOne.addItemDecoration(new ListVerticalItemDecoration(this, R.dimen.dp_5));
        binding.rvOne.setLayoutManager(new LinearLayoutManager(this));
        binding.rvOne.setAdapter(adapter);


    }

    void onText(Text task, InputImage inputImage) {
        List<Text.TextBlock> textBlocks = task.getTextBlocks();
        for (Text.TextBlock t : textBlocks) {
            String s = t.getText().trim();
            if (TextUtils.isEmpty(s))
                continue;
            s = s.replaceAll("\\s+", "");
            if (s.length() == 16) {
                Log.e("text", s);

                for (Row row : adapter.getList()) {
                    if (row.text.equals(s))
                        return;
                }

                //     Bitmap bitmap = getBitmap(inputImage.getMediaImage());
                Row row = new Row(s);
                row.image = inputImage.getMediaImage();
                adapter.add(0, row);
                binding.rvOne.scrollToPosition(0);
            }
        }

    }




    private CameraXSource mCameraSource;
    private InputImage inputImage;

    private void startCameraSource() {

        final TextRecognizer textRecognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS);
        CameraSourceConfig cameraSourceConfig = new CameraSourceConfig.Builder(getApplicationContext(), new Detector<Text>() {

            @NonNull
            @NotNull
            @Override
            public Task<Text> process(@NonNull @NotNull InputImage i) {
                inputImage = i;
                return textRecognizer.process(i);
            }

            @Override
            public void close() throws IOException {

            }
        }, new DetectionTaskCallback<Text>() {
            @Override
            public void onDetectionTaskReceived(@NonNull @NotNull Task<Text> task) {

                task.addOnSuccessListener(new OnSuccessListener<Text>() {
                    @Override
                    public void onSuccess(@NonNull @NotNull Text o) {
                        onText(o, inputImage);

                    }
                });
            }
        }).setFacing(CameraSourceConfig.CAMERA_FACING_BACK)
                .setRequestedPreviewSize(1024, 768)
                .build();
        mCameraSource = new CameraXSource(cameraSourceConfig, binding.surfaceView);
        if (ActivityCompat.checkSelfPermission(TextScanActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mCameraSource.start();
    }


    public static class Row {
        public String text;

        public Image image;

        public Row(String text) {
            this.text = text;
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (mCameraSource != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mCameraSource.start();
        } else {
            Permissions.check(this, Manifest.permission.CAMERA, 0, new PermissionHandler() {
                @Override
                public void onGranted() {
                    startCameraSource();
                }
            });
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mCameraSource != null) {
            mCameraSource.stop();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mCameraSource != null) {
            mCameraSource.close();
        }
    }


}

