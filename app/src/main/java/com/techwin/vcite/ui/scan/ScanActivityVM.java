package com.techwin.vcite.ui.scan;


import android.util.Log;

import com.techwin.vcite.data.beans.CitationData;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.local.dao.CitationDao;
import com.techwin.vcite.data.local.dao.TransactionDao;
import com.techwin.vcite.data.local.entity.CitationBean;
import com.techwin.vcite.data.local.entity.TicketBean;
import com.techwin.vcite.data.remote.helper.NetworkErrorHandler;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.di.base.viewmodel.BaseViewModel;
import com.techwin.vcite.ui.home.CitationsFragment;
import com.techwin.vcite.util.event.SingleRequestEvent;
import com.techwin.vcite.util.location.LiveLocationDetecter;

import java.util.Date;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class ScanActivityVM extends BaseViewModel {


    private final SharedPref sharedPref;
    public final LiveLocationDetecter liveLocationDetecter;
    private final NetworkErrorHandler networkErrorHandler;
    final SingleRequestEvent<CitationData> obrData = new SingleRequestEvent<>();
    ;
    private final TransactionDao transactionDao;
    private final CitationDao citationDao;
    final SingleRequestEvent<CitationData> obrSave = new SingleRequestEvent<>();

    @Inject
    public ScanActivityVM(SharedPref sharedPref, LiveLocationDetecter liveLocationDetecter, NetworkErrorHandler networkErrorHandler, TransactionDao transactionDao, CitationDao citationDao) {

        this.sharedPref = sharedPref;
        this.liveLocationDetecter = liveLocationDetecter;
        this.networkErrorHandler = networkErrorHandler;
        this.transactionDao = transactionDao;
        this.citationDao = citationDao;
    }

    void saveTicket(CitationData citationData) {
        compositeDisposable.add(Observable.fromCallable(new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                TicketBean ticketBean = new TicketBean();
                ticketBean.setCitationNo(citationData.citationNo);
                ticketBean.setOrgNo(citationData.orgNo);
                ticketBean.setTicketType(citationData.citationType);
                ticketBean.setCreatedOn(new Date());
                return transactionDao.saveTicketTrans(ticketBean);
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(Long aLong) throws Exception {
                        obrSave.setValue(Resource.success(citationData, "Saved"));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        obrSave.setValue(Resource.success(citationData, "Error :" + throwable.getMessage()));
                        Log.e(TAG, "error" + throwable.getMessage());
                    }
                }));
    }

    void getCitation(CitationData citationData) {
        obrData.setValue(Resource.loading(null));
        compositeDisposable.add(Observable.fromCallable(() -> citationDao.loadCitation(citationData.citationNo, citationData.orgNo, citationData.citationType))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<CitationBean>() {
                               @Override
                               public void accept(CitationBean bean) throws Exception {
                                   citationData.setUploaded(bean.isUploaded());
                                   obrData.setValue(Resource.success(citationData, "done"));
                               }
                           }, new Consumer<Throwable>() {
                               @Override
                               public void accept(Throwable e) throws Exception {
                                   obrData.setValue(Resource.error(citationData, e.getMessage()));
                               }
                           }
                ));


    }
}
