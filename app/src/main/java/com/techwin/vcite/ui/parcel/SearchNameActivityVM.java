package com.techwin.vcite.ui.parcel;


import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;

import com.techwin.vcite.data.beans.CitationData;
import com.techwin.vcite.data.beans.SearchBean;
import com.techwin.vcite.data.beans.base.ApiResponse;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.local.dao.CitationDao;
import com.techwin.vcite.data.local.dao.TransactionDao;
import com.techwin.vcite.data.local.entity.CitationBean;
import com.techwin.vcite.data.remote.helper.NetworkErrorHandler;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.data.repo.BaseRepo;
import com.techwin.vcite.di.base.viewmodel.BaseViewModel;
import com.techwin.vcite.util.event.SingleLiveEvent;
import com.techwin.vcite.util.event.SingleRequestEvent;
import com.techwin.vcite.util.misc.RxSearchObservable;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;

public class SearchNameActivityVM extends BaseViewModel {
    private final SharedPref sharedPref;
    private final NetworkErrorHandler networkErrorHandler;
    private final BaseRepo webRepo;
    final SingleLiveEvent<String> singleLiveEvent = new SingleLiveEvent<>();
    final SingleRequestEvent<SearchBean> obrSearch = new SingleRequestEvent<>();
    private final TransactionDao transactionDao;
    final SingleRequestEvent<Void> obrSave = new SingleRequestEvent<>();
    final SingleRequestEvent<CitationBean> obrData = new SingleRequestEvent<>();

    private final CitationDao citationDao;

    @Inject
    public SearchNameActivityVM(SharedPref sharedPref, NetworkErrorHandler networkErrorHandler, BaseRepo webRepo, TransactionDao transactionDao, CitationDao citationDao) {
        this.sharedPref = sharedPref;
        this.networkErrorHandler = networkErrorHandler;
        this.webRepo = webRepo;
        this.transactionDao = transactionDao;
        this.citationDao = citationDao;
    }

    void getCitation(CitationData citationData) {
        obrData.setValue(Resource.loading(null));
        compositeDisposable.add(Observable.fromCallable(() -> citationDao.loadCitation(citationData.citationNo, citationData.orgNo, citationData.citationType))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(citationBean ->
                                obrData.setValue(Resource.success(citationBean, "done")),
                        e -> obrData.setValue(Resource.error(null, e.getMessage()))));


    }

    void saveCitation(CitationBean citationBean) {
        compositeDisposable.add(Observable.fromCallable(() -> transactionDao.saveCitationTrans(citationBean)).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(aLong -> {
                    Log.e(TAG, "insert" + aLong);
                    obrSave.setValue(Resource.success(null, "Saved"));
                }, throwable -> {
                    obrSave.setValue(Resource.success(null, "Error :" + throwable.getMessage()));
                    Log.e(TAG, "error" + throwable.getMessage());
                }));
    }

    void setConsumer(EditText editText) {
        compositeDisposable.add(RxSearchObservable.from(editText).
                debounce(RxSearchObservable.DEFAULT_WAIT, TimeUnit.MILLISECONDS)
                .filter(s -> !TextUtils.isEmpty(s)).distinctUntilChanged()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(singleLiveEvent::setValue));
    }

    void search(String url, Map<String, String> map) {
        webRepo.getSearch(url, map).observeOn(AndroidSchedulers.mainThread())
                .map(new Function<ApiResponse<SearchBean>, ApiResponse<SearchBean>>() {
                    @Override
                    public ApiResponse<SearchBean> apply(@NotNull ApiResponse<SearchBean> searchBeanApiResponse) throws Exception {
                        if (searchBeanApiResponse.getData() != null && searchBeanApiResponse.getData().streetNum != null) {
                            List<String> list = new ArrayList<>();
                            for (String s : searchBeanApiResponse.getData().streetNum) {
                                if (!list.contains(s)) {
                                    list.add(s);
                                }

                            }

                            searchBeanApiResponse.getData().streetNum = list;
                        }
                        return searchBeanApiResponse;
                    }
                })
                .subscribeOn(Schedulers.io()).subscribe(new SingleObserver<ApiResponse<SearchBean>>() {
            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
                obrSearch.setValue(Resource.loading(null));
            }

            @Override
            public void onSuccess(ApiResponse<SearchBean> listApiResponse) {
                obrSearch.setValue(Resource.success(listApiResponse.getData(), "Load from server"));
            }

            @Override
            public void onError(Throwable e) {
                obrSearch.setValue(Resource.error(null, networkErrorHandler.getErrMsg(e)));
            }
        });
    }
}
