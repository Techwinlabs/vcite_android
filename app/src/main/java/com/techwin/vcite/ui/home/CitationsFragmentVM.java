package com.techwin.vcite.ui.home;

import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.local.dao.CitationDao;
import com.techwin.vcite.data.local.dao.TransactionDao;
import com.techwin.vcite.data.local.entity.CitationBean;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.di.base.viewmodel.BaseViewModel;
import com.techwin.vcite.util.event.SingleRequestEvent;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class CitationsFragmentVM extends BaseViewModel {

    final SingleRequestEvent<List<CitationBean>> obrData = new SingleRequestEvent<>();
    private final CitationDao citationDao;
    private final TransactionDao transactionDao;
    private final SharedPref sharedPref;

    @Inject
    public CitationsFragmentVM(CitationDao citationDao, TransactionDao transactionDao, SharedPref sharedPref) {
        this.citationDao = citationDao;
        this.transactionDao = transactionDao;
        this.sharedPref = sharedPref;
    }

   /* void getCitationList() {
        obrData.setValue(Resource.loading(null));
        compositeDisposable.add(Observable.fromCallable(() -> citationDao.loadAllCitation())
                .subscribeOn(Schedulers.io())
                .delay(1000, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(citationBean ->
                                obrData.setValue(Resource.success(citationBean, "done")),
                        e -> obrData.setValue(Resource.error(null, e.getMessage()))));


    }*/

    void getCitationList() {
        String orgNo = String.valueOf(sharedPref.getUser().CustKey);
        obrData.setValue(Resource.loading(null));
        compositeDisposable.add(citationDao.loadAllCitationFlowable(orgNo)
                .subscribeOn(Schedulers.io())
                .delay(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(citationBean -> obrData.setValue(Resource.success(citationBean, "done")), e -> obrData.setValue(Resource.error(null, e.getMessage()))));


    }

    void clearAllCitation() {
        String orgNo = String.valueOf(sharedPref.getUser().CustKey);
        compositeDisposable.add(Observable.fromCallable(() -> {
            transactionDao.clearCitation(orgNo);
            return true;
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(aLong -> {
                    success.setValue(Resource.success(null, "Done"));
                    getCitationList();
                }, throwable -> error.setValue(Resource.success(null, "Error :" + throwable.getMessage()))));
    }

    void clear7daybeforeCitation() {
        compositeDisposable.add(Observable.fromCallable(() -> {
            transactionDao.clear7dayCitation();
            return true;
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(aLong -> getCitationList(), throwable -> error.setValue(Resource.success(null, "Error :" + throwable.getMessage()))));
    }

    void clearCitation(CitationBean bean) {
        compositeDisposable.add(Observable.fromCallable(() -> {
            transactionDao.clearCitation(bean);
            return true;
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(aLong -> {
                    success.setValue(Resource.success(null, "Done"));
                    getCitationList();
                }, throwable -> error.setValue(Resource.success(null, "Error :" + throwable.getMessage()))));
    }
}
