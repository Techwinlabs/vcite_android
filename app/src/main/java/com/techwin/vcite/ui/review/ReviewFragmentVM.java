package com.techwin.vcite.ui.review;


import android.util.Log;

import com.techwin.vcite.data.beans.CitationData;
import com.techwin.vcite.data.local.dao.CitationDao;
import com.techwin.vcite.data.local.dao.TransactionDao;
import com.techwin.vcite.data.local.entity.CitationBean;
import com.techwin.vcite.data.local.entity.MediaBean;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.di.base.viewmodel.BaseViewModel;
import com.techwin.vcite.ui.main.MainActivityVM;
import com.techwin.vcite.util.event.SingleRequestEvent;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;


public class ReviewFragmentVM extends BaseViewModel {

    final SingleRequestEvent<CitationBean> obrData = new SingleRequestEvent<>();

    private final TransactionDao transactionDao;
    private final CitationDao citationDao;
    final SingleRequestEvent<Void> obrSave = new SingleRequestEvent<>();
    final SingleRequestEvent<Void> obrSaveBitmap = new SingleRequestEvent<>();
    public final MainActivityVM mainActivityVM;

    @Inject
    public ReviewFragmentVM(TransactionDao transactionDao, CitationDao citationDao, MainActivityVM mainActivityVM) {
        this.transactionDao = transactionDao;
        this.citationDao = citationDao;
        this.mainActivityVM = mainActivityVM;
    }


    void saveCitation(CitationBean citationBean) {
        compositeDisposable.add(Observable.fromCallable(new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                return transactionDao.saveCitationTrans(citationBean);
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(Long aLong) throws Exception {
                        Log.e(TAG, "insert" + aLong);
                        obrSave.setValue(Resource.success(null, "Saved"));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        obrSave.setValue(Resource.success(null, "Error :" + throwable.getMessage()));
                        Log.e(TAG, "error" + throwable.getMessage());
                    }
                }));
    }

    void getCitation(CitationData citationData) {
        obrData.setValue(Resource.loading(null));
        compositeDisposable.add(Observable.fromCallable(() -> citationDao.loadCitation(citationData.citationNo, citationData.orgNo, citationData.citationType))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(citationBean ->
                                obrData.setValue(Resource.success(citationBean, "done")),
                        e -> obrData.setValue(Resource.error(null, e.getMessage()))));


    }

    void saveMediaBeans(List<MediaBean> citationBean) {
        compositeDisposable.add(Observable.fromCallable(new Callable<long[]>() {
            @Override
            public long[] call() throws Exception {
                return transactionDao.saveImagesTrans(citationBean);
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<long[]>() {
                    @Override
                    public void accept(long[] longs) throws Exception {
                        obrSaveBitmap.setValue(Resource.success(null, "Image saved"));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        obrSaveBitmap.setValue(Resource.error(null, "Error :" + throwable.getMessage()));
                    }
                }));
    }
}
