package com.techwin.vcite.ui.selector.campus;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.techwin.vcite.BR;
import com.techwin.vcite.R;
import com.techwin.vcite.data.beans.CampusBean;
import com.techwin.vcite.data.beans.VelTypeLnn;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.data.remote.helper.Status;
import com.techwin.vcite.databinding.ActivityCampusBinding;
import com.techwin.vcite.databinding.ActivityVehMakeBinding;
import com.techwin.vcite.databinding.HolderCampusSelectorBinding;
import com.techwin.vcite.di.base.adapter.SimpleRecyclerViewAdapter;
import com.techwin.vcite.di.base.view.AppActivity;
import com.techwin.vcite.ui.selector.make.VehMakeActivityVM;
import com.techwin.vcite.util.AppUtils;
import com.techwin.vcite.util.decoration.ListVerticalItemDecoration;
import com.techwin.vcite.util.dump.InputUtils;
import com.techwin.vcite.util.event.SingleRequestEvent;

import java.util.ArrayList;
import java.util.List;

public class CampusActivity extends AppActivity<ActivityCampusBinding, CampusActivityVM> {
    @Nullable
    private String selected;
    private SimpleRecyclerViewAdapter<CampusBean.CampusTyp, HolderCampusSelectorBinding> adapter;
    private final List<CampusBean.CampusTyp> dataList = new ArrayList<>();

    @Override
    protected BindingActivity<CampusActivityVM> getBindingActivity() {
        return new BindingActivity<>(R.layout.activity_campus, CampusActivityVM.class);
    }

    public static Intent newIntent(Context activity, @Nullable String selected) {
        Intent intent = new Intent(activity, CampusActivity.class);
        intent.putExtra("selected", selected);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        animateCloseSheet();
    }

    @Override
    protected void subscribeToEvents(final CampusActivityVM vm) {
        selected = getIntent().getStringExtra("selected");
        binding.toolbar.tvTitle.setText("Campus");
        adapter = new SimpleRecyclerViewAdapter<>(R.layout.holder_campus_selector, BR.bean);
        adapter.setCallback((v, bean) -> {
            selected = bean.getAbbreviation();
            for (int i = 0; i < adapter.getItemCount(); i++) {
                adapter.getList().get(i).checked = adapter.getList().get(i).getAbbreviation().equalsIgnoreCase(selected);
            }
            adapter.notifyDataSetChanged();
        });
        binding.rvOne.setLayoutManager(new LinearLayoutManager(this));
        binding.rvOne.addItemDecoration(new ListVerticalItemDecoration(this, R.dimen.dp_5));
        binding.rvOne.setAdapter(adapter);

        vm.onClick.observe(this, new Observer<View>() {
            @Override
            public void onChanged(@Nullable View view) {
                int i = view != null ? view.getId() : 0;
                if (i == R.id.tv_save) {
                    if (adapter.getItemCount() == 0) {
                        selected = binding.etSearch.getText().toString();
                    }
                    Intent intent = new Intent();
                    intent.putExtra("data", selected);
                    setResult(Activity.RESULT_OK, intent);
                    finish(true);
                    animateCloseSheet();
                } else if (i == R.id.iv_back) {
                    finish(true);
                    animateCloseSheet();
                }
            }
        });

        vm.obrData.observe(this, (SingleRequestEvent.RequestObserver<CampusBean>) resource -> {
            switch (resource.status) {
                case LOADING:
                    showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    if (resource.data != null) {
                        dataList.addAll(resource.data.campusTyp);
                    }
                    updateAdapter();
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    break;
            }
        });
        vm.singleLiveEvent.observe(this, s -> {
            if (InputUtils.isEmpty(s)) {
                adapter.setList(dataList);
            } else {
                adapter.clearList();
                for (CampusBean.CampusTyp s1 : dataList) {
                    if (AppUtils.compareText(s1.getAbbreviation(), s)) {
                        adapter.addData(s1);
                    }
                }
            }
        });
        vm.setConsumer(binding.etSearch);
        vm.getList();
    }

    private void updateAdapter() {
        if (selected != null) {
            for (int i = 0; i < dataList.size(); i++) {
                if (dataList.get(i).getAbbreviation().equals(selected)) {
                    dataList.get(i).checked = true;
                    break;
                }
            }

        }
        adapter.setList(dataList);
    }


    /*
    private void showCampusDialog() {
        if (campusTypList == null) {
            CampusBean campusBean = sharedPref.getCampus();
            if (campusBean != null && campusBean.campusTyp != null) {
                campusTypList = campusBean.campusTyp;
            } else {
                viewModel.error.setValue("Campus list not found");
                return;
            }
        }
        final String s = viewModel.field_cust4.get();
        for (int i = 0; i < campusTypList.size(); i++) {
            if (s == null) {
                break;
            } else
                campusTypList.get(i).checked = campusTypList.get(i).getAbbreviation().equalsIgnoreCase(s);
        }

        SimpleRecyclerViewAdapter<CampusBean.CampusTyp, HolderCampusSelectorBinding> adapter = new SimpleRecyclerViewAdapter<>(R.layout.holder_campus_selector, BR.bean);
        adapter.setCallback((v, bean) -> {
            for (int i = 0; i < campusTypList.size(); i++) {
                campusTypList.get(i).checked = campusTypList.get(i).getAbbreviation().equalsIgnoreCase(bean.getAbbreviation());
            }
            adapter.notifyDataSetChanged();
        });
        dialog = new BaseCustomDialog<>(baseContext, R.layout.dialog_list_type, view -> {
            switch (view.getId()) {
                case R.id.tv_ok:
                    for (CampusBean.CampusTyp type : adapter.getList()) {
                        if (type.checked) {
                            viewModel.field_cust4.set(type.getAbbreviation());
                            break;
                        }
                    }
                    dialog.dismiss();
                    break;
                case R.id.tv_cancel:
                    dialog.dismiss();
                    break;
            }
        });

        dialog.getBinding().tvTitle.setText("Campus");
        adapter.setList(campusTypList);
        dialog.getBinding().rvOne.setAdapter(adapter);
        dialog.getBinding().rvOne.setLayoutManager(new LinearLayoutManager(baseContext));
        dialog.show();
    }*/
}

