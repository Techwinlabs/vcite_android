package com.techwin.vcite.ui.home;

import com.techwin.vcite.data.local.entity.CitationBean;

public interface Listioner {
    void onItemClick(CitationBean bean);
   default String missingElements(CitationBean bean){
       return "";
   }
}
