package com.techwin.vcite.ui.selector.color;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.techwin.vcite.BR;
import com.techwin.vcite.R;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.databinding.ActivityPlateColorBinding;
import com.techwin.vcite.databinding.HolderPlateColorSelectorBinding;
import com.techwin.vcite.di.base.adapter.SimpleRecyclerViewAdapter;
import com.techwin.vcite.di.base.view.AppActivity;
import com.techwin.vcite.util.AppUtils;
import com.techwin.vcite.util.decoration.ListVerticalItemDecoration;
import com.techwin.vcite.util.dump.InputUtils;
import com.techwin.vcite.util.event.SingleRequestEvent;

import java.util.ArrayList;
import java.util.List;

public class PlateColorActivity extends AppActivity<ActivityPlateColorBinding, PlateColorActivityVM> {
    @Nullable
    private String selected;
    private SimpleRecyclerViewAdapter<PlateColor, HolderPlateColorSelectorBinding> adapter;
    private final List<PlateColor> dataList = new ArrayList<>();

    @Override
    protected BindingActivity<PlateColorActivityVM> getBindingActivity() {
        return new BindingActivity<>(R.layout.activity_plate_color, PlateColorActivityVM.class);
    }

    public static Intent newIntent(Context activity, @Nullable String selected) {
        Intent intent = new Intent(activity, PlateColorActivity.class);
        intent.putExtra("selected", selected);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        animateCloseSheet();
    }

    @Override
    protected void subscribeToEvents(final PlateColorActivityVM vm) {
        selected = getIntent().getStringExtra("selected");
        binding.toolbar.tvTitle.setText("Plate Color");
        adapter = new SimpleRecyclerViewAdapter<>(R.layout.holder_plate_color_selector, BR.bean);
        adapter.setCallback((v, bean) -> {
            selected = bean.id;
            for (int i = 0; i < adapter.getItemCount(); i++) {
                adapter.getList().get(i).checked = adapter.getList().get(i).id.equalsIgnoreCase(bean.id);
            }
            adapter.notifyDataSetChanged();
        });
        binding.rvOne.setLayoutManager(new LinearLayoutManager(this));
        binding.rvOne.addItemDecoration(new ListVerticalItemDecoration(this, R.dimen.dp_5));
        binding.rvOne.setAdapter(adapter);

        vm.onClick.observe(this, new Observer<View>() {
            @Override
            public void onChanged(@Nullable View view) {
                int i = view != null ? view.getId() : 0;
                if (i == R.id.tv_save) {
                    if (adapter.getItemCount() == 0) {
                        selected = binding.etSearch.getText().toString();
                    }
                    Intent intent = new Intent();
                    intent.putExtra("data", selected);
                    setResult(Activity.RESULT_OK, intent);
                    finish(true);
                    animateCloseSheet();
                } else if (i == R.id.iv_back) {
                    finish(true);
                    animateCloseSheet();
                }
            }
        });

        vm.obrData.observe(this, new SingleRequestEvent.RequestObserver<List<PlateColor>>() {
            @Override
            public void onRequestReceived(@NonNull Resource<List<PlateColor>> resource) {
                switch (resource.status) {
                    case LOADING:
                        showProgressDialog(R.string.plz_wait);
                        break;
                    case SUCCESS:
                        dismissProgressDialog();
                        if (resource.data != null) {
                            dataList.addAll(resource.data);
                        }
                        updateAdapter();
                        break;
                    case ERROR:
                        dismissProgressDialog();
                        vm.error.setValue(resource.message);
                        break;
                }
            }
        });
        vm.singleLiveEvent.observe(this, s -> {
            if (InputUtils.isEmpty(s)) {
                adapter.setList(dataList);
            } else {
                adapter.clearList();
                for (PlateColor s1 : dataList) {
                    if (AppUtils.compareText(s1.type, s)) {
                        adapter.addData(s1);
                    }
                }
            }
        });
        vm.setConsumer(binding.etSearch);
        vm.getList();
    }

    private void updateAdapter() {
        if (selected != null) {
            for (int i = 0; i < dataList.size(); i++) {
                if (dataList.get(i).id.equals(selected)) {
                    dataList.get(i).checked = true;
                    break;
                }
            }

        }
        adapter.setList(dataList);
    }


    public static List<PlateColor> getPlateColorList() {
        List<PlateColor> plateColors = new ArrayList<>();
        plateColors.add(new PlateColor("RED", "Red"));
        plateColors.add(new PlateColor("GRN", "Green"));
        plateColors.add(new PlateColor("YEL", "Yellow"));
        plateColors.add(new PlateColor("BLUE", "Blue"));
        plateColors.add(new PlateColor("A", "Authority"));
        plateColors.add(new PlateColor("M", "Municipal"));
        plateColors.add(new PlateColor("S", "State"));
        return plateColors;
    }

    public static class PlateColor {
        public String id;
        public String type;
        public boolean checked;

        public PlateColor(String id, String type) {
            this.id = id;
            this.type = type;
        }

    }

}

