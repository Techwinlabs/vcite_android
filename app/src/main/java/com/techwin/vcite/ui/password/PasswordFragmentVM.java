package com.techwin.vcite.ui.password;


import androidx.databinding.ObservableField;

import com.techwin.vcite.data.beans.UserBean;
import com.techwin.vcite.data.beans.base.ApiResponse;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.remote.helper.NetworkErrorHandler;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.data.repo.BaseRepo;
import com.techwin.vcite.di.base.viewmodel.BaseViewModel;
import com.techwin.vcite.util.dump.InputUtils;
import com.techwin.vcite.util.event.SingleRequestEvent;
import com.techwin.vcite.util.location.LiveLocationDetecter;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class PasswordFragmentVM extends BaseViewModel {
    private final SharedPref sharedPref;
    public final LiveLocationDetecter liveLocationDetecter;
    private final NetworkErrorHandler networkErrorHandler;
    public final ObservableField<String> field_old = new ObservableField<>();
    public final ObservableField<String> field_new = new ObservableField<>();
    public final ObservableField<String> field_cnew = new ObservableField<>();

    final SingleRequestEvent<Void> obrSubmit = new SingleRequestEvent<>();
    private final BaseRepo baseRepo;

    @Inject
    public PasswordFragmentVM(SharedPref sharedPref, LiveLocationDetecter liveLocationDetecter, NetworkErrorHandler networkErrorHandler, BaseRepo baseRepo) {
        this.sharedPref = sharedPref;
        this.liveLocationDetecter = liveLocationDetecter;
        this.networkErrorHandler = networkErrorHandler;
        this.baseRepo = baseRepo;
    }

    void updatePassword() {
        UserBean userBean = sharedPref.getUser();
        if (userBean == null)
            return;
        if (InputUtils.isEmpty(field_old.get())) {
            info.setValue("Empty old password");
            return;
        }
        if (InputUtils.isEmpty(field_new.get())) {
            info.setValue("Empty new password");
            return;
        }
        if (InputUtils.isEmpty(field_cnew.get())) {
            info.setValue("Empty confirm password");
            return;
        }
        if (!field_new.get().equals(field_cnew.get())) {
            info.setValue("Confirm password not matching");
            return;
        }
        Map<String, String> map = new HashMap<>();
        map.put("userKey", String.valueOf(userBean.UserKey));
        map.put("oldPwd", InputUtils.getTrimString(field_old.get()));
        map.put("newPwd", InputUtils.getTrimString(field_new.get()));

        baseRepo.password(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ApiResponse<String>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrSubmit.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull ApiResponse<String> simpleApiResponse) {
                        if (simpleApiResponse.getData() != null) {
                            obrSubmit.setValue(Resource.success(null, simpleApiResponse.getData()));
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        obrSubmit.setValue(Resource.error(
                                null,
                                networkErrorHandler.getErrMsg(e)));

                    }
                });

    }
}
