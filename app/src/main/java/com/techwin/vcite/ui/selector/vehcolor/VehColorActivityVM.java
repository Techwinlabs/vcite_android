package com.techwin.vcite.ui.selector.vehcolor;


import android.widget.EditText;

import com.techwin.vcite.data.beans.VehicleColorType;
import com.techwin.vcite.data.beans.base.ApiResponse;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.remote.helper.NetworkErrorHandler;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.data.repo.BaseRepo;
import com.techwin.vcite.di.base.viewmodel.BaseViewModel;
import com.techwin.vcite.util.event.SingleLiveEvent;
import com.techwin.vcite.util.event.SingleRequestEvent;
import com.techwin.vcite.util.misc.RxSearchObservable;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;

public class VehColorActivityVM extends BaseViewModel {
    private final SharedPref sharedPref;
    private final NetworkErrorHandler networkErrorHandler;
    final SingleRequestEvent<List<VehicleColorType.VehtTypBean>> obrData = new SingleRequestEvent<>();
    private final BaseRepo baseRepo;
    final SingleLiveEvent<String> singleLiveEvent = new SingleLiveEvent<>();

    @Inject
    public VehColorActivityVM(SharedPref sharedPref, NetworkErrorHandler networkErrorHandler, BaseRepo baseRepo) {
        this.sharedPref = sharedPref;
        this.networkErrorHandler = networkErrorHandler;
        this.baseRepo = baseRepo;
    }

    void setConsumer(EditText editText) {
        compositeDisposable.add(RxSearchObservable.from(editText).
                debounce(RxSearchObservable.DEFAULT_WAIT, TimeUnit.MILLISECONDS)
                .filter(new Predicate<String>() {
                    @Override
                    public boolean test(@NonNull String s) throws Exception {
                        return true;
                    }
                }).distinctUntilChanged()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String s) throws Exception {
                        singleLiveEvent.setValue(s);
                    }
                }));
    }

    void getList() {
        VehicleColorType s = sharedPref.getColorType();
        if (s != null && s.vehtTyp != null && s.vehtTyp.size() > 0)
            obrData.setValue(Resource.success(s.vehtTyp, "Local"));
        else
            baseRepo.getVehicleColor().observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io()).subscribe(new SingleObserver<ApiResponse<VehicleColorType>>() {
                @Override
                public void onSubscribe(Disposable d) {
                    compositeDisposable.add(d);
                    obrData.setValue(Resource.loading(null));
                }

                @Override
                public void onSuccess(ApiResponse<VehicleColorType> listApiResponse) {
                    obrData.setValue(Resource.success(listApiResponse.getData().vehtTyp, "Load from server"));
                }

                @Override
                public void onError(Throwable e) {
                    obrData.setValue(Resource.error(s.vehtTyp, networkErrorHandler.getErrMsg(e)));
                }
            });
    }


}

