package com.techwin.vcite.ui.photo.detail;


import android.graphics.Bitmap;

import com.techwin.vcite.data.beans.CitationData;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.local.dao.CitationDao;
import com.techwin.vcite.data.local.dao.MediaBeanDao;
import com.techwin.vcite.data.local.dao.TransactionDao;
import com.techwin.vcite.data.local.entity.MediaBean;
import com.techwin.vcite.data.remote.helper.NetworkErrorHandler;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.data.repo.BaseRepo;
import com.techwin.vcite.di.base.viewmodel.BaseViewModel;
import com.techwin.vcite.util.AppUtils;
import com.techwin.vcite.util.event.SingleRequestEvent;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class ImageDetailActivityVM extends BaseViewModel {
    private final SharedPref sharedPref;
    private final NetworkErrorHandler networkErrorHandler;
    private final BaseRepo webRepo;
    final SingleRequestEvent<List<MediaBean>> obrData = new SingleRequestEvent<>();
    final SingleRequestEvent<Bitmap> obrBitmap = new SingleRequestEvent<>();
    private final TransactionDao transactionDao;
    private final MediaBeanDao mediaBeanDao;

    @Inject
    public ImageDetailActivityVM(SharedPref sharedPref, NetworkErrorHandler networkErrorHandler, BaseRepo webRepo, TransactionDao transactionDao, CitationDao citationDao, MediaBeanDao mediaBeanDao) {
        this.sharedPref = sharedPref;
        this.networkErrorHandler = networkErrorHandler;
        this.webRepo = webRepo;
        this.transactionDao = transactionDao;
        this.mediaBeanDao = mediaBeanDao;
    }

    void getMediaBeans(CitationData citationData) {
        obrData.setValue(Resource.loading(null));
        compositeDisposable.add(Observable.fromCallable(() -> mediaBeanDao.getList(citationData.citationNo, citationData.orgNo))
                .subscribeOn(Schedulers.io())
                .delay(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(citationBean ->
                                obrData.setValue(Resource.success(citationBean, "done")),
                        e -> obrData.setValue(Resource.error(null, e.getMessage()))));


    }

    public void getBitmap(String media) {
        obrBitmap.setValue(Resource.loading(null));
        compositeDisposable.add(Observable.fromCallable(new Callable<Bitmap>() {
            @Override
            public Bitmap call() throws Exception {
                return AppUtils.base64ToImage(media);
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Bitmap>() {
                    @Override
                    public void accept(Bitmap bitmap) throws Exception {
                        obrBitmap.setValue(Resource.success(bitmap, "done"));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        obrBitmap.setValue(Resource.error(null, throwable.getMessage()));
                    }
                }));
    }
}
