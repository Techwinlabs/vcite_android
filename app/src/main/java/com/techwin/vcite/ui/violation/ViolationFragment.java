package com.techwin.vcite.ui.violation;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ObservableField;
import androidx.lifecycle.Observer;

import com.google.android.gms.maps.model.LatLng;
import com.mindorks.core.ScreenshotBuilder;
import com.mindorks.properties.Quality;
import com.techwin.vcite.R;
import com.techwin.vcite.data.beans.CitTempBean;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.local.entity.CitationBean;
import com.techwin.vcite.data.local.entity.ViolationBean;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.data.remote.helper.Status;
import com.techwin.vcite.databinding.FragmentViolationBinding;
import com.techwin.vcite.di.base.view.AppFragment;
import com.techwin.vcite.ui.location.LocationPickerActivity;
import com.techwin.vcite.ui.main.MainActivity;
import com.techwin.vcite.ui.violation.type.ViolationActivity;
import com.techwin.vcite.util.Constants;
import com.techwin.vcite.util.dump.InputUtils;
import com.techwin.vcite.util.event.SingleRequestEvent;
import com.techwin.vcite.util.location.LocUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

public class ViolationFragment extends AppFragment<FragmentViolationBinding, ViolationFragmentVM> {
    public static final String TAG = "ViolationFragment";
    @Nullable
    private MainActivity mainActivity;
    @Inject
    SharedPref sharedPref;
    @Nullable
    ArrayList<ViolationBean> violationBeanList;
    private List<CitTempBean> citTempBeans;
    @Nullable
    private CitationBean citationBean;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mainActivity = (MainActivity) getActivity();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected BindingFragment<ViolationFragmentVM> getBindingFragment() {
        return new BindingFragment<>(R.layout.fragment_violation, ViolationFragmentVM.class);
    }


    @Override
    public void onPause() {
        super.onPause();
        saveToLocal();
    }

    public static ViolationFragment newInstance() {
        return new ViolationFragment();
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    protected void subscribeToEvents(final ViolationFragmentVM vm) {

        vm.onClick.observe(this, view -> {
            int id = view.getId();
            Intent intent;
            switch (id) {
                case R.id.iv_location:
                    intent = LocationPickerActivity.newIntent(baseContext);
                    launcherPlace.launch(intent);
                    break;
                case R.id.et_voilation:
                    if (!binding.etVoilation.isFocusable()) {
                        intent = ViolationActivity.newIntent(baseContext, violationBeanList);
                        launcherVoliation.launch(intent);
                    }
                    break;
                case R.id.iv_save:
                    if (mainActivity != null) {
                        mainActivity.startBitmapCapture(citationBean, view);
                    }
                    break;
            }
        });
        vm.obrSave.observe(this, (SingleRequestEvent.RequestObserver<Void>) resource -> {
            switch (resource.status) {
                case SUCCESS:
                    //    viewModel.success.setValue(resource.message);
                    break;
                case ERROR:
                    //    viewModel.error.setValue(resource.message);
                    break;
            }
        });
        vm.obrData.observe(this, (SingleRequestEvent.RequestObserver<CitationBean>) resource -> {
            switch (resource.status) {
                case LOADING:
                    //showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    setData(resource.data);
                    if (resource.data != null) {
                        violationBeanList = resource.data.getViolation();
                    }
                    setViolationData();
                    dismissProgressDialog();
                    break;
                case ERROR:
                    dismissProgressDialog();
                    Log.e(TAG, resource.message);
                    break;
            }
        });

        loaddata();
    }

    public Bitmap savePdfBitmap() {
        return new ScreenshotBuilder(getActivity())
                .setView(binding.svOne)
                .setQuality(Quality.AVERAGE).getScreenshot();
    }

    @Override
    public void onResume() {
        super.onResume();
        loaddata();
    }

    private void loaddata() {
        if (mainActivity != null) {
            if (mainActivity.citationData != null)
                citTempBeans = sharedPref.getTemplate(mainActivity.citationData.citationType);
            viewModel.getCitation(mainActivity.citationData);
            if (mainActivity.citationData != null) {
                binding.tvTitle.setText(String.format("Citations #%s", mainActivity.citationData.getCitationNo()));
            }
        }
    }

    private void setData(CitationBean data) {
        citationBean = data;
        viewModel.field_location.set(data.getLocation());
        if (mainActivity != null && mainActivity.citationData.citationType.equals(Constants.CITATION_TYPE_CODE_ENFORCE)) {
            showView(binding.vLocation);
            setLabel(binding.tvLabelLocation, "Location", "Location Details");
            setMode(binding.etLocation, Constants.FIELD_TYPE_DROP, true);
            viewModel.field_location.set(data.getLocation());
            showView(binding.vMeter);
            showView(binding.etMeterno);
            showView(binding.llParcel);
            viewModel.field_meterno.set(data.getMeterno());
            setLabel(binding.etMeterno, null, "Meter no");
            setMode(binding.etMeterno, Constants.FIELD_TYPE_TEXT, false);

            //viewModel.field_parcel.set(data.getParcelid());
         /*   setLabel(binding.etParcelid, null, "Parcel id");
            setMode(binding.etParcelid, Constants.FIELD_TYPE_TEXT);*/
        }
        if (citTempBeans.size() == 0) {
            showView(binding.vVoilation);
            setLabel(binding.tvLabelVoilation, "", "Violations");
            setMode(binding.etVoilation, Constants.FIELD_TYPE_DROP, false);
        }
        for (int i = 0; i < citTempBeans.size(); i++) {
            CitTempBean citTempBean = citTempBeans.get(i);
            switch (citTempBean.getFieldID()) {
                case Constants.fieldID_Location:
                    if (citTempBean.isIncludeField()) {
                        showView(binding.vLocation);
                        setLabel(binding.tvLabelLocation, citTempBean.getFieldName(), "Location Details");
                        setMode(binding.etLocation, citTempBean.getFieldType(), true);
                        viewModel.field_location.set(data.getLocation());
                    } else {
                        if (mainActivity != null && mainActivity.citationData.citationType.equals(Constants.CITATION_TYPE_PARKING))
                            hideView(binding.vLocation);
                    }

                    break;
                case Constants.fieldID_Violation:
                    if (citTempBean.isIncludeField()) {
                        showView(binding.vVoilation);
                        setLabel(binding.tvLabelVoilation, citTempBean.getFieldName(), "Violations");
                        setMode(binding.etVoilation, Constants.FIELD_TYPE_DROP, false);
                    } else {
                        hideView(binding.vVoilation);
                    }

                    break;

            }
        }
        setEditListioner();
        if (citationBean != null) {
            binding.etComment.setText(citationBean.getComment());
            calculateText();
        }
        updateLatLogView(data);
    }

    private void setEditListioner() {
      /*  binding.etComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                calculateText();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });*/
    }

    private void calculateText() {
        String text = binding.etComment.getText().toString();
        binding.tvCount.setText(String.format(Locale.getDefault(), "%d/50", text.length()));
    }

    private void updateLatLogView(CitationBean bean) {
        if (bean == null)
            return;
        if (bean.getLatitude() != 0 && bean.getLongitude() != 0) {
            binding.tvLatlog.setText("" + bean.getLatitude() + " , " + bean.getLongitude());
            binding.vLatlog.setVisibility(View.VISIBLE);
        } else binding.vLatlog.setVisibility(View.GONE);
    }

    private void setMode(EditText view, String mode, boolean islocationField) {
        if (TextUtils.isEmpty(mode))
            mode = Constants.FIELD_TYPE_TEXT;
        switch (mode) {
            case Constants.FIELD_TYPE_TEXT:
                view.setFocusable(true);
                view.setFocusableInTouchMode(true);
                view.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                if (islocationField) {
                    view.setHint("Add Address");
                    //   binding.ivLocation.setVisibility(View.GONE);
                } else view.setHint("Type here");
                break;
            case Constants.FIELD_TYPE_DATE:
                view.setFocusable(false);
                view.setFocusableInTouchMode(false);
                view.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                view.setHint("Select");
                break;
            case Constants.FIELD_TYPE_DROP:
                view.setFocusable(false);
                view.setFocusableInTouchMode(false);
                if (islocationField) {
                    view.setHint("Select Address");
                    binding.ivLocation.setVisibility(View.VISIBLE);
                } else {
                    view.setHint("Drop down");
                    view.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_down, 0);
                }

                break;
        }

    }

    private void setDefaultText(ObservableField<String> field, String fieldValue) {
        if (InputUtils.isEmpty(field.get()))
            field.set(fieldValue);
    }

    private void setLabel(TextView textView, String name, String defaultName) {
        if (TextUtils.isEmpty(name))
            textView.setText(defaultName);
        else textView.setText(name);
    }

    private void hideView(View view) {
        view.setVisibility(View.GONE);
    }

    private void showView(View view) {
        view.setVisibility(View.VISIBLE);
    }


    private final ActivityResultLauncher<Intent> launcherPlace = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
        @Override
        public void onActivityResult(ActivityResult result) {
            if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
                LatLng latLng = result.getData().getParcelableExtra(LocationPickerActivity.EXTRA_LATLNG);
                if (latLng != null) {
                    String s = LocUtils.getAddressString(baseContext, latLng);
                    if (citationBean != null) {
                        citationBean.setLatitude(latLng.latitude);
                        citationBean.setLongitude(latLng.longitude);
                        viewModel.field_location.set(s);
                        saveToLocal();
                    }

                }
            }
        }
    });
    private final ActivityResultLauncher<Intent> launcherVoliation = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
        @Override
        public void onActivityResult(ActivityResult result) {
            if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
                violationBeanList = result.getData().getParcelableArrayListExtra("data");
                saveToLocal();

            }
        }
    });

    private void saveToLocal() {
        if (citationBean != null) {
            citationBean.setDate(new Date());
            citationBean.setComment(InputUtils.getTrimString(binding.etComment.getText().toString()));
            citationBean.setMeterno(InputUtils.getTrimString(viewModel.field_meterno.get()));
            citationBean.setViolation(violationBeanList);
            citationBean.setLocation(InputUtils.getTrimString(viewModel.field_location.get()));
            //  citationBean.setParcelid(InputUtils.getTrimString(viewModel.field_parcel.get()));
            viewModel.saveCitation(citationBean);
        }
    }

    private void setViolationData() {
        if (violationBeanList == null)
            violationBeanList = new ArrayList<>();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < violationBeanList.size(); i++) {
            stringBuilder.append("-");
            stringBuilder.append(violationBeanList.get(i).getDescription());
            if (i < violationBeanList.size() - 1)
                stringBuilder.append("\n");
        }
        viewModel.field_voilation.set(stringBuilder.toString());

    }
}
