package com.techwin.vcite.ui.enforment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableField;
import androidx.lifecycle.Observer;

import com.mindorks.core.ScreenshotBuilder;
import com.mindorks.properties.Quality;
import com.techwin.vcite.R;
import com.techwin.vcite.data.beans.CitTempBean;
import com.techwin.vcite.data.beans.CitationData;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.local.entity.CitationBean;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.data.remote.helper.Status;
import com.techwin.vcite.databinding.FragmentEnforceBinding;
import com.techwin.vcite.databinding.VEstblishmentBinding;
import com.techwin.vcite.databinding.VParcelBinding;
import com.techwin.vcite.databinding.VRemarkBinding;
import com.techwin.vcite.databinding.VStreetNameBinding;
import com.techwin.vcite.databinding.VStreetNoBinding;
import com.techwin.vcite.databinding.VTopBarBinding;
import com.techwin.vcite.databinding.VUnitBinding;
import com.techwin.vcite.databinding.VZipCodeBinding;
import com.techwin.vcite.di.base.view.AppFragment;
import com.techwin.vcite.ui.main.MainActivity;
import com.techwin.vcite.ui.parcel.SearchNameActivity;
import com.techwin.vcite.util.Constants;
import com.techwin.vcite.util.dump.InputUtils;
import com.techwin.vcite.util.event.SingleRequestEvent;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

public class EnforceFragment extends AppFragment<FragmentEnforceBinding, EnforceFragmentVM> {

    public static final String TAG = "EnforceFragment";
    @Nullable
    private MainActivity mainActivity;
    @Inject
    SharedPref sharedPref;
    @Nullable
    private CitationData citationData;
    private List<CitTempBean> citTempBeans;
    private VStreetNameBinding vStreetNameBinding;
    private VZipCodeBinding vZipCodeBinding;
    private VStreetNoBinding vStreetNoBinding;
    private VEstblishmentBinding vEstblishmentBinding;
    private VUnitBinding vUnitBinding;
    private VRemarkBinding vRemarkBinding;
    private VParcelBinding vParcelBinding;
    private VTopBarBinding vTopBarBinding;
    private HashMap<Integer, View> sortedViews;
    private ArrayList<View> unSortedViews;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mainActivity = (MainActivity) context;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected BindingFragment<EnforceFragmentVM> getBindingFragment() {
        return new BindingFragment<>(R.layout.fragment_enforce, EnforceFragmentVM.class);
    }

    public static EnforceFragment newInstance() {
        return new EnforceFragment();
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    protected void subscribeToEvents(final EnforceFragmentVM vm) {
        citTempBeans = sharedPref.getTemplate(Constants.CITATION_TYPE_CODE_ENFORCE);
        getViewsBindings();
        vm.onClick.observe(this, view -> {
            switch (view.getId()) {
                case R.id.iv_save:
                    if (mainActivity != null) {
                        mainActivity.startBitmapCapture(citationBean, view);
                    }
                    break;

            }
        });
        vm.obrSave.observe(this, (SingleRequestEvent.RequestObserver<Void>) resource -> {
            switch (resource.status) {
                case SUCCESS:
                    //   viewModel.success.setValue(resource.message);
                    break;
                case ERROR:
                    //  viewModel.error.setValue(resource.message);
                    break;
            }
        });

        vm.obrData.observe(this, (SingleRequestEvent.RequestObserver<CitationBean>) resource -> {
            switch (resource.status) {
                case LOADING:
                    //showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    if (resource.data != null)
                        setData(resource.data);
                    dismissProgressDialog();
                    break;
                case ERROR:
                    dismissProgressDialog();
                    Log.e(TAG, resource.message);
                    setInitData();
                    break;
            }
        });
        loadData();
        setTemplates();
    }

    @Override
    public void onStart() {
        super.onStart();
        loadData();
    }

    @SuppressLint("SetTextI18n")
    private void loadData() {
        if (mainActivity != null) {
            citationData = mainActivity.citationData;
            viewModel.getCitation(citationData);
            if (citationData != null) {
                binding.tvTitle.setText("Citations #" + citationData.getCitationNo());
            }
        }
    }

    private void setTemplates() {
        if (citTempBeans.size() != 0)
            binding.svOne.removeAllViews();

        binding.svOne.addView(vTopBarBinding.getRoot());

        if (citTempBeans.isEmpty()) {
            binding.svOne.addView(vStreetNoBinding.getRoot());
            binding.svOne.addView(vZipCodeBinding.getRoot());
            binding.svOne.addView(vStreetNameBinding.getRoot());
            binding.svOne.addView(vEstblishmentBinding.getRoot());
            binding.svOne.addView(vUnitBinding.getRoot());
            binding.svOne.addView(vRemarkBinding.getRoot());
            binding.svOne.addView(vParcelBinding.getRoot());
        } else {
            for (int i = 0; i < citTempBeans.size(); i++) {
                CitTempBean citTempBean = citTempBeans.get(i);
                if (Constants.fieldID_StreetNumber.equals(citTempBean.getFieldID())) {
                    if (citTempBean.isIncludeField()) {
                        showView(vStreetNoBinding.vStreetno);
                        setMode(vStreetNoBinding.etStreetno, citTempBean.getFieldType());
                        setLabel(vStreetNoBinding.tvLabelStreetno, citTempBean.getFieldName(), "Street No");
                        setDefaultText(viewModel.field_streetno, citTempBean.getFieldValue());
                        getSequence(vStreetNoBinding.getRoot(), citTempBean);
                    } else {
                        hideView(vStreetNoBinding.vStreetno);
                    }
                } else if (Constants.fieldID_ZipCode.equals(citTempBean.getFieldID())) {
                    if (citTempBean.isIncludeField()) {
                        showView(vZipCodeBinding.vZipcode);
                        setMode(vZipCodeBinding.etZipcode, citTempBean.getFieldType());
                        setLabel(vZipCodeBinding.tvLableZipcode, citTempBean.getFieldName(), "Zip Code");
                        setDefaultText(viewModel.field_zipcode, citTempBean.getFieldValue());
                        getSequence(vZipCodeBinding.getRoot(), citTempBean);
                    } else {
                        hideView(vZipCodeBinding.vZipcode);
                    }
                } else if (Constants.fieldID_StreetName.equals(citTempBean.getFieldID())) {
                    if (citTempBean.isIncludeField()) {
                        showView(vStreetNameBinding.vStreetname);
                        setMode(vStreetNameBinding.etStreetname, citTempBean.getFieldType());
                        setLabel(vStreetNameBinding.tvLableStreetName, citTempBean.getFieldName(), "Street Name");
                        setDefaultText(viewModel.field_streetname, citTempBean.getFieldValue());
                        getSequence(vStreetNameBinding.getRoot(), citTempBean);
                    } else {
                        hideView(vStreetNameBinding.vStreetname);
                    }
                } else if (Constants.fieldID_Establishment.equals(citTempBean.getFieldID())) {
                    if (citTempBean.isIncludeField()) {
                        showView(vEstblishmentBinding.vEstb);
                        setMode(vEstblishmentBinding.etEstb, citTempBean.getFieldType());
                        setLabel(vEstblishmentBinding.tvLablelEstb, citTempBean.getFieldName(), "Establishment");
                        setDefaultText(viewModel.field_estib, citTempBean.getFieldValue());
                        getSequence(vEstblishmentBinding.getRoot(), citTempBean);
                    } else {
                        hideView(vEstblishmentBinding.vEstb);
                    }
                } else if (Constants.fieldID_Unit.equals(citTempBean.getFieldID())) {
                    if (citTempBean.isIncludeField()) {
                        showView(vUnitBinding.vUnit);
                        setMode(vUnitBinding.etUnit, citTempBean.getFieldType());
                        setLabel(vUnitBinding.tvLableUnit, citTempBean.getFieldName(), "Unit");
                        setDefaultText(viewModel.field_unit, citTempBean.getFieldValue());
                        getSequence(vUnitBinding.getRoot(), citTempBean);
                    } else {
                        hideView(vUnitBinding.vUnit);
                    }
                } else if (Constants.fieldID_Remarks.equals(citTempBean.getFieldID())) {
                    if (citTempBean.isIncludeField()) {
                        showView(vRemarkBinding.vRemark);
                        setMode(vRemarkBinding.etRemark, citTempBean.getFieldType());
                        setLabel(vRemarkBinding.tvLableRemark, citTempBean.getFieldName(), "Remarks");
                        setDefaultText(viewModel.field_remark, citTempBean.getFieldValue());
                        getSequence(vRemarkBinding.getRoot(), citTempBean);
                    } else {
                        hideView(vRemarkBinding.vRemark);
                    }
                } else if (Constants.fieldID_Parcel.equals(citTempBean.getFieldID())) {
                    if (citTempBean.isIncludeField()) {
                        showView(vParcelBinding.vRemark);
                        setMode(vParcelBinding.etParcel, citTempBean.getFieldType());
                        setLabel(vParcelBinding.tvLableRemark, citTempBean.getFieldName(), "Parcel");
                        setDefaultText(viewModel.field_parcel, citTempBean.getFieldValue());
                        getSequence(vParcelBinding.getRoot(), citTempBean);
                    } else {
                        hideView(vParcelBinding.vRemark);
                    }
                }
            }
        }


        sortedViews = new HashMap<>();
        unSortedViews = new ArrayList<>();
        Object[] keys = sortedViews.keySet().toArray();
        if (keys != null) {
            for (Object key : keys) {
                if (key instanceof Integer) {
                    binding.svOne.addView(sortedViews.get(key));
                }
            }
        }
        for (View view : unSortedViews)
            binding.svOne.addView(view);

    }

    private void setDefaultText(ObservableField<String> field, String fieldValue) {
        if (InputUtils.isEmpty(field.get()))
            field.set(fieldValue);
    }

    private void setLabel(TextView textView, String name, String defaultName) {
        if (TextUtils.isEmpty(name))
            textView.setText(defaultName);
        else textView.setText(name);
    }

    private void hideView(View view) {
        view.setVisibility(View.GONE);
    }

    private void showView(View view) {
        view.setVisibility(View.VISIBLE);
    }

    private void setMode(EditText view, String mode) {
        if (TextUtils.isEmpty(mode))
            mode = Constants.FIELD_TYPE_TEXT;
        switch (mode) {
            case Constants.FIELD_TYPE_TEXT:
                view.setFocusable(true);
                view.setFocusableInTouchMode(true);
                view.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                view.setHint("Type here");
                break;
            case Constants.FIELD_TYPE_DATE:
                view.setFocusable(false);
                view.setFocusableInTouchMode(false);
                view.setHint("Select");
                view.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_down, 0);
                break;
            case Constants.FIELD_TYPE_Calender:
                view.setFocusable(false);
                view.setFocusableInTouchMode(false);
                view.setHint("Select Date");
                view.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_down, 0);
                break;
            case Constants.FIELD_TYPE_DROP:
                view.setFocusable(false);
                view.setFocusableInTouchMode(false);
                view.setHint("Drop down");
                view.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_down, 0);
                break;
        }

    }


    private void setData(CitationBean data) {
        this.citationBean = data;
        viewModel.field_streetno.set(data.getStreetNo());
        viewModel.field_zipcode.set(data.getStreetZip());
        viewModel.field_streetname.set(data.getStreetName());
        viewModel.field_estib.set(data.getEstab());
        viewModel.field_unit.set(data.getUnit());
        viewModel.field_remark.set(data.getRemarks());
        viewModel.field_parcel.set(data.getParcelid());

    }

    private boolean isShown = true;


    private void setInitData() {
        if (citationData != null) {
            citationBean = new CitationBean();
            citationBean.setCreatedOn(new Date());
            saveToLocal();
        }
    }

    private CitationBean citationBean;

    public void saveToLocal() {
        if (citationData != null) {
            citationBean.setCitationNo(citationData.citationNo);
        }
        citationBean.setOrgNo(citationData.orgNo);
        citationBean.setCitationType(citationData.citationType);
        citationBean.setUploaded(citationData.uploaded);
        citationBean.setStreetNo(InputUtils.getTrimString(viewModel.field_streetno.get()));
        citationBean.setStreetZip(InputUtils.getTrimString(viewModel.field_zipcode.get()));
        citationBean.setStreetName(InputUtils.getTrimString(viewModel.field_streetname.get()));
        citationBean.setEstab(InputUtils.getTrimString(viewModel.field_estib.get()));
        citationBean.setUnit(InputUtils.getTrimString(viewModel.field_unit.get()));
        citationBean.setRemarks(InputUtils.getTrimString(viewModel.field_remark.get()));
        citationBean.setParcelid(InputUtils.getTrimString(viewModel.field_parcel.get()));

        citationBean.setDate(new Date());
        viewModel.saveCitation(citationBean);

    }

    public Bitmap savePdfBitmap() {
        return new ScreenshotBuilder(getActivity())
                .setView(binding.svOne)
                .setQuality(Quality.AVERAGE).getScreenshot();
    }


    @Override
    public void onPause() {
        super.onPause();
        saveToLocal();
    }


    @SuppressLint("SetTextI18n")
    private void getViewsBindings() {
        vStreetNameBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.v_street_name, binding.svOne, false);
        vStreetNameBinding.setVm(viewModel);

        vZipCodeBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.v_zip_code, binding.svOne, false);
        vZipCodeBinding.setVm(viewModel);

        vStreetNoBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.v_street_no, binding.svOne, false);
        vStreetNoBinding.setVm(viewModel);

        vEstblishmentBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.v_estblishment, binding.svOne, false);
        vEstblishmentBinding.setVm(viewModel);

        vRemarkBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.v_remark, binding.svOne, false);
        vRemarkBinding.setVm(viewModel);

        vParcelBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.v_parcel, binding.svOne, false);
        vParcelBinding.setVm(viewModel);

        vUnitBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.v_unit, binding.svOne, false);
        vUnitBinding.setVm(viewModel);

        vTopBarBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.v_top_bar, binding.svOne, false);
        vTopBarBinding.tvHeader.setText("Code Enforcement");
        vTopBarBinding.ivHome.setVisibility(View.VISIBLE);
        vTopBarBinding.ivHome.setOnClickListener(v -> {
            Intent intent = SearchNameActivity.newIntent(baseContext, citationData);
            startNewActivity(intent);
        });

        vParcelBinding.ivParcel.setVisibility(View.GONE);
        vStreetNameBinding.ivStreetname.setVisibility(View.GONE);
        vStreetNoBinding.ivStreetno.setVisibility(View.GONE);
        vUnitBinding.ivUnit.setVisibility(View.GONE);
    }

    private void getSequence(View root, CitTempBean citTempBean) {
        if (citTempBean.getSequenceNo() > 0) {
            sortedViews.put(citTempBean.getSequenceNo(), root);
        } else unSortedViews.add(root);
    }

}
