package com.techwin.vcite.ui.password;

import android.view.View;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;

import com.techwin.vcite.R;
import com.techwin.vcite.databinding.FragmentPasswordBinding;
import com.techwin.vcite.di.base.view.AppFragment;
import com.techwin.vcite.util.event.SingleRequestEvent;

public class PasswordFragment extends AppFragment<FragmentPasswordBinding, PasswordFragmentVM> {

    public static final String TAG = "Password";

    public static PasswordFragment getInstance() {
        return new PasswordFragment();
    }


    @Override
    protected BindingFragment<PasswordFragmentVM> getBindingFragment() {
        return new BindingFragment<>(R.layout.fragment_password, PasswordFragmentVM.class);
    }

    @Override
    protected void subscribeToEvents(final PasswordFragmentVM vm) {
        vm.onClick.observe(this, new Observer<View>() {
            @Override
            public void onChanged(@Nullable View view) {
                switch (view != null ? view.getId() : 0) {
                    case R.id.btn_submit:
                        vm.updatePassword();
                        break;

                }
            }
        });
        vm.obrSubmit.observe(this, (SingleRequestEvent.RequestObserver<Void>) resource -> {
            switch (resource.status) {
                case LOADING:
                    showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    vm.success.setValue(resource.message);
                    restViews();
                    break;
                case WARN:
                    dismissProgressDialog();
                    vm.warn.setValue(resource.message);
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    break;
            }
        });


    }

    private void restViews() {
        viewModel.field_old.set(null);
        viewModel.field_new.set(null);
        viewModel.field_cnew.set(null);
    }


}

