package com.techwin.vcite.ui.selector.platetype;


import android.widget.EditText;

import com.techwin.vcite.data.beans.PlateType;
import com.techwin.vcite.data.beans.UserBean;
import com.techwin.vcite.data.beans.VelTypeLnn;
import com.techwin.vcite.data.beans.base.ApiResponse;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.remote.helper.NetworkErrorHandler;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.data.repo.BaseRepo;
import com.techwin.vcite.di.base.viewmodel.BaseViewModel;
import com.techwin.vcite.util.Constants;
import com.techwin.vcite.util.event.SingleLiveEvent;
import com.techwin.vcite.util.event.SingleRequestEvent;
import com.techwin.vcite.util.misc.RxSearchObservable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;

public class PlateTypeActivityVM extends BaseViewModel {
    private final SharedPref sharedPref;
    private final NetworkErrorHandler networkErrorHandler;
    final SingleRequestEvent<List<PlateType>> obrPlates = new SingleRequestEvent<>();
    private final BaseRepo baseRepo;
    final SingleLiveEvent<String> singleLiveEvent = new SingleLiveEvent<>();

    @Inject
    public PlateTypeActivityVM(SharedPref sharedPref, NetworkErrorHandler networkErrorHandler, BaseRepo baseRepo) {
        this.sharedPref = sharedPref;
        this.networkErrorHandler = networkErrorHandler;
        this.baseRepo = baseRepo;
    }

    void setConsumer(EditText editText) {
        compositeDisposable.add(RxSearchObservable.from(editText).
                debounce(RxSearchObservable.DEFAULT_WAIT, TimeUnit.MILLISECONDS)
                .filter(new Predicate<String>() {
                    @Override
                    public boolean test(@NonNull String s) throws Exception {
                        return true;
                    }
                }).distinctUntilChanged()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String s) throws Exception {
                        singleLiveEvent.setValue(s);
                    }
                }));
    }

    void getList() {
        List<PlateType> s = sharedPref.getPlateTypeList();
        if (s != null && s.size() > 0) {
            obrPlates.setValue(Resource.success(s, "from local"));
        } else {
            UserBean userBean = sharedPref.getUser();
            if (userBean.CustKey == Constants.LYNNMA_CUSTKEY) {
                baseRepo.getPlateForLynn()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver<ApiResponse<VelTypeLnn>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrPlates.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(ApiResponse<VelTypeLnn> listApiResponse) {
                        List<PlateType> plateTypes = new ArrayList<>();
                        if (listApiResponse.getData() != null) {
                            if (listApiResponse.getData().pltTyp != null) {
                                plateTypes.addAll(listApiResponse.getData().pltTyp);
                            }
                        }
                        obrPlates.setValue(Resource.success(plateTypes, "from remote"));

                    }

                    @Override
                    public void onError(Throwable e) {
                        obrPlates.setValue(Resource.error(null, networkErrorHandler.getErrMsg(e)));
                    }
                });

            } else {
                baseRepo.getPlateType().subscribe(new SingleObserver<ApiResponse<List<PlateType>>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrPlates.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(ApiResponse<List<PlateType>> listApiResponse) {
                        obrPlates.setValue(Resource.success(listApiResponse.getData(), "from remote"));
                    }

                    @Override
                    public void onError(Throwable e) {
                        obrPlates.setValue(Resource.error(null, networkErrorHandler.getErrMsg(e)));
                    }
                });
            }

        }
    }
}



