package com.techwin.vcite.ui.main2.vehcile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableField;
import androidx.fragment.app.FragmentManager;

import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.mindorks.core.ScreenshotBuilder;
import com.mindorks.properties.Quality;
import com.techwin.vcite.R;
import com.techwin.vcite.data.beans.CampusBean;
import com.techwin.vcite.data.beans.CitTempBean;
import com.techwin.vcite.data.beans.CitationData;
import com.techwin.vcite.data.beans.UserBean;
import com.techwin.vcite.data.beans.VehType;
import com.techwin.vcite.data.beans.VehicleColorType;
import com.techwin.vcite.data.beans.VelTypeLnn;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.local.entity.CitationBean;
import com.techwin.vcite.data.local.entity.MediaBean;
import com.techwin.vcite.databinding.DialogListTypeBinding;
import com.techwin.vcite.databinding.FragmentVehicle2Binding;
import com.techwin.vcite.databinding.VClerkidBinding;
import com.techwin.vcite.databinding.VColorBinding;
import com.techwin.vcite.databinding.VCustfield4Binding;
import com.techwin.vcite.databinding.VCustfield5Binding;
import com.techwin.vcite.databinding.VDivisionidBinding;
import com.techwin.vcite.databinding.VExpirydateBinding;
import com.techwin.vcite.databinding.VMakeBinding;
import com.techwin.vcite.databinding.VOfficeridBinding;
import com.techwin.vcite.databinding.VPlateColorBinding;
import com.techwin.vcite.databinding.VPlateTypeBinding;
import com.techwin.vcite.databinding.VPlatenoBinding;
import com.techwin.vcite.databinding.VStateofissanceBinding;
import com.techwin.vcite.databinding.VTopBarBinding;
import com.techwin.vcite.databinding.VVehmodelBinding;
import com.techwin.vcite.databinding.VVehtypeBinding;
import com.techwin.vcite.databinding.VVinNoBinding;
import com.techwin.vcite.di.base.view.AppFragment;
import com.techwin.vcite.di.base.view.BaseCustomDialog;
import com.techwin.vcite.ui.main.MainActivity;
import com.techwin.vcite.ui.plate.PlateActivity;
import com.techwin.vcite.ui.scan.text.TextScan2Activity;
import com.techwin.vcite.ui.selector.campus.CampusActivity;
import com.techwin.vcite.ui.selector.color.PlateColorActivity;
import com.techwin.vcite.ui.selector.platetype.PlateTypeActivity;
import com.techwin.vcite.ui.selector.state.StateActivity;
import com.techwin.vcite.util.AppUtils;
import com.techwin.vcite.util.Constants;
import com.techwin.vcite.util.PopupWindowList;
import com.techwin.vcite.util.dump.InputUtils;
import com.techwin.vcite.util.event.SingleRequestEvent;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

public class Vehicle2Fragment extends AppFragment<FragmentVehicle2Binding, Vehicle2FragmentVM> {
    public static final String TAG = "Vehicle2Fragment";
    @Nullable
    private MainActivity mainActivity;
    @Inject
    SharedPref sharedPref;
    @Nullable
    private CitationData citationData;
    private List<CitTempBean> citTempBeans;
    private VColorBinding vColorBinding;
    /*---new ----*/
    private VVehmodelBinding vehmodelBinding;
    private VClerkidBinding vClerkidBinding;
    private VOfficeridBinding vOfficeridBinding;
    private VDivisionidBinding vDivisionidBinding;
    /*-----------------*/
    private VExpirydateBinding vExpirydateBinding;
    private VMakeBinding vMakeBinding;
    private VPlateTypeBinding vPlateTypeBinding;
    private VPlateColorBinding vPlateColorBinding;
    private VPlatenoBinding vPlatenoBinding;
    private VStateofissanceBinding vStateofissanceBinding;
    private VVehtypeBinding vVehtypeBinding;
    private VVinNoBinding vVinNoBinding;
    private VTopBarBinding vTopBarBinding;

    private VCustfield4Binding vCustfield4Binding;
    private VCustfield5Binding vCustfield5Binding;

    private HashMap<Integer, View> sortedViews;
    private ArrayList<View> unSortedViews;

    @Nullable
    private List<CampusBean.CampusTyp> campusTypList;
    private boolean save;
    private final Handler handler = new Handler();

    private PopupWindowList mPopupWindowList;

    private String testString = "";
    private String fAmount = "";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mainActivity = (MainActivity) context;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected BindingFragment<Vehicle2FragmentVM> getBindingFragment() {
        return new BindingFragment<>(R.layout.fragment_vehicle2, Vehicle2FragmentVM.class);
    }

    public static Vehicle2Fragment newInstance() {
        return new Vehicle2Fragment();
    }

    @Override
    protected void subscribeToEvents(final Vehicle2FragmentVM vm) {
        citTempBeans = sharedPref.getTemplate(Constants.CITATION_TYPE_PARKING);
        getViewsBindings();
        vm.onClick.observe(this, view -> {
            int id = view.getId();
            if (id == R.id.et_platetype) {
                if (!vPlateTypeBinding.etPlatetype.isFocusable()) {
                    Intent intent = PlateTypeActivity.newIntent(baseContext, viewModel.field_plateType.get());
                    launcherVehPlateType.launch(intent);
                    animateOpenSheet();
                }
            } else if (id == R.id.et_color) {
//                if (!vColorBinding.etColor.isFocusable()) {
//                    Intent intent = VehColorActivity.newIntent(baseContext, viewModel.field_color.get());
//                    launcherVehColor.launch(intent);
//                    animateOpenSheet();
//                }
            } else if (id == R.id.et_date) {
                showDatePicker();
            } else if (id == R.id.et_state) {
//                if (!vStateofissanceBinding.etState.isFocusable()) {
//                    Intent intent = StateActivity.newIntent(baseContext, viewModel.field_state.get());
//                    launcherStateList.launch(intent);
//                    animateOpenSheet();
//                }
            } else if (id == R.id.et_make) {
//                if (!vMakeBinding.etMake.isFocusable()) {
//                    Intent intent = VehMakeActivity.newIntent(baseContext, viewModel.field_make.get());
//                    launcherVehMake.launch(intent);
//                    animateOpenSheet();
//                }
            } else if (id == R.id.et_custfield4) {
                if (!vCustfield4Binding.etCustfield4.isFocusable()) {
                    Intent intent = CampusActivity.newIntent(baseContext, viewModel.field_cust4.get());
                    launcherCampus.launch(intent);
                    animateOpenSheet();
                }
            } else if (id == R.id.et_plate_color) {
                if (!vPlateColorBinding.etPlateColor.isFocusable()) {
                    Intent intent = PlateColorActivity.newIntent(baseContext, viewModel.field_platecolor.get());
                    launcherVehPlateColor.launch(intent);
                    animateOpenSheet();
                }

            } else if (id == R.id.et_vehtype) {
//                if (!vVehtypeBinding.etVehtype.isFocusable()) {
//                    Intent intent = VehTypeActivity.newIntent(baseContext, viewModel.field_vehtype.get());
//                    launcherVehType.launch(intent);
//                    animateOpenSheet();
//                }
            } else if (id == R.id.iv_plateno) {
                Intent intent = PlateActivity.newIntent(baseContext);
                launcherPlateNo.launch(intent);
            } else if (id == R.id.iv_vin) {
                Intent intent = TextScan2Activity.newIntent(baseContext, citationData);
                launcherVinNo.launch(intent);
            } else if (id == R.id.iv_save) {
                //  saveToLocal();
                if (mainActivity != null) {
                    mainActivity.startBitmapCapture(citationBean, view);
                }
            }
        });
        vm.obrSave.observe(this, (SingleRequestEvent.RequestObserver<Void>) resource -> {
            switch (resource.status) {
                case SUCCESS:
                    //   viewModel.success.setValue(resource.message);
                    break;
                case ERROR:
                    //  viewModel.error.setValue(resource.message);
                    break;
            }
        });

      /*  vm.obrCampusType.observe(this, new SingleRequestEvent.RequestObserver<CampusBean>() {
            @Override
            public void onRequestReceived(@NonNull Resource<CampusBean> resource) {
                switch (resource.status) {
                    case LOADING:
                        showProgressDialog(R.string.plz_wait);
                        break;
                    case SUCCESS:
                        dismissProgressDialog();
                        if (resource.data != null) {
                            campusTypList = resource.data.campusTyp;
                            showCampusDialog();
                        }
                        break;
                    case ERROR:
                        dismissProgressDialog();
                        vm.error.setValue(resource.message);
                        break;
                }
            }
        });*/
        vm.obrData.observe(this, (SingleRequestEvent.RequestObserver<CitationBean>) resource -> {
            switch (resource.status) {
                case LOADING:
                    //showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    setData(resource.data);
                    dismissProgressDialog();
                    break;
                case ERROR:
                    dismissProgressDialog();
                    Log.e(TAG, resource.message);
                    setInitData();
                    break;
            }
        });

        vm.obrStateListData.observe(this, (SingleRequestEvent.RequestObserver<List<StateActivity.DummyBean>>) resource -> {
            switch (resource.status) {
                case LOADING:
                    showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    if (resource.data != null) {
                        ArrayList<String> mStateList = new ArrayList<>();
                        ArrayList<String> mSortName = new ArrayList<>();
                        for (StateActivity.DummyBean bean : resource.data) {
                            mStateList.add(bean.name + "");
                            mSortName.add(bean.abbreviation + "");
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(requireActivity(), android.R.layout.simple_spinner_dropdown_item, mStateList);
                        vStateofissanceBinding.etState.setAdapter(adapter);
                        vStateofissanceBinding.etState.setOnItemClickListener((parent, view, position, id) -> {
                            for (int i = 0; i < mStateList.size(); i++) {
                                if (mStateList.get(i).equalsIgnoreCase(vStateofissanceBinding.etState.getText().toString().trim())) {
                                    vStateofissanceBinding.etState.setText(mSortName.get(i));
                                    AppUtils.hideKeyboard(requireActivity());
                                    vStateofissanceBinding.etState.clearFocus();
                                }
                            }
                            save = true;
                            viewModel.field_state.set(vStateofissanceBinding.etState.getText().toString().trim());
                            saveToLocal();
                        });
                    }
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    break;
            }
        });
        vm.obrVehicleMakeData.observe(this, (SingleRequestEvent.RequestObserver<List<VelTypeLnn.VehtAbbrBean>>) resource -> {
            switch (resource.status) {
                case LOADING:
                    showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    if (resource.data != null) {
                        ArrayList<String> mStateList = new ArrayList<>();
                        ArrayList<String> mSortName = new ArrayList<>();
                        for (VelTypeLnn.VehtAbbrBean bean : resource.data) {
                            mStateList.add(bean.name + "");
                            mSortName.add(bean.abbreviation + "");
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(requireActivity(), android.R.layout.simple_spinner_dropdown_item, mStateList);
                        vMakeBinding.etMake.setAdapter(adapter);
                        vMakeBinding.etMake.setOnItemClickListener((parent, view, position, id) -> {
                            for (int i = 0; i < mStateList.size(); i++) {
                                if (mStateList.get(i).equalsIgnoreCase(vMakeBinding.etMake.getText().toString().trim())) {
                                    vMakeBinding.etMake.setText(mSortName.get(i));
                                    AppUtils.hideKeyboard(requireActivity());
                                    vMakeBinding.etMake.clearFocus();
                                }
                            }
                            save = true;
                            viewModel.field_make.set(vMakeBinding.etMake.getText().toString().trim());
                            saveToLocal();
                        });
                    }
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    break;
            }
        });
        vm.obrVehicleType.observe(this, (SingleRequestEvent.RequestObserver<List<VehType.VehtTypDTO>>) resource -> {
            switch (resource.status) {
                case LOADING:
                    //   showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    if (resource.data != null) {
                        ArrayList<String> mStateList = new ArrayList<>();
                        ArrayList<String> mSortName = new ArrayList<>();
                        for (VehType.VehtTypDTO bean : resource.data) {
                            mStateList.add(bean.getName() + "");
                            mSortName.add(bean.getAbbreviation() + "");
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(requireActivity(), android.R.layout.simple_spinner_dropdown_item, mStateList);
                        vVehtypeBinding.etVehtype.setAdapter(adapter);
                        vVehtypeBinding.etVehtype.setOnItemClickListener((parent, view, position, id) -> {
                            for (int i = 0; i < mStateList.size(); i++) {
                                if (mStateList.get(i).equalsIgnoreCase(vVehtypeBinding.etVehtype.getText().toString().trim())) {
                                    vVehtypeBinding.etVehtype.setText(mSortName.get(i));
                                    AppUtils.hideKeyboard(requireActivity());
                                    vVehtypeBinding.etVehtype.clearFocus();
                                }
                            }
                            save = true;
                            viewModel.field_vehtype.set(vVehtypeBinding.etVehtype.getText().toString().trim());
                            saveToLocal();
                        });
                    }
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    break;
            }
        });
        vm.obrVehicleColor.observe(this, (SingleRequestEvent.RequestObserver<List<VehicleColorType.VehtTypBean>>) resource -> {
            switch (resource.status) {
                case LOADING:
                    //   showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    if (resource.data != null) {
                        ArrayList<String> mStateList = new ArrayList<>();
                        ArrayList<String> mSortName = new ArrayList<>();
                        for (VehicleColorType.VehtTypBean bean : resource.data) {
                            mStateList.add(bean.Name + "");
                            mSortName.add(bean.Abbreviation + "");
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(requireActivity(), android.R.layout.simple_spinner_dropdown_item, mStateList);
                        vColorBinding.etColor.setAdapter(adapter);
                        vColorBinding.etColor.setOnItemClickListener((parent, view, position, id) -> {
                            for (int i = 0; i < mStateList.size(); i++) {
                                if (mStateList.get(i).equalsIgnoreCase(vColorBinding.etColor.getText().toString().trim())) {
                                    vColorBinding.etColor.setText(mSortName.get(i));
                                    AppUtils.hideKeyboard(requireActivity());
                                    vColorBinding.etColor.clearFocus();
                                }
                            }
                            save = true;
                            viewModel.field_color.set(vColorBinding.etColor.getText().toString().trim());
                            saveToLocal();
                        });
                    }
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    break;
            }
        });
        vm.obrCampusList.observe(this, (SingleRequestEvent.RequestObserver<CampusBean>) resource -> {
            switch (resource.status) {
                case LOADING:
                    showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    if (resource.data != null) {
                        ArrayList<String> mStateList = new ArrayList<>();
                        ArrayList<String> mSortName = new ArrayList<>();
                        for (CampusBean.CampusTyp bean : resource.data.campusTyp) {
                            mStateList.add(bean.getName() + "");
                            mSortName.add(bean.getAbbreviation() + "");
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(requireActivity(), android.R.layout.simple_spinner_dropdown_item, mStateList);
                        vCustfield4Binding.etCustfield4.setAdapter(adapter);
                        vCustfield4Binding.etCustfield4.setOnItemClickListener((parent, view, position, id) -> {
                            for (int i = 0; i < mStateList.size(); i++) {
                                if (mStateList.get(i).equalsIgnoreCase(vCustfield4Binding.etCustfield4.getText().toString().trim())) {
                                    vCustfield4Binding.etCustfield4.setText(mSortName.get(i));
                                    AppUtils.hideKeyboard(requireActivity());
                                    vCustfield4Binding.etCustfield4.clearFocus();
                                }
                            }
                            save = true;
                            viewModel.field_cust4.set(vCustfield4Binding.etCustfield4.getText().toString().trim());
                            saveToLocal();
                        });
                    }
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    break;
            }
        });
        loadData();
        setTemplates();
        viewModel.getStateList();
        viewModel.getVehicleMakeList();
        viewModel.getVehicleTypeList();
        viewModel.getVehicleColor();
        viewModel.getCampusList();

    }

    public Bitmap savePdfBitmap() {
        return new ScreenshotBuilder(getActivity())
                .setView(binding.svOne)
                .setQuality(Quality.AVERAGE).getScreenshot();
    }

    @Override
    public void onResume() {
        super.onResume();
        handler.postDelayed(runnable, 1500);

    }

    final Runnable runnable = () -> loadData();


    private void loadData() {
        if (save)
            saveToLocal();
        else if (mainActivity != null) {
            citationData = mainActivity.citationData;
            viewModel.getCitation(citationData);
            if (citationData != null) {
                binding.tvTitle.setText(String.format("Citations #%s", citationData.getCitationNo()));
            }
        }
    }

    private void setTemplates() {
        binding.svOne.removeAllViews();
        binding.svOne.addView(vTopBarBinding.getRoot());
        sortedViews = new HashMap<>();
        unSortedViews = new ArrayList<>();
        for (int i = 0; i < citTempBeans.size(); i++) {
            CitTempBean citTempBean = citTempBeans.get(i);
            Log.e("Templates_" + (i + 1), citTempBean.toString());

            citTempBean.setFieldID(citTempBean.getFieldID().replace("\t\t\r\n", ""));
            if (Constants.fieldID_plateType.equalsIgnoreCase(citTempBean.getFieldID())) {
                if (citTempBean.isIncludeField()) {
                    showView(vPlateTypeBinding.vPlatetype);
                    setMode(vPlateTypeBinding.etPlatetype, citTempBean.getFieldType());
                    setLabel(vPlateTypeBinding.tvLabelPlatetype, citTempBean.getFieldName(), "Plate Type");
                    setDefaultText(viewModel.field_plateType, citTempBean.getFieldValue());
                    getSequence(vPlateTypeBinding.getRoot(), citTempBean);

                } else {
                    hideView(vPlateTypeBinding.vPlatetype);
                }
            } else if (Constants.fieldID_platecolor.equalsIgnoreCase(citTempBean.getFieldID())) {
                if (citTempBean.isIncludeField()) {
                    showView(vPlateColorBinding.vPlatecolor);
                    setMode(vPlateColorBinding.etPlateColor, citTempBean.getFieldType());
                    setLabel(vPlateColorBinding.tvLablePlatecolor, citTempBean.getFieldName(), "Plate Color");
                    setDefaultText(viewModel.field_platecolor, citTempBean.getFieldValue());
                    getSequence(vPlateColorBinding.getRoot(), citTempBean);
                } else {
                    hideView(vPlateColorBinding.vPlatecolor);
                }
            } else if (Constants.fieldID_VehLicense.equalsIgnoreCase(citTempBean.getFieldID())) {
                if (citTempBean.isIncludeField()) {
                    showView(vPlatenoBinding.vLicence);
                    setMode(vPlatenoBinding.etLincenecNo, citTempBean.getFieldType());
                    setLabel(vPlatenoBinding.tvLabelPlateno, citTempBean.getFieldName(), "Licence Plate #");
                    setDefaultText(viewModel.field_licence, citTempBean.getFieldValue());
                    getSequence(vPlatenoBinding.getRoot(), citTempBean);
                } else {
                    hideView(vPlatenoBinding.vLicence);
                }
            } else if (Constants.fieldID_VehState.equalsIgnoreCase(citTempBean.getFieldID())) {
                if (citTempBean.isIncludeField()) {
                    showView(vStateofissanceBinding.vState);
                    //setMode(vStateofissanceBinding.etState, citTempBean.getFieldType());
                    if (citTempBean.getFieldType().equalsIgnoreCase(Constants.FIELD_TYPE_DROP))
                        vStateofissanceBinding.etState.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_down, 0);
                    setLabel(vStateofissanceBinding.tvLabelState, citTempBean.getFieldName(), "State of issuance");

                    //Abbrivtaion split
                    String text = citTempBean.getFieldValue();
                    if (!InputUtils.isEmpty(text)) {
                        if (text.contains("-")) {
                            String[] s = text.split("-");
                            if (s.length > 0)
                                text = s[0];
                        }
                    }
                    setDefaultText(viewModel.field_state, text);
                    getSequence(vStateofissanceBinding.getRoot(), citTempBean);
                } else {
                    hideView(vStateofissanceBinding.vState);
                }
            } else if (Constants.fieldID_ExpDate.equalsIgnoreCase(citTempBean.getFieldID())) {
                if (citTempBean.isIncludeField()) {
                    showView(vExpirydateBinding.vExpdate);
                    setMode(vExpirydateBinding.etDate, citTempBean.getFieldType());
                    setLabel(vExpirydateBinding.tvLabelData, citTempBean.getFieldName(), "Expiration Date");
                    getSequence(vExpirydateBinding.getRoot(), citTempBean);
                } else {
                    hideView(vExpirydateBinding.vExpdate);
                }
            } else if (Constants.fieldID_VIN.equalsIgnoreCase(citTempBean.getFieldID())) {
                if (citTempBean.isIncludeField()) {
                    showView(vVinNoBinding.vVin);
                    setMode(vVinNoBinding.etVinNo, citTempBean.getFieldType());
                    setLabel(vVinNoBinding.tvVin, citTempBean.getFieldName(), "VIN");
                    setDefaultText(viewModel.field_vin, citTempBean.getFieldValue());
                    getSequence(vVinNoBinding.getRoot(), citTempBean);
                } else {
                    hideView(vVinNoBinding.vVin);
                }
            } else if (Constants.fieldID_VehColor.equalsIgnoreCase(citTempBean.getFieldID())) {
                if (citTempBean.isIncludeField()) {
                    showView(vColorBinding.vColor);
                    // setMode(vColorBinding.etColor, citTempBean.getFieldType());
                    if (citTempBean.getFieldType().equalsIgnoreCase(Constants.FIELD_TYPE_DROP))
                        vColorBinding.etColor.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_down, 0);
                    setLabel(vColorBinding.tvLableColor, citTempBean.getFieldName(), "Color");
                    setDefaultText(viewModel.field_color, citTempBean.getFieldValue());
                    getSequence(vColorBinding.getRoot(), citTempBean);
                } else {
                    hideView(vColorBinding.vColor);
                }
            } else if (Constants.fieldID_VehMake.equalsIgnoreCase(citTempBean.getFieldID())) {
                if (citTempBean.isIncludeField()) {
                    showView(vMakeBinding.vMake);
                    // setMode(vMakeBinding.etMake, citTempBean.getFieldType());
                    if (citTempBean.getFieldType().equalsIgnoreCase(Constants.FIELD_TYPE_DROP))
                        vMakeBinding.etMake.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_down, 0);
                    setLabel(vMakeBinding.tvLabelMake, citTempBean.getFieldName(), "Make");
                    setDefaultText(viewModel.field_make, citTempBean.getFieldValue());
                    getSequence(vMakeBinding.getRoot(), citTempBean);
                } else {
                    hideView(vMakeBinding.vMake);
                }
            } else if (Constants.fieldID_VehModel.equalsIgnoreCase(citTempBean.getFieldID())) {
                if (citTempBean.isIncludeField()) {
                    showView(vehmodelBinding.vMain);
                    setMode(vehmodelBinding.etVehmodel, citTempBean.getFieldType());
                    setLabel(vehmodelBinding.tvLabel, citTempBean.getFieldName(), "Model");
                    setDefaultText(viewModel.field_vehmodel, citTempBean.getFieldValue());
                    getSequence(vehmodelBinding.getRoot(), citTempBean);
                } else {
                    hideView(vehmodelBinding.vMain);
                }
            } else if (Constants.fieldID_ClerkID.equalsIgnoreCase(citTempBean.getFieldID())) {
                if (citTempBean.isIncludeField()) {
                    showView(vClerkidBinding.vMain);
                    setMode(vClerkidBinding.etClerkid, citTempBean.getFieldType());
                    setLabel(vClerkidBinding.tvLabel, citTempBean.getFieldName(), "Clerk Id");
                    setDefaultText(viewModel.field_vehclerkid, citTempBean.getFieldValue());
                    getSequence(vClerkidBinding.getRoot(), citTempBean);
                } else {
                    hideView(vClerkidBinding.vMain);
                }
            } else if (Constants.fieldID_OfficerID.equalsIgnoreCase(citTempBean.getFieldID())) {
                if (citTempBean.isIncludeField()) {
                    showView(vOfficeridBinding.vMain);
                    setMode(vOfficeridBinding.etOfficerid, citTempBean.getFieldType());
                    setLabel(vOfficeridBinding.tvLabel, citTempBean.getFieldName(), "Officer Id");
                    UserBean userBean = sharedPref.getUser();
                    setDefaultText(viewModel.field_vehOfficerId, userBean.OfficerID);
                    getSequence(vOfficeridBinding.getRoot(), citTempBean);
                } else {
                    UserBean userBean = sharedPref.getUser();
                    setDefaultText(viewModel.field_vehOfficerId, userBean.OfficerID);
                    hideView(vOfficeridBinding.vMain);
                }
            } else if (Constants.fieldID_Division.equalsIgnoreCase(citTempBean.getFieldID())) {
                if (citTempBean.isIncludeField()) {
                    showView(vDivisionidBinding.vMain);
                    setMode(vDivisionidBinding.etClerkid, citTempBean.getFieldType());
                    setLabel(vDivisionidBinding.tvLabel, citTempBean.getFieldName(), "Division Id");
                    setDefaultText(viewModel.field_vehDivisionId, citTempBean.getFieldValue());
                    getSequence(vDivisionidBinding.getRoot(), citTempBean);
                } else {
                    hideView(vDivisionidBinding.vMain);
                }
            } else if (Constants.fieldID_VehType.equalsIgnoreCase(citTempBean.getFieldID())) {
                if (citTempBean.isIncludeField()) {
                    showView(vVehtypeBinding.vVehtype);
                    // setMode(vVehtypeBinding.etVehtype, citTempBean.getFieldType());
                    if (citTempBean.getFieldType().equalsIgnoreCase(Constants.FIELD_TYPE_DROP))
                        vVehtypeBinding.etVehtype.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_down, 0);
                    setLabel(vVehtypeBinding.tvLableVehtype, citTempBean.getFieldName(), "Vehicle Type");
                    setDefaultText(viewModel.field_vehtype, citTempBean.getFieldValue());
                    getSequence(vVehtypeBinding.getRoot(), citTempBean);
                } else {
                    hideView(vVehtypeBinding.vVehtype);
                }
            } else if (Constants.fieldID_RequireOCR.equalsIgnoreCase(citTempBean.getFieldID())) {
                if (citTempBean.isIncludeField())
                    showView(vPlatenoBinding.ivPlateno);
                else hideView(vPlatenoBinding.ivPlateno);
            } else if (Constants.fieldID_RequireOCRVin.equalsIgnoreCase(citTempBean.getFieldID())) {
                if (citTempBean.isIncludeField())
                    showView(vVinNoBinding.ivVin);
                else hideView(vVinNoBinding.ivVin);
            } else if (Constants.fieldID_CustField4.equalsIgnoreCase(citTempBean.getFieldID())) {
                if (citTempBean.isIncludeField()) {
                    showView(vCustfield4Binding.vCustfield);
                    // setMode(vCustfield4Binding.etCustfield4, citTempBean.getFieldType());
                    if (citTempBean.getFieldType().equalsIgnoreCase(Constants.FIELD_TYPE_DROP))
                        vCustfield4Binding.etCustfield4.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_down, 0);
                    setLabel(vCustfield4Binding.tvLableCustfield, citTempBean.getFieldName(), "CustField4");
                    setDefaultText(viewModel.field_cust4, citTempBean.getFieldValue());
                    getSequence(vCustfield4Binding.getRoot(), citTempBean);
                } else {
                    hideView(vCustfield4Binding.vCustfield);
                }
            } else if (Constants.fieldID_CustField5.equalsIgnoreCase(citTempBean.getFieldID())) {
                if (citTempBean.isIncludeField()) {
                    showView(vCustfield5Binding.vCustfield);
                    setMode(vCustfield5Binding.etCustfield5, Constants.FIELD_TYPE_TEXT);
                    setLabel(vCustfield5Binding.tvLableCustfield, citTempBean.getFieldName(), "CustField5");
                    setDefaultText(viewModel.field_cust5, citTempBean.getFieldValue());
                    getSequence(vCustfield5Binding.getRoot(), citTempBean);
                } else {
                    hideView(vCustfield5Binding.vCustfield);
                }
            }
        }

        Object[] keys = sortedViews.keySet().toArray();
        if (keys != null) {
            for (Object key : keys) {
                if (key instanceof Integer) {
                    binding.svOne.addView(sortedViews.get(key));
                }
            }
        }
        if (sortedViews.size() == 0)
            for (View view : unSortedViews)
                binding.svOne.addView(view);

    }

    private void getSequence(View root, CitTempBean citTempBean) {
        if (citTempBean.getSequenceNo() > 0) {
            sortedViews.put(citTempBean.getSequenceNo(), root);
        } else unSortedViews.add(root);
    }


    private void setDefaultText(ObservableField<String> field, String fieldValue) {
        if (InputUtils.isEmpty(field.get())) {
            if (!TextUtils.isEmpty(fieldValue)) {
                if (!fieldValue.toLowerCase().contains("true"))
                    field.set(fieldValue);
            }
        }
    }

    private void setLabel(TextView textView, String name, String defaultName) {
        if (TextUtils.isEmpty(name))
            textView.setText(defaultName);
        else textView.setText(name);
    }

    private void hideView(View view) {
        view.setVisibility(View.GONE);
    }

    private void showView(View view) {
        view.setVisibility(View.VISIBLE);
    }

    private void setMode(EditText view, String mode) {
        if (TextUtils.isEmpty(mode))
            mode = Constants.FIELD_TYPE_TEXT;
        switch (mode) {
            case Constants.FIELD_TYPE_TEXT:
                view.setFocusable(true);
                view.setFocusableInTouchMode(true);
                view.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                view.setHint("Type here");
                break;
            case Constants.FIELD_TYPE_DATE:
                view.setFocusable(false);
                view.setFocusableInTouchMode(false);
                view.setHint("Select");
                view.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_down, 0);
                break;
            case Constants.FIELD_TYPE_Calender:
                view.setFocusable(false);
                view.setFocusableInTouchMode(false);
                view.setHint("Select Date");
                view.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_down, 0);
                break;
            case Constants.FIELD_TYPE_DROP:
                view.setFocusable(false);
                view.setFocusableInTouchMode(false);
                view.setHint("Drop down");
                view.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_down, 0);
                break;
        }

    }


    private void setData(CitationBean data) {
        this.citationBean = data;
        viewModel.field_plateType.set(data.getPlateType());
        viewModel.field_platecolor.set(data.getPlateColor());
        viewModel.field_licence.set(data.getLicenceNo());
        viewModel.field_state.set(data.getState());
        try {
            viewModel.field_expdata.set(data.getExpiryDate());
        } catch (Exception e) {
            e.printStackTrace();
        }
        viewModel.field_vin.set(data.getVin());
        viewModel.field_color.set(data.getColor());
        viewModel.field_make.set(data.getMake());
        viewModel.field_vehtype.set(data.getVehType());
        viewModel.field_cust4.set(data.getCust4());
        viewModel.field_cust5.set(data.getCust5());

        viewModel.field_vehDivisionId.set(data.getDivisionId());
        viewModel.field_vehclerkid.set(data.getClerkId());
        viewModel.field_vehOfficerId.set(data.getOfficerId());
        viewModel.field_vehmodel.set(data.getVehModel());
    }

    private boolean isShown = true;

    private void addVinListeners() {
        InputFilter[] FilterArray = new InputFilter[2];
        FilterArray[0] = new InputFilter.LengthFilter(17);
        FilterArray[1] = new InputFilter.AllCaps();
        vVinNoBinding.etVinNo.setFilters(FilterArray);
        vVinNoBinding.etVinNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (isShown) {
                    isShown = false;
                    return;
                }
                if (s.length() == 17) {
                    viewModel.info.setValue("A VIN number only has 17 digits");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

                char c[] = new char[s.length()];
                for (int i = 0; i < s.length(); i++) {
                    if (s.charAt(i) == 'O' || s.charAt(i) == 'o') {
                        c[i] = '0';
                    } else if (s.charAt(i) == 'Q' || s.charAt(i) == 'q') {
                        c[i] = '0';
                    } else if (s.charAt(i) == 'I' || s.charAt(i) == 'i') {
                        c[i] = '1';
                    } else {
                        c[i] = s.charAt(i);
                    }
                }
                //now convert to string.
                String text = String.valueOf(c);
                vVinNoBinding.etVinNo.removeTextChangedListener(this);
                vVinNoBinding.etVinNo.setText(text);
                vVinNoBinding.etVinNo.setSelection(text.length());
                vVinNoBinding.etVinNo.addTextChangedListener(this);
            }
        });
    }


    private void setInitData() {
        if (citationData != null) {
            citationBean = new CitationBean();
            citationBean.setCreatedOn(new Date());
            saveToLocal();
        }

        FragmentManager.enableDebugLogging(true);
    }

    private CitationBean citationBean;

    public void saveToLocal() {
        save = false;
        citationBean.setCitationNo(citationData.citationNo);
        citationBean.setOrgNo(citationData.orgNo);
        citationBean.setCitationType(citationData.citationType);
        citationBean.setUploaded(citationData.uploaded);
        citationBean.setPlateType(InputUtils.getTrimString(viewModel.field_plateType.get()));
        citationBean.setPlateColor(InputUtils.getTrimString(viewModel.field_platecolor.get()));
        citationBean.setVehType(InputUtils.getTrimString(viewModel.field_vehtype.get()));
        citationBean.setLicenceNo(InputUtils.getTrimString(viewModel.field_licence.get()));
        citationBean.setState(InputUtils.getTrimString(viewModel.field_state.get()));
        try {
            Long date = viewModel.field_expdata.get();
            if (date != null) {
                citationBean.setExpiryDate(new Date(date).getTime());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        citationBean.setVin(InputUtils.getTrimString(viewModel.field_vin.get()));
        citationBean.setColor(InputUtils.getTrimString(viewModel.field_color.get()));
        citationBean.setMake(InputUtils.getTrimString(viewModel.field_make.get()));
        citationBean.setCust4(InputUtils.getTrimString(viewModel.field_cust4.get()));
        citationBean.setCust5(InputUtils.getTrimString(viewModel.field_cust5.get()));

        citationBean.setDivisionId(InputUtils.getTrimString(viewModel.field_vehDivisionId.get()));
        citationBean.setClerkId(InputUtils.getTrimString(viewModel.field_vehclerkid.get()));
        citationBean.setOfficerId(InputUtils.getTrimString(viewModel.field_vehOfficerId.get()));
        citationBean.setVehModel(InputUtils.getTrimString(viewModel.field_vehmodel.get()));

        citationBean.setDate(new Date());
        viewModel.saveCitation(citationBean);

    }

   /* @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CODE_PLATE_SCAN:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    String resultUri = data.getStringExtra("image");
                    if (citationData != null && resultUri != null) {
                        String path = AppUtils.imagetoBase64(resultUri);
                        MediaBean mediaBean = new MediaBean();
                        mediaBean.setCitationNo(citationData.citationNo);
                        mediaBean.setOrgNo(citationData.orgNo);
                        mediaBean.setImage(path);
                        mediaBean.setPicType(Constants.PIC_TYPE_PLATE);
                        viewModel.saveMedia(mediaBean);
                    }
                    Log.e(TAG, "Plate no=>" + data.getStringExtra("data"));
                    viewModel.field_licence.set(data.getStringExtra("data"));
                    saveToLocal();
                }
                break;
            case CODE_VIN_SCAN:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    viewModel.field_vin.set(data.getStringExtra("data"));
                    Bitmap bitmap = data.getParcelableExtra("image");
                    if (bitmap != null) {
                        String path = AppUtils.imagetoBase64(bitmap);
                        MediaBean mediaBean = new MediaBean();
                        mediaBean.setCitationNo(citationBean.getCitationNo());
                        mediaBean.setOrgNo(citationBean.getOrgNo());
                        mediaBean.setImage(path);
                        mediaBean.setPicType(Constants.PIC_TYPE_VIN);
                        viewModel.saveMedia(mediaBean);
                    }
                    saveToLocal();
                }
                break;
        }

    }*/

    //  private DatePickerDialog datePickerDialog;
    private SingleDateAndTimePickerDialog datePickerDialog;


    private void showDatePicker() {
        Calendar maxdate = Calendar.getInstance();
        maxdate.add(Calendar.YEAR, 1);
        Calendar mindate = Calendar.getInstance();
        mindate.add(Calendar.YEAR, -3);
        /*2022, 2021, 2020, 2019, 2018, 2017, 2016. */
        datePickerDialog = new SingleDateAndTimePickerDialog.Builder(baseContext)
                .title("Expiry Date")
                .bottomSheet()
                .curved()
                .displayMinutes(false)
                .displayHours(false)
                .displayDays(false)
                .displayMonth(true)
                .displayYears(true)
                .displayDaysOfMonth(false)
                .maxDateRange(maxdate.getTime())
                .minDateRange(mindate.getTime())
                .listener(date -> viewModel.field_expdata.set(date.getTime()))
                .build();
        datePickerDialog.display();


    }
    /*----------------------------------------------dialogs---------------------------------------------*/

    private BaseCustomDialog<DialogListTypeBinding> dialog;


    private final ActivityResultLauncher<Intent> launcherCampus = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
        if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
            save = true;
            String date = result.getData().getStringExtra("data");
            if (date != null) viewModel.field_cust4.set(date);
        }
    });
    private final ActivityResultLauncher<Intent> launcherVehMake = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
        if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
            save = true;
            String date = result.getData().getStringExtra("data");
            if (date != null) viewModel.field_make.set(date);
        }
    });
    private final ActivityResultLauncher<Intent> launcherVehPlateColor = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
        if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
            save = true;
            String date = result.getData().getStringExtra("data");
            if (date != null) viewModel.field_platecolor.set(date);
        }
    });
    private final ActivityResultLauncher<Intent> launcherVehPlateType = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
        if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
            save = true;
            String date = result.getData().getStringExtra("data");
            if (date != null) viewModel.field_plateType.set(date);
        }
    });
    private final ActivityResultLauncher<Intent> launcherVehType = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
        if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
            save = true;
            String date = result.getData().getStringExtra("data");
            if (date != null) viewModel.field_vehtype.set(date);
        }
    });
    private final ActivityResultLauncher<Intent> launcherVehColor = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
        if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
            save = true;
            String date = result.getData().getStringExtra("data");
            if (date != null) viewModel.field_color.set(date);
        }
    });
    private final ActivityResultLauncher<Intent> launcherStateList = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
        if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
            save = true;
            String date = result.getData().getStringExtra("data");
            if (date != null) viewModel.field_state.set(date);
        }
    });
    private final ActivityResultLauncher<Intent> launcherPlateNo = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
        if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
            save = true;
            String resultUri = result.getData().getStringExtra("image");
            if (citationData != null && resultUri != null) {
                String path = AppUtils.imagetoBase64(resultUri);
                MediaBean mediaBean = new MediaBean();
                mediaBean.setCitationNo(citationData.citationNo);
                mediaBean.setOrgNo(citationData.orgNo);
                mediaBean.setImage(path);
                mediaBean.setPicType(Constants.PIC_TYPE_PLATE);
                viewModel.saveMedia(mediaBean);
            }
            Log.e(TAG, "Plate no=>" + result.getData().getStringExtra("data"));
            viewModel.field_licence.set(result.getData().getStringExtra("data"));
            saveToLocal();
        }
    });
    private final ActivityResultLauncher<Intent> launcherVinNo = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
        if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
            save = true;
            viewModel.field_vin.set(result.getData().getStringExtra("data"));
            Bitmap bitmap = result.getData().getParcelableExtra("image");
            if (bitmap != null) {
                String path = AppUtils.imagetoBase64(bitmap);
                MediaBean mediaBean = new MediaBean();
                mediaBean.setCitationNo(citationBean.getCitationNo());
                mediaBean.setOrgNo(citationBean.getOrgNo());
                mediaBean.setImage(path);
                mediaBean.setPicType(Constants.PIC_TYPE_VIN);
                viewModel.saveMedia(mediaBean);
            }
            saveToLocal();
        }
    });

    @Override
    public void onPause() {
        super.onPause();
        saveToLocal();
        handler.removeCallbacks(runnable);
    }

    @Override
    public void onDetach() {
        if (datePickerDialog != null)
            datePickerDialog.dismiss();
        super.onDetach();
    }


    private void getViewsBindings() {
        vColorBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.v_color, binding.svOne, false);
        vColorBinding.setVm(viewModel);

        vehmodelBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.v_vehmodel, binding.svOne, false);
        vehmodelBinding.etVehmodel.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        vehmodelBinding.setVm(viewModel);

        vClerkidBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.v_clerkid, binding.svOne, false);
        vClerkidBinding.setVm(viewModel);
        vOfficeridBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.v_officerid, binding.svOne, false);
        vOfficeridBinding.setVm(viewModel);
        vDivisionidBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.v_divisionid, binding.svOne, false);
        vDivisionidBinding.setVm(viewModel);

        vExpirydateBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.v_expirydate, binding.svOne, false);
        vExpirydateBinding.setVm(viewModel);

        vMakeBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.v_make, binding.svOne, false);
        vMakeBinding.setVm(viewModel);

        vPlateTypeBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.v_plate_type, binding.svOne, false);
        vPlateTypeBinding.setVm(viewModel);

        vPlateColorBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.v_plate_color, binding.svOne, false);
        vPlateColorBinding.setVm(viewModel);

        vPlatenoBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.v_plateno, binding.svOne, false);
        vPlatenoBinding.etLincenecNo.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        vPlatenoBinding.setVm(viewModel);

        vStateofissanceBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.v_stateofissance, binding.svOne, false);
        vStateofissanceBinding.setVm(viewModel);

        vVehtypeBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.v_vehtype, binding.svOne, false);
        vVehtypeBinding.setVm(viewModel);


        vCustfield4Binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.v_custfield4, binding.svOne, false);
        vCustfield4Binding.setVm(viewModel);

        vCustfield5Binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.v_custfield5, binding.svOne, false);
        vCustfield5Binding.setVm(viewModel);

        vVinNoBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.v_vin_no, binding.svOne, false);
        vVinNoBinding.setVm(viewModel);
        addVinListeners();

        vTopBarBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.v_top_bar, binding.svOne, false);


    }

}
