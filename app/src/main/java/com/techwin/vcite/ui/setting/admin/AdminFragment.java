package com.techwin.vcite.ui.setting.admin;

import static com.techwin.vcite.ui.selector.state.StateActivity.getSatesList;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.techwin.vcite.BR;
import com.techwin.vcite.R;
import com.techwin.vcite.data.beans.DefaultBean;
import com.techwin.vcite.data.beans.VehicleColorType;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.databinding.DialogListTypeBinding;
import com.techwin.vcite.databinding.FragmentAdminBinding;
import com.techwin.vcite.databinding.HolderColorSelectorBinding;
import com.techwin.vcite.databinding.HolderStateSelectorBinding;
import com.techwin.vcite.di.base.adapter.SimpleRecyclerViewAdapter;
import com.techwin.vcite.di.base.view.AppFragment;
import com.techwin.vcite.di.base.view.BaseCustomDialog;
import com.techwin.vcite.ui.selector.color.PlateColorActivity;
import com.techwin.vcite.ui.selector.make.VehMakeActivity;
import com.techwin.vcite.ui.selector.platetype.PlateTypeActivity;
import com.techwin.vcite.ui.selector.state.StateActivity;
import com.techwin.vcite.util.event.SingleRequestEvent;

import java.util.List;

import javax.inject.Inject;

public class AdminFragment extends AppFragment<FragmentAdminBinding, AdminFragmentVM> {
    public static final String TAG = "AdminFragment";

    @Inject
    SharedPref sharedPref;


    @Override
    protected BindingFragment<AdminFragmentVM> getBindingFragment() {
        return new BindingFragment<>(R.layout.fragment_admin, AdminFragmentVM.class);
    }

    public static AdminFragment newInstance() {
        return new AdminFragment();
    }

    @Override
    protected void subscribeToEvents(final AdminFragmentVM vm) {
        vm.onClick.observe(this, new Observer<View>() {
            @Override
            public void onChanged(View view) {
                int id = view.getId();
                if (id == R.id.tv_platetype) {
                    Intent intent = PlateTypeActivity.newIntent(baseContext, binding.getBean().plateType);
                    launcherVehPlateType.launch(intent);
                    animateOpenSheet();
                } else if (id == R.id.tv_vehColor) {
                    vm.apiVehicleColor();
                } else if (id == R.id.tv_state) {
                    showSatesDialog();
                } else if (id == R.id.tv_vehmake) {
                    Intent intent = VehMakeActivity.newIntent(baseContext, binding.getBean().vehMake);
                    launcherVehMake.launch(intent);
                    animateOpenSheet();
                } else if (id == R.id.tv_platecolor) {
                    Intent intent = PlateColorActivity.newIntent(baseContext, binding.getBean().vehPlateColor);
                    launcherVehPlateColor.launch(intent);
                    animateOpenSheet();
                } else if (id == R.id.btn_restore) {
                    DefaultBean bean = new DefaultBean();
                    sharedPref.putDefault(bean);
                    binding.setBean(bean);
                }
            }
        });
        vm.obrColor.observe(this, new SingleRequestEvent.RequestObserver<VehicleColorType>() {
            @Override
            public void onRequestReceived(@NonNull Resource<VehicleColorType> resource) {
                switch (resource.status) {
                    case LOADING:
                        showProgressDialog(R.string.plz_wait);
                        break;
                    case ERROR:
                        dismissProgressDialog();
                        vm.error.setValue(resource.message);
                        break;
                    case SUCCESS:
                        dismissProgressDialog();
                        if (resource.data != null) {
                            showColorTypeDialog(resource.data.vehtTyp);
                        }
                        break;
                }
            }
        });

        binding.setBean(sharedPref.getDefault());

    }


    private BaseCustomDialog<DialogListTypeBinding> dialog;


    private void showColorTypeDialog(List<VehicleColorType.VehtTypBean> vehtTyp) {
        final String s = binding.getBean().vehColor;
        for (int i = 0; i < vehtTyp.size(); i++) {
            if (s == null) {
                vehtTyp.get(i).checked = true;
                break;
            }
            if (vehtTyp.get(i).Name.equalsIgnoreCase(s)) {
                vehtTyp.get(i).checked = true;
                break;
            }
        }

        SimpleRecyclerViewAdapter<VehicleColorType.VehtTypBean, HolderColorSelectorBinding> adapter = new SimpleRecyclerViewAdapter<>(R.layout.holder_color_selector, BR.bean);
        adapter.setCallback((v, bean) -> {
            for (int i = 0; i < vehtTyp.size(); i++) {
                if (vehtTyp.get(i).VehColorKey == bean.VehColorKey) {
                    vehtTyp.get(i).checked = true;
                } else vehtTyp.get(i).checked = false;
            }
            adapter.notifyDataSetChanged();
        });
        dialog = new BaseCustomDialog<>(baseContext, R.layout.dialog_list_type, view -> {
            switch (view.getId()) {
                case R.id.tv_ok:
                    for (VehicleColorType.VehtTypBean type : adapter.getList()) {
                        if (type.checked) {
                            DefaultBean citationBean = binding.getBean();
                            citationBean.vehColor = type.Name;
                            binding.setBean(citationBean);
                            break;
                        }
                    }
                    dialog.dismiss();
                    break;
                case R.id.tv_cancel:
                    dialog.dismiss();
                    break;
            }
        });

        dialog.getBinding().tvTitle.setText("Color Type");
        adapter.setList(vehtTyp);
        dialog.getBinding().rvOne.setAdapter(adapter);
        dialog.getBinding().rvOne.setLayoutManager(new LinearLayoutManager(baseContext));
        dialog.show();
    }


    private void showSatesDialog() {
        List<StateActivity.DummyBean> statesList = getSatesList();
        final String s = binding.getBean().vehState;
        for (int i = 0; i < statesList.size(); i++) {
            if (s == null) {
                statesList.get(i).checked = true;
                break;
            } else if (statesList.get(i).name.equalsIgnoreCase(s)) {
                statesList.get(i).checked = true;
                break;
            }
        }

        SimpleRecyclerViewAdapter<StateActivity.DummyBean, HolderStateSelectorBinding> adapter = new SimpleRecyclerViewAdapter<>(R.layout.holder_state_selector, BR.bean);
        adapter.setCallback((v, bean) -> {
            for (int i = 0; i < statesList.size(); i++) {
                if (statesList.get(i).abbreviation.equalsIgnoreCase(bean.abbreviation)) {
                    statesList.get(i).checked = true;
                } else statesList.get(i).checked = false;
            }
            adapter.notifyDataSetChanged();
        });
        dialog = new BaseCustomDialog<>(baseContext, R.layout.dialog_list_type, view -> {
            switch (view.getId()) {
                case R.id.tv_ok:
                    for (StateActivity.DummyBean type : adapter.getList()) {
                        if (type.checked) {
                            DefaultBean citationBean = binding.getBean();
                            citationBean.vehState = type.name;
                            binding.setBean(citationBean);
                            break;
                        }
                    }
                    dialog.dismiss();
                    break;
                case R.id.tv_cancel:
                    dialog.dismiss();
                    break;
            }
        });

        dialog.getBinding().tvTitle.setText("State of issuance");
        adapter.setList(statesList);
        dialog.getBinding().rvOne.setAdapter(adapter);
        dialog.getBinding().rvOne.setLayoutManager(new LinearLayoutManager(baseContext));
        dialog.show();
    }


    private final ActivityResultLauncher<Intent> launcherVehMake = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
        if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
            String date = result.getData().getStringExtra("data");
            if (date != null) {
                DefaultBean citationBean = binding.getBean();
                citationBean.vehMake = date;
                binding.setBean(citationBean);
            }
        }
    });
    private final ActivityResultLauncher<Intent> launcherVehPlateColor = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
        if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
            String date = result.getData().getStringExtra("data");
            if (date != null) {
                DefaultBean citationBean = binding.getBean();
                citationBean.vehPlateColor = date;
                binding.setBean(citationBean);
            }
        }
    });
    private final ActivityResultLauncher<Intent> launcherVehPlateType = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
        if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
            String date = result.getData().getStringExtra("data");
            if (date != null) {
                DefaultBean citationBean = binding.getBean();
                citationBean.plateType = date;
                binding.setBean(citationBean);
            }
        }
    });

    @Override
    public void onStop() {
        sharedPref.putDefault(binding.getBean());
        super.onStop();
    }
}
