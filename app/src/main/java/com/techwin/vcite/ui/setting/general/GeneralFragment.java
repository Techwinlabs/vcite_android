package com.techwin.vcite.ui.setting.general;

import android.content.Intent;
import android.view.View;

import androidx.lifecycle.Observer;

import com.techwin.vcite.R;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.databinding.FragmentGeneralBinding;
import com.techwin.vcite.di.base.view.AppFragment;
import com.techwin.vcite.ui.login.LoginActivity;

import javax.inject.Inject;

public class GeneralFragment extends AppFragment<FragmentGeneralBinding, GeneralFragmentVM> {
    public static final String TAG = "GeneralFragment";

    @Inject
    SharedPref sharedPref;


    @Override
    protected BindingFragment<GeneralFragmentVM> getBindingFragment() {
        return new BindingFragment<>(R.layout.fragment_general, GeneralFragmentVM.class);
    }

    public static GeneralFragment newInstance() {
        return new GeneralFragment();
    }

    @Override
    protected void subscribeToEvents(final GeneralFragmentVM vm) {
        vm.onClick.observe(this, new Observer<View>() {
            @Override
            public void onChanged(View view) {
                switch (view.getId()) {
                    case R.id.tv_logout:
                        Intent intent = LoginActivity.newIntent(baseContext);
                        startNewActivity(intent);
                        if (getActivity() != null)
                            getActivity().finishAffinity();
                        break;
                }
            }
        });
    }


}
