package com.techwin.vcite.ui.setting.general;



import androidx.databinding.ObservableField;

import com.techwin.vcite.di.base.viewmodel.BaseViewModel;
import com.techwin.vcite.util.event.SingleLiveEvent;

import javax.inject.Inject;


public class GeneralFragmentVM extends BaseViewModel {

    final SingleLiveEvent<CharSequence> obrOther = new SingleLiveEvent<>();

    public final ObservableField<String> fieldOther = new ObservableField<>();

    @Inject
    public GeneralFragmentVM() {

    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
        obrOther.setValue(s);
    }
}
