package com.techwin.vcite.ui.violation.type;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.library.baseAdapters.BR;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.techwin.vcite.R;
import com.techwin.vcite.data.local.entity.ViolationBean;
import com.techwin.vcite.databinding.ActivityViolationBinding;
import com.techwin.vcite.databinding.HolderViolationBinding;
import com.techwin.vcite.di.base.adapter.SimpleRecyclerViewAdapter;
import com.techwin.vcite.di.base.view.AppActivity;
import com.techwin.vcite.util.decoration.ListVerticalItemDecoration;
import com.techwin.vcite.util.event.SingleRequestEvent;

import java.util.ArrayList;
import java.util.List;

public class ViolationActivity extends AppActivity<ActivityViolationBinding, ViolationActivityVM> {

    private SimpleRecyclerViewAdapter<ViolationBean, HolderViolationBinding> adapter;
    @Nullable
    private ArrayList<ViolationBean> violationBeans;
    ArrayList<ViolationBean> mList = new ArrayList<>();
    ArrayList<ViolationBean> selectedList = new ArrayList<>();

    @Override
    protected BindingActivity<ViolationActivityVM> getBindingActivity() {
        return new BindingActivity<>(R.layout.activity_violation, ViolationActivityVM.class);
    }

    public static Intent newIntent(Context activity) {
        Intent intent = new Intent(activity, ViolationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    public static Intent newIntent(Context activity, ArrayList<ViolationBean> violationBeans) {
        Intent intent = new Intent(activity, ViolationActivity.class);
        intent.putParcelableArrayListExtra("list", violationBeans);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    @Override
    protected void subscribeToEvents(final ViolationActivityVM vm) {
        initView();
        initAdapter();
        initObservers();
        initSearchFilter();
        vm.getList();
    }

    private void initSearchFilter() {
        binding.etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void afterTextChanged(Editable s) {
                if (s == null || s.toString().equalsIgnoreCase("")) {
                    adapter.setList(mList);
                } else {
                    ArrayList<ViolationBean> sortedList = new ArrayList<>();
                    for (ViolationBean mBean : mList) {
                        if (mBean.getDescription().toLowerCase().contains(s.toString().toLowerCase()))
                            sortedList.add(mBean);
                    }
                    adapter.setList(sortedList);
                }
            }
        });
    }

    @SuppressLint("NonConstantResourceId")
    private void initObservers() {
        viewModel.onClick.observe(this, view -> {
            switch (view != null ? view.getId() : 0) {
                case R.id.tv_save:
                    Intent intent = new Intent();
                    intent.putParcelableArrayListExtra("data", selectedList);
                    setResult(Activity.RESULT_OK, intent);
                    finish(true);
                    break;
                case R.id.iv_back:
                    finish(true);
                    break;

            }
        });

        viewModel.obrData.observe(this, (SingleRequestEvent.RequestObserver<List<ViolationBean>>) resource -> {
            switch (resource.status) {
                case LOADING:
                    showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    setDataInAdapter(resource.data);
                    if (resource.data != null) {
                        mList.addAll(resource.data);
                    }
                    break;
                case ERROR:
                    dismissProgressDialog();
                    viewModel.error.setValue(resource.message);
                    break;
            }
        });
    }

    private void initAdapter() {
        adapter = new SimpleRecyclerViewAdapter<>(R.layout.holder_violation, BR.bean);
        adapter.setCallback(new SimpleRecyclerViewAdapter.SimpleCallback<ViolationBean>() {
            @Override
            public void onItemClick(View v, ViolationBean imageBean) {
            }

            @Override
            public void onItemClick(View view, int pos) {
                if (adapter.getList().get(pos).isChecked()) {
                    adapter.getList().get(pos).setChecked(false);
                    for (int i = 0; i < selectedList.size(); i++) {
                        if (selectedList.get(i).getId() == adapter.getList().get(pos).getId()) {
                            selectedList.remove(selectedList.get(i));
                        }
                    }
                    adapter.notifyItemChanged(pos);
                } else if (selectedList.size() >= 3) {
                    viewModel.info.setValue("Not more than 3");
                } else {
                    adapter.getList().get(pos).setChecked(true);
                    selectedList.add(adapter.getList().get(pos));
                    adapter.notifyItemChanged(pos);
                }
            }
        });

        binding.rvOne.setLayoutManager(new LinearLayoutManager(this));
        binding.rvOne.addItemDecoration(new ListVerticalItemDecoration(this, R.dimen.dp_5));
        binding.rvOne.setAdapter(adapter);
    }

    @SuppressLint("SetTextI18n")
    private void initView() {
        violationBeans = getIntent().getParcelableArrayListExtra("list");
        binding.toolbar.tvTitle.setText("Select Violations");
    }

    private void setDataInAdapter(List<ViolationBean> data) {
        List<Long> ids = new ArrayList<>();
        if (violationBeans != null) {
            for (ViolationBean violationBean : violationBeans) {
                ids.add(violationBean.getId());
                selectedList.add(violationBean);
            }
        }
        for (int i = 0; i < data.size(); i++) {
            if (ids.contains(data.get(i).getId())) {
                data.get(i).setChecked(true);
            }
        }
        adapter.setList(data);
    }
}

