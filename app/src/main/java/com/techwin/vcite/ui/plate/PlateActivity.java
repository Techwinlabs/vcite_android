package com.techwin.vcite.ui.plate;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.techwin.vcite.BR;
import com.techwin.vcite.R;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.data.remote.helper.Status;
import com.techwin.vcite.databinding.ActivityPlateBinding;
import com.techwin.vcite.databinding.HolderPlateNoBinding;
import com.techwin.vcite.di.base.adapter.SimpleRecyclerViewAdapter;
import com.techwin.vcite.di.base.view.AppActivity;
import com.techwin.vcite.ui.imagePicker.CustomImagePickerActivity;
import com.techwin.vcite.util.decoration.ListVerticalItemDecoration;

import org.openalpr.model.Candidate;
import org.openalpr.model.Result;
import org.openalpr.model.Results;
import org.openalpr.model.ResultsError;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class PlateActivity extends AppActivity<ActivityPlateBinding, PlateActivityVM> {

    private final int REQUEST_FILE = 42;
    private final int STORAGE = 1;
    private String ANDROID_DATA_DIR;
    private File imageFile;
    private SimpleRecyclerViewAdapter<Result, HolderPlateNoBinding> adapter;

    @Override
    protected BindingActivity<PlateActivityVM> getBindingActivity() {
        return new BindingActivity<>(R.layout.activity_plate, PlateActivityVM.class);
    }

    public static Intent newIntent(Context activity) {
        Intent intent = new Intent(activity, PlateActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }


    @SuppressLint({"SetTextI18n", "NonConstantResourceId"})
    @Override
    protected void subscribeToEvents(final PlateActivityVM vm) {
        binding.toolbar.tvTitle.setText("Licence Plate");
        ANDROID_DATA_DIR = this.getApplicationInfo().dataDir;
        vm.onClick.observe(this, view -> {
            switch (view.getId()) {
                case R.id.btnTakePicture:
                    takePicture();
                    break;
                case R.id.iv_back:
                    finish(true);
                    break;
            }
        });
        vm.obrResult.observe(this, (Observer<Resource<String>>) stringResource -> {
            if (stringResource.status == Status.SUCCESS) {
                processResult(stringResource.data);
            } else if (stringResource.status == Status.ERROR) {
                vm.error.setValue(stringResource.message);
            }
        });
        adapter = new SimpleRecyclerViewAdapter<>(R.layout.holder_plate_no, BR.bean);
        adapter.setCallback((v, resultsBean) -> {
            Intent intent = new Intent();
            intent.putExtra("data", resultsBean.getPlate());
            if (binding.cbOne.isChecked() && imageFile != null)
                intent.putExtra("image", imageFile.getAbsolutePath());
            setResult(Activity.RESULT_OK, intent);
            finish(true);
        });
        binding.rvOne.addItemDecoration(new ListVerticalItemDecoration(this, R.dimen.dp_5));
        binding.rvOne.setLayoutManager(new LinearLayoutManager(this));
        binding.rvOne.setAdapter(adapter);
        takePicture();
    }

    private void processResult(String result) {
        Log.d("OPEN ALPR", result);
        dismissProgressDialog();
        try {
            final int[] x1 = {0};
            final int[] x2 = {0};
            final int[] y1 = {0};
            final int[] y2 = {0};
            final String[] plate = {""};
            final Results results = new Gson().fromJson(result, Results.class);
            if (results == null || results.getResults() == null || results.getResults().size() == 0) {
                viewModel.error.setValue("It was not possible to detect the licence plate.");
                adapter.setList(null);
            } else {
                List<Result> temp = new ArrayList<>();

                for (Result result1 : results.getResults()) {
                    temp.add(result1);
                    if (result1.getCandidates() != null) {
                        for (Candidate candidate : result1.getCandidates()) {
                            temp.add(new Result(candidate.getPlate(), candidate.getConfidence(), null, null, null, null, null, null));

                        }
                    }
                }


                adapter.setList(temp);
                Glide.with(PlateActivity.this).load(imageFile).centerCrop().into(binding.imageView);
                if (binding.imageView.getDrawable() != null) {
                    Bitmap bitmap = ((BitmapDrawable) binding.imageView.getDrawable()).getBitmap();
                    Bitmap originalBitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath(), new BitmapFactory.Options());

                    float viewWidth = bitmap.getWidth();
                    float viewHeigth = bitmap.getHeight();
                    float originalWidth = originalBitmap.getWidth();
                    float originalHeigth = originalBitmap.getHeight();

                    Canvas canvas = new Canvas(bitmap);
                    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
                    paint.setColor(Color.GREEN);
                    paint.setStyle(Paint.Style.STROKE);
                    paint.setStrokeWidth(8);

                    // map rectangle coordinates to imageview
                    int p1_x = (int) ((x1[0] * viewWidth) / originalWidth);
                    int p1_y = (int) ((y1[0] * viewHeigth) / originalHeigth);
                    int p2_x = (int) ((x2[0] * viewWidth) / originalWidth);
                    int p2_y = (int) ((y2[0] * viewHeigth) / originalHeigth);
                    canvas.drawRect(new Rect(p1_x, p1_y, p2_x, p2_y), paint);

                    paint.setTextSize(75);
                    paint.setStyle(Paint.Style.FILL);
                    paint.setTypeface(Typeface.DEFAULT_BOLD);
                    paint.setColor(Color.YELLOW);
                    canvas.drawText(plate[0], p1_x, p1_y - 10, paint);
                    binding.imageView.setImageBitmap(bitmap);
                }

            }
        } catch (JsonSyntaxException exception) {
            final ResultsError resultsError = new Gson().fromJson(result, ResultsError.class);
            viewModel.info.postValue(resultsError.getMsg());
        }

    }

/*
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
        // CropImage.ActivityResult result = CropImage.getActivityResult(data);
        // if (resultCode == RESULT_OK) {
        //  Uri resultUri = result.getUri();
        //  final String openAlprConfFile = ANDROID_DATA_DIR + File.separatorChar + "runtime_data" + File.separatorChar + "openalpr.conf";
        //  imageFile = new File(resultUri.getPath());
        //  int candidates = binding.txtCandidatesNum.getText().toString().isEmpty() ? 10 : Integer.parseInt((binding.txtCandidatesNum.getText().toString()));
        // showProgressDialog(R.string.plz_wait);
        //   viewModel.getResult(this, ANDROID_DATA_DIR, binding.txtCountry.getText().toString(), binding.txtRegion.getText().toString(), imageFile.getAbsolutePath(), openAlprConfFile, candidates);

        //  } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

        //  }
        //   }
        if (requestCode == ImagePicker.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                imageFile = ImagePicker.Companion.getFile(data);
                final String openAlprConfFile = ANDROID_DATA_DIR + File.separatorChar + "runtime_data" + File.separatorChar + "openalpr.conf";
                int candidates = binding.txtCandidatesNum.getText().toString().isEmpty() ? 10 : Integer.parseInt((binding.txtCandidatesNum.getText().toString()));
                showProgressDialog(R.string.plz_wait);
                viewModel.getResult(this, ANDROID_DATA_DIR, binding.txtCountry.getText().toString(), binding.txtRegion.getText().toString(), imageFile.getAbsolutePath(), openAlprConfFile, candidates);

            } else if (resultCode == ImagePicker.RESULT_ERROR) {
                viewModel.error.setValue(ImagePicker.Companion.getError(data));
            }
        }
    }*/

 /*   public void takePicture() {
        // CropImage.activity().start(this);
        ImagePicker.Companion.with(this)
                .compress(1024)
                .cameraOnly()
                .start();
    }
*/

    public void takePicture() {
        Intent intent = CustomImagePickerActivity.newIntent(this);
        launcher.launch(intent);
//        Intent intent = ImagePicker.Companion.with(this)
//                .crop()
//                .cropFreeStyle()
//                .cameraOnly()//Crop image(Optional), Check Customization for more option
//                .maxResultSize(512, 512, true)    //Final image resolution will be less than 1080 x 1080(Optional)
//                .createIntent();
//        launcher.launch(intent);
    }


    private final ActivityResultLauncher<Intent> launcher =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), (ActivityResult result) -> {
                if (result.getResultCode() == RESULT_OK && result.getData() != null) {
                    try {
                        imageFile = new File(result.getData().getSerializableExtra("image_arr").toString());
                        final String openAlprConfFile = ANDROID_DATA_DIR + File.separatorChar + "runtime_data" + File.separatorChar + "openalpr.conf";
                        int candidates = binding.txtCandidatesNum.getText().toString().isEmpty() ? 10 : Integer.parseInt((binding.txtCandidatesNum.getText().toString()));
                        showProgressDialog(R.string.plz_wait);
                        viewModel.getResult(this, ANDROID_DATA_DIR, binding.txtCountry.getText().toString(), binding.txtRegion.getText().toString(), imageFile.getAbsolutePath(), openAlprConfFile, candidates);
                    } catch (Exception e) {

                    }
                }
                else {
                    finish();
                }
//                if (result.getResultCode() == RESULT_OK) {
//                imageFile = ImagePicker.Companion.getFile(result.getData());
//                final String openAlprConfFile = ANDROID_DATA_DIR + File.separatorChar + "runtime_data" + File.separatorChar + "openalpr.conf";
//                int candidates = binding.txtCandidatesNum.getText().toString().isEmpty() ? 10 : Integer.parseInt((binding.txtCandidatesNum.getText().toString()));
//                showProgressDialog(R.string.plz_wait);
//                viewModel.getResult(this, ANDROID_DATA_DIR, binding.txtCountry.getText().toString(), binding.txtRegion.getText().toString(), imageFile.getAbsolutePath(), openAlprConfFile, candidates);

//                } else if (result.getResultCode() == ImagePicker.RESULT_ERROR) {
//                    viewModel.error.setValue(ImagePicker.Companion.getError(result.getData()));
//                } else if (result.getResultCode() == RESULT_CANCELED) {
//                    finish();
//                }
            });


    @Override
    protected void onResume() {
        super.onResume();
        if (imageFile != null) {
            Glide.with(PlateActivity.this).load(imageFile).centerCrop().into(binding.imageView);

        }
    }


}
