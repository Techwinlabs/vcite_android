package com.techwin.vcite.ui.home;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.techwin.vcite.R;
import com.techwin.vcite.data.beans.CitTempBean;
import com.techwin.vcite.data.beans.CitationData;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.local.entity.CitationBean;
import com.techwin.vcite.databinding.FragmentCitationsBinding;
import com.techwin.vcite.di.base.view.AppFragment;
import com.techwin.vcite.di.base.view.BaseAlertDialog;
import com.techwin.vcite.ui.main.MainActivity;
import com.techwin.vcite.ui.photo.detail.ImageDetailActivity;
import com.techwin.vcite.util.AppUtils;
import com.techwin.vcite.util.Constants;
import com.techwin.vcite.util.decoration.ListVerticalItemDecoration;
import com.techwin.vcite.util.event.SingleRequestEvent;
import com.techwin.vcite.util.misc.SwipeHelper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

public class CitationsFragment extends AppFragment<FragmentCitationsBinding, CitationsFragmentVM> {
    public static final String TAG = "CitationsFragment";
    @Nullable
    private MainActivity mainActivity;
    @Inject
    SharedPref sharedPref;
    private Adapter adapter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mainActivity = (MainActivity) getActivity();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected BindingFragment<CitationsFragmentVM> getBindingFragment() {
        return new BindingFragment<>(R.layout.fragment_citations, CitationsFragmentVM.class);
    }

    public static CitationsFragment newInstance() {
        return new CitationsFragment();
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    protected void subscribeToEvents(final CitationsFragmentVM vm) {
        List<CitTempBean> tempPark = sharedPref.getTemplate(Constants.CITATION_TYPE_PARKING);
        List<CitTempBean> tempCode = sharedPref.getTemplate(Constants.CITATION_TYPE_CODE_ENFORCE);
        adapter = new Adapter(new Listioner() {
            @Override
            public void onItemClick(CitationBean citationBean) {
                if (mainActivity != null) {
                /*    if (citationBean.isUploaded()) {
                        vm.info.setValue("Ticket is uploaded");*/
                    if (AppUtils.is24hrspassed(citationBean.getCreatedOn())) {
                        showwarningdialog(citationBean);
                    } else {
                        mainActivity.citationData = new CitationData(citationBean.getCitationNo(), citationBean.getOrgNo(), citationBean.getCitationType(), citationBean.isUploaded());
                        mainActivity.openFirstTab();
                    }
                }
            }

            @Override
            public String missingElements(CitationBean bean) {
                if (bean.getCitationType().equalsIgnoreCase(Constants.CITATION_TYPE_PARKING)) {
                    String missing = AppUtils.convertArrayToString(AppUtils.missing(bean, tempPark));
                    if (missing != null) {
                        return "Missing :" + missing;
                    }
                } else if (bean.getCitationType().equalsIgnoreCase(Constants.CITATION_TYPE_CODE_ENFORCE)) {
                    String missing = AppUtils.convertArrayToString(AppUtils.missingEnforcement(bean, tempCode));
                    if (missing != null) {
                        return "Missing :" + missing;
                    }
                }
                return null;
            }
        });
        binding.rvOne.setLayoutManager(new LinearLayoutManager(baseContext));
        binding.rvOne.addItemDecoration(new ListVerticalItemDecoration(baseContext, R.dimen.dp_5));
        binding.rvOne.setAdapter(adapter);
        binding.rvOne.setEmptyView(binding.vEmpty);
        new SwipeHelper(baseContext, binding.rvOne) {
            @SuppressLint("ResourceAsColor")
            @Override
            public void instantiateUnderlayButton(RecyclerView.ViewHolder viewHolder, List<UnderlayButton> underlayButtons) {
                underlayButtons.add(new UnderlayButton("Delete", R.drawable.ic_delete, R.color.red, R.font.medium, pos -> {
                    if (pos < adapter.getItemCount())
                        deleteItem(adapter.getDataList().get(pos));
                }));
            }
        };

        vm.onClick.observe(this, view -> {
            switch (view.getId()) {
                case R.id.iv_add:
                    if (mainActivity != null) {
                        mainActivity.openScanner();
                    }
                    break;
                case R.id.btn_upload:
                    if (mainActivity != null) {
                        mainActivity.syncData();
                    }
                    break;
                case R.id.btn_clear:
                    showDeleteDialog();
                    break;
            }
        });
        vm.obrData.observe(this, (SingleRequestEvent.RequestObserver<List<CitationBean>>) resource -> {
            switch (resource.status) {
                case LOADING:
                    if (!binding.srOne.isRefreshing())
                        binding.srOne.setRefreshing(true);
                    break;
                case SUCCESS:
                    binding.srOne.setRefreshing(false);
                    setDataInAdapter(resource.data);
                    break;
                case ERROR:
                    binding.srOne.setRefreshing(false);
                    setDataInAdapter(null);
                    break;
            }
        });
        //  vm.clear7daybeforeCitation();
        viewModel.getCitationList();
        binding.srOne.setOnRefreshListener(() -> viewModel.getCitationList());
    }


    private void showwarningdialog(CitationBean citationBean) {
        BaseAlertDialog alertDialog = new BaseAlertDialog(baseContext);
        alertDialog.setLabels("Edit Information", "Can't Edit ticket after 24 hrs", "View Media", "Cancel");
        alertDialog.setListioner(new BaseAlertDialog.ClickListioner() {
            @Override
            public void onOkClick() {
                alertDialog.dismiss();
                CitationData citationData = new CitationData(citationBean.getCitationNo(), citationBean.getOrgNo(), citationBean.getCitationType(), citationBean.isUploaded());
                Intent intent = ImageDetailActivity.newIntent(baseContext, 0, citationData);
                startNewActivity(intent);
            }

            @Override
            public void onCancelClick() {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
        alertDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
    }

    private void deleteItem(CitationBean bean) {
        new AlertDialog.Builder(requireContext())
                .setTitle("Delete Item")
                .setMessage("Are you sure want to delete this item?")
                .setNegativeButton("No", (dialog, which) -> dialog.dismiss())
                .setPositiveButton("Yes", (dialog, which) -> {
                    dialog.dismiss();
                    viewModel.clearCitation(bean);
                })
                .show();

    }

    private void showDeleteDialog() {
        BaseAlertDialog alertDialog = new BaseAlertDialog(baseContext);
        alertDialog.setLabels("Confirm", "Are you sure want to remove this citation from log?", "Ok", "Cancel");
        alertDialog.setListioner(new BaseAlertDialog.ClickListioner() {
            @Override
            public void onOkClick() {
                alertDialog.dismiss();
                viewModel.clearAllCitation();
            }

            @Override
            public void onCancelClick() {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
        alertDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
    }

    private void setDataInAdapter(List<CitationBean> data) {
        adapter.setList(data);
        if (data != null && data.size() > 0) {
            boolean show = false;
            for (CitationBean bean : data) {
                if (!bean.isUploaded()) {
                    show = true;
                    break;
                }
            }
            if (show)
                binding.setDate(new SimpleDateFormat("dd MMM yyyy", Locale.US).format(Calendar.getInstance().getTime()));
            else binding.setDate(null);
        } else binding.setDate(null);
    }

}
