package com.techwin.vcite.ui.main;


import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;

import com.google.gson.Gson;
import com.techwin.vcite.data.beans.MenuBean;
import com.techwin.vcite.data.beans.PlateType;
import com.techwin.vcite.data.beans.UserBean;
import com.techwin.vcite.data.beans.UserPermission;
import com.techwin.vcite.data.beans.VehType;
import com.techwin.vcite.data.beans.VehicleColorType;
import com.techwin.vcite.data.beans.VelTypeLnn;
import com.techwin.vcite.data.beans.base.ApiResponse;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.local.dao.CitationDao;
import com.techwin.vcite.data.local.dao.MediaBeanDao;
import com.techwin.vcite.data.local.dao.TransactionDao;
import com.techwin.vcite.data.local.entity.CitationBean;
import com.techwin.vcite.data.local.entity.MediaBean;
import com.techwin.vcite.data.local.entity.ViolationBean;
import com.techwin.vcite.data.remote.helper.NetworkErrorHandler;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.data.repo.BaseRepo;
import com.techwin.vcite.di.base.viewmodel.BaseViewModel;
import com.techwin.vcite.util.AppUtils;
import com.techwin.vcite.util.Constants;
import com.techwin.vcite.util.event.SingleActionEvent;
import com.techwin.vcite.util.event.SingleLiveEvent;
import com.techwin.vcite.util.event.SingleRequestEvent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainActivityVM extends BaseViewModel {

    final SingleActionEvent<MenuItem> bottombar = new SingleActionEvent<>();
    private final SharedPref sharedPref;
    private final NetworkErrorHandler networkErrorHandler;
    final SingleLiveEvent<MenuBean> menu_bean = new SingleActionEvent<>();
    final SingleRequestEvent<List<PlateType>> obrPlates = new SingleRequestEvent<>();
    final SingleRequestEvent<VehicleColorType> obrColor = new SingleRequestEvent<>();
    final SingleRequestEvent<List<VelTypeLnn.VehtAbbrBean>> obrMake = new SingleRequestEvent<>();
    public final SingleRequestEvent<String> obrSubmit = new SingleRequestEvent<>();
    private final BaseRepo baseRepo;
    private final MediaBeanDao mediaBeanDao;
    final SingleRequestEvent<List<CitationBean>> obrList = new SingleRequestEvent<>();
    final SingleRequestEvent<Void> obrLogout = new SingleRequestEvent<>();
    private final CitationDao citationDao;
    private final TransactionDao transactionDao;
    final SingleRequestEvent<Void> obrSave = new SingleRequestEvent<>();

    @Inject
    public MainActivityVM(SharedPref sharedPref, NetworkErrorHandler networkErrorHandler, BaseRepo baseRepo, MediaBeanDao mediaBeanDao, CitationDao citationDao, TransactionDao transactionDao) {
        this.sharedPref = sharedPref;
        this.networkErrorHandler = networkErrorHandler;
        this.baseRepo = baseRepo;
        this.mediaBeanDao = mediaBeanDao;
        this.citationDao = citationDao;
        this.transactionDao = transactionDao;
    }


    void saveCitation(CitationBean citationBean) {
        compositeDisposable.add(Observable.fromCallable(() -> transactionDao.updateTrans(citationBean)).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(aLong -> {
                    Log.e(TAG, "insert" + aLong);
                    obrSave.setValue(Resource.success(null, "Saved"));
                }, throwable -> {
                    obrSave.setValue(Resource.success(null, "Error :" + throwable.getMessage()));
                    Log.e(TAG, "error" + throwable.getMessage());
                }));
    }

    public boolean onNavigationClick(MenuItem menuItem) {
        bottombar.call(menuItem);
        return true;

    }

    public void setMenuItem(MenuBean s) {
        compositeDisposable.add(Observable.just(s).delay(200, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(menu_bean::setValue));
    }

    void apiPlateType() {
        List<PlateType> s = sharedPref.getPlateTypeList();
        if (s != null && s.size() > 0) {
            obrPlates.setValue(Resource.success(s, "from local"));
        } else {
            UserBean userBean = sharedPref.getUser();
            if (userBean.CustKey == Constants.LYNNMA_CUSTKEY) {
                baseRepo.getPlateForLynn().subscribe(new SingleObserver<ApiResponse<VelTypeLnn>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrPlates.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(ApiResponse<VelTypeLnn> listApiResponse) {
                        List<PlateType> plateTypes = new ArrayList<>();
                        if (listApiResponse.getData() != null) {
                            if (listApiResponse.getData().pltTyp != null) {
                                plateTypes.addAll(listApiResponse.getData().pltTyp);
                            }
                        }
                        obrPlates.setValue(Resource.success(plateTypes, "from remote"));

                    }

                    @Override
                    public void onError(Throwable e) {
                        obrPlates.setValue(Resource.error(null, networkErrorHandler.getErrMsg(e)));
                    }
                });

            } else {
                baseRepo.getPlateType().subscribe(new SingleObserver<ApiResponse<List<PlateType>>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrPlates.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(ApiResponse<List<PlateType>> listApiResponse) {
                        obrPlates.setValue(Resource.success(listApiResponse.getData(), "from remote"));
                    }

                    @Override
                    public void onError(Throwable e) {
                        obrPlates.setValue(Resource.error(null, networkErrorHandler.getErrMsg(e)));
                    }
                });
            }
        }
    }

    void apiVehicleColor() {
        VehicleColorType s = sharedPref.getColorType();
        if (s != null) {
            obrColor.setValue(Resource.success(s, "from local"));
        } else {
            baseRepo.getVehicleColor().subscribe(new SingleObserver<ApiResponse<VehicleColorType>>() {
                @Override
                public void onSubscribe(Disposable d) {
                    compositeDisposable.add(d);
                    obrColor.setValue(Resource.loading(null));
                }

                @Override
                public void onSuccess(ApiResponse<VehicleColorType> listApiResponse) {
                    obrColor.setValue(Resource.success(listApiResponse.getData(), "Done"));
                }

                @Override
                public void onError(Throwable e) {
                    obrColor.setValue(Resource.error(null, networkErrorHandler.getErrMsg(e)));
                }
            });
        }
    }

    void apiVehMake() {
        List<VelTypeLnn.VehtAbbrBean> s = sharedPref.getVehMake();
        if (s != null) {
            obrMake.setValue(Resource.success(s, "from local"));
        } else {
            baseRepo.vehMake().subscribe(new SingleObserver<ApiResponse<VelTypeLnn>>() {
                @Override
                public void onSubscribe(Disposable d) {
                    compositeDisposable.add(d);
                    obrMake.setValue(Resource.loading(null));
                }

                @Override
                public void onSuccess(ApiResponse<VelTypeLnn> listApiResponse) {
                    if (listApiResponse.getData() != null)
                        obrMake.setValue(Resource.success(listApiResponse.getData().vehtAbbr, "Done"));
                }

                @Override
                public void onError(Throwable e) {
                    obrMake.setValue(Resource.error(null, networkErrorHandler.getErrMsg(e)));
                }
            });
        }
    }

    public void apiSubmitCitation(CitationBean citationData) {
        Log.i("HTTP_RESPONSE", new Gson().toJson(citationData));
        compositeDisposable.add(Observable.fromCallable(() -> mediaBeanDao.getList(citationData.getCitationNo(), citationData.getOrgNo()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mediaBeans -> apiSubmitCitationCall(citationData, mediaBeans), throwable -> apiSubmitCitationCall(citationData, null)));


    }

    void apiUpdateCitation(CitationBean citationData) {
        compositeDisposable.add(Observable.fromCallable(() -> mediaBeanDao.getList(citationData.getCitationNo(), citationData.getOrgNo()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mediaBeans -> apiSubmitCitationCall(citationData, mediaBeans), throwable -> apiSubmitCitationCall(citationData, null)));
    }

    void apiSubmitCitationCall(CitationBean citationBean, List<MediaBean> mediaBeans) {
//        if (AppUtils.missing(citationBean, sharedPref.getTemplate(citationBean.getCitationType())).size() > 0)
//            return;
        UserBean userBean = sharedPref.getUser();
        JSONObject map = new JSONObject();
        try {
            //createdon
            map.put("issueDate", new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(citationBean.getCreatedOn()));
            map.put("issueTime", new SimpleDateFormat("HH:mm", Locale.US).format(citationBean.getCreatedOn()));

            //"vechLic": CD_PLATENUMBER,
            if (!TextUtils.isEmpty(citationBean.getLicenceNo()))
                map.put("vechLic", citationBean.getLicenceNo());
            // "PlateType": CD_PLATETYPE,
            if (!TextUtils.isEmpty(citationBean.getPlateType()))
                map.put("PlateType", citationBean.getPlateType());
//  "PlateColor": CD_PLATECOLOR,
            if (!TextUtils.isEmpty(citationBean.getColor()))
                map.put("PlateColor", citationBean.getColor());
//    "VehState": CD_STATE,
            if (!TextUtils.isEmpty(citationBean.getState()))
                map.put("VehState", citationBean.getState());
//    "VehColor": CD_COLOR,
            if (!TextUtils.isEmpty(citationBean.getColor()))
                map.put("VehColor", citationBean.getColor());
//    "VehMake": CD_MAKE,
            if (!TextUtils.isEmpty(citationBean.getMake()))
                map.put("VehMake", citationBean.getMake());

            /*    "DocKey": UserDefaults.standard.integer(forKey: UD_DOCKEY),
                        "StatusKey": "99",
                        "InitUserKey": UserDefaults.standard.integer(forKey: UD_USERKEY),
                        "SerialNumber": CD_CITATIONNUMBER,
                        "location": CD_LOCATION,*/
            map.put("DocKey", userBean.DocKey);
            map.put("StatusKey", "99");
            map.put("InitUserKey", userBean.UserKey);

            if (!TextUtils.isEmpty(citationBean.getCitationNo()))
                map.put("SerialNumber", citationBean.getCitationNo());

            if (!TextUtils.isEmpty(citationBean.getLocation()))
                map.put("location", citationBean.getLocation());


            JSONArray jsonArray = new JSONArray();
            List<ViolationBean> violationBeans = citationBean.getViolation();
            if (violationBeans != null) {
                for (ViolationBean bean : violationBeans) {
                    jsonArray.put(bean.getViolationId());
                }
            }
            /*   "violation":array,
                        "comment" : CD_VIOLATIONCOMMENT,
                        "orgCode": "\(CD_ORAGANISATION)",*/
            map.put("violation", jsonArray);

            if (!TextUtils.isEmpty(citationBean.getComment()))
                map.put("comment", citationBean.getComment());

            map.put("orgCode", citationBean.getOrgNo());
            /*---------------------------------------------------------------------------------*/
/*  "Vin":CD_VIN,
                        "isvoid": CD_ISVOID,
                        "warning": CD_ISWARNING,
                        "Latitude": String(CD_LATITUDE),
                        "Longitude":String(CD_LONGITUDE),
                        "expirationDate": CD_EXPIRATIONDATE,*/
            if (!TextUtils.isEmpty(citationBean.getVin()))
                map.put("Vin", citationBean.getVin());

            map.put("isvoid", citationBean.isIsvoid());
            map.put("warning", citationBean.isWarning());

            if (!TextUtils.isEmpty(String.valueOf(citationBean.getLatitude())))
                map.put("Latitude", String.valueOf(citationBean.getLatitude()));
            if (!TextUtils.isEmpty(String.valueOf(citationBean.getLongitude())))
                map.put("Longitude", String.valueOf(citationBean.getLongitude()));
            long seconds = citationBean.getExpiryDate();
            if (seconds >= 0) {
                Date date = new Date();
                map.put("expirationDate", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).format(date));
            }

            /*-----------------------------------------------------------------------------------------------*/


            JSONArray l = new JSONArray();
            JSONArray format = new JSONArray();
            JSONArray mediaName = new JSONArray();
            String ticket = null;
            if (mediaBeans != null) {
                for (int i = 0; i < mediaBeans.size(); i++) {
                    if (mediaBeans.get(i).getPicType() == Constants.PIC_TYPE_TICKET) {
                        ticket = mediaBeans.get(i).getImage();
                    } else {
                        l.put(mediaBeans.get(i).getImage());
                        format.put("jpg");
                        mediaName.put("img" + (i + 1) + ".jpg");
                    }
                }

            }/*
                        "IsDeleted": false,
                        "MediaType": 1,
                        "parkingViolPDFMedia": CD_TICKETIMAGE?.base64EncodedString() ?? "",
                        "PDFFormat": "jpg",
                        "PDFMediaName": "img.jpg",*/
            if (ticket != null) {
                map.put("parkingViolPDFMedia", ticket);
                map.put("PDFFormat", "jpg");
                map.put("PDFMediaName", "img.jpg");
            }
            map.put("Media", l);
            map.put("Format", format);
            map.put("MediaName", mediaName);

            map.put("IsDeleted", false);

            map.put("MediaType", 1);
            /*------------------------------------------------------------------------------------------------------------------*/


            //     "OfficerID":CD_OFFICERID,
            if (!TextUtils.isEmpty(citationBean.getOfficerId()))
                map.put("OfficerID", citationBean.getOfficerId());
            //      "Division":CD_DIVISION,

            if (!TextUtils.isEmpty(citationBean.getDivisionId()))
                map.put("Division", citationBean.getDivisionId());

//"ClerkID":CD_CLERKID,
            if (!TextUtils.isEmpty(citationBean.getClerkId()))
                map.put("ClerkID", citationBean.getClerkId());

            //"VehModel":CD_VEHMODEL,
            if (!TextUtils.isEmpty(citationBean.getVehModel()))
                map.put("VehModel", citationBean.getVehModel());

            if (!TextUtils.isEmpty(citationBean.getState()))
                map.put("VehState", citationBean.getState());

//  "VehType":CD_VEHICLETYPE,
            if (!TextUtils.isEmpty(citationBean.getVehType()))
                map.put("VehType", citationBean.getVehType());


            //   "MeterNo":CD_METERNO,
            if (!TextUtils.isEmpty(citationBean.getMeterno()))
                map.put("MeterNo", citationBean.getMeterno());
//  "ParcelID":CD_PARCEL,
            if (!TextUtils.isEmpty(citationBean.getParcelid()))
                map.put("ParcelID", citationBean.getParcelid());

            if (!TextUtils.isEmpty(citationBean.getStreetName()))
                map.put("Street", citationBean.getStreetName());

            if (!TextUtils.isEmpty(citationBean.getStreetZip()))
                map.put("zipCode", citationBean.getStreetZip());

            if (!TextUtils.isEmpty(citationBean.getStreetNo()))
                map.put("StreetNumber", citationBean.getStreetNo());
/*  "custfield4":CD_CAMPUS,
     "custfield5":CD_LOT*/
            if (!TextUtils.isEmpty(citationBean.getCust4()))
                map.put("custfield4", citationBean.getCust4());

            if (!TextUtils.isEmpty(citationBean.getCust5()))
                map.put("custfield5", citationBean.getCust5());


        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "request" + map);
        Single<ApiResponse<String>> call;
        if (citationBean.isUploaded()) {
            call = baseRepo.editCitation(map.toString(), citationBean);

        } else {
            call = baseRepo.addCitation(map.toString(), citationBean);
        }
        call.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ApiResponse<String>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrSubmit.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(ApiResponse<String> listApiResponse) {
                        obrSubmit.setValue(Resource.success(listApiResponse.getData(), "Done"));
                    }

                    @Override
                    public void onError(Throwable e) {
                        obrSubmit.setValue(Resource.error(map.toString(), networkErrorHandler.getErrMsg(e)));
                    }
                });
    }

    void syncList() {
        String orgNo = String.valueOf(sharedPref.getUser().CustKey);
        obrList.setValue(Resource.loading(null));
        compositeDisposable.add(Observable.fromCallable(() -> citationDao.loadAllCitation(false, orgNo))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(citationBean ->
                                obrList.setValue(Resource.success(citationBean, "done")),
                        e -> obrList.setValue(Resource.error(null, e.getMessage()))));

    }

    final SingleRequestEvent<VehType> obrVehType = new SingleRequestEvent<>();

    void getVehType() {
        baseRepo.vehType().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new SingleObserver<ApiResponse<VehType>>() {
            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
                obrVehType.setValue(Resource.loading(null));
            }

            @Override
            public void onSuccess(ApiResponse<VehType> listApiResponse) {
                obrVehType.setValue(Resource.success(listApiResponse.getData(), "Load from server"));
            }

            @Override
            public void onError(Throwable e) {
                obrVehType.setValue(Resource.error(null, networkErrorHandler.getErrMsg(e)));
            }
        });
    }


    void getPermissions(String userKey) {
        Map<String, String> map = new HashMap<>();
        map.put("userKey", userKey);
        baseRepo.getUserPermission(map).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new SingleObserver<ApiResponse<List<UserPermission>>>() {
            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
            }

            @Override
            public void onSuccess(ApiResponse<List<UserPermission>> listApiResponse) {

            }

            @Override
            public void onError(Throwable e) {
                error.setValue(networkErrorHandler.getErrMsg(e));
            }
        });

    }


    void logout() {
        compositeDisposable.add(Observable.fromCallable(() -> {
            transactionDao.clearVolation();
            return true;
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(aLong -> obrLogout.setValue(Resource.success(null, "done")), throwable -> obrLogout.setValue(Resource.success(null, "Error :" + throwable.getMessage()))));
    }

}
