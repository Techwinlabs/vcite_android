package com.techwin.vcite.ui.imagePicker;

import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.remote.helper.NetworkErrorHandler;
import com.techwin.vcite.di.base.viewmodel.BaseViewModel;
import com.techwin.vcite.util.location.LiveLocationDetecter;

import javax.inject.Inject;

public class CustomImagePickerActivityVm extends BaseViewModel {
    private final SharedPref sharedPref;
    public final LiveLocationDetecter liveLocationDetecter;
    private final NetworkErrorHandler networkErrorHandler;

    @Inject
    public CustomImagePickerActivityVm(SharedPref sharedPref, LiveLocationDetecter liveLocationDetecter, NetworkErrorHandler networkErrorHandler) {
        this.sharedPref = sharedPref;
        this.liveLocationDetecter = liveLocationDetecter;
        this.networkErrorHandler = networkErrorHandler;
    }

}
