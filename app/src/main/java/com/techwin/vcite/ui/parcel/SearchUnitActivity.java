package com.techwin.vcite.ui.parcel;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.techwin.vcite.BR;
import com.techwin.vcite.R;
import com.techwin.vcite.data.beans.CitationData;
import com.techwin.vcite.data.beans.SearchBean;
import com.techwin.vcite.data.local.entity.CitationBean;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.data.remote.helper.Status;
import com.techwin.vcite.databinding.ActivitySearchUnitBinding;
import com.techwin.vcite.databinding.HolderSearchItemBinding;
import com.techwin.vcite.di.base.adapter.SimpleRecyclerViewAdapter;
import com.techwin.vcite.di.base.view.AppActivity;
import com.techwin.vcite.ui.main.MainActivity;
import com.techwin.vcite.util.dump.InputUtils;
import com.techwin.vcite.util.event.SingleRequestEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.techwin.vcite.ui.parcel.SearchNameActivity.getAddressLine;

public class SearchUnitActivity extends AppActivity<ActivitySearchUnitBinding, SearchUnitActivityVM> {
    private CitationBean citationBean;
    private String unitNo = "";
    private SimpleRecyclerViewAdapter<String, HolderSearchItemBinding> adapter;
    private final ArrayList<String> list = new ArrayList<>();

    @Override
    protected BindingActivity<SearchUnitActivityVM> getBindingActivity() {
        return new BindingActivity<>(R.layout.activity_search_unit, SearchUnitActivityVM.class);
    }

    public static Intent newIntent(Context activity, CitationData citationData) {
        Intent intent = new Intent(activity, SearchUnitActivity.class);
        intent.putExtra("bean", citationData);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }


    @SuppressLint("NonConstantResourceId")
    @Override
    protected void subscribeToEvents(final SearchUnitActivityVM vm) {
        binding.toolbar.tvTitle.setText(R.string.prperty_lookup);
        adapter = new SimpleRecyclerViewAdapter<>(R.layout.holder_search_item, BR.bean, (v, s) -> {
            unitNo = s;
            binding.tvLine3.setText(getAddressLine(citationBean.getStreetNo(), citationBean.getStreetName(), unitNo));

        });
        binding.rvOne.setLayoutManager(new LinearLayoutManager(this));
        binding.rvOne.setAdapter(adapter);
        binding.etDns.setHint("Search Unit");
        vm.onClick.observe(this, view -> {
            switch (view != null ? view.getId() : 0) {
                case R.id.iv_back: {
                    finish();
                }
                break;
                case R.id.btn_submit: {
                    apiGetParcelId();
                }
                break;
            }
        });
        vm.obrData.observe(this, (SingleRequestEvent.RequestObserver<CitationBean>) resource -> {
            switch (resource.status) {
                case LOADING:
                    showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    if (resource.data != null) {
                        citationBean = resource.data;
                        unitNo = citationBean.getUnit();
                        binding.tvLine3.setText(getAddressLine(citationBean.getStreetNo(), citationBean.getStreetName(), unitNo));
                        Map<String, String> map = new HashMap<>();
                        map.put("street", citationBean.getStreetName());
                        map.put("streetNumber", citationBean.getStreetNo());
                        vm.search("getUnit", map);
                    }
                    dismissProgressDialog();
                    break;
                case ERROR:
                    dismissProgressDialog();
                    break;
            }
        });
        vm.obrSearch.observe(this, (SingleRequestEvent.RequestObserver<SearchBean>) resource -> {
            switch (resource.status) {
                case LOADING:
                    binding.pbOne.setVisibility(View.VISIBLE);
                    break;
                case SUCCESS:
                    binding.pbOne.setVisibility(View.GONE);
                    if (resource.data != null) {
                        if (resource.data.streetParcel != null) {
                            saveToLocal(unitNo, resource.data.streetParcel);
                        } else {
                            list.clear();
                            if (resource.data.streetNum != null)
                                list.addAll(resource.data.streetNum);
                            adapter.setList(list);
                        }
                    }

                    dismissProgressDialog();
                    break;
                case WARN:
                    binding.pbOne.setVisibility(View.GONE);
                    vm.warn.setValue(resource.message);
                    dismissProgressDialog();
                    break;
                case ERROR:
                    dismissProgressDialog();
                    binding.pbOne.setVisibility(View.GONE);
                    vm.error.setValue(resource.message);
                    break;
            }
        });
        vm.obrSave.observe(this, (SingleRequestEvent.RequestObserver<Void>) resource -> {
            switch (resource.status) {
                case SUCCESS:
                    viewModel.success.setValue(resource.message);
                    showMainUi();
                    break;
                case ERROR:
                    viewModel.error.setValue(resource.message);
                    break;
            }
        });
        vm.singleLiveEvent.observe(this, s -> {
            if (InputUtils.isEmpty(s)) {
                adapter.setList(list);
            } else {
                adapter.clearList();
                for (String s1 : list) {
                    if (s1 != null && s1.contains(s)) {
                        adapter.addData(s1);
                    }
                }
            }
        });
        vm.setConsumer(binding.etDns);
    }

    private void showMainUi() {
        CitationData citationData = getIntent().getParcelableExtra("bean");
        if (citationData != null) {
            Intent intent = MainActivity.newIntent(this);
            intent.putExtra("lookup", citationData);
            startNewActivity(intent);
        }
    }

    private void apiGetParcelId() {
        Map<String, String> map = new HashMap<>();
        map.put("street", citationBean.getStreetName());
        map.put("streetNumber", citationBean.getStreetNo());
        if (!TextUtils.isEmpty(unitNo))
            map.put("unit", unitNo);
        viewModel.search("getParcelID", map);
        showProgressDialog(R.string.plz_wait);
    }


    public void saveToLocal(String unitNo, String parcelId) {
        citationBean.setParcelid(InputUtils.getTrimString(parcelId));
        citationBean.setUnit(InputUtils.getTrimString(unitNo));
        viewModel.saveCitation(citationBean);
    }

    @Override
    protected void onStart() {
        super.onStart();
        CitationData citationData = getIntent().getParcelableExtra("bean");
        if (citationData != null) {
            viewModel.getCitation(citationData);
        }
    }
}

