package com.techwin.vcite.ui.imagePicker;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;
import android.hardware.Camera;
import android.media.ExifInterface;
import android.media.MediaActionSound;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Settings;
import android.view.SurfaceHolder;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;

import com.google.common.util.concurrent.ListenableFuture;
import com.techwin.vcite.R;
import com.techwin.vcite.databinding.ActivityCustomImagePickerBinding;
import com.techwin.vcite.di.base.view.AppActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CustomImagePickerActivity extends AppActivity<ActivityCustomImagePickerBinding, CustomImagePickerActivityVm> {

    ImageCapture imageCapture;
    File outputDirectory;
    ExecutorService cameraExecutor;
    ImageAnalysis imageAnalysis;

    @Override
    protected BindingActivity<CustomImagePickerActivityVm> getBindingActivity() {
        return new BindingActivity<>(R.layout.activity_custom_image_picker, CustomImagePickerActivityVm.class);
    }

    public static Intent newIntent(Context activity) {
        Intent intent = new Intent(activity, CustomImagePickerActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    protected void subscribeToEvents(CustomImagePickerActivityVm vm) {
        startCamera();
        outputDirectory = getOutputDirectory();
        cameraExecutor = Executors.newSingleThreadExecutor();
        binding.cameraCaptureButton.setOnClickListener(v -> capture());
        viewModel.onClick.observe(this, view -> {
            switch (view.getId()) {
                case R.id.iv_back:
                    finish(true);
                    break;
            }
        });
    }

    private File getOutputDirectory() {
        File mediaDir = new File(getCacheDir() + getString(R.string.app_name) + System.currentTimeMillis());
        if (!mediaDir.exists()) {
            mediaDir.mkdir();
        }
        return mediaDir;
    }

    private void startCamera() {
        ListenableFuture<ProcessCameraProvider> cameraProviderFuture = ProcessCameraProvider.getInstance(this);
        cameraProviderFuture.addListener(() -> {
            try {
                ProcessCameraProvider cameraProvider = cameraProviderFuture.get();
                Preview preview = new Preview.Builder().build();
                preview.setTargetRotation(getWindowManager().getDefaultDisplay().getRotation());
                preview.setSurfaceProvider(binding.viewFinder.getSurfaceProvider());
                imageCapture = new ImageCapture.Builder().build();
                imageCapture.setTargetRotation(getWindowManager().getDefaultDisplay().getRotation());
                imageAnalysis = new ImageAnalysis.Builder().build();
                CameraSelector cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA;
                cameraProvider.unbindAll();
                cameraProvider.bindToLifecycle(this, cameraSelector, preview, imageCapture, imageAnalysis);
            } catch (ExecutionException | InterruptedException e) {
                Toast.makeText(this, e.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        }, ContextCompat.getMainExecutor(this));

    }

    private void capture() {
        playSound();
        File photoFile = new File(outputDirectory, new SimpleDateFormat(
                "yyyy-MM-dd-HH-mm-ss-SSS", Locale.US
        ).format(System.currentTimeMillis()) + ".jpg");

        ImageCapture.OutputFileOptions outputOptions = new ImageCapture.OutputFileOptions.Builder(photoFile).build();
        imageCapture.takePicture(outputOptions, ContextCompat.getMainExecutor(this), new ImageCapture.OnImageSavedCallback() {
            @RequiresApi(api = Build.VERSION_CODES.Q)
            @Override
            public void onImageSaved(@NonNull ImageCapture.OutputFileResults outputFileResults) {
                Intent intent = new Intent();
                intent.putExtra("image_arr", photoFile.getAbsoluteFile());
                setResult(RESULT_OK, intent);
                finish();
            }

            @Override
            public void onError(@NonNull ImageCaptureException exception) {

            }
        });
    }

    private void playSound() {
        MediaActionSound sound = new MediaActionSound();
        sound.play(MediaActionSound.SHUTTER_CLICK);
    }

}
