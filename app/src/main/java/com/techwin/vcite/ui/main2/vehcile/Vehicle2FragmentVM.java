package com.techwin.vcite.ui.main2.vehcile;


import android.util.Log;

import androidx.databinding.ObservableField;

import com.techwin.vcite.data.beans.CampusBean;
import com.techwin.vcite.data.beans.CitationData;
import com.techwin.vcite.data.beans.PlateType;
import com.techwin.vcite.data.beans.UserBean;
import com.techwin.vcite.data.beans.VehType;
import com.techwin.vcite.data.beans.VehicleColorType;
import com.techwin.vcite.data.beans.VelTypeLnn;
import com.techwin.vcite.data.beans.base.ApiResponse;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.local.dao.CitationDao;
import com.techwin.vcite.data.local.dao.TransactionDao;
import com.techwin.vcite.data.local.entity.CitationBean;
import com.techwin.vcite.data.local.entity.MediaBean;
import com.techwin.vcite.data.remote.helper.NetworkErrorHandler;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.data.repo.BaseRepo;
import com.techwin.vcite.di.base.viewmodel.BaseViewModel;
import com.techwin.vcite.ui.selector.state.StateActivity;
import com.techwin.vcite.util.Constants;
import com.techwin.vcite.util.event.SingleLiveEvent;
import com.techwin.vcite.util.event.SingleRequestEvent;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class Vehicle2FragmentVM extends BaseViewModel {

    final SingleLiveEvent<CharSequence> obrOther = new SingleLiveEvent<>();
    final SingleRequestEvent<CitationBean> obrData = new SingleRequestEvent<>();
    public final ObservableField<String> fieldOther = new ObservableField<>();
    private final TransactionDao transactionDao;
    private final CitationDao citationDao;
    final SingleRequestEvent<Void> obrSave = new SingleRequestEvent<>();

    final SingleRequestEvent<List<StateActivity.DummyBean>> obrStateListData = new SingleRequestEvent<>();
    final SingleRequestEvent<List<VelTypeLnn.VehtAbbrBean>> obrVehicleMakeData = new SingleRequestEvent<>();
    final SingleRequestEvent<List<VehType.VehtTypDTO>> obrVehicleType = new SingleRequestEvent<>();
    final SingleRequestEvent<List<VehicleColorType.VehtTypBean>> obrVehicleColor = new SingleRequestEvent<>();
    final SingleRequestEvent<CampusBean> obrCampusList = new SingleRequestEvent<>();

    public final ObservableField<String> field_plateType = new ObservableField<>();
    public final ObservableField<String> field_platecolor = new ObservableField<>();
    public final ObservableField<String> field_vehtype = new ObservableField<>();
    public final ObservableField<String> field_vehmodel = new ObservableField<>();
    public final ObservableField<String> field_vehclerkid = new ObservableField<>();
    public final ObservableField<String> field_vehOfficerId = new ObservableField<>();
    public final ObservableField<String> field_vehDivisionId = new ObservableField<>();
    public final ObservableField<String> field_licence = new ObservableField<>();
    public final ObservableField<String> field_state = new ObservableField<>();
    public final ObservableField<Long> field_expdata = new ObservableField<>();
    public final ObservableField<String> field_vin = new ObservableField<>();
    public final ObservableField<String> field_color = new ObservableField<>();
    public final ObservableField<String> field_make = new ObservableField<>();
    public final ObservableField<String> field_cust4 = new ObservableField<>();
    public final ObservableField<String> field_cust5 = new ObservableField<>();

    //  final SingleRequestEvent<CampusBean> obrCampusType = new SingleRequestEvent<>();
    private final BaseRepo baseRepo;
    private final SharedPref sharedPref;
    private final NetworkErrorHandler networkErrorHandler;

    @Inject
    public Vehicle2FragmentVM(TransactionDao transactionDao, CitationDao citationDao, BaseRepo baseRepo, SharedPref sharedPref, NetworkErrorHandler networkErrorHandler) {

        this.transactionDao = transactionDao;
        this.citationDao = citationDao;
        this.baseRepo = baseRepo;
        this.sharedPref = sharedPref;
        this.networkErrorHandler = networkErrorHandler;
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
        obrOther.setValue(s);
    }


    void saveCitation(CitationBean citationBean) {
        compositeDisposable.add(Observable.fromCallable(() -> transactionDao.saveCitationTrans(citationBean)).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(aLong -> {
                    Log.e(TAG, "insert" + aLong);
                    obrSave.setValue(Resource.success(null, "Saved"));
                }, throwable -> {
                    obrSave.setValue(Resource.success(null, "Error :" + throwable.getMessage()));
                    Log.e(TAG, "error" + throwable.getMessage());
                }));
    }

    void getCitation(CitationData citationData) {
        obrData.setValue(Resource.loading(null));
        compositeDisposable.add(Observable.fromCallable(() -> citationDao.loadCitation(citationData.citationNo, citationData.orgNo, citationData.citationType))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(citationBean ->
                                obrData.setValue(Resource.success(citationBean, "done")),
                        e -> obrData.setValue(Resource.error(null, e.getMessage()))));


    }

    void saveMedia(MediaBean citationBean) {
        compositeDisposable.add(Observable.fromCallable(() -> transactionDao.saveMedia(citationBean)).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(longs -> obrSave.setValue(Resource.success(null, "Evidence image saved")), throwable -> {
                    obrSave.setValue(Resource.success(null, "Error :" + throwable.getMessage()));
                    Log.e(TAG, "error" + throwable.getMessage());
                }));
    }

    public void getStateList() {
        obrStateListData.setValue(Resource.success(StateActivity.getSatesList(), "from local"));
    }

    public void getVehicleMakeList() {
        List<VelTypeLnn.VehtAbbrBean> s = sharedPref.getVehMake();
        if (s != null) {
            obrVehicleMakeData.setValue(Resource.success(s, "from local"));
        } else {
            baseRepo.vehMake().subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<ApiResponse<VelTypeLnn>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                            obrVehicleMakeData.setValue(Resource.loading(null));
                        }

                        @Override
                        public void onSuccess(ApiResponse<VelTypeLnn> listApiResponse) {
                            if (listApiResponse.getData() != null)
                                obrVehicleMakeData.setValue(Resource.success(listApiResponse.getData().vehtAbbr, "Done"));
                        }

                        @Override
                        public void onError(Throwable e) {
                            obrVehicleMakeData.setValue(Resource.error(null, networkErrorHandler.getErrMsg(e)));
                        }
                    });
        }
    }

    public void getVehicleTypeList() {
        List<VehType.VehtTypDTO> vehtTypDTOList = sharedPref.getVehType();
        if (vehtTypDTOList != null && vehtTypDTOList.size() > 0)
            obrVehicleType.setValue(Resource.success(vehtTypDTOList, "Local"));
        else
            baseRepo.vehType().observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io()).subscribe(new SingleObserver<ApiResponse<VehType>>() {
                @Override
                public void onSubscribe(Disposable d) {
                    compositeDisposable.add(d);
                    obrVehicleType.setValue(Resource.loading(null));
                }

                @Override
                public void onSuccess(ApiResponse<VehType> listApiResponse) {
                    obrVehicleType.setValue(Resource.success(listApiResponse.getData().getVehtTyp(), "Load from server"));
                }

                @Override
                public void onError(Throwable e) {
                    obrVehicleType.setValue(Resource.error(vehtTypDTOList, networkErrorHandler.getErrMsg(e)));
                }
            });
    }

    public void getVehicleColor() {
        VehicleColorType s = sharedPref.getColorType();
        if (s != null && s.vehtTyp != null && s.vehtTyp.size() > 0)
            obrVehicleColor.setValue(Resource.success(s.vehtTyp, "Local"));
        else
            baseRepo.getVehicleColor().observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io()).subscribe(new SingleObserver<ApiResponse<VehicleColorType>>() {
                @Override
                public void onSubscribe(Disposable d) {
                    compositeDisposable.add(d);
                    obrVehicleColor.setValue(Resource.loading(null));
                }

                @Override
                public void onSuccess(ApiResponse<VehicleColorType> listApiResponse) {
                    obrVehicleColor.setValue(Resource.success(listApiResponse.getData().vehtTyp, "Load from server"));
                }

                @Override
                public void onError(Throwable e) {
                    if (s != null) {
                        obrVehicleColor.setValue(Resource.error(s.vehtTyp, networkErrorHandler.getErrMsg(e)));
                    }
                }
            });
    }

    public void getCampusList() {
        CampusBean s = sharedPref.getCampus();
        if (s != null) {
            obrCampusList.setValue(Resource.success(s, "from local"));
        }
    }

   /* void getCampusValues() {
        baseRepo.campusType().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new SingleObserver<ApiResponse<CampusBean>>() {
            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
                obrCampusType.setValue(Resource.loading(null));
            }

            @Override
            public void onSuccess(ApiResponse<CampusBean> listApiResponse) {
                if(listApiResponse.getData()!=null)
                obrCampusType.setValue(Resource.success(listApiResponse.getData(), "Load from server"));
              else   obrCampusType.setValue(Resource.error(null,"Error"));
            }

            @Override
            public void onError(Throwable e) {
                obrCampusType.setValue(Resource.error(null, networkErrorHandler.getErrMsg(e)));
            }
        });
    }*/

}
