package com.techwin.vcite.ui.selector.campus;


import android.widget.EditText;

import com.techwin.vcite.data.beans.CampusBean;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.remote.helper.NetworkErrorHandler;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.data.repo.BaseRepo;
import com.techwin.vcite.di.base.viewmodel.BaseViewModel;
import com.techwin.vcite.util.event.SingleLiveEvent;
import com.techwin.vcite.util.event.SingleRequestEvent;
import com.techwin.vcite.util.misc.RxSearchObservable;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;

public class CampusActivityVM extends BaseViewModel {
    private final SharedPref sharedPref;
    private final NetworkErrorHandler networkErrorHandler;
    final SingleRequestEvent<CampusBean> obrData = new SingleRequestEvent<>();
    private final BaseRepo baseRepo;
    final SingleLiveEvent<String> singleLiveEvent = new SingleLiveEvent<>();

    @Inject
    public CampusActivityVM(SharedPref sharedPref, NetworkErrorHandler networkErrorHandler, BaseRepo baseRepo) {
        this.sharedPref = sharedPref;
        this.networkErrorHandler = networkErrorHandler;
        this.baseRepo = baseRepo;
    }

    void setConsumer(EditText editText) {
        compositeDisposable.add(RxSearchObservable.from(editText).
                debounce(RxSearchObservable.DEFAULT_WAIT, TimeUnit.MILLISECONDS)
                .filter(new Predicate<String>() {
                    @Override
                    public boolean test(@NonNull String s) throws Exception {
                        return true;
                    }
                }).distinctUntilChanged()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String s) throws Exception {
                        singleLiveEvent.setValue(s);
                    }
                }));
    }

    void getList() {
        CampusBean s = sharedPref.getCampus();
        if (s != null) {
            obrData.setValue(Resource.success(s, "from local"));
        }
    }

}

