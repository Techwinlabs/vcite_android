package com.techwin.vcite.ui.scan.text;


import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.local.dao.TransactionDao;
import com.techwin.vcite.data.local.entity.MediaBean;
import com.techwin.vcite.data.remote.helper.NetworkErrorHandler;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.di.base.viewmodel.BaseViewModel;
import com.techwin.vcite.util.event.SingleRequestEvent;
import com.techwin.vcite.util.location.LiveLocationDetecter;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class TextScanActivityVM extends BaseViewModel {


    private final SharedPref sharedPref;
    public final LiveLocationDetecter liveLocationDetecter;
    private final NetworkErrorHandler networkErrorHandler;
    private final TransactionDao transactionDao;
    final SingleRequestEvent<String> obrSave = new SingleRequestEvent<>();

    @Inject
    public TextScanActivityVM(SharedPref sharedPref, LiveLocationDetecter liveLocationDetecter, NetworkErrorHandler networkErrorHandler, TransactionDao transactionDao) {

        this.sharedPref = sharedPref;
        this.liveLocationDetecter = liveLocationDetecter;
        this.networkErrorHandler = networkErrorHandler;
        this.transactionDao = transactionDao;
    }

    void saveMediaBeans(String text, List<MediaBean> citationBean) {
        compositeDisposable.add(Observable.fromCallable(new Callable<long[]>() {
            @Override
            public long[] call() throws Exception {
                return transactionDao.saveImagesTrans(citationBean);
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<long[]>() {
                    @Override
                    public void accept(long[] longs) throws Exception {
                        obrSave.setValue(Resource.success(text, "Image saved"));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        obrSave.setValue(Resource.error(text, "Error :" + throwable.getMessage()));
                    }
                }));
    }

}

