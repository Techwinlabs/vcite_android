package com.techwin.vcite.ui.location;


import com.techwin.vcite.data.remote.helper.NetworkErrorHandler;
import com.techwin.vcite.di.base.viewmodel.BaseViewModel;
import com.techwin.vcite.util.RxBus;

import javax.inject.Inject;

public class LocationPickerActivityVM extends BaseViewModel {
    private final NetworkErrorHandler networkErrorHandler;
    private final RxBus rxBus;

    @Inject
    public LocationPickerActivityVM(NetworkErrorHandler networkErrorHandler, RxBus rxBus) {
        this.networkErrorHandler = networkErrorHandler;
        this.rxBus = rxBus;

    }
}
