package com.techwin.vcite.ui.photo;


import android.util.Log;

import com.techwin.vcite.data.beans.CitationData;
import com.techwin.vcite.data.local.dao.CitationDao;
import com.techwin.vcite.data.local.dao.MediaBeanDao;
import com.techwin.vcite.data.local.dao.TransactionDao;
import com.techwin.vcite.data.local.entity.CitationBean;
import com.techwin.vcite.data.local.entity.MediaBean;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.di.base.viewmodel.BaseViewModel;
import com.techwin.vcite.util.event.SingleRequestEvent;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;


public class PhotoFragmentVM extends BaseViewModel {
    final SingleRequestEvent<CitationBean> obrCitationData = new SingleRequestEvent<>();
    private final CitationDao citationDao;
    final SingleRequestEvent<List<MediaBean>> obrData = new SingleRequestEvent<>();
    final SingleRequestEvent<Void> obrSave = new SingleRequestEvent<>();
    private final TransactionDao transactionDao;
    private final MediaBeanDao mediaBeanDao;

    @Inject
    public PhotoFragmentVM(CitationDao citationDao, TransactionDao transactionDao, MediaBeanDao mediaBeanDao) {
        this.citationDao = citationDao;
        this.transactionDao = transactionDao;
        this.mediaBeanDao = mediaBeanDao;
    }

    void saveMediaBeans(List<MediaBean> citationBean) {
        compositeDisposable.add(Observable.fromCallable(new Callable<long[]>() {
            @Override
            public long[] call() throws Exception {
                return transactionDao.saveImagesTrans(citationBean);
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<long[]>() {
                    @Override
                    public void accept(long[] longs) throws Exception {
                        obrSave.setValue(Resource.success(null, "Saved"));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        obrSave.setValue(Resource.success(null, "Error :" + throwable.getMessage()));
                        Log.e(TAG, "error" + throwable.getMessage());
                    }
                }));
    }

    void deleteMediaBeans(MediaBean citationBean, CitationData citationData) {
        compositeDisposable.add(Observable.fromCallable(() -> mediaBeanDao.delete(citationBean)).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(integer -> {
                    if (integer > 0)
                        getMediaBeans(citationData);
                }, throwable -> {
                    error.setValue("Error :" + throwable.getMessage());
                    Log.e(TAG, "error" + throwable.getMessage());
                }));
    }

    void getMediaBeans(CitationData citationData) {
        obrData.setValue(Resource.loading(null));
        compositeDisposable.add(Observable.fromCallable(() -> mediaBeanDao.getList(citationData.citationNo, citationData.orgNo))
                .subscribeOn(Schedulers.io())
                .delay(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(citationBean ->
                                obrData.setValue(Resource.success(citationBean, "done")),
                        e -> obrData.setValue(Resource.error(null, e.getMessage()))));


    }

    public void getCitation(CitationData citationData) {
        obrCitationData.setValue(Resource.loading(null));
        compositeDisposable.add(Observable.fromCallable(() -> citationDao.loadCitation(citationData.citationNo, citationData.orgNo, citationData.citationType))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(citationBean ->
                                obrCitationData.setValue(Resource.success(citationBean, "done")),
                        e -> obrCitationData.setValue(Resource.error(null, e.getMessage()))));


    }
}
