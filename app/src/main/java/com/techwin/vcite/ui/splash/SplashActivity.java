package com.techwin.vcite.ui.splash;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;

import com.techwin.vcite.R;
import com.techwin.vcite.data.beans.CitTempBean;
import com.techwin.vcite.data.beans.TemplateBean;
import com.techwin.vcite.data.beans.UserBean;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.databinding.ActivitySplashBinding;
import com.techwin.vcite.di.base.view.AppActivity;
import com.techwin.vcite.ui.login.LoginActivity;
import com.techwin.vcite.ui.main.MainActivity;
import com.techwin.vcite.util.event.SingleRequestEvent;

import java.util.List;

import javax.inject.Inject;

public class SplashActivity extends AppActivity<ActivitySplashBinding, SplashActivityVM> {

    @Inject
    SharedPref sharedPref;
    private Handler handler = new Handler();

    @Override
    protected BindingActivity<SplashActivityVM> getBindingActivity() {
        return new BindingActivity<>(R.layout.activity_splash, SplashActivityVM.class);
    }

    public static Intent newIntent(Activity activity) {
        Intent intent = new Intent(activity, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }


    @Override
    protected void subscribeToEvents(final SplashActivityVM vm) {
        vm.onClick.observe(this, new Observer<View>() {
            @Override
            public void onChanged(@Nullable View view) {
                switch (view != null ? view.getId() : 0) {

                }
            }
        });
        vm.obrData.observe(this, new SingleRequestEvent.RequestObserver<Boolean>() {
            @Override
            public void onRequestReceived(@NonNull Resource<Boolean> resource) {
                switch (resource.status) {
                    case LOADING:
                        binding.pbOne.setVisibility(View.VISIBLE);
                        break;
                    case SUCCESS:{
                        binding.pbOne.setVisibility(View.GONE);
                        Intent intent = MainActivity.newIntent(SplashActivity.this);
                        startNewActivity(intent, true);}
                        break;
                    case ERROR:{
                        binding.pbOne.setVisibility(View.GONE);
                        vm.error.setValue(resource.message);
                        Intent intent = MainActivity.newIntent(SplashActivity.this);
                        startNewActivity(intent, true);}
                        break;
                }
            }
        });
        // throw new RuntimeException("Test Crash");
    }

    private void printdata(List<CitTempBean> citTempList) {
        StringBuilder stringBuilder = new StringBuilder();
        for (CitTempBean bean : citTempList) {
            stringBuilder.append("String fieldID_" + bean.getFieldID() + "=\"" + bean.getFieldID() + "\";");
            stringBuilder.append("\n");
        }
        Log.e("fields", "size" + citTempList.size() + " \n" + stringBuilder.toString());
        // String fieldID_VehLicense = "VehLicense";
    }

    private void loginView() {
        binding.pbOne.setVisibility(View.VISIBLE);
        handler.postDelayed(runnable, 1000);
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            Intent intent = LoginActivity.newIntent(SplashActivity.this);
            startNewActivity(intent, true);
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        UserBean userBean = sharedPref.getUser();
        if (userBean != null) {
            viewModel.getTemplate(userBean.CustKey);
        } else {
            loginView();
        }
    }

    @Override
    protected void onStop() {
        binding.pbOne.setVisibility(View.GONE);
        handler.removeCallbacks(runnable);
        super.onStop();
    }
}

