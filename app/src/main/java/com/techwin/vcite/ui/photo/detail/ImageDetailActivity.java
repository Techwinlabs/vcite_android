package com.techwin.vcite.ui.photo.detail;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Environment;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.viewpager2.widget.ViewPager2;

import com.arvind.android.permissions.PermissionHandler;
import com.arvind.android.permissions.Permissions;
import com.techwin.vcite.BR;
import com.techwin.vcite.R;
import com.techwin.vcite.data.beans.CitationData;
import com.techwin.vcite.data.local.entity.MediaBean;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.databinding.ActivityImageDetailBinding;
import com.techwin.vcite.databinding.HolderPhotoDetailBinding;
import com.techwin.vcite.di.base.adapter.SimpleRecyclerViewAdapter;
import com.techwin.vcite.di.base.view.AppActivity;
import com.techwin.vcite.util.event.SingleRequestEvent;
import com.techwin.vcite.util.transformation.Pager2_DeptTransformer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ImageDetailActivity extends AppActivity<ActivityImageDetailBinding, ImageDetailActivityVM> {

    private SimpleRecyclerViewAdapter<MediaBean, HolderPhotoDetailBinding> adapter;
    private CitationData citationData;

    @Override
    protected BindingActivity<ImageDetailActivityVM> getBindingActivity() {
        return new BindingActivity<>(R.layout.activity_image_detail, ImageDetailActivityVM.class);
    }

    public static Intent newIntent(Context activity, int pos, CitationData citationData) {
        Intent intent = new Intent(activity, ImageDetailActivity.class);
        intent.putExtra("bean", citationData);
        intent.putExtra("pos", pos);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }


    @Override
    protected void subscribeToEvents(final ImageDetailActivityVM vm) {
        citationData = getIntent().getParcelableExtra("bean");
        if (citationData == null)
            finish(true);
        binding.toolbar.tvTitle.setText(String.format("Citations #%s", citationData.getCitationNo()));
        adapter = new SimpleRecyclerViewAdapter<>(R.layout.holder_photo_detail, BR.bean, new SimpleRecyclerViewAdapter.SimpleCallback<MediaBean>() {
            @Override
            public void onItemClick(View v, MediaBean s) {
                if (v.getId() == R.id.iv_download) {
                    downLoadImage(s.getImage());
                }
            }
        });

        binding.rvOne.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                binding.tvPage.setText(String.format(Locale.US, "%d/%d", position + 1, adapter.getItemCount()));
            }
        });
        binding.rvOne.setPageTransformer(new Pager2_DeptTransformer());
        binding.rvOne.setAdapter(adapter);
        vm.onClick.observe(this, new Observer<View>() {
            @Override
            public void onChanged(@Nullable View view) {
                switch (view != null ? view.getId() : 0) {
                    case R.id.iv_back: {
                        finish();
                    }
                    break;

                }
            }
        });
        vm.obrData.observe(this, new SingleRequestEvent.RequestObserver<List<MediaBean>>() {
            @Override
            public void onRequestReceived(@NonNull Resource<List<MediaBean>> resource) {
                switch (resource.status) {
                    case LOADING:
                        showProgressDialog(R.string.plz_wait);
                        break;
                    case SUCCESS:
                        dismissProgressDialog();
                        adapter.setList(resource.data);
                        int pos = getIntent().getIntExtra("pos", 0);
                        if (pos < adapter.getItemCount()) {
                            binding.rvOne.setCurrentItem(pos, false);
                        }
                        break;
                    case ERROR:
                        dismissProgressDialog();
                        break;
                }
            }
        });
        vm.obrBitmap.observe(this, new Observer<Resource<Bitmap>>() {
            @Override
            public void onChanged(Resource<Bitmap> bitmapResource) {
                switch (bitmapResource.status) {
                    case LOADING:
                        showProgressDialog(R.string.plz_wait);
                        break;
                    case SUCCESS:
                        getFilePath(bitmapResource.data);
                        break;
                    case ERROR:
                        dismissProgressDialog();
                        viewModel.error.setValue(bitmapResource.message);
                        break;
                }
            }
        });
    }

    private void downLoadImage(String image) {
        Permissions.check(this, Manifest.permission.READ_EXTERNAL_STORAGE, 0, new PermissionHandler() {
            @Override
            public void onGranted() {
                viewModel.getBitmap(image);
            }

            @Override
            public void onDenied(Context context, ArrayList<String> deniedPermissions) {
                super.onDenied(context, deniedPermissions);
                viewModel.info.setValue("Give Permission");
            }
        });

    }

    public void getFilePath(Bitmap bitmap) {
        String ts = "Cit" + citationData.getCitationNo() + new SimpleDateFormat("HHmmssSSS", Locale.US).format(new Date()) + ".png";
        File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), ts);
        try {
            FileOutputStream fos = new FileOutputStream(dir.getAbsolutePath());
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.flush();
            fos.close();
            viewModel.success.setValue("File saved: " + dir.getAbsolutePath());
            dismissProgressDialog();
        } catch (FileNotFoundException e) {
            dismissProgressDialog();
            viewModel.error.setValue("File not found: " + e.getMessage());
        } catch (IOException e) {
            dismissProgressDialog();
            viewModel.error.setValue("Error accessing file: " + e.getMessage());
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        viewModel.getMediaBeans(citationData);
    }
}

