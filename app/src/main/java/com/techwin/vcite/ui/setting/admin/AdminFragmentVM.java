package com.techwin.vcite.ui.setting.admin;


import com.techwin.vcite.data.beans.MenuBean;
import com.techwin.vcite.data.beans.PlateType;
import com.techwin.vcite.data.beans.UserBean;
import com.techwin.vcite.data.beans.VehicleColorType;
import com.techwin.vcite.data.beans.VelTypeLnn;
import com.techwin.vcite.data.beans.base.ApiResponse;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.remote.helper.NetworkErrorHandler;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.data.repo.BaseRepo;
import com.techwin.vcite.di.base.viewmodel.BaseViewModel;
import com.techwin.vcite.util.Constants;
import com.techwin.vcite.util.event.SingleActionEvent;
import com.techwin.vcite.util.event.SingleLiveEvent;
import com.techwin.vcite.util.event.SingleRequestEvent;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;


public class AdminFragmentVM extends BaseViewModel {

    final SingleLiveEvent<MenuBean> menu_bean = new SingleActionEvent<>();
    final SingleRequestEvent<VehicleColorType> obrColor = new SingleRequestEvent<>();
    final SingleRequestEvent<String> obrSubmit = new SingleRequestEvent<>();
    private final BaseRepo baseRepo;
    private final SharedPref sharedPref;
    private final NetworkErrorHandler networkErrorHandler;

    @Inject
    public AdminFragmentVM(BaseRepo baseRepo, SharedPref sharedPref, NetworkErrorHandler networkErrorHandler) {
        this.baseRepo = baseRepo;
        this.sharedPref = sharedPref;
        this.networkErrorHandler = networkErrorHandler;
    }


    void apiVehicleColor() {
        VehicleColorType s = sharedPref.getColorType();
        if (s != null) {
            obrColor.setValue(Resource.success(s, "from local"));
        } else {
            baseRepo.getVehicleColor().subscribe(new SingleObserver<ApiResponse<VehicleColorType>>() {
                @Override
                public void onSubscribe(Disposable d) {
                    compositeDisposable.add(d);
                    obrColor.setValue(Resource.loading(null));
                }

                @Override
                public void onSuccess(ApiResponse<VehicleColorType> listApiResponse) {
                    obrColor.setValue(Resource.success(listApiResponse.getData(), "Done"));
                }

                @Override
                public void onError(Throwable e) {
                    obrColor.setValue(Resource.error(null, networkErrorHandler.getErrMsg(e)));
                }
            });
        }
    }
}
