package com.techwin.vcite.ui.login;


import com.techwin.vcite.data.beans.UserBean;
import com.techwin.vcite.data.beans.base.ApiResponse;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.local.dao.TransactionDao;
import com.techwin.vcite.data.remote.helper.NetworkErrorHandler;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.data.repo.BaseRepo;
import com.techwin.vcite.di.base.viewmodel.BaseViewModel;
import com.techwin.vcite.util.event.SingleRequestEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class LoginActivityVM extends BaseViewModel {
    private final SharedPref sharedPref;
    private final NetworkErrorHandler networkErrorHandler;
    private final BaseRepo webRepo;
    final SingleRequestEvent<UserBean> obrLogin = new SingleRequestEvent<>();
    private final TransactionDao transactionDao;

    @Inject
    public LoginActivityVM(SharedPref sharedPref, NetworkErrorHandler networkErrorHandler, BaseRepo webRepo, TransactionDao transactionDao) {
        this.sharedPref = sharedPref;
        this.networkErrorHandler = networkErrorHandler;
        this.webRepo = webRepo;
        this.transactionDao = transactionDao;
    }

    void login(String host, String id, String password) {
        Map<String, String> map = new HashMap<>();
        map.put("dns", host);
        map.put("userID", id.toLowerCase());
        map.put("pwd", password);
        webRepo.login(map).subscribe(new SingleObserver<ApiResponse<UserBean>>() {
            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
                obrLogin.setValue(Resource.loading(null));
            }

            @Override
            public void onSuccess(ApiResponse<UserBean> listApiResponse) {
                if (listApiResponse.getData() != null)
                    obrLogin.setValue(Resource.success(listApiResponse.getData(), "Login Successful"));
                else obrLogin.setValue(Resource.warn(listApiResponse.getData(), "Error"));
            }

            @Override
            public void onError(Throwable e) {
                obrLogin.setValue(Resource.error(null, "User Id or Password are wrong"));
            }
        });
    }

}
