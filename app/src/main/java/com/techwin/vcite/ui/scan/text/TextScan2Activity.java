package com.techwin.vcite.ui.scan.text;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bumptech.glide.Glide;
import com.github.drjacky.imagepicker.ImagePicker;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.text.Text;
import com.google.mlkit.vision.text.TextRecognition;
import com.google.mlkit.vision.text.TextRecognizer;
import com.google.mlkit.vision.text.TextRecognizerOptions;
import com.techwin.vcite.BR;
import com.techwin.vcite.R;
import com.techwin.vcite.data.beans.CitationData;
import com.techwin.vcite.data.local.entity.MediaBean;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.data.remote.helper.Status;
import com.techwin.vcite.databinding.ActivityTextScan2Binding;
import com.techwin.vcite.databinding.HolderVinNoBinding;
import com.techwin.vcite.di.base.adapter.SimpleRecyclerViewAdapter;
import com.techwin.vcite.di.base.view.AppActivity;
import com.techwin.vcite.ui.imagePicker.CustomImagePickerActivity;
import com.techwin.vcite.util.AppUtils;
import com.techwin.vcite.util.decoration.ListVerticalItemDecoration;
import com.techwin.vcite.util.event.SingleRequestEvent;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TextScan2Activity extends AppActivity<ActivityTextScan2Binding, TextScanActivityVM> {


    private static final String TAG = "TextScan2Activity";
    private static final int requestPermissionID = 567;
    private SimpleRecyclerViewAdapter<TextScanActivity.Row, HolderVinNoBinding> adapter;
    private CitationData citationData;

    @Override
    protected BindingActivity<TextScanActivityVM> getBindingActivity() {
        return new BindingActivity<>(R.layout.activity_text_scan2, TextScanActivityVM.class);
    }

    public static Intent newIntent(Context activity, CitationData citationData) {
        Intent intent = new Intent(activity, TextScan2Activity.class);
        intent.putExtra("data", citationData);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }


    @SuppressLint("NonConstantResourceId")
    @Override
    protected void subscribeToEvents(final TextScanActivityVM vm) {
        binding.toolbar.tvTitle.setText("Vin Scanner");
        citationData = getIntent().getParcelableExtra("data");
        if (citationData == null)
            finish();
        vm.onClick.observe(this, view -> {
            switch (view != null ? view.getId() : 0) {
                case R.id.imageView:
                    if (imageFile != null) {
                        try {
                            InputImage image = InputImage.fromFilePath(TextScan2Activity.this, Uri.fromFile(imageFile));
                            recognizeText(image);
                        } catch (IOException e) {
                            e.printStackTrace();
                            viewModel.error.setValue(e.getMessage());
                        }
                    }
                    break;
                case R.id.btnTakePicture:
                    takePicture();
                    break;
                case R.id.iv_back:
                    finish(true);
                    break;

            }
        });
        vm.obrSave.observe(this, (SingleRequestEvent.RequestObserver<String>) resource -> {
            switch (resource.status) {
                case SUCCESS: {
                    viewModel.success.setValue(resource.message);
                    sendIntent(resource.data);
                }
                break;
                case ERROR:
                    viewModel.error.setValue(resource.message);
                    break;
            }
        });
        adapter = new SimpleRecyclerViewAdapter<>(R.layout.holder_vin_no, BR.bean);
        adapter.setCallback((v, resultsBean) -> {
            if (binding.cbOne.isChecked()) {
                saveImage(resultsBean.text);
            } else
                sendIntent(resultsBean.text);
        });
        binding.rvOne.addItemDecoration(new ListVerticalItemDecoration(this, R.dimen.dp_5));
        binding.rvOne.setLayoutManager(new LinearLayoutManager(this));
        binding.rvOne.setAdapter(adapter);
        takePicture();
    }

    private void sendIntent(String text) {
        Intent intent = new Intent();
        intent.putExtra("data", text);
        setResult(Activity.RESULT_OK, intent);
        finish(true);
    }

    private void onText(Text task) {
        List<Text.TextBlock> textBlocks = task.getTextBlocks();
        for (Text.TextBlock t : textBlocks) {
            String s = t.getText().trim();
            s = s.replaceAll("\\s+", "");
            TextScanActivity.Row row = new TextScanActivity.Row(s);
            adapter.add(0, row);
            binding.rvOne.scrollToPosition(0);

        }
    }

    private void recognizeText(InputImage image) {
        showProgressDialog(getString(R.string.plz_wait));
        adapter.clearList();
        TextRecognizer recognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS);
        Task<Text> result = recognizer.process(image)
                .addOnSuccessListener(visionText -> {
                    dismissProgressDialog();
                    onText(visionText);
                })
                .addOnFailureListener(
                        e -> {
                            dismissProgressDialog();
                            e.printStackTrace();
                        });

    }

    private void saveImage(String text) {
        if (citationData != null && imageFile != null) {
            String path = AppUtils.imagetoBase64(imageFile.getAbsolutePath());
            MediaBean mediaBean = new MediaBean();
            mediaBean.setCitationNo(citationData.citationNo);
            mediaBean.setOrgNo(citationData.orgNo);
            mediaBean.setImage(path);
            List<MediaBean> bean = new ArrayList<>();
            bean.add(mediaBean);
            viewModel.saveMediaBeans(text, bean);
        }
    }

    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ImagePicker.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                imageFile = ImagePicker.Companion.getFile(data);
                try {
                    InputImage image = InputImage.fromFilePath(TextScan2Activity.this, Uri.fromFile(imageFile));
                    recognizeText(image);
                } catch (IOException e) {
                    e.printStackTrace();
                    viewModel.error.setValue(e.getMessage());
                }
            } else if (resultCode == ImagePicker.RESULT_ERROR) {
                viewModel.error.setValue(ImagePicker.Companion.getError(data));
            }
        }
    }*/

    public void takePicture() {
        Intent intent = CustomImagePickerActivity.newIntent(this);
        launcher.launch(intent);
//        Intent intent = ImagePicker.Companion.with(this)
//                .crop()
//                .cropFreeStyle()
//                .cameraOnly()//Crop image(Optional), Check Customization for more option
//                .maxResultSize(512, 512, true)    //Final image resolution will be less than 1080 x 1080(Optional)
//                .createIntent();
//        launcher.launch(intent);
    }

    @Nullable
    private File imageFile;
    private final ActivityResultLauncher<Intent> launcher =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), (ActivityResult result) -> {
                if (result.getResultCode() == RESULT_OK && result.getData() != null) {
                    try {
                        imageFile = new File(result.getData().getSerializableExtra("image_arr").toString());
                        try {
                            InputImage image = InputImage.fromFilePath(TextScan2Activity.this, Uri.fromFile(imageFile));
                            recognizeText(image);
                        } catch (IOException e) {
                            e.printStackTrace();
                            viewModel.error.setValue(e.getMessage());
                        }
                    } catch (Exception e) {

                    }
                }
                else {
                    finish();
                }
//                if (result.getResultCode() == RESULT_OK) {
//                    if (result.getData() != null) {
//                        imageFile = ImagePicker.Companion.getFile(result.getData());
//                        try {
//                            InputImage image = InputImage.fromFilePath(TextScan2Activity.this, Uri.fromFile(imageFile));
//                            recognizeText(image);
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                            viewModel.error.setValue(e.getMessage());
//                        }
//                    }
//                } else if (result.getResultCode() == ImagePicker.RESULT_ERROR) {
//                    viewModel.error.setValue(ImagePicker.Companion.getError(result.getData()));
//                } else if (result.getResultCode() == RESULT_CANCELED) {
//                    finish();
//                }
            });


    @Override
    protected void onResume() {
        super.onResume();
        if (imageFile != null) {
            Glide.with(this).load(imageFile).centerCrop().into(binding.imageView);
        }
    }


}

