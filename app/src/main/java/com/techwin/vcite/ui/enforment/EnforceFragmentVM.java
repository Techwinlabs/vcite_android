package com.techwin.vcite.ui.enforment;


import android.util.Log;

import androidx.databinding.ObservableField;

import com.techwin.vcite.data.beans.CitationData;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.local.dao.CitationDao;
import com.techwin.vcite.data.local.dao.TransactionDao;
import com.techwin.vcite.data.local.entity.CitationBean;
import com.techwin.vcite.data.local.entity.MediaBean;
import com.techwin.vcite.data.remote.helper.NetworkErrorHandler;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.data.repo.BaseRepo;
import com.techwin.vcite.di.base.viewmodel.BaseViewModel;
import com.techwin.vcite.util.event.SingleRequestEvent;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;


public class EnforceFragmentVM extends BaseViewModel {

    final SingleRequestEvent<CitationBean> obrData = new SingleRequestEvent<>();
    private final TransactionDao transactionDao;
    private final CitationDao citationDao;
    final SingleRequestEvent<Void> obrSave = new SingleRequestEvent<>();


    public final ObservableField<String> field_streetno = new ObservableField<>();
    public final ObservableField<String> field_zipcode = new ObservableField<>();
    public final ObservableField<String> field_streetname = new ObservableField<>();
    public final ObservableField<String> field_estib = new ObservableField<>();
    public final ObservableField<String> field_unit = new ObservableField<>();
    public final ObservableField<String> field_remark = new ObservableField<>();
    public final ObservableField<String> field_parcel = new ObservableField<>();
    private final BaseRepo baseRepo;
    private final SharedPref sharedPref;
    private final NetworkErrorHandler networkErrorHandler;

    @Inject
    public EnforceFragmentVM(TransactionDao transactionDao, CitationDao citationDao, BaseRepo baseRepo, SharedPref sharedPref, NetworkErrorHandler networkErrorHandler) {

        this.transactionDao = transactionDao;
        this.citationDao = citationDao;
        this.baseRepo = baseRepo;
        this.sharedPref = sharedPref;
        this.networkErrorHandler = networkErrorHandler;
    }


    void saveCitation(CitationBean citationBean) {
        compositeDisposable.add(Observable.fromCallable(() -> transactionDao.saveCitationTrans(citationBean)).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(aLong -> {
                    Log.e(TAG, "insert" + aLong);
                    obrSave.setValue(Resource.success(null, "Saved"));
                }, throwable -> {
                    obrSave.setValue(Resource.success(null, "Error :" + throwable.getMessage()));
                    Log.e(TAG, "error" + throwable.getMessage());
                }));
    }

    void getCitation(CitationData citationData) {
        obrData.setValue(Resource.loading(null));
        compositeDisposable.add(Observable.fromCallable(() -> citationDao.loadCitation(citationData.citationNo, citationData.orgNo, citationData.citationType))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(citationBean ->
                                obrData.setValue(Resource.success(citationBean, "done")),
                        e -> obrData.setValue(Resource.error(null, e.getMessage()))));


    }

    void saveMediaBeans(List<MediaBean> citationBean) {
        compositeDisposable.add(Observable.fromCallable(new Callable<long[]>() {
            @Override
            public long[] call() throws Exception {
                return transactionDao.saveImagesTrans(citationBean);
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<long[]>() {
                    @Override
                    public void accept(long[] longs) throws Exception {
                        obrSave.setValue(Resource.success(null, "Evidence image saved"));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        obrSave.setValue(Resource.success(null, "Error :" + throwable.getMessage()));
                        Log.e(TAG, "error" + throwable.getMessage());
                    }
                }));
    }


}
