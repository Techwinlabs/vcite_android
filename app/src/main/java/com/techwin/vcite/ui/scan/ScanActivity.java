package com.techwin.vcite.ui.scan;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;

import com.arvind.android.permissions.PermissionHandler;
import com.arvind.android.permissions.Permissions;
import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.DecodeCallback;
import com.budiyev.android.codescanner.ErrorCallback;
import com.google.zxing.Result;
import com.techwin.vcite.R;
import com.techwin.vcite.data.beans.CitationData;
import com.techwin.vcite.data.beans.UserBean;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.databinding.ActivityScanBinding;
import com.techwin.vcite.di.base.view.AppActivity;
import com.techwin.vcite.util.dump.InputUtils;
import com.techwin.vcite.util.event.SingleRequestEvent;

import java.util.Date;

import javax.inject.Inject;

public class ScanActivity extends AppActivity<ActivityScanBinding, ScanActivityVM> {

    @Inject
    SharedPref sharedPref;
    @Nullable
    private UserBean userBean;

    @Override
    protected BindingActivity<ScanActivityVM> getBindingActivity() {
        return new BindingActivity<>(R.layout.activity_scan, ScanActivityVM.class);
    }

    public static Intent newIntent(Activity activity) {
        Intent intent = new Intent(activity, ScanActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }


    @Override
    protected void subscribeToEvents(final ScanActivityVM vm) {
        userBean = sharedPref.getUser();
        binding.toolbar.tvTitle.setText("QR Scan");
        vm.onClick.observe(this, new Observer<View>() {
            @Override
            public void onChanged(@Nullable View view) {
                switch (view != null ? view.getId() : 0) {
                    case R.id.iv_scan:
                        Permissions.check(ScanActivity.this, Manifest.permission.CAMERA, null, new PermissionHandler() {
                            @Override
                            public void onGranted() {
                                binding.scannerView.setVisibility(View.VISIBLE);
                                initScanner();
                            }
                        });
                        break;
                    case R.id.ll_continue:
                        CitationData citationData = binding.getScan();
                        if(citationData!=null)
                        viewModel.saveTicket(citationData);
                        break;
                    case R.id.iv_back:
                        finish(true);
                        break;

                }
            }
        });
        vm.obrData.observe(this, new SingleRequestEvent.RequestObserver<CitationData>() {
            @Override
            public void onRequestReceived(@NonNull Resource<CitationData> resource) {
                switch (resource.status) {
                    case LOADING:
                        break;
                    case SUCCESS:
                    case ERROR:
                        playSuccessBeep();
                        binding.setScan(resource.data);
                        binding.scannerView.setVisibility(View.GONE);
                }
            }
        });
        vm.obrSave.observe(this, new SingleRequestEvent.RequestObserver<CitationData>() {
            @Override
            public void onRequestReceived(@NonNull Resource<CitationData> resource) {
                if (resource.data != null) {
                    Intent intent = new Intent();
                    intent.putExtra("data", resource.data);
                    setResult(RESULT_OK, intent);
                    finish(true);
                }
            }
        });
    }


    @Nullable
    private CodeScanner mCodeScanner;

    private void initScanner() {
        binding.setScan(null);
        if (mCodeScanner == null) {
            mCodeScanner = new CodeScanner(this, binding.scannerView);
            mCodeScanner.setDecodeCallback(new DecodeCallback() {
                @Override
                public void onDecoded(@NonNull final Result result) {

                    ScanActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (binding.getScan() == null) {
                                Log.e("Result", result.getText());
                                CitationData citationData = getData(result.getText());
                                if (citationData != null) {
                                    viewModel.getCitation(citationData);
                                } else {
                                    playErrorBeep();
                                    mCodeScanner.startPreview();
                                }

                            }
                        }
                    });
                }
            });
            mCodeScanner.setErrorCallback(new ErrorCallback() {
                @Override
                public void onError(@NonNull Exception error) {
                    ScanActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            playErrorBeep();
                            viewModel.error.setValue("Scanner Error :" + error.getMessage());
                        }
                    });
                }
            });
        }
        mCodeScanner.startPreview();
    }

    private void playSuccessBeep() {
        try {
            ToneGenerator toneGen1 = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);
            toneGen1.startTone(ToneGenerator.TONE_PROP_BEEP, 350);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void playErrorBeep() {
        try {
            ToneGenerator toneGen1 = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);
            toneGen1.startTone(ToneGenerator.TONE_PROP_BEEP2, 350);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (mCodeScanner != null) {
            mCodeScanner.startPreview();
        }
    }

    @Override
    protected void onPause() {
        if (mCodeScanner != null) {
            mCodeScanner.releaseResources();
        }
        super.onPause();
    }

    @Nullable
    private CitationData getData(String result) {
        Log.e("Scanner", "Result=" + result);
      /*  try {
            URL url = new URL(result);
            Map<String, String> query_pairs = new LinkedHashMap<String, String>();
            String query = url.getQuery();
            String[] pairs = query.split("&");
            for (String pair : pairs) {
                int idx = pair.indexOf("=");
                query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
            }

            CitationData citationData = new CitationData();
            if (query_pairs.containsKey("cid")) {
                citationData.orgNo = query_pairs.get("cid");
            } else {
                return null;
            }

            if (query_pairs.containsKey("sn")) {
                citationData.citationNo = query_pairs.get("sn");
            } else {
                return null;
            }
            return citationData;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }*/
        /*cid:38^ct:Code enforcement^sn:P5500002*/
        if (userBean != null) {
            try {
                CitationData citationData = new CitationData();
                String[] array1 = result.split("\\^");
                if (array1.length == 3) {
                    String[] a0 = array1[0].split(":");
                    citationData.setOrgNo(a0[1]);
                    if (userBean.CustKey != Integer.parseInt(a0[1])) {
                        viewModel.error.setValue("Wrong QR code(Check login customer key)");
                        return null;
                    }


                    String[] a1 = array1[1].split(":");
                    citationData.setCitationType(a1[1]);
                    String[] a2 = array1[2].split(":");
                    citationData.setCitationNo(a2[1]);
                    return citationData;
                }
            } catch (Exception e) {
                viewModel.error.setValue(e.getMessage());
                return null;
            }
            try {
                CitationData citationData = new CitationData();
                String[] array1 = result.split(",");
                if (array1.length == 2) {
                    citationData.setOrgNo(String.valueOf(userBean.CustKey));
                    citationData.setCitationNo(array1[1]);
                    return citationData;
                }
                return null;
            } catch (Exception e) {
                viewModel.error.setValue(e.getMessage());
                return null;
            }
        } else return null;
    }
}

