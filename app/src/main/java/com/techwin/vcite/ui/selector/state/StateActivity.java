package com.techwin.vcite.ui.selector.state;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.techwin.vcite.BR;
import com.techwin.vcite.R;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.data.remote.helper.Status;
import com.techwin.vcite.databinding.ActivityPlateColorBinding;
import com.techwin.vcite.databinding.ActivityStateBinding;
import com.techwin.vcite.databinding.HolderPlateColorSelectorBinding;
import com.techwin.vcite.databinding.HolderStateSelectorBinding;
import com.techwin.vcite.di.base.adapter.SimpleRecyclerViewAdapter;
import com.techwin.vcite.di.base.view.AppActivity;
import com.techwin.vcite.di.base.view.BaseCustomDialog;
import com.techwin.vcite.ui.selector.color.PlateColorActivityVM;
import com.techwin.vcite.util.AppUtils;
import com.techwin.vcite.util.decoration.ListVerticalItemDecoration;
import com.techwin.vcite.util.dump.InputUtils;
import com.techwin.vcite.util.event.SingleRequestEvent;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class StateActivity extends AppActivity<ActivityStateBinding, StateActivityVM> {
    @Nullable
    private String selected;
    private  SimpleRecyclerViewAdapter<DummyBean, HolderStateSelectorBinding> adapter;
    private final List<DummyBean> dataList = new ArrayList<>();

    @Override
    protected BindingActivity<StateActivityVM> getBindingActivity() {
        return new BindingActivity<>(R.layout.activity_state, StateActivityVM.class);
    }

    public static Intent newIntent(Context activity, @Nullable String selected) {
        Intent intent = new Intent(activity, StateActivity.class);
        intent.putExtra("selected", selected);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    @Override
    protected void subscribeToEvents(final StateActivityVM vm) {
        selected = getIntent().getStringExtra("selected");
        binding.toolbar.tvTitle.setText("State of issuance");
        adapter = new SimpleRecyclerViewAdapter<>(R.layout.holder_state_selector, BR.bean);
        adapter.setCallback((v, bean) -> {
            selected = bean.abbreviation;
            for (int i = 0; i < adapter.getItemCount(); i++) {
                adapter.getList().get(i).checked = adapter.getList().get(i).abbreviation.equalsIgnoreCase(bean.abbreviation);
            }
            adapter.notifyDataSetChanged();
        });
        binding.rvOne.setLayoutManager(new LinearLayoutManager(this));
        binding.rvOne.addItemDecoration(new ListVerticalItemDecoration(this, R.dimen.dp_5));
        binding.rvOne.setAdapter(adapter);

        vm.onClick.observe(this, view -> {
            int i = view != null ? view.getId() : 0;
            if (i == R.id.tv_save) {
                if (adapter.getItemCount() == 0) {
                    selected = binding.etSearch.getText().toString();
                }
                Intent intent = new Intent();
                intent.putExtra("data", selected);
                setResult(Activity.RESULT_OK, intent);
                finish(true);
                animateCloseSheet();
            } else if (i == R.id.iv_back) {
                finish(true);
                animateCloseSheet();
            }
        });

        vm.obrData.observe(this, (SingleRequestEvent.RequestObserver<List<DummyBean>>) resource -> {
            switch (resource.status) {
                case LOADING:
                    showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    if (resource.data != null) {
                        dataList.addAll(resource.data);
                    }
                    updateAdapter();
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    break;
            }
        });
        vm.singleLiveEvent.observe(this, s -> {
            if (InputUtils.isEmpty(s)) {
                adapter.setList(dataList);
            } else {
                adapter.clearList();
                for (DummyBean s1 : dataList) {
                    if (AppUtils.compareText(s1.name, s)) {
                        adapter.addData(s1);
                    }
                }
            }
        });
        vm.setConsumer(binding.etSearch);
        vm.getList();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        animateCloseSheet();
    }

    private void updateAdapter() {
        if (selected != null) {
            for (int i = 0; i < dataList.size(); i++) {
                if (dataList.get(i).abbreviation.equals(selected)) {
                    dataList.get(i).checked = true;
                    break;
                }
            }

        }
        adapter.setList(dataList);
    }




    public static List<DummyBean> getSatesList() {
        String list = "[\n" +
                "{\n" +
                "\"name\": \"Massachusetts\",\n" +
                "        \"abbreviation\": \"MA\"\n" +
                "    },\n" +
                "{\n" +
                "\"name\": \"Rhode Island\",\n" +
                "        \"abbreviation\": \"RI\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Alabama\",\n" +
                "        \"abbreviation\": \"AL\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Alaska\",\n" +
                "        \"abbreviation\": \"AK\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"American Samoa\",\n" +
                "        \"abbreviation\": \"AS\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Arizona\",\n" +
                "        \"abbreviation\": \"AZ\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Arkansas\",\n" +
                "        \"abbreviation\": \"AR\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"California\",\n" +
                "        \"abbreviation\": \"CA\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Colorado\",\n" +
                "        \"abbreviation\": \"CO\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Connecticut\",\n" +
                "        \"abbreviation\": \"CT\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Delaware\",\n" +
                "        \"abbreviation\": \"DE\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"District Of Columbia\",\n" +
                "        \"abbreviation\": \"DC\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Federated States Of Micronesia\",\n" +
                "        \"abbreviation\": \"FM\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Florida\",\n" +
                "        \"abbreviation\": \"FL\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Georgia\",\n" +
                "        \"abbreviation\": \"GA\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Guam\",\n" +
                "        \"abbreviation\": \"GU\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Hawaii\",\n" +
                "        \"abbreviation\": \"HI\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Idaho\",\n" +
                "        \"abbreviation\": \"ID\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Illinois\",\n" +
                "        \"abbreviation\": \"IL\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Indiana\",\n" +
                "        \"abbreviation\": \"IN\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Iowa\",\n" +
                "        \"abbreviation\": \"IA\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Kansas\",\n" +
                "        \"abbreviation\": \"KS\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Kentucky\",\n" +
                "        \"abbreviation\": \"KY\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Louisiana\",\n" +
                "        \"abbreviation\": \"LA\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Maine\",\n" +
                "        \"abbreviation\": \"ME\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Marshall Islands\",\n" +
                "        \"abbreviation\": \"MH\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Maryland\",\n" +
                "        \"abbreviation\": \"MD\"\n" +
                "    },\n" +

                "    {\n" +
                "        \"name\": \"Michigan\",\n" +
                "        \"abbreviation\": \"MI\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Minnesota\",\n" +
                "        \"abbreviation\": \"MN\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Mississippi\",\n" +
                "        \"abbreviation\": \"MS\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Missouri\",\n" +
                "        \"abbreviation\": \"MO\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Montana\",\n" +
                "        \"abbreviation\": \"MT\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Nebraska\",\n" +
                "        \"abbreviation\": \"NE\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Nevada\",\n" +
                "        \"abbreviation\": \"NV\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"New Hampshire\",\n" +
                "        \"abbreviation\": \"NH\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"New Jersey\",\n" +
                "        \"abbreviation\": \"NJ\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"New Mexico\",\n" +
                "        \"abbreviation\": \"NM\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"New York\",\n" +
                "        \"abbreviation\": \"NY\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"North Carolina\",\n" +
                "        \"abbreviation\": \"NC\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"North Dakota\",\n" +
                "        \"abbreviation\": \"ND\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Northern Mariana Islands\",\n" +
                "        \"abbreviation\": \"MP\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Ohio\",\n" +
                "        \"abbreviation\": \"OH\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Oklahoma\",\n" +
                "        \"abbreviation\": \"OK\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Oregon\",\n" +
                "        \"abbreviation\": \"OR\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Palau\",\n" +
                "        \"abbreviation\": \"PW\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Pennsylvania\",\n" +
                "        \"abbreviation\": \"PA\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Puerto Rico\",\n" +
                "        \"abbreviation\": \"PR\"\n" +
                "    },\n" +

                "    {\n" +
                "        \"name\": \"South Carolina\",\n" +
                "        \"abbreviation\": \"SC\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"South Dakota\",\n" +
                "        \"abbreviation\": \"SD\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Tennessee\",\n" +
                "        \"abbreviation\": \"TN\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Texas\",\n" +
                "        \"abbreviation\": \"TX\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Utah\",\n" +
                "        \"abbreviation\": \"UT\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Vermont\",\n" +
                "        \"abbreviation\": \"VT\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Virgin Islands\",\n" +
                "        \"abbreviation\": \"VI\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Virginia\",\n" +
                "        \"abbreviation\": \"VA\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Washington\",\n" +
                "        \"abbreviation\": \"WA\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"West Virginia\",\n" +
                "        \"abbreviation\": \"WV\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Wisconsin\",\n" +
                "        \"abbreviation\": \"WI\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"name\": \"Wyoming\",\n" +
                "        \"abbreviation\": \"WY\"\n" +
                "    }\n" +
                "]";

        Type type = new TypeToken<ArrayList<DummyBean>>() {
        }.getType();
        return new Gson().fromJson(list, type);
    }


    public static class DummyBean {

        /**
         * name : Wyoming
         * abbreviation : WY
         */

        @SerializedName("name")
        public String name;
        @SerializedName("abbreviation")
        public String abbreviation;
        public boolean checked;

        public DummyBean(String name) {
            this.name = name;
        }

        public DummyBean(String name, String abbreviation, boolean checked) {
            this.name = name;
            this.abbreviation = abbreviation;
            this.checked = checked;
        }
    }
}

