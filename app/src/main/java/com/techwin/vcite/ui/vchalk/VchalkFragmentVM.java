package com.techwin.vcite.ui.vchalk;



import androidx.databinding.ObservableField;

import com.techwin.vcite.di.base.viewmodel.BaseViewModel;
import com.techwin.vcite.util.event.SingleLiveEvent;

import javax.inject.Inject;


public class VchalkFragmentVM extends BaseViewModel {

    final SingleLiveEvent<CharSequence> obrOther = new SingleLiveEvent<>();

    public final ObservableField<String> fieldOther = new ObservableField<>();

    @Inject
    public VchalkFragmentVM() {

    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
        obrOther.setValue(s);
    }
}
