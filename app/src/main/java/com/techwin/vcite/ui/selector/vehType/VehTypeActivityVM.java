package com.techwin.vcite.ui.selector.vehType;


import android.widget.EditText;

import com.techwin.vcite.data.beans.VehType;
import com.techwin.vcite.data.beans.base.ApiResponse;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.remote.helper.NetworkErrorHandler;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.data.repo.BaseRepo;
import com.techwin.vcite.di.base.viewmodel.BaseViewModel;
import com.techwin.vcite.util.event.SingleLiveEvent;
import com.techwin.vcite.util.event.SingleRequestEvent;
import com.techwin.vcite.util.misc.RxSearchObservable;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;

public class VehTypeActivityVM extends BaseViewModel {
    private final SharedPref sharedPref;
    private final NetworkErrorHandler networkErrorHandler;
    final SingleRequestEvent<List<VehType.VehtTypDTO>> obrData = new SingleRequestEvent<>();
    private final BaseRepo baseRepo;
    final SingleLiveEvent<String> singleLiveEvent = new SingleLiveEvent<>();

    @Inject
    public VehTypeActivityVM(SharedPref sharedPref, NetworkErrorHandler networkErrorHandler, BaseRepo baseRepo) {
        this.sharedPref = sharedPref;
        this.networkErrorHandler = networkErrorHandler;
        this.baseRepo = baseRepo;
    }

    void setConsumer(EditText editText) {
        compositeDisposable.add(RxSearchObservable.from(editText).
                debounce(RxSearchObservable.DEFAULT_WAIT, TimeUnit.MILLISECONDS)
                .filter(new Predicate<String>() {
                    @Override
                    public boolean test(@NonNull String s) throws Exception {
                        return true;
                    }
                }).distinctUntilChanged()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String s) throws Exception {
                        singleLiveEvent.setValue(s);
                    }
                }));
    }

    void getList() {
        List<VehType.VehtTypDTO> vehtTypDTOList = sharedPref.getVehType();
        if (vehtTypDTOList != null && vehtTypDTOList.size() > 0)
            obrData.setValue(Resource.success(vehtTypDTOList, "Local"));
        else
            baseRepo.vehType().observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io()).subscribe(new SingleObserver<ApiResponse<VehType>>() {
                @Override
                public void onSubscribe(Disposable d) {
                    compositeDisposable.add(d);
                    obrData.setValue(Resource.loading(null));
                }

                @Override
                public void onSuccess(ApiResponse<VehType> listApiResponse) {
                    obrData.setValue(Resource.success(listApiResponse.getData().getVehtTyp(), "Load from server"));
                }

                @Override
                public void onError(Throwable e) {
                    obrData.setValue(Resource.error(vehtTypDTOList, networkErrorHandler.getErrMsg(e)));
                }
            });
    }


}

