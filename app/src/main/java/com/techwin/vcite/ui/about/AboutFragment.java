package com.techwin.vcite.ui.about;

import android.content.Context;

import androidx.annotation.Nullable;

import com.techwin.vcite.BuildConfig;
import com.techwin.vcite.R;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.databinding.FragmentAboutBinding;
import com.techwin.vcite.di.base.view.AppFragment;
import com.techwin.vcite.ui.main.MainActivity;

import javax.inject.Inject;

public class AboutFragment extends AppFragment<FragmentAboutBinding, AboutFragmentVM> {
    public static final String TAG = "AboutFragment";
    @Nullable
    private MainActivity mainActivity;
    @Inject
    SharedPref sharedPref;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mainActivity = (MainActivity) context;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected BindingFragment<AboutFragmentVM> getBindingFragment() {
        return new BindingFragment<>(R.layout.fragment_about, AboutFragmentVM.class);
    }

    public static AboutFragment newInstance() {
        return new AboutFragment();
    }

    @Override
    protected void subscribeToEvents(final AboutFragmentVM vm) {

        binding.tvVersion.setText("v"+BuildConfig.VERSION_NAME + "( " + BuildConfig.VERSION_CODE + " )");
    }


}
