package com.techwin.vcite.ui.parcel;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.techwin.vcite.BR;
import com.techwin.vcite.R;
import com.techwin.vcite.data.beans.CitationData;
import com.techwin.vcite.data.beans.SearchBean;
import com.techwin.vcite.data.local.entity.CitationBean;
import com.techwin.vcite.databinding.ActivitySearchStreetnameBinding;
import com.techwin.vcite.databinding.HolderSearchItemBinding;
import com.techwin.vcite.di.base.adapter.SimpleRecyclerViewAdapter;
import com.techwin.vcite.di.base.view.AppActivity;
import com.techwin.vcite.util.dump.InputUtils;
import com.techwin.vcite.util.event.SingleRequestEvent;

import java.util.HashMap;
import java.util.Map;

public class SearchNameActivity extends AppActivity<ActivitySearchStreetnameBinding, SearchNameActivityVM> {

    private CitationBean citationBean;
    private SimpleRecyclerViewAdapter<String, HolderSearchItemBinding> adapter;
    private String streetName;

    @Override
    protected BindingActivity<SearchNameActivityVM> getBindingActivity() {
        return new BindingActivity<>(R.layout.activity_search_streetname, SearchNameActivityVM.class);
    }

    public static Intent newIntent(Context activity, CitationData citationData) {
        Intent intent = new Intent(activity, SearchNameActivity.class);
        intent.putExtra("bean", citationData);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }


    @SuppressLint("NonConstantResourceId")
    @Override
    protected void subscribeToEvents(final SearchNameActivityVM vm) {
        binding.toolbar.tvTitle.setText(R.string.prperty_lookup);
        adapter = new SimpleRecyclerViewAdapter<>(R.layout.holder_search_item, BR.bean, (v, s) -> {
            streetName = s;
            binding.tvLine3.setText(getAddressLine(citationBean.getStreetNo(), streetName, citationBean.getUnit()));
        });

        binding.rvOne.setLayoutManager(new LinearLayoutManager(this));
        binding.rvOne.setAdapter(adapter);
        binding.etDns.setHint("Search Street Name");
        vm.onClick.observe(this, view -> {
            switch (view != null ? view.getId() : 0) {
                case R.id.iv_back: {
                    finish();
                }
                break;
                case R.id.btn_submit: {
                    if (InputUtils.isEmpty(streetName))
                        vm.info.setValue("Select Street Name");
                    else
                        saveToLocal();
                }
                break;
            }
        });
        vm.obrData.observe(this, (SingleRequestEvent.RequestObserver<CitationBean>) resource -> {
            switch (resource.status) {
                case LOADING:
                    showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    if (resource.data != null) {
                        citationBean = resource.data;
                        streetName = citationBean.getStreetName();
                        binding.tvLine3.setText(getAddressLine(citationBean.getStreetNo(), streetName, citationBean.getUnit()));
                        binding.etDns.setText("");

                    }
                    dismissProgressDialog();
                    break;
                case ERROR:
                    dismissProgressDialog();
                    break;
            }
        });
        vm.obrSearch.observe(this, (SingleRequestEvent.RequestObserver<SearchBean>) resource -> {
            switch (resource.status) {
                case LOADING:
                    binding.pbOne.setVisibility(View.VISIBLE);
                    break;
                case SUCCESS:
                    binding.pbOne.setVisibility(View.GONE);
                    if (resource.data != null) {
                        adapter.setList(resource.data.streetNum);
                    }
                    dismissProgressDialog();
                    break;
                case WARN:
                    binding.pbOne.setVisibility(View.GONE);
                    vm.warn.setValue(resource.message);
                    dismissProgressDialog();
                    break;
                case ERROR:
                    dismissProgressDialog();
                    binding.pbOne.setVisibility(View.GONE);
                    vm.error.setValue(resource.message);
                    break;
            }
        });
        vm.obrSave.observe(this, (SingleRequestEvent.RequestObserver<Void>) resource -> {
            switch (resource.status) {
                case SUCCESS:
                    viewModel.success.setValue(resource.message);
                    showStreetNoUi();
                    break;
                case ERROR:
                    viewModel.error.setValue(resource.message);
                    break;
            }
        });
        vm.singleLiveEvent.observe(this, s -> {
            Map<String, String> map = new HashMap<>();
            map.put("street", s);
            vm.search("searchStreet", map);
        });
        vm.setConsumer(binding.etDns);
    }

    private void showStreetNoUi() {
        CitationData citationData = getIntent().getParcelableExtra("bean");
        if (citationData != null) {
            Intent intent = SearchNoActivity.newIntent(this, citationData);
            startNewActivity(intent);
        }
    }


    public static String getAddressLine(String no, String name, String unit) {
        StringBuilder stringBuilder = new StringBuilder();
        if (!InputUtils.isEmpty(no)) {
            stringBuilder.append(no);
            stringBuilder.append(" ");
        }
        if (!InputUtils.isEmpty(name)) {
            stringBuilder.append(name);
            stringBuilder.append(" ");
        }
        if (!InputUtils.isEmpty(unit)) {
            stringBuilder.append(unit);
            stringBuilder.append(" ");
        }
        return stringBuilder.toString();
    }

    public void saveToLocal() {
        if (!citationBean.getStreetName().equals(InputUtils.getTrimString(streetName))) {
            citationBean.setStreetNo("");
            citationBean.setUnit("");
            citationBean.setParcelid("");

        }
        citationBean.setStreetName(InputUtils.getTrimString(streetName));
        viewModel.saveCitation(citationBean);
    }

    @Override
    protected void onStart() {
        super.onStart();
        CitationData citationData = getIntent().getParcelableExtra("bean");
        if (citationData != null) {
            viewModel.getCitation(citationData);

        }
    }
}

