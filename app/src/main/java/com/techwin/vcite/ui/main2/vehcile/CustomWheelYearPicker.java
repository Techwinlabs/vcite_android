package com.techwin.vcite.ui.main2.vehcile;

import android.content.Context;
import android.util.AttributeSet;

import com.github.florent37.singledateandtimepicker.widget.WheelYearPicker;

public class CustomWheelYearPicker extends WheelYearPicker {
    public CustomWheelYearPicker(Context context) {
        super(context);
    }

    public CustomWheelYearPicker(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setShowOnlyFutureDate(boolean showOnlyFutureDate) {
        super.setShowOnlyFutureDate(showOnlyFutureDate);
        updateAdapter();
    }

    @Override
    public void setMinYear(int minYear) {
        super.setMinYear(minYear);
        updateAdapter();
    }

    @Override
    public void setMaxYear(int maxYear) {
        super.setMaxYear(maxYear);
        updateAdapter();
    }

}
