package com.techwin.vcite.ui.multiImagePicker;

import static com.techwin.vcite.util.databinding.ImageViewBindingUtils.loadImagePath;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaActionSound;
import android.media.SoundPool;
import android.os.Build;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.core.content.ContextCompat;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.common.util.concurrent.ListenableFuture;
import com.techwin.vcite.BR;
import com.techwin.vcite.R;
import com.techwin.vcite.databinding.ActivityCustomImagePickerBinding;
import com.techwin.vcite.databinding.ActivityMultipleImagePickerBinding;
import com.techwin.vcite.databinding.DialogFullScreenImageBinding;
import com.techwin.vcite.databinding.HolderImageCaptureBinding;
import com.techwin.vcite.di.base.adapter.SimpleRecyclerViewAdapter;
import com.techwin.vcite.di.base.view.AppActivity;
import com.techwin.vcite.di.base.view.BaseCustomDialog;
import com.techwin.vcite.ui.imagePicker.CustomImagePickerActivityVm;
import com.techwin.vcite.widget.toast.MessageUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MultipleImagePickerActivity extends AppActivity<ActivityMultipleImagePickerBinding, MultipleImagePickerActivityVm> {

    ImageCapture imageCapture;
    File outputDirectory;
    ExecutorService cameraExecutor;
    ImageAnalysis imageAnalysis;
    private SimpleRecyclerViewAdapter<MultipleImageBean, HolderImageCaptureBinding> adapterImages;
    private BaseCustomDialog<DialogFullScreenImageBinding> dialogFullScreen;

    @Override
    protected BindingActivity<MultipleImagePickerActivityVm> getBindingActivity() {
        return new BindingActivity<>(R.layout.activity_multiple_image_picker, MultipleImagePickerActivityVm.class);
    }

    public static Intent newIntent(Context activity) {
        Intent intent = new Intent(activity, MultipleImagePickerActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    @Override
    protected void subscribeToEvents(MultipleImagePickerActivityVm vm) {
        startCamera();
        outputDirectory = getOutputDirectory();
        cameraExecutor = Executors.newSingleThreadExecutor();
        binding.cameraCaptureButton.setOnClickListener(v -> capture());
        initAdapter();
        initOnClick();
    }

    @SuppressLint("NonConstantResourceId")
    private void initOnClick() {
        viewModel.onClick.observe(this, v -> {
            switch (v.getId()) {
                case R.id.tv_title:
                    ArrayList<String> list = new ArrayList<>();
                    for (int i = 0; i < adapterImages.getItemCount(); i++) {
                        list.add(adapterImages.getList().get(i).image + "");
                    }
                    Intent intent = new Intent();
                    intent.putStringArrayListExtra("IMAGES_DATA", list);
                    setResult(RESULT_OK, intent);
                    finish(true);
                    break;
                case R.id.iv_back:
                    finish(true);
                    break;
            }
        });
    }

    @SuppressLint("NonConstantResourceId")
    private void initAdapter() {
        adapterImages = new SimpleRecyclerViewAdapter<>(R.layout.holder_image_capture, BR.bean, new SimpleRecyclerViewAdapter.SimpleCallback<MultipleImageBean>() {
            @Override
            public void onItemClick(View v, MultipleImageBean multipleImageBean) {
                switch (v.getId()) {
                    case R.id.iv_eye:
                        showFullScreenDialog(multipleImageBean.image + "");
                        break;
                }
            }

            @Override
            public void onItemClick(View view, int pos) {
                switch (view.getId()) {
                    case R.id.iv_delete:
                        adapterImages.removeItem(pos);
                        adapterImages.notifyItemChanged(pos);
                        if (adapterImages.getItemCount() == 0)
                            binding.rvImages.setVisibility(View.GONE);
                        break;

                }
            }
        });
        binding.rvImages.setAdapter(adapterImages);
    }

    @SuppressLint("NonConstantResourceId")
    private void showFullScreenDialog(String path) {
        dialogFullScreen = new BaseCustomDialog<>(this, R.layout.dialog_full_screen_image, R.style.Dialog_Window, view -> {
            switch (view.getId()) {
                case R.id.iv_back:
                    dialogFullScreen.dismiss();
                    break;
            }
        });
        loadImagePath(dialogFullScreen.getBinding().ivImage, path);
        dialogFullScreen.setCancelable(true);
        dialogFullScreen.setCanceledOnTouchOutside(true);
        dialogFullScreen.show();
    }

    private File getOutputDirectory() {
        File mediaDir = new File(getCacheDir() + getString(R.string.app_name) + System.currentTimeMillis());
        if (!mediaDir.exists()) {
            mediaDir.mkdir();
        }
        return mediaDir;
    }

    private void startCamera() {
        ListenableFuture<ProcessCameraProvider> cameraProviderFuture = ProcessCameraProvider.getInstance(this);
        cameraProviderFuture.addListener(() -> {
            try {
                ProcessCameraProvider cameraProvider = cameraProviderFuture.get();
                Preview preview = new Preview.Builder().build();
                preview.setTargetRotation(getWindowManager().getDefaultDisplay().getRotation());
                preview.setSurfaceProvider(binding.viewFinder.getSurfaceProvider());
                imageCapture = new ImageCapture.Builder().build();
                imageCapture.setTargetRotation(getWindowManager().getDefaultDisplay().getRotation());
                imageAnalysis = new ImageAnalysis.Builder().build();
                CameraSelector cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA;
                cameraProvider.unbindAll();
                cameraProvider.bindToLifecycle(this, cameraSelector, preview, imageCapture, imageAnalysis);
            } catch (ExecutionException | InterruptedException e) {
                Toast.makeText(this, e.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        }, ContextCompat.getMainExecutor(this));

    }

    private void capture() {
        playSound();
        File photoFile = new File(outputDirectory, new SimpleDateFormat(
                "yyyy-MM-dd-HH-mm-ss-SSS", Locale.US
        ).format(System.currentTimeMillis()) + ".jpg");

        ImageCapture.OutputFileOptions outputOptions = new ImageCapture.OutputFileOptions.Builder(photoFile).build();
        imageCapture.takePicture(outputOptions, ContextCompat.getMainExecutor(this), new ImageCapture.OnImageSavedCallback() {
            @SuppressLint("NotifyDataSetChanged")
            @RequiresApi(api = Build.VERSION_CODES.Q)
            @Override
            public void onImageSaved(@NonNull ImageCapture.OutputFileResults outputFileResults) {
                ArrayList<MultipleImageBean> list = new ArrayList<>();
                if (adapterImages.getItemCount() == 0) {
                    binding.rvImages.setVisibility(View.VISIBLE);
                    list.add(new MultipleImageBean(photoFile.getAbsolutePath()));
                    adapterImages.setList(list);
                } else {
                    adapterImages.addData(new MultipleImageBean(photoFile.getAbsolutePath()));
                    adapterImages.notifyDataSetChanged();
                }
            }

            @Override
            public void onError(@NonNull ImageCaptureException exception) {

            }
        });
    }

    private void playSound() {
        MediaActionSound sound = new MediaActionSound();
        sound.play(MediaActionSound.SHUTTER_CLICK);
    }

}
