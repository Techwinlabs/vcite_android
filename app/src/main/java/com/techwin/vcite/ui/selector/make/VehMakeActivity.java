package com.techwin.vcite.ui.selector.make;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.techwin.vcite.R;
import com.techwin.vcite.data.beans.VelTypeLnn;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.data.remote.helper.Status;
import com.techwin.vcite.databinding.ActivityVehMakeBinding;
import com.techwin.vcite.databinding.HolderVehmakeSelectorBinding;
import com.techwin.vcite.di.base.adapter.SimpleRecyclerViewAdapter;
import com.techwin.vcite.di.base.view.AppActivity;
import com.techwin.vcite.util.AppUtils;
import com.techwin.vcite.util.decoration.ListVerticalItemDecoration;
import com.techwin.vcite.util.dump.InputUtils;
import com.techwin.vcite.util.event.SingleRequestEvent;

import java.util.ArrayList;
import java.util.List;

public class VehMakeActivity extends AppActivity<ActivityVehMakeBinding, VehMakeActivityVM> {
    @Nullable
    private String selected;
    private SimpleRecyclerViewAdapter<VelTypeLnn.VehtAbbrBean, HolderVehmakeSelectorBinding> adapter;
    private final List<VelTypeLnn.VehtAbbrBean> dataList = new ArrayList<>();

    @Override
    protected BindingActivity<VehMakeActivityVM> getBindingActivity() {
        return new BindingActivity<>(R.layout.activity_veh_make, VehMakeActivityVM.class);
    }

    public static Intent newIntent(Context activity, @Nullable String selected) {
        Intent intent = new Intent(activity, VehMakeActivity.class);
        intent.putExtra("selected", selected);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        animateCloseSheet();
    }

    @Override
    protected void subscribeToEvents(final VehMakeActivityVM vm) {
        selected = getIntent().getStringExtra("selected");
        binding.toolbar.tvTitle.setText("Vehicle Make");
        adapter = new SimpleRecyclerViewAdapter<>(R.layout.holder_vehmake_selector, com.techwin.vcite.BR.bean);
        adapter.setCallback((v, bean) -> {
            selected = bean.abbreviation;
            for (int i = 0; i < adapter.getItemCount(); i++) {
                adapter.getList().get(i).checked = adapter.getList().get(i).abbreviation.equalsIgnoreCase(selected);
            }
            adapter.notifyDataSetChanged();
        });
        binding.rvOne.setLayoutManager(new LinearLayoutManager(this));
        binding.rvOne.addItemDecoration(new ListVerticalItemDecoration(this, R.dimen.dp_5));
        binding.rvOne.setAdapter(adapter);

        vm.onClick.observe(this, view -> {
            int i = view != null ? view.getId() : 0;
            if (i == R.id.tv_save) {
                if (adapter.getItemCount() == 0) {
                    selected = binding.etSearch.getText().toString();
                }
                Intent intent = new Intent();
                intent.putExtra("data", selected);
                setResult(Activity.RESULT_OK, intent);
                finish(true);
                animateCloseSheet();
            } else if (i == R.id.iv_back) {
                finish(true);
                animateCloseSheet();
            }
        });

        vm.obrData.observe(this, (SingleRequestEvent.RequestObserver<List<VelTypeLnn.VehtAbbrBean>>) resource -> {
            switch (resource.status) {
                case LOADING:
                    showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    if (resource.data != null) {
                        dataList.addAll(resource.data);
                    }
                    updateAdapter();
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    break;
            }
        });
        vm.singleLiveEvent.observe(this, s -> {
            if (InputUtils.isEmpty(s)) {
                adapter.setList(dataList);
            } else {
                adapter.clearList();
                for (VelTypeLnn.VehtAbbrBean s1 : dataList) {
                    if (AppUtils.compareText(s1.abbreviation,s)) {
                        adapter.addData(s1);
                    }
                }
            }
        });
        vm.setConsumer(binding.etSearch);
        vm.getList();
    }

    private void updateAdapter() {
        if (selected != null) {
            for (int i = 0; i < dataList.size(); i++) {
                if (dataList.get(i).abbreviation.equals(selected)) {
                    dataList.get(i).checked = true;
                    break;
                }
            }

        }
        adapter.setList(dataList);
    }
}

