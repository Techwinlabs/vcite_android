package com.techwin.vcite.ui.violation.type;


import com.techwin.vcite.data.beans.UserBean;
import com.techwin.vcite.data.beans.ViolationType;
import com.techwin.vcite.data.beans.base.ApiResponse;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.local.dao.ViolationDao;
import com.techwin.vcite.data.local.entity.ViolationBean;
import com.techwin.vcite.data.remote.helper.NetworkErrorHandler;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.data.repo.BaseRepo;
import com.techwin.vcite.di.base.viewmodel.BaseViewModel;
import com.techwin.vcite.util.event.SingleRequestEvent;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class ViolationActivityVM extends BaseViewModel {
    private final SharedPref sharedPref;
    private final NetworkErrorHandler networkErrorHandler;
    final SingleRequestEvent<List<ViolationBean>> obrData = new SingleRequestEvent<>();
    private final ViolationDao violationDao;
    private final BaseRepo baseRepo;

    @Inject
    public ViolationActivityVM(SharedPref sharedPref, NetworkErrorHandler networkErrorHandler, ViolationDao violationDao, BaseRepo baseRepo) {
        this.sharedPref = sharedPref;
        this.networkErrorHandler = networkErrorHandler;
        this.violationDao = violationDao;
        this.baseRepo = baseRepo;
    }

    void getList() {
        obrData.setValue(Resource.loading(null));
        compositeDisposable.add(Observable.fromCallable(() -> violationDao.getList())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<ViolationBean>>() {
                    @Override
                    public void accept(List<ViolationBean> violationBeans) throws Exception {
                        obrData.setValue(Resource.success(violationBeans, "Load from cache"));
                        if (violationBeans.size() == 0)
                            apiViolationType();

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable e) throws Exception {
                        obrData.setValue(Resource.error(null, e.getMessage()));
                        apiViolationType();
                    }
                }));
    }

    void apiViolationType() {
        UserBean userBean = sharedPref.getUser();
        if (userBean == null)
            return;
        Map<String, String> map = new HashMap<>();
        map.put("orgCode", String.valueOf(userBean.CustKey));
        map.put("docKey", String.valueOf(userBean.DocKey));
        baseRepo.getViolation(map).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new SingleObserver<ApiResponse<ViolationType>>() {
            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
                obrData.setValue(Resource.loading(null));
            }

            @Override
            public void onSuccess(ApiResponse<ViolationType> listApiResponse) {
                obrData.setValue(Resource.success(listApiResponse.getData().voilTyp, "Load from server"));
            }

            @Override
            public void onError(Throwable e) {
                obrData.setValue(Resource.error(null, networkErrorHandler.getErrMsg(e)));
            }
        });
    }
}

