package com.techwin.vcite.ui.login;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;

import com.techwin.vcite.BuildConfig;
import com.techwin.vcite.R;
import com.techwin.vcite.data.beans.UserBean;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.data.remote.helper.Status;
import com.techwin.vcite.databinding.ActivityLoginBinding;
import com.techwin.vcite.di.base.view.AppActivity;
import com.techwin.vcite.ui.main.MainActivity;
import com.techwin.vcite.ui.splash.SplashActivity;
import com.techwin.vcite.util.AppUtils;
import com.techwin.vcite.util.event.SingleRequestEvent;

public class LoginActivity extends AppActivity<ActivityLoginBinding, LoginActivityVM> {


    @Override
    protected BindingActivity<LoginActivityVM> getBindingActivity() {
        return new BindingActivity<>(R.layout.activity_login, LoginActivityVM.class);
    }

    public static Intent newIntent(Context activity) {
        Intent intent = new Intent(activity, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    @SuppressLint({"SetTextI18n", "NonConstantResourceId"})
    @Override
    protected void subscribeToEvents(final LoginActivityVM vm) {
        binding.tvAndroidid.setText("Device ID : " + AppUtils.getAndroidId(this));
        binding.tvVersion.setText("v"+ BuildConfig.VERSION_NAME + "( " + BuildConfig.VERSION_CODE + " )");
        vm.onClick.observe(this, view -> {
            switch (view != null ? view.getId() : 0) {
               /* case R.id.btn_signup: {
                    Intent intent = SignupActivity.newIntent(LoginActivity.this);
                    startNewActivity(intent);
                }
                break;*/
                case R.id.btn_login: {
                    if (TextUtils.isEmpty(binding.etUserid.getText().toString().trim())) {
                        vm.error.setValue("User Id empty");
                    } else if (TextUtils.isEmpty(binding.etPassword.getText().toString().trim())) {
                        vm.error.setValue("Password empty");
                    } else if (TextUtils.isEmpty(binding.etDns.getText().toString().trim())) {
                        vm.error.setValue("Dns empty");
                    } else
                        viewModel.login(binding.etDns.getText().toString().trim(), binding.etUserid.getText().toString().trim(),AppUtils.toBase64(binding.etPassword.getText().toString().trim()));


                }
                break;
            }
        });
        vm.obrLogin.observe(this, (SingleRequestEvent.RequestObserver<UserBean>) resource -> {
            switch (resource.status) {
                case LOADING:
                    showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    vm.success.setValue(resource.message);
                    Intent intent = SplashActivity.newIntent(LoginActivity.this);
                    startNewActivity(intent, true);
                    break;
                case WARN:
                    dismissProgressDialog();
                    vm.warn.setValue(resource.message);
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    break;
            }
        });
       // vm.clearCitation();
    }


}

