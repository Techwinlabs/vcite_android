package com.techwin.vcite.ui.refrence;

import android.content.Context;

import androidx.annotation.Nullable;

import com.techwin.vcite.R;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.databinding.FragmentRefrenceBinding;
import com.techwin.vcite.di.base.view.AppFragment;
import com.techwin.vcite.ui.main.MainActivity;

import javax.inject.Inject;

public class RefrenceFragment extends AppFragment<FragmentRefrenceBinding, RefrenceFragmentVM> {
    public static final String TAG = "RefrenceFragment";
    @Nullable
    private MainActivity mainActivity;
    @Inject
    SharedPref sharedPref;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mainActivity = (MainActivity) context;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected BindingFragment<RefrenceFragmentVM> getBindingFragment() {
        return new BindingFragment<>(R.layout.fragment_refrence, RefrenceFragmentVM.class);
    }

    public static RefrenceFragment newInstance() {
        return new RefrenceFragment();
    }

    @Override
    protected void subscribeToEvents(final RefrenceFragmentVM vm) {
        binding.tvInfor.fromAsset("reference.pdf").show();

    }


}
