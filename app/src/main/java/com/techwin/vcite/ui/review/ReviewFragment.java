package com.techwin.vcite.ui.review;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ObservableField;
import androidx.databinding.library.baseAdapters.BR;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.mindorks.core.ScreenshotBuilder;
import com.mindorks.properties.Quality;
import com.permissionx.guolindev.PermissionX;
import com.permissionx.guolindev.callback.ExplainReasonCallback;
import com.permissionx.guolindev.callback.RequestCallback;
import com.permissionx.guolindev.request.ExplainScope;
import com.techwin.vcite.R;
import com.techwin.vcite.data.beans.CitTempBean;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.local.entity.CitationBean;
import com.techwin.vcite.data.local.entity.MediaBean;
import com.techwin.vcite.data.local.entity.ViolationBean;
import com.techwin.vcite.data.remote.helper.Resource;
import com.techwin.vcite.data.remote.helper.Status;
import com.techwin.vcite.databinding.FragmentReviewBinding;
import com.techwin.vcite.databinding.HolderReviewOneBinding;
import com.techwin.vcite.databinding.HolderReviewTwoBinding;
import com.techwin.vcite.di.base.adapter.SimpleRecyclerViewAdapter;
import com.techwin.vcite.di.base.view.AppFragment;
import com.techwin.vcite.di.base.view.BaseAlertDialog;
import com.techwin.vcite.ui.home.CitationsFragment;
import com.techwin.vcite.ui.main.MainActivity;
import com.techwin.vcite.util.AppUtils;
import com.techwin.vcite.util.Constants;
import com.techwin.vcite.util.decoration.ListVerticalItemDecoration;
import com.techwin.vcite.util.dump.InputUtils;
import com.techwin.vcite.util.event.SingleRequestEvent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

public class ReviewFragment extends AppFragment<FragmentReviewBinding, ReviewFragmentVM> {
    public static final String TAG = "ReviewFragment";
    @Nullable
    private MainActivity mainActivity;
    @Inject
    SharedPref sharedPref;
    private SimpleRecyclerViewAdapter<Row, HolderReviewOneBinding> adapter;
    private SimpleRecyclerViewAdapter<Row, HolderReviewTwoBinding> adapter2;
    private List<CitTempBean> citTempBeans;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mainActivity = (MainActivity) getActivity();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected BindingFragment<ReviewFragmentVM> getBindingFragment() {
        return new BindingFragment<>(R.layout.fragment_review, ReviewFragmentVM.class);
    }

    public static ReviewFragment newInstance() {
        return new ReviewFragment();
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    protected void subscribeToEvents(final ReviewFragmentVM vm) {
        vm.onClick.observe(this, view -> {
            switch (view.getId()) {
                case R.id.iv_save:
                    if (mainActivity != null && binding.getData() != null) {
                        mainActivity.startBitmapCapture(binding.getData(), view);
                    }
                    break;
                case R.id.btn_submit:
                    CitationBean citationBean = binding.getData();
                    if (citationBean != null) {
                        saveToLocal();
                        if (AppUtils.is24hrspassed(citationBean.getCreatedOn())) {
                            showwarningdialog();
                        } else {
                            if (citationBean.getCitationType().equalsIgnoreCase(Constants.CITATION_TYPE_PARKING)) {
                                String missing = AppUtils.convertArrayToString(AppUtils.missing(citationBean, sharedPref.getTemplate(citationBean.getCitationType())));
                                if (missing != null) {
                                    vm.info.setValue("Missing :" + missing);
                                } else {
                                    viewModel.mainActivityVM.apiSubmitCitation(binding.getData());
                                }
                            } else if (citationBean.getCitationType().equalsIgnoreCase(Constants.CITATION_TYPE_CODE_ENFORCE)) {
                                String missing = AppUtils.convertArrayToString(AppUtils.missingEnforcement(citationBean, sharedPref.getTemplate(citationBean.getCitationType())));
                                if (missing != null) {
                                    vm.info.setValue("Missing :" + missing);
                                } else {
                                    viewModel.mainActivityVM.apiSubmitCitation(binding.getData());
                                }
                            }
                        }
                    }
                    break;
            }
        });
        adapter = new SimpleRecyclerViewAdapter<>(R.layout.holder_review_one, BR.bean);
        binding.rvOne.setLayoutManager(new LinearLayoutManager(baseContext));
        binding.rvOne.addItemDecoration(new ListVerticalItemDecoration(baseContext, R.dimen.dp1));
        binding.rvOne.setAdapter(adapter);
        adapter2 = new SimpleRecyclerViewAdapter<>(R.layout.holder_review_two, BR.bean);
        binding.rvTwo.setLayoutManager(new LinearLayoutManager(baseContext));
        binding.rvTwo.addItemDecoration(new ListVerticalItemDecoration(baseContext, R.dimen.dp1));
        binding.rvTwo.setAdapter(adapter2);
        vm.obrSave.observe(this, (SingleRequestEvent.RequestObserver<Void>) resource -> {
            switch (resource.status) {
                case SUCCESS:
                    //  viewModel.success.setValue(resource.message);
                    break;
                case ERROR:
                    viewModel.error.setValue(resource.message);
                    break;
            }
        });
        vm.obrSaveBitmap.observe(this, (SingleRequestEvent.RequestObserver<Void>) resource -> {
            switch (resource.status) {
                case SUCCESS:
                    viewModel.success.setValue(resource.message);
                    if (alertDialog != null)
                        alertDialog.dismiss();
                    break;
                case ERROR:
                    viewModel.error.setValue(resource.message);
                    break;
            }
        });
        vm.obrData.observe(this, (SingleRequestEvent.RequestObserver<CitationBean>) resource -> {
            switch (resource.status) {
                case LOADING:
                    //showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    if (resource.data != null) {
                        binding.setData(resource.data);
                        setbtndata(resource.data);
                        dismissProgressDialog();
                        setRowOneData();
                        setRowTwoData();
                    }
                    break;
                case ERROR:
                    dismissProgressDialog();
                    Log.e(TAG, resource.message);
                    //  setInitData();
                    break;
            }
        });
        vm.mainActivityVM.obrSubmit.observe(this, (SingleRequestEvent.RequestObserver<String>) resource -> {
            switch (resource.status) {
                case LOADING:
                    showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    if (resource.data != null) {
                        if (resource.data.equalsIgnoreCase("Success")) {
                            viewModel.success.setValue(resource.data);
                            if (mainActivity != null) {
                                mainActivity.changeFragment(CitationsFragment.TAG);
                            }
                        } else
                            viewModel.error.setValue(resource.data);
                    }
                    break;
                case ERROR:
                    dismissProgressDialog();
                    showErrorDialog(resource.data, resource.message);
                    break;
            }
        });

        loaddata();
        setRowOneData();
        setData();

    }

    private BaseAlertDialog alertDialog;

    private void showErrorDialog(String payload, String message) {
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
        alertDialog = new BaseAlertDialog(baseContext);
        alertDialog.setLabels("Error", message, "Save", "Not now");
        alertDialog.setListioner(new BaseAlertDialog.ClickListioner() {
            @Override
            public void onOkClick() {
                PermissionX.init(ReviewFragment.this)
                        .permissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .onExplainRequestReason(new ExplainReasonCallback() {
                            @Override
                            public void onExplainReason(@NonNull ExplainScope scope, @NonNull List<String> deniedList) {
                                scope.showRequestReasonDialog(deniedList, "Error creator is based on these permissions", "OK", "Cancel");
                            }
                        })
                        .request(new RequestCallback() {
                            @Override
                            public void onResult(boolean allGranted, @NonNull List<String> grantedList, @NonNull List<String> deniedList) {
                                Bitmap bitmap = new ScreenshotBuilder(getActivity())
                                        .setView(alertDialog.getWindow().getDecorView())
                                        .setQuality(Quality.AVERAGE).getScreenshot();
                                saveErrorBitmap(bitmap);
                            }
                        });
            }

            @Override
            public void onCancelClick() {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
        alertDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
    }


    @Override
    public void onResume() {
        super.onResume();
        loaddata();
    }

    private void loaddata() {
        if (mainActivity != null) {
            if (mainActivity.citationData != null)
                citTempBeans = sharedPref.getTemplate(mainActivity.citationData.citationType);
            viewModel.getCitation(mainActivity.citationData);
            if (mainActivity.citationData != null) {
                binding.tvTitle.setText(String.format("Citations #%s", mainActivity.citationData.getCitationNo()));
            }
        }
    }


    private void setbtndata(CitationBean data) {
        if (data.isUploaded()) {
            binding.btnSubmit.setText("Update");
        } else binding.btnSubmit.setText("Submit");
    }

    private void showwarningdialog() {
        alertDialog = new BaseAlertDialog(baseContext);
        alertDialog.setLabels("Edit Error", "Can't Edit ticket after 24 hrs", "Ok", null);
        alertDialog.setListioner(new BaseAlertDialog.ClickListioner() {
            @Override
            public void onOkClick() {
                alertDialog.dismiss();
            }

            @Override
            public void onCancelClick() {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
        alertDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
    }

    private void saveErrorBitmap(Bitmap bitmap) {
        CitationBean citationBean = binding.getData();
        if (bitmap != null && citationBean != null) {
            String path = AppUtils.imagetoBase64(bitmap);
            MediaBean mediaBean = new MediaBean();
            mediaBean.setCitationNo(citationBean.getCitationNo());
            mediaBean.setOrgNo(citationBean.getOrgNo());
            mediaBean.setImage(path);
            List<MediaBean> bean = new ArrayList<>();
            bean.add(mediaBean);
            viewModel.saveMediaBeans(bean);
        }
    }

    private void setData() {
        if (citTempBeans != null)
            for (int i = 0; i < citTempBeans.size(); i++) {
                CitTempBean citTempBean = citTempBeans.get(i);
                switch (citTempBean.getFieldID()) {
                    case Constants.fieldID_Warning:
                        if (citTempBean.isIncludeField()) {
                            showView(binding.vWarning);
                            setLabel(binding.tvWarning, citTempBean.getFieldName(), "Warning");
                        } else {
                            hideView(binding.vWarning);
                        }

                        break;
                    case Constants.fieldID_Void:
                        if (citTempBean.isIncludeField()) {
                            showView(binding.vvoid);
                            setLabel(binding.tvvoid, citTempBean.getFieldName(), "Void");
                        } else {
                            hideView(binding.vvoid);
                        }
                        break;

                }
            }
    }

    private void setMode(EditText view, String mode) {
        if (TextUtils.isEmpty(mode))
            mode = Constants.FIELD_TYPE_TEXT;
        switch (mode) {
            case Constants.FIELD_TYPE_TEXT:
                view.setFocusable(true);
                view.setFocusableInTouchMode(true);
                view.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                view.setHint("Type here");
                break;
            case Constants.FIELD_TYPE_DATE:
                view.setFocusable(false);
                view.setFocusableInTouchMode(false);
                view.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                view.setHint("Select");
                break;
            case Constants.FIELD_TYPE_DROP:
                view.setFocusable(false);
                view.setFocusableInTouchMode(false);
                view.setHint("Drop down");
                view.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_down, 0);
                break;
        }

    }

    private void setDefaultText(ObservableField<String> field, String fieldValue) {
        if (InputUtils.isEmpty(field.get()))
            field.set(fieldValue);
    }

    private void setLabel(TextView textView, String name, String defaultName) {
        if (TextUtils.isEmpty(name))
            textView.setText(defaultName);
        else textView.setText(name);
    }

    private void hideView(View view) {
        view.setVisibility(View.GONE);
    }

    private void showView(View view) {
        view.setVisibility(View.VISIBLE);
    }

    private void setRowTwoData() {
        List<Row> rowList = new ArrayList<>();
        CitationBean citationBean = binding.getData();
        if (citationBean != null && citationBean.getViolation() != null) {
            for (ViolationBean violationBean : citationBean.getViolation()) {
                rowList.add(new Row(violationBean.getDescription()));
            }
        }
        adapter2.setList(rowList);
    }

    private void setRowOneData() {
        List<Row> rowList = new ArrayList<>();
        if (mainActivity == null)
            return;
        if (mainActivity.citationData != null && mainActivity.citationData.getCitationType().equalsIgnoreCase(Constants.CITATION_TYPE_PARKING)) {
            CitationBean citationBean = binding.getData();
            if (citationBean != null) {
                rowList.add(new Row("Licence #", citationBean.getLicenceNo()));
                rowList.add(new Row("State", citationBean.getState()));
                rowList.add(new Row("VIN", citationBean.getVin()));
                rowList.add(new Row("Location", citationBean.getLocation()));
            } else {
                rowList.add(new Row("Licence #"));
                rowList.add(new Row("State"));
                rowList.add(new Row("VIN"));
                rowList.add(new Row("Location"));
            }

        } else {
            CitationBean citationBean = binding.getData();
            if (citationBean != null) {
                rowList.add(new Row("Street #", citationBean.getStreetNo()));
                rowList.add(new Row("Street", citationBean.getStreetName()));
                rowList.add(new Row("Unit", citationBean.getUnit()));
                rowList.add(new Row("Location", citationBean.getLocation()));
            } else {
                rowList.add(new Row("Street #"));
                rowList.add(new Row("Street"));
                rowList.add(new Row("Unit"));
                rowList.add(new Row("Location"));
            }
        }
        adapter.setList(rowList);

    }


    public static class Row {
        public String key;
        public String value;

        public Row(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public Row(String key) {
            this.key = key;
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        saveToLocal();
    }

    private void saveToLocal() {
        if (mainActivity != null && mainActivity.citationData != null) {
            if (binding.getData() != null) {
                CitationBean bean = binding.getData();
                bean.setDate(new Date());
                viewModel.saveCitation(bean);
            }
        }
    }

    public Bitmap savePdfBitmap() {
        return new ScreenshotBuilder(getActivity())
                .setView(binding.parent)
                .setQuality(Quality.AVERAGE).getScreenshot();
    }
}
