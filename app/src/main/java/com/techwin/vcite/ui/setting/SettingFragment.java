package com.techwin.vcite.ui.setting;

import android.content.Context;

import androidx.annotation.Nullable;

import com.techwin.vcite.R;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.databinding.FragmentRefrenceBinding;
import com.techwin.vcite.databinding.FragmentSettingBinding;
import com.techwin.vcite.di.base.adapter.NamedPagerAdapter;
import com.techwin.vcite.di.base.view.AppFragment;
import com.techwin.vcite.ui.main.MainActivity;
import com.techwin.vcite.ui.refrence.RefrenceFragmentVM;
import com.techwin.vcite.ui.setting.admin.AdminFragment;
import com.techwin.vcite.ui.setting.general.GeneralFragment;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class SettingFragment extends AppFragment<FragmentSettingBinding, SettingFragmentVM> {
    public static final String TAG = "SettingFragment";
    @Nullable
    private MainActivity mainActivity;
    @Inject
    SharedPref sharedPref;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mainActivity = (MainActivity) context;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected BindingFragment<SettingFragmentVM> getBindingFragment() {
        return new BindingFragment<>(R.layout.fragment_setting, SettingFragmentVM.class);
    }

    public static SettingFragment newInstance() {
        return new SettingFragment();
    }

    @Override
    protected void subscribeToEvents(final SettingFragmentVM vm) {
        setViewPager();
    }

    private void setViewPager() {
        binding.tabs.setupWithViewPager(binding.pager1);
        List<NamedPagerAdapter.NavViewItem> navViewItems = new ArrayList<>();
        navViewItems.add(new NamedPagerAdapter.NavViewItem("General", GeneralFragment.newInstance()));
        navViewItems.add(new NamedPagerAdapter.NavViewItem("Admin", AdminFragment.newInstance()));
        NamedPagerAdapter namedPagerAdapter = new NamedPagerAdapter(getChildFragmentManager(), navViewItems);
        binding.pager1.setAdapter(namedPagerAdapter);

    }
}
