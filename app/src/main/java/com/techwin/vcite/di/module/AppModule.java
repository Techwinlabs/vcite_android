package com.techwin.vcite.di.module;

import android.content.Context;

import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.di.qualifier.ApiDateFormat;
import com.techwin.vcite.di.qualifier.ApiEndpoint;
import com.techwin.vcite.di.qualifier.AppContext;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.android.support.DaggerApplication;
import okhttp3.HttpUrl;

@Module
public class AppModule {
    @AppContext
    @Provides
    static Context context(DaggerApplication daggerApplication) {
        return daggerApplication;
    }

    @ApiEndpoint
    @Singleton
    @Provides
    static HttpUrl apiEndpoint(SharedPref sharedPref) {
        return HttpUrl.parse(sharedPref.getDefault().serverUrl);
    }


    @ApiDateFormat
    @Singleton
    @Provides
    static String apiDateFormat() {
        return "yyyy-MM-dd HH:mm:ss";
    }


}
