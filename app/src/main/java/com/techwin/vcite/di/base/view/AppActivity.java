package com.techwin.vcite.di.base.view;

import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.IdRes;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.techwin.vcite.BuildConfig;
import com.techwin.vcite.R;
import com.techwin.vcite.di.base.viewmodel.BaseViewModel;
import com.techwin.vcite.widget.toast.MessageUtils;
import com.techwin.vcite.util.event.SingleMessageEvent;

public abstract class AppActivity<B extends ViewDataBinding, V extends BaseViewModel> extends BaseActivity<B, V> {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        subscriveBaseEvents();

    }

    private void subscriveBaseEvents() {
        viewModel.normal.observe(this, new SingleMessageEvent.MessageObserver() {
            @Override
            public void onMessageReceived(int msgResId) {
                MessageUtils.normal(AppActivity.this, getString(msgResId));
            }

            @Override
            public void onMessageReceived(String msg) {
                if (msg != null && !TextUtils.isEmpty(msg))
                    MessageUtils.normal(AppActivity.this, msg);
            }
        });
        viewModel.success.observe(this, new SingleMessageEvent.MessageObserver() {
            @Override
            public void onMessageReceived(int msgResId) {
                MessageUtils.success(AppActivity.this, getString(msgResId));
            }

            @Override
            public void onMessageReceived(String msg) {
                if (msg != null && !TextUtils.isEmpty(msg))
                    MessageUtils.success(AppActivity.this, msg);

            }
        });
        viewModel.error.observe(this, new SingleMessageEvent.MessageObserver() {
            @Override
            public void onMessageReceived(int msgResId) {
                MessageUtils.error(AppActivity.this, getString(msgResId));
            }

            @Override
            public void onMessageReceived(String msg) {
                if (msg != null && !TextUtils.isEmpty(msg))
                    MessageUtils.error(AppActivity.this, msg);
            }
        });
        viewModel.warn.observe(this, new SingleMessageEvent.MessageObserver() {
            @Override
            public void onMessageReceived(int msgResId) {

                MessageUtils.warning(AppActivity.this, getString(msgResId));
            }

            @Override
            public void onMessageReceived(String msg) {
                if (msg != null && !TextUtils.isEmpty(msg))
                    MessageUtils.warning(AppActivity.this, msg);
            }
        });
        viewModel.info.observe(this, new SingleMessageEvent.MessageObserver() {
            @Override
            public void onMessageReceived(int msgResId) {
                MessageUtils.info(AppActivity.this, getString(msgResId));
            }

            @Override
            public void onMessageReceived(String msg) {
                if (msg != null && !TextUtils.isEmpty(msg))
                    MessageUtils.info(AppActivity.this, msg);
            }
        });


    }


    @Override
    protected void onPause() {
        super.onPause();
    }


    protected void changeFragment(@IdRes int layoutid, Fragment fragment, boolean animate) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction().replace(layoutid, fragment);
        if (BuildConfig.EnableAnim && animate)
            transaction.setCustomAnimations(R.anim.alpha_undim, R.anim.alpha_dim, R.anim.alpha_undim, R.anim.alpha_dim);
        transaction.commit();
    }



  /*  private void showBiomatricUi() {
        new BiometricManager.BiometricBuilder(this)
                .setTitle(R.string.auth)
                .setSubtitle(R.string.sub_title)
                .setDescription(R.string.desc)
                .setNegativeButtonText(R.string.cancel)
                .build()
                .authenticate(new BiometricCallback() {
                    @Override
                    public void onError(ArrayList<BioErrorType> errorType) {
                        PasscodeDialog dialog = new PasscodeDialog(AppActivity.this, new PassCodeCallback() {
                            @Override
                            public void onAuthenticationFailed() {

                                // viewModel.error.setValue(R.string.wrong_code);
                            }

                            @Override
                            public void onAuthenticationCancelled() {
                                //   showMainUi();
                            }

                            @Override
                            public void pnPasscodeSaved() {
                                // viewModel.success.setValue(getString(R.string.passcode_saved));
                                //   showMainUi();

                            }

                            @Override
                            public void onAuthenticationSuccessful() {
                                //     showMainUi();

                            }
                        });
                        dialog.show();


                    }


                    @Override
                    public void onAuthenticationFailed() {

                    }

                    @Override
                    public void onAuthenticationCancelled() {


                    }

                    @Override
                    public void onAuthenticationSuccessful() {
                        // showMainUi();
                    }

                    @Override
                    public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
                        *//*
     * This method is called when a non-fatal error has occurred during the authentication
     * process. The callback will be provided with an help code to identify the cause of the
     * error, along with a help message.
     *//*
                    }

                    @Override
                    public void onAuthenticationError(int errorCode, CharSequence errString) {
                        *//*
     * When an unrecoverable error has been encountered and the authentication process has
     * completed without success, then this callback will be triggered. The callback is provided
     * with an error code to identify the cause of the error, along with the error message.
     *//*
                    }
                });
*/


}
