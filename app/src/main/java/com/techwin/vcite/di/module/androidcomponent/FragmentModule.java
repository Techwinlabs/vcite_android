package com.techwin.vcite.di.module.androidcomponent;


import com.techwin.vcite.ui.about.AboutFragment;
import com.techwin.vcite.ui.enforment.EnforceFragment;
import com.techwin.vcite.ui.home.CitationsFragment;
import com.techwin.vcite.ui.main2.vehcile.Vehicle2Fragment;
import com.techwin.vcite.ui.password.PasswordFragment;
import com.techwin.vcite.ui.photo.PhotoFragment;
import com.techwin.vcite.ui.refrence.RefrenceFragment;
import com.techwin.vcite.ui.review.ReviewFragment;
import com.techwin.vcite.ui.setting.SettingFragment;
import com.techwin.vcite.ui.setting.admin.AdminFragment;
import com.techwin.vcite.ui.setting.general.GeneralFragment;
import com.techwin.vcite.ui.vchalk.VchalkFragment;
import com.techwin.vcite.ui.violation.ViolationFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentModule {
    // NOTE: user  here
    @ContributesAndroidInjector
    abstract PasswordFragment passwordFragment();

    @ContributesAndroidInjector
    abstract Vehicle2Fragment vehicle2Fragment();

    @ContributesAndroidInjector
    abstract EnforceFragment enforceFragment();

    @ContributesAndroidInjector
    abstract GeneralFragment generalFragment();


    @ContributesAndroidInjector
    abstract VchalkFragment vchalkFragment();

    @ContributesAndroidInjector
    abstract AdminFragment adminFragment();


    @ContributesAndroidInjector
    abstract SettingFragment settingFragment();


    @ContributesAndroidInjector
    abstract ReviewFragment reviewFragment();


    @ContributesAndroidInjector
    abstract CitationsFragment tagDetailFragment();


    @ContributesAndroidInjector
    abstract RefrenceFragment infoFragment();

    @ContributesAndroidInjector
    abstract AboutFragment aboutFragment();

    @ContributesAndroidInjector
    abstract PhotoFragment photoFragment();

    @ContributesAndroidInjector
    abstract ViolationFragment violationFragment();


}
