package com.techwin.vcite.di.module;


import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.data.local.dao.TransactionDao;
import com.techwin.vcite.data.remote.api.BaseApi;
import com.techwin.vcite.data.repo.BaseRepo;
import com.techwin.vcite.data.repo.BaseRepoImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RepositoryModule {

    @Singleton
    @Provides
    static BaseRepo baseRepo(BaseApi baseApi, SharedPref sharedPref, TransactionDao transactionDao) {
        return new BaseRepoImpl(baseApi, sharedPref, transactionDao);
    }

}
