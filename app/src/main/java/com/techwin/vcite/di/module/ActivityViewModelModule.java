package com.techwin.vcite.di.module;


import androidx.lifecycle.ViewModel;

import com.techwin.vcite.di.mapkey.ViewModelKey;
import com.techwin.vcite.ui.imagePicker.CustomImagePickerActivityVm;
import com.techwin.vcite.ui.location.LocationPickerActivityVM;
import com.techwin.vcite.ui.login.LoginActivityVM;
import com.techwin.vcite.ui.main.MainActivityVM;
import com.techwin.vcite.ui.multiImagePicker.MultipleImagePickerActivityVm;
import com.techwin.vcite.ui.parcel.SearchNameActivityVM;
import com.techwin.vcite.ui.parcel.SearchNoActivityVM;
import com.techwin.vcite.ui.parcel.SearchUnitActivityVM;
import com.techwin.vcite.ui.photo.detail.ImageDetailActivityVM;
import com.techwin.vcite.ui.plate.PlateActivityVM;
import com.techwin.vcite.ui.scan.ScanActivityVM;
import com.techwin.vcite.ui.scan.text.TextScanActivityVM;
import com.techwin.vcite.ui.selector.campus.CampusActivityVM;
import com.techwin.vcite.ui.selector.color.PlateColorActivityVM;
import com.techwin.vcite.ui.selector.make.VehMakeActivityVM;
import com.techwin.vcite.ui.selector.platetype.PlateTypeActivityVM;
import com.techwin.vcite.ui.selector.state.StateActivityVM;
import com.techwin.vcite.ui.selector.vehType.VehTypeActivityVM;
import com.techwin.vcite.ui.selector.vehcolor.VehColorActivity;
import com.techwin.vcite.ui.selector.vehcolor.VehColorActivityVM;
import com.techwin.vcite.ui.splash.SplashActivityVM;
import com.techwin.vcite.ui.violation.type.ViolationActivityVM;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ActivityViewModelModule {
    // NOTE: customize here
    @Binds
    @IntoMap
    @ViewModelKey(VehColorActivityVM.class)
    abstract ViewModel VehColorActivityVM(VehColorActivityVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(CampusActivityVM.class)
    abstract ViewModel CampusActivityVM(CampusActivityVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(StateActivityVM.class)
    abstract ViewModel StateActivityVM(StateActivityVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(VehMakeActivityVM.class)
    abstract ViewModel VehMakeActivityVM(VehMakeActivityVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(VehTypeActivityVM.class)
    abstract ViewModel VehTypeActivityVM(VehTypeActivityVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(PlateTypeActivityVM.class)
    abstract ViewModel PlateTypeActivityVM(PlateTypeActivityVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(PlateColorActivityVM.class)
    abstract ViewModel PlateColorActivityVM(PlateColorActivityVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(SearchNoActivityVM.class)
    abstract ViewModel SearchNoActivityVM(SearchNoActivityVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(SearchUnitActivityVM.class)
    abstract ViewModel SearchUnitActivityVM(SearchUnitActivityVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(MainActivityVM.class)
    abstract ViewModel MainActivityVM(MainActivityVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(ImageDetailActivityVM.class)
    abstract ViewModel ImageDetailActivityVM(ImageDetailActivityVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(SearchNameActivityVM.class)
    abstract ViewModel ParcelActivityVM(SearchNameActivityVM vm);


    @Binds
    @IntoMap
    @ViewModelKey(TextScanActivityVM.class)
    abstract ViewModel TextScanActivityVM(TextScanActivityVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(ViolationActivityVM.class)
    abstract ViewModel ViolationActivityVM(ViolationActivityVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(LocationPickerActivityVM.class)
    abstract ViewModel LocationPickerActivityVM(LocationPickerActivityVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(PlateActivityVM.class)
    abstract ViewModel PlateActivityVM(PlateActivityVM vm);


    @Binds
    @IntoMap
    @ViewModelKey(SplashActivityVM.class)
    abstract ViewModel SplashActivityVM(SplashActivityVM vm);


    @Binds
    @IntoMap
    @ViewModelKey(LoginActivityVM.class)
    abstract ViewModel LoginActivityVM(LoginActivityVM vm);


    @Binds
    @IntoMap
    @ViewModelKey(ScanActivityVM.class)
    abstract ViewModel AuthorActivityVM(ScanActivityVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(CustomImagePickerActivityVm.class)
    abstract ViewModel CustomImagePickerActivityVm(CustomImagePickerActivityVm vm);

    @Binds
    @IntoMap
    @ViewModelKey(MultipleImagePickerActivityVm.class)
    abstract ViewModel MultipleImagePickerActivityVm(MultipleImagePickerActivityVm vm);

}
