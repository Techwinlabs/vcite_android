package com.techwin.vcite.di.base.viewmodel;

import android.view.View;

import androidx.annotation.CallSuper;
import androidx.lifecycle.ViewModel;

import com.techwin.vcite.util.event.SingleActionEvent;
import com.techwin.vcite.util.event.SingleMessageEvent;

import io.reactivex.disposables.CompositeDisposable;

/**
 * This will serve as the base class for all Activity ViewModels
 * <p>
 */
public abstract class BaseViewModel extends ViewModel {
    public static String TAG = BaseViewModel.class.getSimpleName();

    public final CompositeDisposable compositeDisposable = new CompositeDisposable();
    public final SingleActionEvent<View> onClick = new SingleActionEvent<>();
    public final SingleMessageEvent normal = new SingleMessageEvent();
    public final SingleMessageEvent success = new SingleMessageEvent();
    public final SingleMessageEvent error = new SingleMessageEvent();
    public final SingleMessageEvent info = new SingleMessageEvent();
    public final SingleMessageEvent warn = new SingleMessageEvent();


    public void onClick(View view) {
        onClick.call(view);
    }

    @CallSuper
    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.clear();
    }
}
