package com.techwin.vcite.di.base.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.CallSuper;
import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.techwin.vcite.BR;
import com.techwin.vcite.BuildConfig;
import com.techwin.vcite.R;
import com.techwin.vcite.data.local.SharedPref;
import com.techwin.vcite.di.base.viewmodel.BaseViewModel;
import com.techwin.vcite.util.progress.ProgressDialogAvl;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;


public abstract class BaseActivity<B extends ViewDataBinding, V extends BaseViewModel> extends DaggerAppCompatActivity {
    @Inject
    SharedPref sharedPref;
    @Inject
    public ViewModelProvider.Factory viewModelFactory;
    protected V viewModel;
    protected B binding;
    @Nullable
    // private ProgressDialog progressDialog;
    protected ProgressDialogAvl progressDialogAvl;

    protected abstract BindingActivity<V> getBindingActivity();

    @CallSuper
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BindingActivity<V> bindingActivity = getBindingActivity();
        if (bindingActivity == null) {
            throw new NullPointerException("Binding activity cannot be null");
        }
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(bindingActivity.getClazz());
        binding = DataBindingUtil.setContentView(this, bindingActivity.getLayoutResId());
        binding.setVariable(BR.vm, viewModel);
        progressDialogAvl = new ProgressDialogAvl(this);
        subscribeToEvents(viewModel);

    }


    protected abstract void subscribeToEvents(V vm);

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    protected final void showProgressDialog(@Nullable String msg) {
        if (progressDialogAvl == null)
            progressDialogAvl = new ProgressDialogAvl(this);
        progressDialogAvl.isLoading(true);
    }

    protected final void showProgressDialog(@StringRes int msgResId) {
        showProgressDialog(getString(msgResId));
    }

    protected final void dismissProgressDialog() {
        if (progressDialogAvl != null)
            progressDialogAvl.isLoading(false);
    }


    /* protected final void showProgressDialog(@Nullable String msg) {
         if (progressDialog == null) {
             progressDialog = new ProgressDialog(this, R.style.MyAlertDialogStyle);
         }
         progressDialog.setMessage(msg != null ? msg : "");
         progressDialog.setIndeterminate(true);
         progressDialog.setCancelable(true);
         progressDialog.show();
     }

     protected final void showProgressDialog(@StringRes int msgResId) {
         showProgressDialog(getString(msgResId));
     }

     protected final void dismissProgressDialog() {
         if (progressDialog != null && this.progressDialog.isShowing()) {
             progressDialog.dismiss();
         }
     }
 */
    protected static class BindingActivity<V extends BaseViewModel> {
        @LayoutRes
        private int layoutResId;
        private Class<V> clazz;

        public BindingActivity(@LayoutRes int layoutResId, Class<V> clazz) {
            this.layoutResId = layoutResId;
            this.clazz = clazz;
        }

        int getLayoutResId() {
            return layoutResId;
        }

        Class<V> getClazz() {
            return clazz;
        }
    }


    protected void startNewActivity(Intent intent, boolean finishExisting) {
        startNewActivity(intent, finishExisting, true);
    }

    protected void startNewActivity(Intent intent, boolean finishExisting, boolean animate) {
        startActivity(intent);
        if (finishExisting)
            finish();
        if (animate)
            animateActivity();
    }

    protected void startNewActivity(Intent intent) {
        startNewActivity(intent, false, true);
    }


    protected void onBackPressed(boolean animate) {
        super.onBackPressed();
        if (BuildConfig.EnableAnim && animate)
            overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);

    }

    public void animateActivity() {
        if (BuildConfig.EnableAnim)
            overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
    }

    protected void finish(boolean animate) {
        finish();
        if (BuildConfig.EnableAnim && animate)
            overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
    }

    public void animateOpenSheet() {
        overridePendingTransition(R.anim.activity_sheet_in, R.anim.activity_out);
    }

    protected void animateCloseSheet() {
        overridePendingTransition(0, R.anim.activity_back_sheet_out);
    }
}