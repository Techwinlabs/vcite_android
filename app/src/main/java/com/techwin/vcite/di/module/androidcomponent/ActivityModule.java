package com.techwin.vcite.di.module.androidcomponent;

import com.techwin.vcite.ui.imagePicker.CustomImagePickerActivity;
import com.techwin.vcite.ui.location.LocationPickerActivity;
import com.techwin.vcite.ui.login.LoginActivity;
import com.techwin.vcite.ui.main.MainActivity;
import com.techwin.vcite.ui.multiImagePicker.MultipleImagePickerActivity;
import com.techwin.vcite.ui.multiImagePicker.MultipleImagePickerActivityVm;
import com.techwin.vcite.ui.parcel.SearchNameActivity;
import com.techwin.vcite.ui.parcel.SearchNoActivity;
import com.techwin.vcite.ui.parcel.SearchUnitActivity;
import com.techwin.vcite.ui.photo.detail.ImageDetailActivity;
import com.techwin.vcite.ui.plate.PlateActivity;
import com.techwin.vcite.ui.scan.ScanActivity;
import com.techwin.vcite.ui.scan.text.TextScan2Activity;
import com.techwin.vcite.ui.scan.text.TextScanActivity;
import com.techwin.vcite.ui.selector.campus.CampusActivity;
import com.techwin.vcite.ui.selector.color.PlateColorActivity;
import com.techwin.vcite.ui.selector.make.VehMakeActivity;
import com.techwin.vcite.ui.selector.platetype.PlateTypeActivity;
import com.techwin.vcite.ui.selector.state.StateActivity;
import com.techwin.vcite.ui.selector.vehType.VehTypeActivity;
import com.techwin.vcite.ui.selector.vehcolor.VehColorActivity;
import com.techwin.vcite.ui.splash.SplashActivity;
import com.techwin.vcite.ui.violation.type.ViolationActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityModule {
    // NOTE: UserApp  here

    @ContributesAndroidInjector
    abstract CampusActivity campusActivity();

    @ContributesAndroidInjector
    abstract PlateTypeActivity plateTypeActivity();

    @ContributesAndroidInjector
    abstract StateActivity stateActivity();

    @ContributesAndroidInjector
    abstract VehColorActivity vehColorActivity();

    @ContributesAndroidInjector
    abstract VehTypeActivity vehTypeActivity();

    @ContributesAndroidInjector
    abstract SearchNoActivity searchNoActivity();

    @ContributesAndroidInjector
    abstract PlateColorActivity plateColorActivity();

    @ContributesAndroidInjector
    abstract VehMakeActivity vehMakeActivity();

    @ContributesAndroidInjector
    abstract TextScan2Activity textScan2Activity();

    @ContributesAndroidInjector
    abstract ImageDetailActivity imageDetailActivity();

    @ContributesAndroidInjector
    abstract SearchUnitActivity searchUnitActivity();

    @ContributesAndroidInjector
    abstract ViolationActivity violationActivity();


    @ContributesAndroidInjector
    abstract SearchNameActivity parcelActivity();


    @ContributesAndroidInjector
    abstract MainActivity mainActivity();

    @ContributesAndroidInjector
    abstract LocationPickerActivity locationPickerActivity();

    @ContributesAndroidInjector
    abstract PlateActivity plateActivity();

    @ContributesAndroidInjector
    abstract SplashActivity splashActivity();


    @ContributesAndroidInjector
    abstract LoginActivity loginActivity();


    @ContributesAndroidInjector
    abstract ScanActivity authorActivity();

    @ContributesAndroidInjector
    abstract TextScanActivity textScanActivity();

    @ContributesAndroidInjector
    abstract CustomImagePickerActivity customImagePickerActivity();

   @ContributesAndroidInjector
    abstract MultipleImagePickerActivity MultipleImagePickerActivityVm();



}
