package com.techwin.vcite.di.component;


import com.techwin.vcite.di.module.ApiModule;
import com.techwin.vcite.di.module.AppModule;
import com.techwin.vcite.di.module.DataModule;
import com.techwin.vcite.di.module.LocalModule;
import com.techwin.vcite.di.module.ManagerModule;
import com.techwin.vcite.di.module.NetworkModule;
import com.techwin.vcite.di.module.RepositoryModule;
import com.techwin.vcite.di.module.SystemModule;
import com.techwin.vcite.di.module.ViewModelModule;
import com.techwin.vcite.di.module.androidcomponent.AndroidComponentsModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import dagger.android.support.DaggerApplication;

@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        AppModule.class,
        LocalModule.class,
        ManagerModule.class,
        ApiModule.class,
        DataModule.class,
        RepositoryModule.class,
        NetworkModule.class,
        SystemModule.class,
        AndroidComponentsModule.class,
        ViewModelModule.class,


})
interface AppComponent extends AndroidInjector<DaggerApplication> {

    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<DaggerApplication> {

    }




}
