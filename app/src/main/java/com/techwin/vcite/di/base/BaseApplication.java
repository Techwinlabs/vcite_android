package com.techwin.vcite.di.base;


import androidx.multidex.MultiDex;


import com.techwin.vcite.di.component.DaggerAppComponent;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import io.reactivex.exceptions.OnErrorNotImplementedException;
import io.reactivex.exceptions.ProtocolViolationException;
import io.reactivex.functions.Consumer;
import io.reactivex.plugins.RxJavaPlugins;

public abstract class BaseApplication extends DaggerApplication {


    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        attachErrorHandler();
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().create(this);
    }


    private void attachErrorHandler() {
        RxJavaPlugins.setErrorHandler(new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                if (throwable instanceof OnErrorNotImplementedException || throwable instanceof ProtocolViolationException) {

                }
            }
        });
    }


}
