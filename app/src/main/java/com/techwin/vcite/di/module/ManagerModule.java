package com.techwin.vcite.di.module;


import com.techwin.vcite.util.RxBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ManagerModule {

    @Singleton
    @Provides
    static RxBus getBus() {
        return new RxBus();
    }



}