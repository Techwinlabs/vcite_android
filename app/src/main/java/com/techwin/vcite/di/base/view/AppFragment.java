package com.techwin.vcite.di.base.view;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;

import com.techwin.vcite.di.base.viewmodel.BaseViewModel;
import com.techwin.vcite.util.event.SingleMessageEvent;
import com.techwin.vcite.widget.toast.MessageUtils;


public abstract class AppFragment<B extends ViewDataBinding, V extends BaseViewModel> extends BaseFragment<B, V> {

    protected String TAG = AppFragment.class.getSimpleName();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        subscriveBaseEvents();

    }


    private void subscriveBaseEvents() {
        BaseViewModel model = viewModel;
        model.normal.observe(this, new SingleMessageEvent.MessageObserver() {
            @Override
            public void onMessageReceived(int msgResId) {
                MessageUtils.normal(baseContext, getString(msgResId));
            }

            @Override
            public void onMessageReceived(String msg) {
                MessageUtils.normal(baseContext, msg);
            }
        });
        model.success.observe(this, new SingleMessageEvent.MessageObserver() {
            @Override
            public void onMessageReceived(int msgResId) {
                MessageUtils.success(baseContext, getString(msgResId));
            }

            @Override
            public void onMessageReceived(String msg) {
                MessageUtils.success(baseContext, msg);

            }
        });
        model.error.observe(this, new SingleMessageEvent.MessageObserver() {
            @Override
            public void onMessageReceived(int msgResId) {
                MessageUtils.error(baseContext, getString(msgResId));
            }

            @Override
            public void onMessageReceived(String msg) {
                MessageUtils.error(baseContext, msg);
            }
        });
        model.warn.observe(this, new SingleMessageEvent.MessageObserver() {
            @Override
            public void onMessageReceived(int msgResId) {
                MessageUtils.warning(baseContext, getString(msgResId));
            }

            @Override
            public void onMessageReceived(String msg) {
                MessageUtils.warning(baseContext, msg);
            }
        });
        model.info.observe(this, new SingleMessageEvent.MessageObserver() {
            @Override
            public void onMessageReceived(int msgResId) {
                MessageUtils.info(baseContext, getString(msgResId));
            }

            @Override
            public void onMessageReceived(String msg) {
                MessageUtils.info(baseContext, msg);
            }
        });

    }

}
