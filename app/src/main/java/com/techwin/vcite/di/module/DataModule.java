package com.techwin.vcite.di.module;

import com.techwin.vcite.data.local.dao.CitationDao;
import com.techwin.vcite.data.local.dao.MediaBeanDao;
import com.techwin.vcite.data.local.dao.TicketBeanDao;
import com.techwin.vcite.data.local.dao.TransactionDao;
import com.techwin.vcite.data.local.dao.ViolationDao;
import com.techwin.vcite.data.local.database.AppDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DataModule {
    @Singleton
    @Provides
    static CitationDao citationDao(AppDatabase appDatabase) {
        return appDatabase.citationDao();
    }

    @Singleton
    @Provides
    static MediaBeanDao mediaBeanDao(AppDatabase appDatabase) {
        return appDatabase.mediaDao();
    }

    @Singleton
    @Provides
    static TicketBeanDao ticketBeanDao(AppDatabase appDatabase) {
        return appDatabase.ticketBeanDao();
    }

    @Singleton
    @Provides
    static ViolationDao violationDao(AppDatabase appDatabase) {
        return appDatabase.violationDao();
    }


    @Singleton
    @Provides
    static TransactionDao transactionDao(AppDatabase appDatabase) {
        return appDatabase.transactionDao();
    }

 /*   @Singleton
    @Provides
    static ConcernDao concernDao(AppDatabase appDatabase) {
        return appDatabase.concernDao();
    }

    @Singleton
    @Provides
    static ConcernFileDao concernFileDao(AppDatabase appDatabase) {
        return appDatabase.concernFileDao();
    }

    @Singleton
    @Provides
    static ConditionDao conditionDao(AppDatabase appDatabase) {
        return appDatabase.conditionDao();
    }

    @Singleton
    @Provides
    static InputGroupDao inputGroupDao(AppDatabase appDatabase) {
        return appDatabase.inputGroupDao();
    }

    @Singleton
    @Provides
    static InspectionDao inspectionDao(AppDatabase appDatabase) {
        return appDatabase.inspectionDao();
    }

    @Singleton
    @Provides
    static InspectionFileDao inspectionFileDao(AppDatabase appDatabase) {
        return appDatabase.inspectionFileDao();
    }

    @Singleton
    @Provides
    static ParameterDao parameterDao(AppDatabase appDatabase) {
        return appDatabase.parameterDao();
    }

    @Singleton
    @Provides
    static PartDao partDao(AppDatabase appDatabase) {
        return appDatabase.partDao();
    }

    @Singleton
    @Provides
    static QuestionDao questionDao(AppDatabase appDatabase) {
        return appDatabase.questionDao();
    }

    @Singleton
    @Provides
    static RecommendationDao recommendationDao(AppDatabase appDatabase) {
        return appDatabase.recommendationDao();
    }

    @Singleton
    @Provides
    static StepDao stepDao(AppDatabase appDatabase) {
        return appDatabase.stepDao();
    }

    @Singleton
    @Provides
    static StepGroupDao stepGroupDao(AppDatabase appDatabase) {
        return appDatabase.stepGroupDao();
    }

    @Singleton
    @Provides
    static SuperGroupDao superGroupDao(AppDatabase appDatabase) {
        return appDatabase.superGroupDao();
    }

    @Singleton
    @Provides
    static TechnicianDao technicianDao(AppDatabase appDatabase) {
        return appDatabase.technicianDao();
    }

    @Singleton
    @Provides
    static TemplateDao templateDao(AppDatabase appDatabase) {
        return appDatabase.templateDao();
    }

    @Singleton
    @Provides
    static TicketDao ticketDao(AppDatabase appDatabase) {
        return appDatabase.ticketDao();
    }

    @Singleton
    @Provides
    static TopicDao topicDao(AppDatabase appDatabase) {
        return appDatabase.topicDao();
    }

   */
}
