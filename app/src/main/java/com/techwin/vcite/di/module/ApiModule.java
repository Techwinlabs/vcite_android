package com.techwin.vcite.di.module;

import com.techwin.vcite.data.remote.api.BaseApi;
import com.techwin.vcite.di.qualifier.ApiEndpoint;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class ApiModule {

    @Singleton
    @Provides
    static BaseApi baseApi(@ApiEndpoint Retrofit retrofit) {
        return retrofit.create(BaseApi.class);
    }

}
