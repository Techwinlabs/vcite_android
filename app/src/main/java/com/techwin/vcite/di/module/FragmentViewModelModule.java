package com.techwin.vcite.di.module;


import androidx.lifecycle.ViewModel;

import com.techwin.vcite.di.mapkey.ViewModelKey;
import com.techwin.vcite.ui.about.AboutFragmentVM;
import com.techwin.vcite.ui.enforment.EnforceFragmentVM;
import com.techwin.vcite.ui.home.CitationsFragmentVM;
import com.techwin.vcite.ui.main2.vehcile.Vehicle2FragmentVM;
import com.techwin.vcite.ui.password.PasswordFragmentVM;
import com.techwin.vcite.ui.photo.PhotoFragmentVM;
import com.techwin.vcite.ui.refrence.RefrenceFragmentVM;
import com.techwin.vcite.ui.review.ReviewFragmentVM;
import com.techwin.vcite.ui.setting.SettingFragmentVM;
import com.techwin.vcite.ui.setting.admin.AdminFragmentVM;
import com.techwin.vcite.ui.setting.general.GeneralFragmentVM;
import com.techwin.vcite.ui.vchalk.VchalkFragmentVM;
import com.techwin.vcite.ui.violation.ViolationFragmentVM;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class FragmentViewModelModule {
    // NOTE: customize here

    @Binds
    @IntoMap
    @ViewModelKey(PasswordFragmentVM.class)
    abstract ViewModel PasswordFragmentVM(PasswordFragmentVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(Vehicle2FragmentVM.class)
    abstract ViewModel Vehicle2FragmentVM(Vehicle2FragmentVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(EnforceFragmentVM.class)
    abstract ViewModel EnforceFragmentVM(EnforceFragmentVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(GeneralFragmentVM.class)
    abstract ViewModel GeneralFragmentVM(GeneralFragmentVM vm);


    @Binds
    @IntoMap
    @ViewModelKey(VchalkFragmentVM.class)
    abstract ViewModel VchalkFragmentVM(VchalkFragmentVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(AdminFragmentVM.class)
    abstract ViewModel AdminFragmentVM(AdminFragmentVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(ReviewFragmentVM.class)
    abstract ViewModel ReviewFragmentVM(ReviewFragmentVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(SettingFragmentVM.class)
    abstract ViewModel SettingFragmentVM(SettingFragmentVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(ViolationFragmentVM.class)
    abstract ViewModel ViolationFragmentVM(ViolationFragmentVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(PhotoFragmentVM.class)
    abstract ViewModel PhotoFragmentVM(PhotoFragmentVM vm);


    @Binds
    @IntoMap
    @ViewModelKey(CitationsFragmentVM.class)
    abstract ViewModel TagDetailFragmentVM(CitationsFragmentVM vm);


    @Binds
    @IntoMap
    @ViewModelKey(RefrenceFragmentVM.class)
    abstract ViewModel InfoFragmentVM(RefrenceFragmentVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(AboutFragmentVM.class)
    abstract ViewModel AboutFragmentVM(AboutFragmentVM vm);

}
