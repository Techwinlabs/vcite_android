package com.techwin.vcite;

import android.content.Context;

import androidx.multidex.MultiDex;

import com.facebook.stetho.Stetho;
import com.techwin.vcite.di.base.BaseApplication;
import com.techwin.vcite.util.misc.AppVisibilityDetector;

public class MyApplication extends BaseApplication {
    private static final String TAG = MyApplication.class.getSimpleName();
    private static MyApplication application;

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
        Stetho.initializeWithDefaults(this);
        AppVisibilityDetector.init(this, new AppVisibilityDetector.AppVisibilityCallback() {
            @Override
            public void onAppGotoForeground() {

              /*  if (sharedPref.contains("userdata")) {
                    long ct = System.currentTimeMillis() / 1000;
                    if (lastonlinetime != -1 && ct - lastonlinetime < DEFAULT_ONLINE_TIME_THRESHOLD) {
                        lastonlinetime = ct;
                    } else {
                        startActivity(SecurityActivity.newIntent(application));
                    }
                }*/
            }

            @Override
            public void onAppGotoBackground() {
                // lastonlinetime = System.currentTimeMillis() / 1000;
            }
        });


    }


    public static MyApplication getInstance() {
        return application;
    }

    public void restartApp() {
       /* Intent intent = SplashActivity.newIntent(this);
        intent.putExtra("reset", true);
        startActivity(intent);*/

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this); // this is the key code
    }
}
